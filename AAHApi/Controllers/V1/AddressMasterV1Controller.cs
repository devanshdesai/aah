﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using GoogleMaps.LocationServices;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddressMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAddressMasterServices abstractAddressMasterServices;
        private readonly AbstractCityMasterServices abstractCityMasterServices;
        private readonly AbstractStateMasterServices abstractStateMasterServices;
        private readonly AbstractCountryMasterServices abstractCountryMasterServices;
        private readonly AbstractAreaMasterServices abstractAreaMasterServices;

        #endregion

        #region Cnstr
        public AddressMasterV1Controller(AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractCityMasterServices abstractCityMasterServices, AbstractStateMasterServices abstractStateMasterServices,
            AbstractCountryMasterServices abstractCountryMasterServices, AbstractAreaMasterServices abstractAreaMasterServices)
        {
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractCityMasterServices = abstractCityMasterServices;
            this.abstractStateMasterServices = abstractStateMasterServices;
            this.abstractCountryMasterServices = abstractCountryMasterServices;
            this.abstractAreaMasterServices = abstractAreaMasterServices;
        }
        #endregion

        // AddressMaster_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressMaster_Upsert")]
        public async Task<IHttpActionResult> AddressMaster_Upsert(AddressMasterRequestModel addressMaster)
        {
            if (addressMaster.Latitude == null || addressMaster.Latitude == "")
            {
                var gls = new GoogleLocationService(ConfigurationManager.AppSettings["locationKey"] + "&amp");
                var latlong = gls.GetLatLongFromAddress(addressMaster.AddressLine1 + "," + addressMaster.AddressLine2 + "," +
                    addressMaster.PinCode);
                addressMaster.Latitude = ConvertTo.String(latlong.Latitude);
            }

            if (addressMaster.Longitute == null || addressMaster.Longitute == "")
            {
                var gls = new GoogleLocationService(ConfigurationManager.AppSettings["locationKey"] + "&amp");
                var latlong = gls.GetLatLongFromAddress(addressMaster.AddressLine1 + "," + addressMaster.AddressLine2 + "," +
                    addressMaster.PinCode);
                addressMaster.Longitute = ConvertTo.String(latlong.Longitude);
            }
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;

            var areaData = abstractAreaMasterServices.AreaMaster_All(pageParam, "").Values;
            var isInArea = false;
            for (int i = 0; i < areaData.Count; i++)
            {
                if (areaData[i].IsActive)
                {
                    List<double> latlong = new List<double>();
                    double l = new double();
                    l = Convert.ToDouble(addressMaster.Latitude);
                    latlong.Add(l);
                    l = Convert.ToDouble(addressMaster.Longitute);
                    latlong.Add(l);

                    var polygonData = JsonConvert.DeserializeObject<List<List<double>>>(areaData[i].GeoFencing);
                    pRoot pRoot = new pRoot();
                    pRoot.MyArray = polygonData;
                    isInArea = inside(pRoot, latlong);

                    if (isInArea)
                    {
                        addressMaster.AreaMasterId = areaData[i].Id;
                    }
                }
            }

            var quote = abstractAddressMasterServices.AddressMaster_Upsert(addressMaster);

            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        public bool inside(pRoot vs, List<double> point)
        {

            var x = point[0];
            var y = point[1];

            var inside = false;
            for (int i = 0, j = vs.MyArray.Count - 1; i < vs.MyArray.Count; j = i++)
            {
                var xi = vs.MyArray[i][0];
                var yi = vs.MyArray[i][1];
                var xj = vs.MyArray[j][0];
                var yj = vs.MyArray[j][1];

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        }
        public class pRoot
        {
            public List<List<double>> MyArray { get; set; }
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressDefaultById")]
        public async Task<IHttpActionResult> AddressDefaultById(int UserId, int AddressId)
        {
            var quote = abstractAddressMasterServices.AddressDefaultById(UserId, AddressId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // AddressMaster_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressMaster_All")]
        public async Task<IHttpActionResult> AddressMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractAddressMasterServices.AddressMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //AddressMaster_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressMaster_ById")]
        public async Task<IHttpActionResult> AddressMaster_ById(long Id)
        {
            var quote = abstractAddressMasterServices.AddressMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressMaster_ByUserId")]
        public async Task<IHttpActionResult> AddressMaster_ByUserId(PageParam pageParam, string search = "", long ByUserId = 0)
        {
            var quote = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, search, ByUserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //AddressMaster_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressMaster_ActInAct")]
        public async Task<IHttpActionResult> AddressMaster_ActInAct(long Id, long UpdatedBy)
        {
            var quote = abstractAddressMasterServices.AddressMaster_ActInAct(Id, UpdatedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //AddressMaster_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressMaster_Delete")]
        public async Task<IHttpActionResult> AddressMaster_Delete(long Id, long DeletedBy)
        {
            var quote = abstractAddressMasterServices.AddressMaster_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // CityMaster_ByStateMasterId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CityMaster_ByStateMasterId")]
        public async Task<IHttpActionResult> CityMaster_ByStateMasterId(PageParam pageParam, string search = "", long StateMasterId = 0)
        {
            var quote = abstractCityMasterServices.CityMaster_ByStateMasterId(pageParam, search, StateMasterId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // StateMaster_ByCountryMasterId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("StateMaster_ByCountryMasterId")]
        public async Task<IHttpActionResult> StateMaster_ByCountryMasterId(PageParam pageParam, string search = "", long CountryMasterId = 0)
        {
            var quote = abstractStateMasterServices.StateMaster_ByCountryMasterId(pageParam, search, CountryMasterId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // CountryMaster_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CountryMaster_All")]
        public async Task<IHttpActionResult> CountryMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCountryMasterServices.CountryMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}