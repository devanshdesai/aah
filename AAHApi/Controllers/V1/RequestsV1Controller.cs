﻿using AAH.APICommon;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.Web.Http.Cors;
using AAH.Common.Paging;
using GoogleMaps.LocationServices;
using AAH.Common;
using System.Configuration;
using System.Web;
using System;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RequestsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractRequestsServices abstractRequestsServices;
        #endregion

        #region Cnstr
        public RequestsV1Controller(AbstractRequestsServices abstractRequestsServices)
        {
            this.abstractRequestsServices = abstractRequestsServices;
        }
        #endregion

        // Requests_Insert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_Insert")]
        public async Task<IHttpActionResult> Requests_Insert()
        {
            Requests Requests = new Requests();
            var httpRequest = HttpContext.Current.Request;
            Requests.UserId = Convert.ToInt64(httpRequest.Params["UserId"]);
            Requests.ServiceMasterId = Convert.ToInt64(httpRequest.Params["ServiceMasterId"]);
            Requests.SubServiceMasterId = Convert.ToInt64(httpRequest.Params["SubServiceMasterId"]);
            Requests.SubSubServiceMasterId = Convert.ToInt64(httpRequest.Params["SubSubServiceMasterId"]);
            Requests.AmountRequested = Convert.ToDecimal(httpRequest.Params["AmountRequested"]);
            Requests.AddressId = Convert.ToInt64(httpRequest.Params["AddressId"]);
            Requests.AddressLine1 = Convert.ToString(httpRequest.Params["AddressLine1"]);
            Requests.AddressLine2 = Convert.ToString(httpRequest.Params["AddressLine2"]);
            Requests.CityName = Convert.ToString(httpRequest.Params["CityName"]);
            Requests.StateName = Convert.ToString(httpRequest.Params["StateName"]);
            Requests.CountryName = Convert.ToString(httpRequest.Params["CountryName"]);
            Requests.Latitude = Convert.ToString(httpRequest.Params["Latitude"]);
            Requests.Longitude = Convert.ToString(httpRequest.Params["Longitude"]);
            Requests.UserDescription = Convert.ToString(httpRequest.Params["UserDescription"]);
            Requests.PayType = Convert.ToInt64(httpRequest.Params["PayType"]);

            var a = httpRequest.Params["audio"];

            if (Requests.AddressId == 0)
            {
                if (Requests.Latitude == null || Requests.Latitude == "")
                {
                    var gls = new GoogleLocationService(ConfigurationManager.AppSettings["locationKey"] + "&amp");
                    var latlong = gls.GetLatLongFromAddress(Requests.AddressLine1 + "," + Requests.AddressLine2 + "," +
                        Requests.CityName + "," + Requests.StateName + "," + Requests.CountryName);
                    Requests.Latitude = ConvertTo.String(latlong.Latitude);
                }
                if (Requests.Longitude == null || Requests.Longitude == "")
                {
                    var gls = new GoogleLocationService(ConfigurationManager.AppSettings["locationKey"] + "&amp");
                    var latlong = gls.GetLatLongFromAddress(Requests.AddressLine1 + "," + Requests.AddressLine2 + "," +
                        Requests.CityName + "," + Requests.StateName + "," + Requests.CountryName);
                    Requests.Longitude = ConvertTo.String(latlong.Longitude);
                }
            }

            string basePath = "";
            string fileName = "";
            if (httpRequest.Files.Count > 0)
            {
                if(httpRequest.Files.AllKeys[0] == "audio")
                {

                }
                var myFile = httpRequest.Files[0];
                basePath = "RequestAudio/Audio/" + Requests.UserId + "/";
                fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                Requests.AudioUrl = basePath + fileName;
            }

            var quote = abstractRequestsServices.Requests_Insert(Requests);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Requests_AssignRequests API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_AssignDeliveryExecutive")]
        public async Task<IHttpActionResult> Requests_AssignDeliveryExecutive(Requests Requests)
        {
            Requests.IsAssignedByAdmin = 0;
            var quote = abstractRequestsServices.Requests_AssignDeliveryExecutive(Requests);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Request_UpdateStatus API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Request_UpdateStatus")]
        public async Task<IHttpActionResult> Request_UpdateStatus(long RequestId = 0, long StatusId = 0, long DeliveryExecutiveId = 0, decimal AmountGiven = 0,decimal RequestedAmount = 0, long OTP = 0)
        {
            var quote = abstractRequestsServices.Request_UpdateStatus(RequestId, StatusId, DeliveryExecutiveId, AmountGiven, RequestedAmount,OTP);
            //if(quote.Code == 200)
            //{
            //    PushNotification.Main();
            //}
            //PushNotification.Main();
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Requests_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_ById")]
        public async Task<IHttpActionResult> Requests_ById(long Id)
        {
            var quote = abstractRequestsServices.Requests_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Requests_UpdateByDeliveryExecutive API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_UpdateByDeliveryExecutive")]
        public async Task<IHttpActionResult> Requests_UpdateByDeliveryExecutive(Requests requests)
        {
            var quote = abstractRequestsServices.Requests_UpdateByDeliveryExecutive(requests);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // DeliveryExecutiveRequestAmount_Update API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveRequestAmount_Update")]
        public async Task<IHttpActionResult> DeliveryExecutiveRequestAmount_Update(long Id, decimal DeliveryExecutiveRequestAmount)
        {
            var quote = abstractRequestsServices.DeliveryExecutiveRequestAmount_Update(Id, DeliveryExecutiveRequestAmount);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // Requests_UpdateEstimatedETA API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_UpdateEstimatedETA")]
        public async Task<IHttpActionResult> Requests_UpdateEstimatedETA(long Id, DateTime EstimatedETA)
        {
            var quote = abstractRequestsServices.Requests_UpdateEstimatedETA(Id, EstimatedETA);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // IsApprovedByUser_Update API
        [System.Web.Http.HttpPost]
        [InheritedRoute("IsApprovedByUser_Update")]
        public async Task<IHttpActionResult> IsApprovedByUser_Update(long Id, int IsApprovedByUser)
        {
            var quote = abstractRequestsServices.IsApprovedByUser_Update(Id, IsApprovedByUser);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // Requests_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_All")]
        public async Task<IHttpActionResult> Requests_All(PageParam pageParam, string Search = "",
            long UserId = 0, long RequestsId = 0, long ServiceMasterId = 0, long SubServiceMasterId = 0, 
            long StatusId = 0)
        {
            var quote = abstractRequestsServices.Requests_All(pageParam, Search,
                UserId, RequestsId, ServiceMasterId, SubServiceMasterId, StatusId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // Requests_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestsStatus_All")]
        public async Task<IHttpActionResult> RequestsStatus_All(PageParam pageParam, string Search = "",long UserId = 0,long StatusId = 0)
        {
            var quote = abstractRequestsServices.RequestsStatus_All(pageParam, Search,UserId, StatusId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("GetRequestByDeliveryExecutiveId")]
        public async Task<IHttpActionResult> GetRequestByDeliveryExecutiveId(PageParam pageParam, string Search = "",
            long DeliveryExecutiveId = 0,long StatusId = 0)
        {
            var quote = abstractRequestsServices.GetRequestByDeliveryExecutiveId(pageParam, Search,
                DeliveryExecutiveId, StatusId);
            return this.Content((HttpStatusCode)200, quote);
        }
/*
        // Review_ByDeliveryExecutive API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Review_ByDeliveryExecutive")]
        public async Task<IHttpActionResult> Review_ByDeliveryExecutive(long Id, decimal RatingByDeliveryExecutive, 
            string ReviewByDeliveryExecutive)
        {
            var quote = abstractRequestsServices.Review_ByDeliveryExecutive(Id, RatingByDeliveryExecutive, ReviewByDeliveryExecutive);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }*/
        // Review_ByDeliveryExecutive API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Review_ByDeliveryExecutive")]
        public async Task<IHttpActionResult> Review_ByDeliveryExecutive(long Id, decimal RatingByUser,
            string ReviewByUser)
        {
            var quote = abstractRequestsServices.Review_ByDeliveryExecutive(Id, RatingByUser, ReviewByUser);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Review_ByUser API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Review_ByUser")]
        public async Task<IHttpActionResult> Review_ByUser(long Id, decimal RatingByDeliveryExecutive,
            string ReviewByDeliveryExecutive)
        {
            var quote = abstractRequestsServices.Review_ByUser(Id, RatingByDeliveryExecutive, ReviewByDeliveryExecutive);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Requests_Urgent API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_Urgent")]
        public async Task<IHttpActionResult> Requests_Urgent(PageParam pageParam, long UserId = 0, long DeliveryExecutiveId = 0)
        {
            var quote = abstractRequestsServices.Requests_Urgent(pageParam, UserId, DeliveryExecutiveId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // Requests_ByStatusCount API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_ByStatusCount")]
        public async Task<IHttpActionResult> Requests_ByStatusCount()
        {
            var quote = abstractRequestsServices.Requests_ByStatusCount();
            return this.Content((HttpStatusCode)200, quote);
        }

        // Requests_ReachTime API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Requests_ReachTime")]
        public async Task<IHttpActionResult> Requests_ReachTime(long Id)
        {
            var quote = abstractRequestsServices.Requests_ReachTime(Id);
            var duration = "";

            if (quote.Code == 200)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["locationAPI"]).
                Replace("#destinationsLATLONG#", quote.Item.RequestsLatitude + "," + quote.Item.RequestsLongitude)
                .Replace("#originsLATLONG#", quote.Item.DeliveryExecutiveLatitude + "," + quote.Item.DeliveryExecutiveLongitude).ToString());
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode("https://maps.googleapis.com/maps/api/distancematrix/json?destinations=23.0694343,72.5412793&origins=23.0684347,72.5712697&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg"));
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["locationAPI"]));
                request.Method = "GET";
                request.ContentType = "application/json";

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                    using (StreamReader responseReader = new StreamReader(webStream))
                    {
                        string response = responseReader.ReadToEnd();
                        var result = JsonConvert.DeserializeObject<Root>(response);
                        duration = result.rows[0].elements[0].duration.text;
                    }
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("-----------------");
                    Console.Out.WriteLine(e.Message);
                }
            }
            return this.Content((HttpStatusCode)200, duration);
        }
    }
}


public class Distance
{
    public string text { get; set; }
    public int value { get; set; }
}

public class Duration
{
    public string text { get; set; }
    public int value { get; set; }
}

public class Element
{
    public Distance distance { get; set; }
    public Duration duration { get; set; }
    public string status { get; set; }
}

public class Row
{
    public List<Element> elements { get; set; }
}

public class Root
{
    public List<string> destination_addresses { get; set; }
    public List<string> origin_addresses { get; set; }
    public List<Row> rows { get; set; }
    public string status { get; set; }
}