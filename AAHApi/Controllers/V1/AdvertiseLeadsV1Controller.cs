﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdvertiseLeadsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdvertiseLeadsServices abstractAdvertiseLeadsServices;
        #endregion

        #region Cnstr
        public AdvertiseLeadsV1Controller(AbstractAdvertiseLeadsServices abstractAdvertiseLeadsServices)
        {
            this.abstractAdvertiseLeadsServices = abstractAdvertiseLeadsServices;
        }
        #endregion

       
        // AdvertiseLeads_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdvertiseLeads_Upsert")]
        public async Task<IHttpActionResult> AdvertiseLeads_Upsert(AdvertiseLeads advertiseLeads)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            //advertiseLeads.Password = new String(stringChars);
            var quote = abstractAdvertiseLeadsServices.AdvertiseLeads_Upsert(advertiseLeads);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

       
    }
}