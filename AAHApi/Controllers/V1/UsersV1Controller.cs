﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUsersServices abstractUsersServices;
        #endregion

        #region Cnstr
        public UsersV1Controller(AbstractUsersServices abstractUsersServices)
        {
            this.abstractUsersServices = abstractUsersServices;
        }
        #endregion

        // Users_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Upsert")]
        public async Task<IHttpActionResult> Users_Upsert()
        {
            Users users = new Users();
            var httpRequest = HttpContext.Current.Request;
            users.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            users.FirstName = Convert.ToString(httpRequest.Params["FirstName"]);
            users.LastName = Convert.ToString(httpRequest.Params["LastName"]);
            users.Email = Convert.ToString(httpRequest.Params["Email"]);
            users.Gender = Convert.ToInt32(httpRequest.Params["Gender"]);
            users.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);

            string basePath = "";
            string fileName = "";
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                basePath = "Users/Profile/" + users.Id + "/";
                fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                users.ProfileURL = basePath + fileName;
            }

            var quote = abstractUsersServices.Users_Upsert(users);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Users_Logout API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Logout")]
        public async Task<IHttpActionResult> Users_Logout(string DeviceToken, string UDID, string IMEI)
        {
            var quote = abstractUsersServices.Users_Logout(DeviceToken, UDID, IMEI);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Users_VerifyOtp API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_VerifyOtp")]
        public async Task<IHttpActionResult> Users_VerifyOtp(long Id,string DeviceToken, long Otp, string UDID, string IMEI)
        {
            var quote = abstractUsersServices.Users_VerifyOtp(Id,DeviceToken, Otp, UDID, IMEI);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Users_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_ActInAct")]
        public async Task<IHttpActionResult> Users_ActInAct(long Id, long UpdatedBy)
        {
            var quote = abstractUsersServices.Users_ActInAct(Id, UpdatedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_ById")]
        public async Task<IHttpActionResult> Users_ById(long Id)
        {
            var quote = abstractUsersServices.Users_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // Users_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_All")]
        public async Task<IHttpActionResult> Users_All(PageParam pageParam, string search = "", string UserName = "", string MobileNumber = "",
            string Email = "", int Gender = 0, long IsActive = 0)
        {
            var quote = abstractUsersServices.Users_All(pageParam, search, UserName, MobileNumber, Email, Gender, IsActive,0);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Users_SendOTP API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_SendOTP")]
        public async Task<IHttpActionResult> Users_SendOTP(string MobileNumber, string DeviceToken,string DeviceType, string UDID, string IMEI)
        {
            string otp = Convert.ToString(new Random().Next(111111, 999999).ToString("D6"));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).
                Replace("#mobile#", MobileNumber.ToString()).Replace("#otp#", otp).ToString());
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            var quote = abstractUsersServices.Users_SendOTP(MobileNumber, DeviceToken, DeviceType, UDID, IMEI, Convert.ToInt64(otp));
            return this.Content((HttpStatusCode)quote.Code, quote);

        }
    }
}