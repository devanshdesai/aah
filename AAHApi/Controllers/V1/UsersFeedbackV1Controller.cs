﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersFeedbackV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUsersFeedbackServices abstractUsersFeedbackServices;
        #endregion

        #region Cnstr
        public UsersFeedbackV1Controller(AbstractUsersFeedbackServices abstractUsersFeedbackServices)
        {
            this.abstractUsersFeedbackServices = abstractUsersFeedbackServices;
        }
        #endregion

        // UsersFeedback_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UsersFeedback_Insert")]
        public async Task<IHttpActionResult> UsersFeedback_Insert(UsersFeedback usersFeedback)
        {
            //UsersFeedback UsersFeedback = new UsersFeedback();
            //var httpRequest = HttpContext.Current.Request;
            //UsersFeedback.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            //UsersFeedback.UserId = Convert.ToInt64(httpRequest.Params["UserId"]);
            //UsersFeedback.Description = Convert.ToString(httpRequest.Params["Description"]);
           
            var quote = abstractUsersFeedbackServices.UsersFeedback_Insert(usersFeedback);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UsersFeedback_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UsersFeedback_All")]
        public async Task<IHttpActionResult> UsersFeedback_All(PageParam pageParam, string search = "", string FromDate = "", string ToDate = "",int UserId = 0)
        {
            var quote = abstractUsersFeedbackServices.UsersFeedback_All(pageParam, search, FromDate, ToDate, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}