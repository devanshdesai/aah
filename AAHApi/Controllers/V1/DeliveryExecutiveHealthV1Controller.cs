﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryExecutiveHealthV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDeliveryExecutiveHealthServices abstractDeliveryExecutiveHealthServices;
        #endregion

        #region Cnstr
        public DeliveryExecutiveHealthV1Controller(AbstractDeliveryExecutiveHealthServices abstractDeliveryExecutiveHealthServices)
        {
            this.abstractDeliveryExecutiveHealthServices = abstractDeliveryExecutiveHealthServices;
        }
        #endregion

        // DeliveryExecutiveHealth_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveHealth_All")]
        public async Task<IHttpActionResult> DeliveryExecutiveHealth_All(PageParam pageParam, string search = "")
        {
            var quote = abstractDeliveryExecutiveHealthServices.DeliveryExecutiveHealth_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveHealth_ByUserId")]
        public async Task<IHttpActionResult> DeliveryExecutiveHealth_ByUserId(PageParam pageParam, string search = "",int UserId = 0,int DeliveryExecutiveId = 0)
        {
            var quote = abstractDeliveryExecutiveHealthServices.DeliveryExecutiveHealth_ByUserId(pageParam, search,UserId,DeliveryExecutiveId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // DeliveryExecutiveHealth_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveHealth_ById")]
        public async Task<IHttpActionResult> DeliveryExecutiveHealth_ById(long Id)
        {
            var quote = abstractDeliveryExecutiveHealthServices.DeliveryExecutiveHealth_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DeliveryExecutiveHealth_ByDeliveryExecutiveId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveHealth_ByDeliveryExecutiveId")]
        public async Task<IHttpActionResult> DeliveryExecutiveHealth_ByDeliveryExecutiveId(long Id)
        {
            var quote = abstractDeliveryExecutiveHealthServices.DeliveryExecutiveHealth_ByDeliveryExecutiveId(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // DeliveryExecutiveHealth_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveHealth_Upsert")]
        public async Task<IHttpActionResult> DeliveryExecutiveHealth_Upsert(DeliveryExecutiveHealth DeliveryExecutiveHealth)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            //deliveryExecutiveHealth.Password = new String(stringChars);
            var quote = abstractDeliveryExecutiveHealthServices.DeliveryExecutiveHealth_Upsert(DeliveryExecutiveHealth);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }
}