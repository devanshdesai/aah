﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryExecutiveDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDeliveryExecutiveDocumentsServices abstractDeliveryExecutiveDocumentsServices;
        #endregion

        #region Cnstr
        public DeliveryExecutiveDocumentsV1Controller(AbstractDeliveryExecutiveDocumentsServices abstractDeliveryExecutiveDocumentsServices)
        {
            this.abstractDeliveryExecutiveDocumentsServices = abstractDeliveryExecutiveDocumentsServices;
        }
        #endregion



        // DeliveryExecutiveDocuments_ByDeliveryExecutiveId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveDocuments_ByDeliveryExecutiveId")]
        public async Task<IHttpActionResult> DeliveryExecutiveDocuments_ByDeliveryExecutiveId(PageParam pageparam, string Search, long DeliveryExecutiveId)
        {
            var quote = abstractDeliveryExecutiveDocumentsServices.DeliveryExecutiveDocuments_ByDeliveryExecutiveId(pageparam, Search, DeliveryExecutiveId);
            return this.Content((HttpStatusCode)200, quote);
        }
        //DeliveryExecutiveDocuments_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveDocuments_Verify")]
        public async Task<IHttpActionResult> DeliveryExecutiveDocuments_Verify(long Id, long AdminId)
        {
            var quote = abstractDeliveryExecutiveDocumentsServices.DeliveryExecutiveDocuments_Verify(Id, AdminId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //DeliveryExecutiveDocuments_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveDocuments_Delete")]
        public async Task<IHttpActionResult> DeliveryExecutiveDocuments_Delete(long Id, long DeletedBy)
        {
            var quote = abstractDeliveryExecutiveDocumentsServices.DeliveryExecutiveDocuments_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //DeliveryExecutiveDocuments_Upsert Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveDocuments_Upsert")]
        public async Task<IHttpActionResult> DeliveryExecutiveDocuments_Upsert()
        {
            DeliveryExecutiveDocuments deliveryExecutiveDocuments = new DeliveryExecutiveDocuments();
            var httpRequest = HttpContext.Current.Request;
            deliveryExecutiveDocuments.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            deliveryExecutiveDocuments.DeliveryExecutiveId = Convert.ToInt64(httpRequest.Params["DeliveryExecutiveId"]);
            deliveryExecutiveDocuments.DocumentTypeId = Convert.ToInt64(httpRequest.Params["DocumentTypeId"]);
            deliveryExecutiveDocuments.AdminId = Convert.ToInt64(httpRequest.Params["AdminId"]);


            string basePath = "";
            string fileName = "";
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                basePath = "Documents/" + deliveryExecutiveDocuments.DeliveryExecutiveId + "/";
                fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                deliveryExecutiveDocuments.DocumentUrl = basePath + fileName;
            }

            var quote = abstractDeliveryExecutiveDocumentsServices.DeliveryExecutiveDocuments_Upsert(deliveryExecutiveDocuments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}