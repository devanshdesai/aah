﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersLatLongV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUsersLatLongServices abstractUsersLatLongServices;
        #endregion

        #region Cnstr
        public UsersLatLongV1Controller(AbstractUsersLatLongServices abstractUsersLatLongServices)
        {
            this.abstractUsersLatLongServices = abstractUsersLatLongServices;
        }
        #endregion
        [System.Web.Http.HttpPost]
        [InheritedRoute("UsersLatLong_ByUserId")]
        public async Task<IHttpActionResult> UsersLatLong_ByUserId(PageParam pageParam, string search = "", long UserId = 0)
        {

            var quote = abstractUsersLatLongServices.UsersLatLong_ByUserId(pageParam, search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("UsersLatLong_ByRequestId")]
        public async Task<IHttpActionResult> UsersLatLong_ByRequestId(PageParam pageParam, string search = "", long ByRequestId = 0)
        {
            var quote = abstractUsersLatLongServices.UsersLatLong_ByRequestId(pageParam, search, ByRequestId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("UsersLatLong_Insert")]
        public async Task<IHttpActionResult> UsersLatLong_Insert(List<UsersLatLong> UsersLatLongs)  
        {
            string Json = JsonConvert.SerializeObject(UsersLatLongs);
            var quote = abstractUsersLatLongServices.UsersLatLong_Insert(Json);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}