﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryExecutiveCheckInCheckOutV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDeliveryExecutiveCheckInCheckOutServices abstractDeliveryExecutiveCheckInCheckOutServices;
        #endregion

        #region Cnstr
        public DeliveryExecutiveCheckInCheckOutV1Controller(AbstractDeliveryExecutiveCheckInCheckOutServices abstractDeliveryExecutiveCheckInCheckOutServices)
        {
            this.abstractDeliveryExecutiveCheckInCheckOutServices = abstractDeliveryExecutiveCheckInCheckOutServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveCheckInCheckOut_Upsert")]
        public async Task<IHttpActionResult> DeliveryExecutiveCheckInCheckOut_Upsert(DeliveryExecutiveCheckInCheckOut DeliveryExecutiveCheckInCheckOut)
        {
            var quote = abstractDeliveryExecutiveCheckInCheckOutServices.DeliveryExecutiveCheckInCheckOut_Upsert(DeliveryExecutiveCheckInCheckOut);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DeliveryExecutiveCheckInCheckOut_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId")]
        public async Task<IHttpActionResult> DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId(PageParam pageParam,
            long DeliveryExecutiveId = 0, string ActionFormDate = "", string ActionToDate = "", long TypeId = 0)
        {
            var quote = abstractDeliveryExecutiveCheckInCheckOutServices.DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId(pageParam,
                DeliveryExecutiveId, ActionFormDate, ActionToDate, TypeId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //DeliveryExecutiveCheckInCheckOut_ById API
        [System.Web.Http.HttpPost] 
        [InheritedRoute("DeliveryExecutiveCheckInCheckOut_ById")]
        public async Task<IHttpActionResult> DeliveryExecutiveCheckInCheckOut_ById(long Id)
        {
            var quote = abstractDeliveryExecutiveCheckInCheckOutServices.DeliveryExecutiveCheckInCheckOut_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }
}