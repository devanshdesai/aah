﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterPayTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterPayTypeServices abstractMasterPayTypeServices;
        #endregion

        #region Cnstr
        public MasterPayTypeV1Controller(AbstractMasterPayTypeServices abstractMasterPayTypeServices)
        {
            this.abstractMasterPayTypeServices = abstractMasterPayTypeServices;
        }
        #endregion

        // MasterPayType_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPayType_All")]
        public async Task<IHttpActionResult> MasterPayType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractMasterPayTypeServices.MasterPayType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}