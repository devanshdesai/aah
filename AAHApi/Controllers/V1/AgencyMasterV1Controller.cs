﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AgencyMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAgencyMasterServices abstractAgencyMasterServices;
        #endregion

        #region Cnstr
        public AgencyMasterV1Controller(AbstractAgencyMasterServices abstractAgencyMasterServices)
        {
            this.abstractAgencyMasterServices = abstractAgencyMasterServices;
        }
        #endregion

       
        // AgencyMaster_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AgencyMaster_Upsert")]
        public async Task<IHttpActionResult> AgencyMaster_Upsert(AgencyMaster AgencyMaster)
        {
            var quote = abstractAgencyMasterServices.AgencyMaster_Upsert(AgencyMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // AgencyMaster_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AgencyMaster_All")]
        public async Task<IHttpActionResult> AgencyMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractAgencyMasterServices.AgencyMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("AgencyMaster_ById")]
        public async Task<IHttpActionResult> AgencyMaster_ById(long Id)
        {
            var quote = abstractAgencyMasterServices.AgencyMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        
    }
}