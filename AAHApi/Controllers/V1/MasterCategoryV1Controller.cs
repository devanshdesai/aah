﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using GoogleMaps.LocationServices;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterCategoryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterCategoryServices abstractMasterCategoryServices;
       
        #endregion

        #region Cnstr
        public MasterCategoryV1Controller(AbstractMasterCategoryServices abstractMasterCategoryServices
           )
        {
            this.abstractMasterCategoryServices = abstractMasterCategoryServices;
           
        }
        #endregion

        // MasterCategory_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterCategory_All")]
        public async Task<IHttpActionResult> MasterCategory_All(PageParam pageParam, string search = "")
        {
            var quote = abstractMasterCategoryServices.MasterCategory_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}