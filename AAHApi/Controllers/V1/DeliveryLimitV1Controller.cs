﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryLimitV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDeliveryLimitServices abstractDeliveryLimitServices;
        #endregion

        #region Cnstr
        public DeliveryLimitV1Controller(AbstractDeliveryLimitServices abstractDeliveryLimitServices)
        {
            this.abstractDeliveryLimitServices = abstractDeliveryLimitServices;
        }
        #endregion
        
        // DeliveryLimit_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryLimit_All")]
        public async Task<IHttpActionResult> DeliveryLimit_All(PageParam pageParam, string search = "")
        {
            var quote = abstractDeliveryLimitServices.DeliveryLimit_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

      
    }
}