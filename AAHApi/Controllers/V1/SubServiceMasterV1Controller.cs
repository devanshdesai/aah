﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SubServiceMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractSubServiceMasterServices abstractSubServiceMasterServices;
        #endregion

        #region Cnstr
        public SubServiceMasterV1Controller(AbstractSubServiceMasterServices abstractSubServiceMasterServices)
        {
            this.abstractSubServiceMasterServices = abstractSubServiceMasterServices;
        }
        #endregion

        // SubServiceMaster_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SubServiceMaster_Upsert")]
        public async Task<IHttpActionResult> SubServiceMaster_Upsert(SubServiceMaster SubServiceMaster)
        {
            var quote = abstractSubServiceMasterServices.SubServiceMaster_Upsert(SubServiceMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // SubServiceMaster_Upsert API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("SubServiceMaster_Upsert")]
        //public async Task<IHttpActionResult> SubServiceMaster_Upsert()
        //{
        //    SubServiceMaster subServiceMaster = new SubServiceMaster();

        //    var httpRequest = HttpContext.Current.Request;
        //    subServiceMaster.Id = Convert.ToInt64(httpRequest.Params["Id"]);
        //    subServiceMaster.ServiceMasterId = Convert.ToInt64(httpRequest.Params["ServiceMasterId"]);
        //    subServiceMaster.SubServiceName = Convert.ToString(httpRequest.Params["SubServiceName"]);
        //    subServiceMaster.Link = Convert.ToString(httpRequest.Params["Link"]);
        //    subServiceMaster.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);
        //    subServiceMaster.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);

        //    string basePath = "";
        //    string fileName = "";
        //    if (httpRequest.Files.Count > 0)
        //    {
        //        var myFile = httpRequest.Files[0];
        //        basePath = "Icon/SubServiceMaster/" + subServiceMaster.Id + "/";
        //        fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

        //        if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
        //        {
        //            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
        //        }
        //        myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
        //        subServiceMaster.Icon = basePath + fileName;
        //    }

        //    var quote = abstractSubServiceMasterServices.SubServiceMaster_Upsert(subServiceMaster);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        // SubServiceMaster_ByServiceMasterId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SubServiceMaster_ByServiceMasterId")]
        public async Task<IHttpActionResult> SubServiceMaster_ByServiceMasterId(PageParam pageParam, string search = "", long ServiceMasterId = 0)
        {
            var quote = abstractSubServiceMasterServices.SubServiceMaster_ByServiceMasterId(pageParam, search, ServiceMasterId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("SubServiceMaster_ById")]
        public async Task<IHttpActionResult> SubServiceMaster_ById(long Id)
        {
            var quote = abstractSubServiceMasterServices.SubServiceMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //SubServiceMaster_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("SubServiceMaster_Delete")]
        public async Task<IHttpActionResult> SubServiceMaster_Delete(long Id, long DeletedBy)
        {
            var quote = abstractSubServiceMasterServices.SubServiceMaster_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}