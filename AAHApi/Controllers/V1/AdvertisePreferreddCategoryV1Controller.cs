﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdvertisePreferredCategoryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdvertisePreferredCategoryServices abstractAdvertisePreferredCategoryServices;
        #endregion

        #region Cnstr
        public AdvertisePreferredCategoryV1Controller(AbstractAdvertisePreferredCategoryServices abstractAdvertisePreferredCategoryServices)
        {
            this.abstractAdvertisePreferredCategoryServices = abstractAdvertisePreferredCategoryServices;
        }
        #endregion

        // AdvertisePreferredCategory_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdvertisePreferredCategory_Upsert")]
        public async Task<IHttpActionResult> AdvertisePreferredCategory_Upsert(AdvertisePreferredCategory AdvertisePreferredCategory)
        {
            var quote = abstractAdvertisePreferredCategoryServices.AdvertisePreferredCategory_Upsert(AdvertisePreferredCategory);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        
    }
}