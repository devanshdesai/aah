﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdminServices abstractAdminServices;
        #endregion

        #region Cnstr
        public AdminV1Controller(AbstractAdminServices abstractAdminServices)
        {
            this.abstractAdminServices = abstractAdminServices;
        }
        #endregion

        // Admin_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_SignIn")]
        public async Task<IHttpActionResult> Admin_SignIn(string Email, string Password)
        {
            var quote = abstractAdminServices.Admin_SignIn(Email, Password);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Admin_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ChangePassword")]
        public async Task<IHttpActionResult> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            var quote = abstractAdminServices.Admin_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_SignOut")]
        public async Task<IHttpActionResult> Admin_SignOut(long Id)
        {
            var quote = abstractAdminServices.Admin_SignOut(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        // Admin_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Upsert")]
        public async Task<IHttpActionResult> Admin_Upsert(Admin admin)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            admin.Password = new String(stringChars);
            var quote = abstractAdminServices.Admin_Upsert(admin);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Admin_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_All")]
        public async Task<IHttpActionResult> Admin_All(PageParam pageParam, string search = "",string Name = "",string Email = "",string MobileNumber = "",long AdminTypeId = 0,long IsActive = 0)
        {
            var quote = abstractAdminServices.Admin_All(pageParam, search, Name,Email,MobileNumber,AdminTypeId,IsActive);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ById")]
        public async Task<IHttpActionResult> Admin_ById(long Id)
        {
            var quote = abstractAdminServices.Admin_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Admin_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ActInAct")]
        public async Task<IHttpActionResult> Admin_ActInAct(long Id, long UpdatedBy)
        {
            var quote = abstractAdminServices.Admin_ActInAct(Id, UpdatedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // AdminType_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminType_All")]
        public async Task<IHttpActionResult> AdminType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractAdminServices.AdminType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}