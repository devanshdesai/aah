﻿using AAH.APICommon;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using AAH.Common.Paging;
using AAH.Services.Contract;
using System.Web.Http.Cors;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RequestsDelieveryExecutiveTrackerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractRequestsDelieveryExecutiveTrackerServices abstractRequestsDelieveryExecutiveTrackerServices;
        #endregion

        #region Cnstr
        public RequestsDelieveryExecutiveTrackerV1Controller(AbstractRequestsDelieveryExecutiveTrackerServices abstractRequestsDelieveryExecutiveTrackerServices)
        {
            this.abstractRequestsDelieveryExecutiveTrackerServices = abstractRequestsDelieveryExecutiveTrackerServices;
        }
        #endregion


        // RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId")]
        public async Task<IHttpActionResult> RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId(PageParam pageParam, string Search = "", long DeliveryExecutiveId = 0)
        {
            var quote = abstractRequestsDelieveryExecutiveTrackerServices.RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId(pageParam, Search, DeliveryExecutiveId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // RequestsDelieveryExecutiveTracker_ByRequestId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestsDelieveryExecutiveTracker_ByRequestId")]
        public async Task<IHttpActionResult> RequestsDelieveryExecutiveTracker_ByRequestId(PageParam pageParam, string Search = "", long RequestId = 0)
        {
            var quote = abstractRequestsDelieveryExecutiveTrackerServices.RequestsDelieveryExecutiveTracker_ByRequestId(pageParam, Search, RequestId);
            return this.Content((HttpStatusCode)200, quote);
        }

        


    }
}
