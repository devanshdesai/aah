﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryExecutiveLatLongV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices;
        #endregion

        #region Cnstr
        public DeliveryExecutiveLatLongV1Controller(AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices)
        {
            this.abstractDeliveryExecutiveLatLongServices = abstractDeliveryExecutiveLatLongServices;
        }
        #endregion
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveLatLong_ByDeliveryExecutiveId")]
        public async Task<IHttpActionResult> DeliveryExecutiveLatLong_ByDeliveryExecutiveId(PageParam pageParam, string search = "", 
            long DeliveryExecutiveId = 0, string ActionFormDate = "", string ActionToDate = "", string ActionFormTime = "", string ActionToTime = "")
        {
            var quote = abstractDeliveryExecutiveLatLongServices.DeliveryExecutiveLatLong_ByDeliveryExecutiveId(pageParam, search, DeliveryExecutiveId,
                ActionFormDate, ActionToDate, ActionFormTime, ActionToTime);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveLatLong_ByRequestId")]
        public async Task<IHttpActionResult> DeliveryExecutiveLatLong_ByRequestId(PageParam pageParam, string search = "", long ByRequestId = 0)
        {
            var quote = abstractDeliveryExecutiveLatLongServices.DeliveryExecutiveLatLong_ByRequestId(pageParam, search, ByRequestId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveLatLong_Insert")]
        public async Task<IHttpActionResult> DeliveryExecutiveLatLong_Insert(List<DeliveryExecutiveLatLong> deliveryExecutiveLatLongs)
        {
            string Json = JsonConvert.SerializeObject(deliveryExecutiveLatLongs);
            var quote = abstractDeliveryExecutiveLatLongServices.DeliveryExecutiveLatLong_Insert(Json);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("OnboarderLatLong_ByAdminId")]
        public async Task<IHttpActionResult> OnboarderLatLong_ByAdminId(PageParam pageParam, string search = "",
            long AdminId = 0, string ActionFormDate = "", string ActionToDate = "", string ActionFormTime = "", string ActionToTime = "")
        {
            var quote = abstractDeliveryExecutiveLatLongServices.OnboarderLatLong_ByAdminId(pageParam, search, AdminId,
                ActionFormDate, ActionToDate, ActionFormTime, ActionToTime);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("OnboarderLatLong_Insert")]
        public async Task<IHttpActionResult> OnboarderLatLong_Insert(List<OnboarderLatLong> onboarderLatLong)
        {
            string Json = JsonConvert.SerializeObject(onboarderLatLong);
            var quote = abstractDeliveryExecutiveLatLongServices.OnboarderLatLong_Insert(Json);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}