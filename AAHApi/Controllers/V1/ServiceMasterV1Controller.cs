﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ServiceMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractServiceMasterServices abstractServiceMasterServices;
        #endregion

        #region Cnstr
        public ServiceMasterV1Controller(AbstractServiceMasterServices abstractServiceMasterServices)
        {
            this.abstractServiceMasterServices = abstractServiceMasterServices;
        }
        #endregion

        // ServiceMaster_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ServiceMaster_Upsert")]
        public async Task<IHttpActionResult> ServiceMaster_Upsert(ServiceMaster ServiceMaster)
        {
            var quote = abstractServiceMasterServices.ServiceMaster_Upsert(ServiceMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // SubServiceMaster_Upsert API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("ServiceMaster_Upsert")]
        //public async Task<IHttpActionResult> ServiceMaster_Upsert()
        //{
        //    ServiceMaster serviceMaster = new ServiceMaster();

        //    var httpRequest = HttpContext.Current.Request;
        //    serviceMaster.Id = Convert.ToInt64(httpRequest.Params["Id"]);
        //    serviceMaster.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);
        //    serviceMaster.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);

        //    string basePath = "";
        //    string fileName = "";
        //    if (httpRequest.Files.Count > 0)
        //    {
        //        var myFile = httpRequest.Files[0];
        //        basePath = "Icon/ServiceMaster/" + serviceMaster.Id + "/";
        //        fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

        //        if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
        //        {
        //            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
        //        }
        //        myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
        //        serviceMaster.Icon = basePath + fileName;
        //    }

        //    var quote = abstractServiceMasterServices.ServiceMaster_Upsert(serviceMaster);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        // ServiceMaster_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ServiceMaster_All")]
        public async Task<IHttpActionResult> ServiceMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractServiceMasterServices.ServiceMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("ServiceMaster_ById")]
        public async Task<IHttpActionResult> ServiceMaster_ById(long Id)
        {
            var quote = abstractServiceMasterServices.ServiceMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ServiceMaster_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ServiceMaster_Delete")]
        public async Task<IHttpActionResult> ServiceMaster_Delete(long Id, long DeletedBy)
        {
            var quote = abstractServiceMasterServices.ServiceMaster_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}