﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ResponderVsTrialVideosV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractResponderVsTrialVideosServices abstractResponderVsTrialVideosServices;
        #endregion

        #region Cnstr
        public ResponderVsTrialVideosV1Controller(AbstractResponderVsTrialVideosServices abstractResponderVsTrialVideosServices)
        {
            this.abstractResponderVsTrialVideosServices = abstractResponderVsTrialVideosServices;
        }
        #endregion

        // ResponderVsTrialVideos_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ResponderVsTrialVideos_Upsert")]
        public async Task<IHttpActionResult> ResponderVsTrialVideos_Upsert(ResponderVsTrialVideos ResponderVsTrialVideos)
        {
            var quote = abstractResponderVsTrialVideosServices.ResponderVsTrialVideos_Upsert(ResponderVsTrialVideos);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        
    }
}