﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HowGinnyBuddyWorksV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractHowGinnyBuddyWorksServices abstractHowGinnyBuddyWorksServices;
        #endregion

        #region Cnstr
        public HowGinnyBuddyWorksV1Controller(AbstractHowGinnyBuddyWorksServices abstractHowGinnyBuddyWorksServices)
        {
            this.abstractHowGinnyBuddyWorksServices = abstractHowGinnyBuddyWorksServices;
        }
        #endregion
        
        // HowGinnyBuddyWorks_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("HowGinnyBuddyWorks_All")]
        public async Task<IHttpActionResult> HowGinnyBuddyWorks_All(PageParam pageParam, string search = "")
        {
            var quote = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

      
    }
}