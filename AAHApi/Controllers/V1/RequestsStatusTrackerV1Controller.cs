﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using AAHApi.Controllers.V1;
using AAHApi.Controllers.V1;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RequestsStatusTrackerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractRequestsStatusTrackerServices abstractRequestsStatusTrackerServices;
        #endregion

        #region Cnstr
        public RequestsStatusTrackerV1Controller(AbstractRequestsStatusTrackerServices abstractRequestsStatusTrackerServices)
        {
            this.abstractRequestsStatusTrackerServices = abstractRequestsStatusTrackerServices;
        }
        #endregion


        // RequestsStatusTracker_ByRequestId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestsStatusTracker_ByRequestId")]
        public async Task<IHttpActionResult> RequestsStatusTracker_ByRequestId(PageParam pageParam, string Search = "", long RequestId = 0)
        {
            var quote = abstractRequestsStatusTrackerServices.RequestsStatusTracker_ByRequestId(pageParam, Search, RequestId);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
