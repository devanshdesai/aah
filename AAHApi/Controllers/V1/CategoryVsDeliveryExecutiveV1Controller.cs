﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using GoogleMaps.LocationServices;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CategoryVsDeliveryExecutiveV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCategoryVsDeliveryExecutiveServices abstractCategoryVsDeliveryExecutiveServices;
       
        #endregion

        #region Cnstr
        public CategoryVsDeliveryExecutiveV1Controller(AbstractCategoryVsDeliveryExecutiveServices abstractCategoryVsDeliveryExecutiveServices
           )
        {
            this.abstractCategoryVsDeliveryExecutiveServices = abstractCategoryVsDeliveryExecutiveServices;
           
        }
        #endregion

        // CategoryVsDeliveryExecutive_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CategoryVsDeliveryExecutive_Upsert")]
        public async Task<IHttpActionResult> CategoryVsDeliveryExecutive_Upsert(CategoryVsDeliveryExecutive CategoryVsDeliveryExecutive)
        {
            var quote = abstractCategoryVsDeliveryExecutiveServices.CategoryVsDeliveryExecutive_Upsert(CategoryVsDeliveryExecutive);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // CategoryVsDeliveryExecutive_RequestId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CategoryVsDeliveryExecutive_RequestId")]
        public async Task<IHttpActionResult> CategoryVsDeliveryExecutive_RequestId(PageParam pageParam, string search = "", long RequestId =0)
        {
            var quote = abstractCategoryVsDeliveryExecutiveServices.CategoryVsDeliveryExecutive_RequestId(pageParam, search, RequestId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // CategoryVsDeliveryExecutive_DeliveryExecutiveId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CategoryVsDeliveryExecutive_DeliveryExecutiveId")]
        public async Task<IHttpActionResult> CategoryVsDeliveryExecutive_DeliveryExecutiveId(PageParam pageParam, string search = "", long DeliveryExecutiveId = 0)
        {
            var quote = abstractCategoryVsDeliveryExecutiveServices.CategoryVsDeliveryExecutive_DeliveryExecutiveId(pageParam, search, DeliveryExecutiveId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //CategoryVsDeliveryExecutive_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("CategoryVsDeliveryExecutive_Delete")]
        public async Task<IHttpActionResult> CategoryVsDeliveryExecutive_Delete(long Id, long DeletedBy)
        {
            var quote = abstractCategoryVsDeliveryExecutiveServices.CategoryVsDeliveryExecutive_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }
}