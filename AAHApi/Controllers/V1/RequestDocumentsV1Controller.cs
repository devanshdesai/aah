﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using GoogleMaps.LocationServices;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RequestDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractRequestDocumentsServices abstractRequestDocumentsServices;
        #endregion

        #region Cnstr
        public RequestDocumentsV1Controller(AbstractRequestDocumentsServices abstractRequestDocumentsServices)
        {
            this.abstractRequestDocumentsServices = abstractRequestDocumentsServices;
        }
        #endregion

        // RequestDocuments_Insert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestDocuments_Insert")]
        public async Task<IHttpActionResult> RequestDocuments_Insert()
        {
            RequestDocuments requestDocuments = new RequestDocuments();

            var httpRequest = HttpContext.Current.Request;
            requestDocuments.RequestId = Convert.ToInt64(httpRequest.Params["RequestId"]);
            requestDocuments.IsUploadedByDeliveryExecutive = Convert.ToBoolean(httpRequest.Params["IsUploadedByDeliveryExecutive"]);
            requestDocuments.IsUploadedByUser = Convert.ToBoolean(httpRequest.Params["IsUploadedByUser"]);
            requestDocuments.CreatedBy = Convert.ToInt64(httpRequest.Params["CreatedBy"]);
           
            string basePath = "";
            string fileName = "";
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                basePath = "RequestDocuments/" + requestDocuments.RequestId + "/";
                fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                requestDocuments.DocumentURL = basePath + fileName;
            }

            var quote = abstractRequestDocumentsServices.RequestDocuments_Insert(requestDocuments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    

        //RequestDocuments_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestDocuments_ById")]
        public async Task<IHttpActionResult> RequestDocuments_ById(long Id)
        {
            var quote = abstractRequestDocumentsServices.RequestDocuments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //RequestDocuments_ByRequestId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestDocuments_ByRequestId")]
        public async Task<IHttpActionResult> RequestDocuments_ByRequestId(PageParam pageParam, long RequestId = 0)
        {
            var quote = abstractRequestDocumentsServices.RequestDocuments_ByRequestId(pageParam, RequestId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //RequestDocuments_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("RequestDocuments_Delete")]
        public async Task<IHttpActionResult> RequestDocuments_Delete(long Id, long DeletedBy)
        {
            var quote = abstractRequestDocumentsServices.RequestDocuments_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}