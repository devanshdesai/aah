﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using GoogleMaps.LocationServices;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddressTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAddressTypeServices abstractAddressTypeServices;
       
        #endregion

        #region Cnstr
        public AddressTypeV1Controller(AbstractAddressTypeServices abstractAddressTypeServices
           )
        {
            this.abstractAddressTypeServices = abstractAddressTypeServices;
           
        }
        #endregion

        // AddressType_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressType_Upsert")]
        public async Task<IHttpActionResult> AddressType_Upsert(AddressType AddressType)
        {
            var quote = abstractAddressTypeServices.AddressType_Upsert(AddressType);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // AddressType_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressType_All")]
        public async Task<IHttpActionResult> AddressType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractAddressTypeServices.AddressType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //AddressType_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressType_ById")]
        public async Task<IHttpActionResult> AddressType_ById(long Id)
        {
            var quote = abstractAddressTypeServices.AddressType_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

      
        //AddressType_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("AddressType_Delete")]
        public async Task<IHttpActionResult> AddressType_Delete(long Id, long DeletedBy)
        {
            var quote = abstractAddressTypeServices.AddressType_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }
}