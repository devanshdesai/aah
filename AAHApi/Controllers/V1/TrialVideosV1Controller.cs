﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TrialVideosV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTrialVideosServices abstractTrialVideosServices;
        #endregion

        #region Cnstr
        public TrialVideosV1Controller(AbstractTrialVideosServices abstractTrialVideosServices)
        {
            this.abstractTrialVideosServices = abstractTrialVideosServices;
        }
        #endregion

        // TrialVideos_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("TrialVideos_Upsert")]
        public async Task<IHttpActionResult> TrialVideos_Upsert(TrialVideos TrialVideos)
        {
            var quote = abstractTrialVideosServices.TrialVideos_Upsert(TrialVideos);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("AfterTrialVideos_Complete")]
        public async Task<IHttpActionResult> AfterTrialVideos_Complete(long DeliveryExecutiveId , long TrialVideosId)
        {
            var quote = abstractTrialVideosServices.AfterTrialVideos_Complete(DeliveryExecutiveId, TrialVideosId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }
}