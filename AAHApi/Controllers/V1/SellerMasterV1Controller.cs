﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SellerMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractSellerMasterServices abstractSellerMasterServices;
        #endregion

        #region Cnstr
        public SellerMasterV1Controller(AbstractSellerMasterServices abstractSellerMasterServices)
        {
            this.abstractSellerMasterServices = abstractSellerMasterServices;
        }
        #endregion

        // SellerMaster_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SellerMaster_Upsert")]
        public async Task<IHttpActionResult> SellerMaster_Upsert()
        {
            SellerMaster sellerMaster = new SellerMaster();
            var httpRequest = HttpContext.Current.Request;
            sellerMaster.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            sellerMaster.Name = Convert.ToString(httpRequest.Params["Name"]);
            sellerMaster.Lat = Convert.ToString(httpRequest.Params["Lat"]);
            sellerMaster.Long = Convert.ToString(httpRequest.Params["Long"]);
            sellerMaster.OwnerName = Convert.ToString(httpRequest.Params["OwnerName"]);
            sellerMaster.ContactNumber = Convert.ToString(httpRequest.Params["ContactNumber"]);
            sellerMaster.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);
            sellerMaster.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            string basePath = "";
            string fileName = "";
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                basePath = "doc/SellerMaster/" + sellerMaster.Id + "/";
                fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                sellerMaster.PicUrl = basePath + fileName;
            }

            var quote = abstractSellerMasterServices.SellerMaster_Upsert(sellerMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}