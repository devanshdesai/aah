﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryExecutiveAdminCommentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDeliveryExecutiveAdminCommentsServices abstractDeliveryExecutiveAdminCommentsServices;
        #endregion

        #region Cnstr
        public DeliveryExecutiveAdminCommentsV1Controller(AbstractDeliveryExecutiveAdminCommentsServices abstractDeliveryExecutiveAdminCommentsServices)
        {
            this.abstractDeliveryExecutiveAdminCommentsServices = abstractDeliveryExecutiveAdminCommentsServices;
        }
        #endregion

        // DeliveryExecutiveAdminComments_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveAdminComments_Upsert")]
        public async Task<IHttpActionResult> DeliveryExecutiveAdminComments_Upsert(DeliveryExecutiveAdminComments DeliveryExecutiveAdminComments)
        {
            var quote = abstractDeliveryExecutiveAdminCommentsServices.DeliveryExecutiveAdminComments_Upsert(DeliveryExecutiveAdminComments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DeliveryExecutiveAdminComments_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveAdminComments_All")]
        public async Task<IHttpActionResult> DeliveryExecutiveAdminComments_All(PageParam pageParam, string search = "", long DeliveryExecutiveId = 0, long AdminId = 0)
        {
            var quote = abstractDeliveryExecutiveAdminCommentsServices.DeliveryExecutiveAdminComments_All(pageParam, search, DeliveryExecutiveId, AdminId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //DeliveryExecutiveAdminComments_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveAdminComments_ById")]
        public async Task<IHttpActionResult> DeliveryExecutiveAdminComments_ById(long Id)
        {
            var quote = abstractDeliveryExecutiveAdminCommentsServices.DeliveryExecutiveAdminComments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //DeliveryExecutiveAdminComments_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveAdminComments_Delete")]
        public async Task<IHttpActionResult> DeliveryExecutiveAdminComments_Delete(long Id, long DeletedBy)
        {
            var quote = abstractDeliveryExecutiveAdminCommentsServices.DeliveryExecutiveAdminComments_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}