﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryExecutiveNotificationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices;
        #endregion

        #region Cnstr
        public DeliveryExecutiveNotificationsV1Controller(AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices)
        {
            this.abstractDeliveryExecutiveNotificationsServices = abstractDeliveryExecutiveNotificationsServices;
        }
        #endregion


        // DeliveryExecutiveNotifications_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveNotifications_Insert")]
        public async Task<IHttpActionResult> DeliveryExecutiveNotifications_Insert(DeliveryExecutiveNotifications deliveryExecutiveNotifications)
        {
            var quote = abstractDeliveryExecutiveNotificationsServices.DeliveryExecutiveNotifications_Insert(deliveryExecutiveNotifications);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // DeliveryExecutiveNotifications_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveNotifications_ByDeliveryExecutiveId")]
        public async Task<IHttpActionResult> DeliveryExecutiveNotifications_ByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId = 0, long RequestId = 0, string ActionFormDate = "", string ActionToDate = "", int IsSent = 0)
        {
            var quote = abstractDeliveryExecutiveNotificationsServices.DeliveryExecutiveNotifications_ByDeliveryExecutiveId(pageParam, DeliveryExecutiveId, RequestId, ActionFormDate, ActionToDate, IsSent);
            return this.Content((HttpStatusCode)200, quote);
        }

        // DeliveryExecutiveNotifications_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveNotifications_ByRequestId")]
        public async Task<IHttpActionResult> DeliveryExecutiveNotifications_ByRequestId(PageParam pageParam, long RequestId = 0)
        {
            var quote = abstractDeliveryExecutiveNotificationsServices.DeliveryExecutiveNotifications_ByRequestId(pageParam, RequestId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // DeliveryExecutiveNotifications_RequestStatusUpdate API
        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryExecutiveNotifications_RequestStatusUpdate")]
        public async Task<IHttpActionResult> DeliveryExecutiveNotifications_RequestStatusUpdate(long Id, long StatusId)
        {
            var quote = abstractDeliveryExecutiveNotificationsServices.DeliveryExecutiveNotifications_RequestStatusUpdate(Id, StatusId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}