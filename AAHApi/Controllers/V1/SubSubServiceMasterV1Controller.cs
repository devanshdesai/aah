﻿using AAH.APICommon;
using AAHApi.Models;
using AAH.Common;
using AAHApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SubSubServiceMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractSubSubServiceMasterServices abstractSubSubServiceMasterServices;
        #endregion

        #region Cnstr
        public SubSubServiceMasterV1Controller(AbstractSubSubServiceMasterServices abstractSubSubServiceMasterServices)
        {
            this.abstractSubSubServiceMasterServices = abstractSubSubServiceMasterServices;
        }
        #endregion

    
        // SubSubServiceMaster_Upsert API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("SubSubServiceMaster_Upsert")]
        //public async Task<IHttpActionResult> SubSubServiceMaster_Upsert()
        //{


        //    SubSubServiceMaster subSubServiceMaster = new SubSubServiceMaster();

        //    var httpRequest = HttpContext.Current.Request;
        //    subSubServiceMaster.Id = Convert.ToInt64(httpRequest.Params["Id"]);
        //    subSubServiceMaster.SubServiceMasterId = Convert.ToInt64(httpRequest.Params["SubServiceMasterId"]);
        //    subSubServiceMaster.SubSubServiceName = Convert.ToString(httpRequest.Params["SubSubServiceName"]);
        //    subSubServiceMaster.Description = Convert.ToString(httpRequest.Params["Description"]);
        //    subSubServiceMaster.Link = Convert.ToString(httpRequest.Params["Link"]);
        //    subSubServiceMaster.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);
        //    subSubServiceMaster.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);

            

        //    string basePath = "";
        //    string fileName = "";
        //    if (httpRequest.Files.Count > 0)
        //    {
        //        var myFile = httpRequest.Files[0];
        //        basePath = "Icon/SubSubServiceMaster/" + subSubServiceMaster.Id + "/";
        //        fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);

        //        if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
        //        {
        //            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
        //        }
        //        myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
        //        subSubServiceMaster.Icon = basePath + fileName;
        //    }

        //    var quote = abstractSubSubServiceMasterServices.SubSubServiceMaster_Upsert(subSubServiceMaster);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        // SubSubServiceMaster_ByServiceMasterId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SubSubServiceMaster_BySubServiceMasterId")]
        public async Task<IHttpActionResult> SubSubServiceMaster_BySubServiceMasterId(PageParam pageParam, string search = "", long SubServiceMasterId = 0)
        {
            var quote = abstractSubSubServiceMasterServices.SubSubServiceMaster_BySubServiceMasterId(pageParam, search, SubServiceMasterId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("SubSubServiceMaster_ById")]
        public async Task<IHttpActionResult> SubSubServiceMaster_ById(long Id)
        {
            var quote = abstractSubSubServiceMasterServices.SubSubServiceMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
       
    }
}