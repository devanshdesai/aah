﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResendRequestNotification
{
    class Program
    {
        public static string ConnStr = ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString;

        static void Main(string[] args)
        {

            SqlConnection conn = new SqlConnection(ConnStr);
            while (true)
            {
                try
                {
                    string q = "select RequestId from DeliveryExecutiveVsRequests where StatusId = 2 and CreatedDate <= DATEADD(minute, -3,  GETDATE()) group by RequestId";
                    SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                    DataTable dt = new DataTable();
                    ad.Fill(dt);

                    string q2 = "select RequestId from DeliveryExecutiveVsRequests where StatusId = 2 and CreatedDate <= DATEADD(minute, -6,  GETDATE()) group by RequestId";
                    SqlDataAdapter ad2 = new SqlDataAdapter(q2, ConnStr);
                    DataTable dt2 = new DataTable();
                    ad2.Fill(dt2);

                    conn.Open();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["RequestId"].ToString() != "")
                            {
                                SqlCommand sqlCmd = new SqlCommand("ResendDeliveryExecutive_Radar", conn);
                                sqlCmd.CommandType = CommandType.StoredProcedure;
                                sqlCmd.Parameters.AddWithValue("@RequestId", SqlDbType.BigInt).Value = row["RequestId"].ToString();

                                // Execute the command and get the data in a data reader.
                                SqlDataReader reader = sqlCmd.ExecuteReader();
                            }
                        }
                    }

                    if (dt2.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt2.Rows)
                        {
                            if (row["RequestId"].ToString() != "")
                            {
                                SqlCommand sqlCmd2 = new SqlCommand("UpdateRequest_NotAttended", conn);
                                sqlCmd2.CommandType = CommandType.StoredProcedure;
                                sqlCmd2.Parameters.AddWithValue("@RequestId", SqlDbType.BigInt).Value = row["RequestId"].ToString();

                                // Execute the command and get the data in a data reader.
                                SqlDataReader reader = sqlCmd2.ExecuteReader();
                            }
                        }
                    }


                    conn.Close();

                    Console.WriteLine("response", "success");

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }

            }
        }
    }
}
