﻿using AAH.Common;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GinnyBuddy.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string RId = "0")
        {

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DEDownloadInsert", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@DEId", SqlDbType.Int).Value = RId;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}