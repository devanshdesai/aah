﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace SendNotifications
{
    class Program
    {
        public static string ConnStr = "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=AAH;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";

        static void Main(string[] args)
        {

            //List<string> DeviceT = new List<string>();
            //Console.WriteLine("Getting Connection ...");

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnStr);
            while (true)
            {
                try
                {
                    string q = "select * from [dbo].[DeliveryExecutiveNotifications] where IsSent = 2 and CreatedDate >= DATEADD(minute, -1,  GETDATE())";
                    SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                    DataTable dt = new DataTable();
                    ad.Fill(dt);

                    string qu = "select * from [dbo].[UserNotifications] where IsSent = 2 and CreatedDate >= DATEADD(minute, -1,  GETDATE())";
                    SqlDataAdapter uad = new SqlDataAdapter(qu, ConnStr);
                    DataTable dt1 = new DataTable();
                    uad.Fill(dt1);

                    
                    if (dt.Rows.Count > 0)
                    {
                        List<string> DeviceT = new List<string>();
                        Console.WriteLine("Getting Connection ...");

                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["DeliveryExecutiveId"].ToString() != "")
                            {
                                string getDT = "select DeviceToken from [dbo].[DeliveryExecutiveDevices] where DeliveryExecutiveId = " + row["DeliveryExecutiveId"].ToString() + " AND DeviceToken IS NOT NULL order by Id desc";
                                SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                                DataTable DEDeviceToken = new DataTable();
                                DT.Fill(DEDeviceToken);
                                DeviceT = new List<string>();

                                if (DEDeviceToken.Rows.Count > 0)
                                {
                                    DeviceT = DEDeviceToken.AsEnumerable()
                                              .Select(r => r.Field<string>("DeviceToken"))
                                              .ToList();

                                    if (DeviceT.Count > 0)
                                    {
                                        SendNotification(DeviceT, row["NotificationJSON"].ToString(), row["id"].ToString());
                                    }
                                }
                            }
                        }
                    }

                    if (dt1.Rows.Count > 0)
                    {
                        List<string> DeviceT = new List<string>();
                        Console.WriteLine("Getting Connection ...");

                        foreach (DataRow row in dt1.Rows)
                        {
                            if (row["Id"].ToString() != "")
                            {
                                string getDT = "select DeviceToken from [dbo].[UserDevices] where UserId = " + row["UserId"].ToString() + " AND DeviceToken IS NOT NULL order by Id desc"; /*AND IsVerified=1*/
                                //string getDT = "select DeviceToken from [dbo].[UserDevices] where Id = 13  AND LastLogin IS NULL  order by Id desc"; /*AND IsVerified=1*/
                                SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                                DataTable DEDeviceToken = new DataTable();
                                DT.Fill(DEDeviceToken);
                                DeviceT = new List<string>();

                                if (DEDeviceToken.Rows.Count > 0)
                                {
                                    DeviceT = DEDeviceToken.AsEnumerable()
                                              .Select(r => r.Field<string>("DeviceToken"))
                                              .ToList();

                                    if (DeviceT.Count > 0)
                                    {
                                        SendUserNotification(DeviceT, row["NotificationJSON"].ToString(), row["id"].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
                
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
            Console.Read();
        }

        public static string SendUserNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = "AAAAJZAGPpE:APA91bH_7g7sqU_ErXU7OAEl56QAX0wtoPnic_JOmJ4ojhY3CQonf_lNgMD7cGZT6CtvzWTAS0ZLHI5OeCzztnejzCGlR09iXiT-VRx4eUEsXAV7HZL_imyYnQyGHk387LhneNesXJCk"; // Something very long
            //string serverKey = "AAAA6tsjjhM:APA91bEbAdQr_VHZ5pHjtTUhspCHe_bN2GTQfYXbgu4skFl7n2_jGcAhw2A8Rox1qAf5UyFW7eSpVmREt-9VcWe_MENAnNSU9ZUDFw7WUzxFt06kcsB2p0mtor1mAp4IRQSspn0QnLNK"; // Something very long
            //string senderId = "439168650109";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

       

            var data = new
            {
                registration_ids = DeviceT,
                //notification = new
                //{
                //    body = Description,
                //    title = "Grettings from GinnyBuddy !",
                //    sound = "Enabled"
                //},
                data = new
                {
                    body = Description,
                    title = "",
                    sound = "default",
                    mutable_contenct = true,
                    badge = 4,
                    data = new { }
                },
                mutable_contenct = true,
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            Conn.Open();
                            string updateIsSent = "UPDATE UserNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            UpdateDENotificationcmd.ExecuteNonQuery();

                            string updateIsSent2 = "update UserNotifications set IsSent = 1 where IsSent =2 and CreatedDate < DATEADD(minute, -1,  GETDATE())";
                            SqlCommand UpdateDENotificationcmd2 = new SqlCommand(updateIsSent2, Conn);
                            UpdateDENotificationcmd2.ExecuteNonQuery();

                            Conn.Close();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }

            return "";
        }

        public static string SendNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = "AAAAs4iKRw8:APA91bEeoO6dUNqlXGCicI8lEV3YaeGs2__g7v4EYjO988EjBfhuZEc-cV0lbPe6hYQ4mFGMSOWGRdhSLzD7pFvyb66GGJuiKCRe8n0vsdkDFETbTbvtGVWyAnu9F6Ra1Xm3FUTEt6JC"; // Something very long
                                                                                                                                                                                           //string serverKey = "AAAA4Dq4gQM:APA91bG57-L2ntII-gNUTWcMLZymfUEwJbgmjYkFOMHjCZzUCdiJYrtzVeCgYu-WqOLPymPWkJ44xII9Y3Noj0_cFqYCXdDOLBD8ALB_ijq6BVGHSk01VOrhtGd6xxawL4RxmghTTjPF"; // Something very long
                                                                                                                                                                                           //string senderId = "439168650109";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
                registration_ids = DeviceT,
                //to = deviceId,
                data = new
                {
                    body = Description,//"Your job Status has been changed",
                    title = "",//"Fitting job has been completed.",
                    sound = "default",
                    mutable_contenct = true,
                    badge = 4,
                    data = new { }
                },
                mutable_contenct = true
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            Conn.Open();
                            string updateIsSent = "UPDATE DeliveryExecutiveNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            UpdateDENotificationcmd.ExecuteNonQuery();

                            string updateIsSent2 = "update DeliveryExecutiveNotifications set IsSent = 1 where IsSent =2 and CreatedDate < DATEADD(minute, -1,  GETDATE())";
                            SqlCommand UpdateDENotificationcmd2 = new SqlCommand(updateIsSent2, Conn);
                            UpdateDENotificationcmd2.ExecuteNonQuery();

                            Conn.Close();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }

            return "";
        }
    }
}
