﻿namespace AAH.Common
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web.Script.Serialization;

    public class PushNotification
    {
        public static string ConnStr = Configurations.ConnectionString;

        public static bool Main()
        {
            SqlConnection conn = new SqlConnection(ConnStr);
            try
            {
                string q = "select * from [dbo].[DeliveryExecutiveNotifications] where IsSent = 2 and CreatedDate >= DATEADD(minute, -1,  GETDATE())";
                SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                DataTable dt = new DataTable();
                ad.Fill(dt);

                string qu = "select * from [dbo].[UserNotifications] where IsSent = 2 and CreatedDate >= DATEADD(minute, -1,  GETDATE())";
                SqlDataAdapter uad = new SqlDataAdapter(qu, ConnStr);
                DataTable dt1 = new DataTable();
                uad.Fill(dt1);

                if (dt.Rows.Count > 0)
                {
                    List<string> DeviceT = new List<string>();

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["DeliveryExecutiveId"].ToString() != "")
                        {
                            string getDT = "select DeviceToken from [dbo].[DeliveryExecutiveDevices] where DeliveryExecutiveId = " + row["DeliveryExecutiveId"].ToString() + " AND DeviceToken IS NOT NULL order by Id desc";
                            SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                            DataTable DEDeviceToken = new DataTable();
                            DT.Fill(DEDeviceToken);
                            DeviceT = new List<string>();

                            if (DEDeviceToken.Rows.Count > 0)
                            {
                                DeviceT = DEDeviceToken.AsEnumerable()
                                          .Select(r => r.Field<string>("DeviceToken"))
                                          .ToList();

                                if (DeviceT.Count > 0)
                                {
                                    SendNotification(DeviceT, row["NotificationJSON"].ToString(), row["id"].ToString());
                                }
                            }
                        }
                    }
                }

                if (dt1.Rows.Count > 0)
                {
                    List<string> DeviceT = new List<string>();

                    foreach (DataRow row in dt1.Rows)
                    {
                        if (row["Id"].ToString() != "")
                        {
                            string getDT = "select DeviceToken from [dbo].[UserDevices] where UserId = " + row["UserId"].ToString() + " AND DeviceToken IS NOT NULL order by Id desc"; /*AND IsVerified=1*/
                            SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                            DataTable DEDeviceToken = new DataTable();
                            DT.Fill(DEDeviceToken);
                            DeviceT = new List<string>();

                            if (DEDeviceToken.Rows.Count > 0)
                            {
                                DeviceT = DEDeviceToken.AsEnumerable()
                                            .Select(r => r.Field<string>("DeviceToken"))
                                            .ToList();

                                if (DeviceT.Count > 0)
                                {
                                    SendUserNotification(DeviceT, row["NotificationJSON"].ToString(), row["id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public static bool SendUserNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = Configurations.PushNotificationServerKey;
            //string serverKey = "AAAA4Dq4gQM:APA91bG57-L2ntII-gNUTWcMLZymfUEwJbgmjYkFOMHjCZzUCdiJYrtzVeCgYu-WqOLPymPWkJ44xII9Y3Noj0_cFqYCXdDOLBD8ALB_ijq6BVGHSk01VOrhtGd6xxawL4RxmghTTjPF";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
                registration_ids = DeviceT,
                //notification = new
                //{
                //    body = Description,
                //    title = "Grettings from GinnyBuddy !",
                //    sound = "Enabled"
                //},
                data = new
                {
                    body = Description,
                    title = "",
                    sound = "default",
                    mutable_contenct = true,
                    badge = 4,
                    data = new { }
                },
                mutable_contenct = true,
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            string updateIsSent = "UPDATE UserNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            string updateIsSent2 = "update UserNotifications set IsSent = 1 where IsSent =2 and CreatedDate < DATEADD(minute, -1,  GETDATE())";
                            SqlCommand UpdateDENotificationcmd2 = new SqlCommand(updateIsSent2, Conn);
                            Conn.Open();
                            UpdateDENotificationcmd.ExecuteNonQuery();
                            UpdateDENotificationcmd2.ExecuteNonQuery();
                            Conn.Close();
                        }
                    }
                }
            }

            return true;
        }


        public static bool SendNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = Configurations.DEPushNotificationServerKey;
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
                registration_ids = DeviceT,
                //to = deviceId,
                data = new
                {
                    body = Description,//"Your job Status has been changed",
                    title = "",//"Fitting job has been completed.",
                    sound = "default",
                    mutable_contenct = true,
                    badge = 4,
                    data = new { }
                },
                mutable_contenct = true
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            string updateIsSent = "UPDATE DeliveryExecutiveNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);

                            string updateIsSent2 = "update DeliveryExecutiveNotifications set IsSent = 1 where IsSent =2 and CreatedDate < DATEADD(minute, -1,  GETDATE())";
                            SqlCommand UpdateDENotificationcmd2 = new SqlCommand(updateIsSent2, Conn);

                            Conn.Open();
                            UpdateDENotificationcmd.ExecuteNonQuery();
                            UpdateDENotificationcmd2.ExecuteNonQuery();
                            Conn.Close();
                        }
                    }
                }
            }

            return true;
        }
    }
}
