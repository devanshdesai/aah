﻿using System.Web;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;

namespace AAH.Common
{
    public class ProjectSession
    {
        public static long AdminId
        {
            get
            {
                if (HttpContext.Current.Session["AdminId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["AdminId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminId"] = value;
            }
        }

        public static int IsAvailable
        {
            get
            {
                if (HttpContext.Current.Session["IsAvailable"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["IsAvailable"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsAvailable"] = value;
            }
        }
        public static int IsOnline
        {
            get
            {
                if (HttpContext.Current.Session["IsOnline"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["IsOnline"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsOnline"] = value;
            }
        }
        public static bool IsMarketingPerson
        {
            get
            {
                if (HttpContext.Current.Session["IsMarketingPerson"] == null)
                {
                    return false;
                }
                else
                {
                    return ConvertTo.Boolean(HttpContext.Current.Session["IsMarketingPerson"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsMarketingPerson"] = value;
            }
        }

        public static long StatusId
        {
            get
            {
                if (HttpContext.Current.Session["StatusId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["StatusId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["StatusId"] = value;
            }
        }

        public static string UserType
        {
            get
            {
                if (HttpContext.Current.Session["UserType"].ToString() == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["UserType"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserType"] = value;
            }
        }

        public static string AreaName
        {
            get
            {
                if (HttpContext.Current.Session["AreaName"] == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["AreaName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AreaName"] = value;
            }
        }

        public static string AdminName
        {
            get
            {
                if (HttpContext.Current.Session["AdminName"] == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["AdminName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminName"] = value;
            }
        }

        public static string LoginAdminEmail
        {
            get
            {
                if (HttpContext.Current.Session["LoginAdminEmail"].ToString() == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["LoginAdminEmail"]);
                }
            }

            set
            {
                HttpContext.Current.Session["LoginAdminEmail"] = value;
            }
        }
        public static string MobileNumber
        {
            get
            {
                if (HttpContext.Current.Session["MobileNumber"].ToString() == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["MobileNumber"]);
                }
            }

            set
            {
                HttpContext.Current.Session["MobileNumber"] = value;
            }
        }
        public static string ProfileURL
        {
            get
            {
                if (HttpContext.Current.Session["ProfileURL"].ToString() == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["ProfileURL"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ProfileURL"] = value;
            }
        }

    }
}