﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractAdvertisePreferredCategoryDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Upsert(AbstractAdvertisePreferredCategory abstractAdvertisePreferredCategory);
        public abstract SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_ById(int Id);
        public abstract PagedList<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Delete(int Id, int DeletedBy);
        
        
    }
}
