﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractDeliveryExecutiveLatLongDao : AbstractBaseDao

    {
        public abstract PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_ByDeliveryExecutiveId(PageParam pageparam, string Search, 
            long DeliveryExecutiveId, string ActionFormDate, string ActionToDate, string ActionFormTime, string ActionToTime);
        public abstract PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_ByRequestId(PageParam pageparam, string Search, long RequestId);
        public abstract PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_Map();
        public abstract SuccessResult<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_Insert(string Json);

        public abstract PagedList<AbstractOnboarderLatLong> OnboarderLatLong_ByAdminId(PageParam pageparam, string Search,
            long AdminId, string ActionFormDate, string ActionToDate, string ActionFormTime, string ActionToTime);
        public abstract SuccessResult<AbstractOnboarderLatLong> OnboarderLatLong_Insert(string Json);
    }
}
