﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractSurveyDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractSurvey> Survey_Insert(AbstractSurvey abstractSurvey);
        public abstract PagedList<AbstractSurvey> Survey_All(PageParam pageParam, string Search);
    }
}
