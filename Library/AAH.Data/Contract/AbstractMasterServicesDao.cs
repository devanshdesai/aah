﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractMasterServicesDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractMasterServices> MasterServices_Upsert(AbstractMasterServices abstractMasterServices);
        public abstract SuccessResult<AbstractMasterServices> MasterServices_ById(long Id);
        public abstract SuccessResult<AbstractMasterServices> MasterServices_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractMasterServices> MasterServices_All(PageParam pageParam, string Search);
    }
}
