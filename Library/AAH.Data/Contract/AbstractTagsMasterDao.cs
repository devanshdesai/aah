﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractTagsMasterDao: AbstractBaseDao

    {
        public abstract PagedList<AbstractTagsMaster> TagsMaster_All(PageParam pageParam, string search);
    }
}