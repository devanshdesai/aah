﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractMasterPayTypeDao: AbstractBaseDao
    {
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_Upsert(AbstractMasterPayType abstractMasterPayType);
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_ById(long Id);
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_ActInAct(long Id, int UpdatedBy);
        public abstract PagedList<AbstractMasterPayType> MasterPayType_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_Delete(long Id, int DeletedBy);
       
    }
}
