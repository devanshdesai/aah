﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractSubServiceMasterDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractSubServiceMaster> SubServiceMaster_Upsert(AbstractSubServiceMaster abstractSubServiceMaster);
        public abstract PagedList<AbstractSubServiceMaster> SubServiceMaster_ByServiceMasterId(PageParam pageParam, string search, long ServiceMasterId);
        public abstract SuccessResult<AbstractSubServiceMaster> SubServiceMaster_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractSubServiceMaster> SubServiceMaster_ById(long Id);
    }
}
