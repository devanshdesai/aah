﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractDeliveryLimitDao : AbstractBaseDao

    {
        public abstract SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Upsert(AbstractDeliveryLimit abstractDeliveryLimit);
        public abstract SuccessResult<AbstractDeliveryLimit> DeliveryLimit_ById(long Id);
        public abstract PagedList<AbstractDeliveryLimit> DeliveryLimit_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Delete(long Id, int DeletedBy);
    }
}
