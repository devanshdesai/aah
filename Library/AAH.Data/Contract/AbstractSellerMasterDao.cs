﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractSellerMasterDao: AbstractBaseDao
    {
        public abstract SuccessResult<AbstractSellerMaster> SellerMaster_Upsert(AbstractSellerMaster abstractSellerMaster);
    }
}
