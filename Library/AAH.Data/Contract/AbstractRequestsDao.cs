﻿using System;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractRequestsDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractRequests> Requests_Insert(AbstractRequests abstractRequests);
        public abstract SuccessResult<AbstractRequestcomments> Requestcomments_Insert(AbstractRequestcomments abstractRequests);
        public abstract PagedList<AbstractRequestcomments> Requestcomments_ByRequestId(PageParam pageParam, string Search, long RequestId);
        public abstract SuccessResult<AbstractRequests> Requests_AssignDeliveryExecutive(AbstractRequests abstractRequests);
        public abstract SuccessResult<AbstractRequests> Request_UpdateStatus(long RequestId, long StatusId, long DeliveryExecutiveId, decimal AmountGiven,decimal RequestedAmount, long OTP = 0, string DECancleReason = "");
        public abstract SuccessResult<AbstractRequests> Requests_ById(long Id, long LoginId = 0);
        public abstract SuccessResult<AbstractRequests> Requests_UpdateIsSettledWithAgency(long AgencyId, string FromDate, string ToDate, long DeliveryExecutiveId, int IsSettledWithAgency);
        public abstract SuccessResult<AbstractRequests> Requests_UpdateIsSettledWithAdmin(long Id, int IsSettledWithAdmin);
        public abstract SuccessResult<AbstractRequests> Requests_UpdateByAdmin(long RequestedId,long ServiceMasterTypeId,long MasterPayTypeId,decimal AmountRequested,long DeliveryExecutiveId);
        public abstract SuccessResult<AbstractRequests> Requests_UpdateByDeliveryExecutive(AbstractRequests abstractRequests);
        public abstract SuccessResult<AbstractRequests> DeliveryExecutiveRequestAmount_Update(long Id, decimal DeliveryExecutiveRequestAmount);
        public abstract SuccessResult<AbstractRequests> IsApprovedByUser_Update(long Id, int IsApprovedByUser);
        public abstract PagedList<AbstractRequests> Requests_All(PageParam pageParam, string Search, long UserId, long DeliveryExecutiveId, long ServiceMasterId, long SubServiceMasterId, long StatusId, long loginId =0, long Type = 0,long FilterType = 0);
        public abstract PagedList<AbstractRequests> RequestsStatus_All(PageParam pageParam, string Search, long UserId, long StatusId);
        public abstract PagedList<AbstractRequests> GetRequestByDeliveryExecutiveId(PageParam pageParam, string Search, long DeliveryExecutiveId , long StatusId );
        public abstract PagedList<AbstractRequests> RequestByUserId(PageParam pageParam, long UserId);
        public abstract PagedList<AbstractRequests> RequestByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId);
        public abstract SuccessResult<AbstractRequests> Review_ByUser(long Id, decimal RatingByDeliveryExecutive, string ReviewByDeliveryExecutive);
        public abstract SuccessResult<AbstractRequests> Review_ByDeliveryExecutive(long Id, decimal RatingByUser, string ReviewByUser);
        public abstract PagedList<AbstractRequests> Requests_Urgent(PageParam pageParam, long UserId, long DeliveryExecutiveId);
        public abstract PagedList<AbstractRequests> Requests_Attended(PageParam pageParam, string ActionFormDate, string ActionToDate);
        public abstract SuccessResult<AbstractRequestsCount> Requests_ByStatusCount();
        public abstract SuccessResult<AbstractRequestsReachTime> Requests_ReachTime(long Id);
        public abstract SuccessResult<AbstractRequests> Requests_UpdateEstimatedETA(long Id, DateTime EstimatedETA);
        public abstract SuccessResult<AbstractRequests> Update_TipAmount(long Id,bool IsGiveTip,decimal TipAmount);
        public abstract PagedList<AbstractRequests> Report_All(PageParam pageParam, string search, string FromDate, string ToDate, long AgencyId, long DeliveryExecutiveId = 0, long IsSettled = 0);

    }
}
