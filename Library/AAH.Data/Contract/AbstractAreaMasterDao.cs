﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{

    public abstract class AbstractAreaMasterDao : AbstractBaseDao

    {
        public abstract SuccessResult<AbstractAreaMaster> AreaMaster_Upsert(AbstractAreaMaster abstractAreaMaster);
        public abstract SuccessResult<AbstractAreaMaster> AreaMaster_ById(long Id);
        public abstract SuccessResult<AbstractAreaMaster> AreaMaster_ActInAct(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractAreaMaster> AreaMaster_AppUnApp(long Id, long UpdatedBy, int Type = 0);
        public abstract PagedList<AbstractAreaMaster> AreaMaster_All(PageParam pageParam, string Search, long LoginId =0, long Type = 0);
        public abstract PagedList<AbstractAreaMaster> AreaMaster_ByAgencyId(PageParam pageParam, string search,  long AgencyId = 0);
        public abstract SuccessResult<AbstractAreaMaster> AreaMaster_Delete(long Id, int DeletedBy);
    }
}
