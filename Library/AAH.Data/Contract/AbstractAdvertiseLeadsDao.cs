﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractAdvertiseLeadsDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractAdvertiseLeads> AdvertiseLeads_Upsert(AbstractAdvertiseLeads abstractAdvertiseLeads);
        public abstract PagedList<AbstractAdvertiseLeads> AdvertiseLeads_All(PageParam pageParam, string Search);
        public abstract PagedList<AbstractAdvertiseLeads> AdvertiseLeads_ByRequestId(PageParam pageParam, string Search,long AdvertiseId);
    }
}
