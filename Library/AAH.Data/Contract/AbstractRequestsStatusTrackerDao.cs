﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractRequestsStatusTrackerDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractRequestsStatusTracker> RequestsStatusTracker_ByRequestId(PageParam pageParam, string Search, long RequestId);
    }
}
