﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractDeliveryExecutiveHealthDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ById(long Id);
        public abstract SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ByDeliveryExecutiveId(long Id);
        public abstract PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_All(PageParam pageParam, string Search);
        public abstract PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ByUserId(PageParam pageParam, string Search,int UserId,int DeliveryExecutiveId);
        public abstract SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_Upsert(AbstractDeliveryExecutiveHealth abstractDeliveryExecutiveHealth);
    }
}
