﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractDeliveryExecutiveNotificationsDao : AbstractBaseDao

    {
        public abstract SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_Insert(AbstractDeliveryExecutiveNotifications abstractDeliveryExecutiveNotifications);
        public abstract PagedList<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_ByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent);
        public abstract PagedList<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_ByRequestId(PageParam pageParam, long RequestId);
        public abstract SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_RequestStatusUpdate(long Id, long StatusId);
    }
}