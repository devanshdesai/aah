﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractTicketCommentsDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractTicketComments> TicketComments_ById(long Id);
        public abstract SuccessResult<AbstractTicketComments> TicketComments_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractTicketComments> TicketComments_Upsert(AbstractTicketComments abstractTicketComments);
        public abstract SuccessResult<AbstractTicketComments> TicketComments_ActInAct(long Id, long UpdatedBy);
        public abstract PagedList<AbstractTicketComments> TicketComments_All(PageParam pageParam, string Search);
        public abstract PagedList<AbstractTicketComments> TicketComments_ByTicketId(PageParam pageParam, string Search,long TicketId);
    }
}
