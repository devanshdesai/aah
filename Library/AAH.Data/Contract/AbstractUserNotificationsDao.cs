﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractUserNotificationsDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_Insert(AbstractUserNotifications abstractUserNotifications);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_ByRequestId(PageParam pageParam, long RequestId, long UserId);

    }
}