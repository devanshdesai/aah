﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractItemsDao: AbstractBaseDao
    {
        public abstract SuccessResult<AbstractItems> Items_Upsert(AbstractItems abstractItems);
        public abstract SuccessResult<AbstractItems> Items_ById(long Id);
        public abstract SuccessResult<AbstractItems> Items_ActInAct(long Id);
        public abstract SuccessResult<AbstractItems> Items_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractItems> Items_All(PageParam pageParam, string Search);
        public abstract PagedList<AbstractItems> AllItems(PageParam pageParam, string Search);
        public abstract PagedList<AbstractItems> SearchItems_All(PageParam pageParam, string Search,long UserId);
        public abstract PagedList<AbstractItems> VendorsVsItems_AddItemsList(PageParam pageParam, string Search,long VendorsId);
    }
}
