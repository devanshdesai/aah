﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractAgencyVsAreaDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Upsert(AbstractAgencyVsArea abstractAgencyVsArea);

        public abstract SuccessResult<AbstractAgencyVsArea> AfterAgencyVsArea_Complete(long DeliveryExecutiveId ,long AgencyVsAreaId);

        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_ById(int Id);
        public abstract SuccessResult<AbstractAgencyVsArea> AgencyVsArea_ActInAct(int Id, int UpdatedBy);
        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_All(PageParam pageParam, string Search);
        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_byAgencyId(PageParam pageParam, string Search,long AgencyId);
        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_AssignedDE(PageParam pageParam,long AgencyId,long MasterAreaId);
        public abstract SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Delete(int Id, int AreaId);
    }
}
