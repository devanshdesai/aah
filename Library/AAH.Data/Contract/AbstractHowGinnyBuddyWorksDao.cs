﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractHowGinnyBuddyWorksDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_Upsert(AbstractHowGinnyBuddyWorks abstractHowGinnyBuddyWorks);
        public abstract SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_ById(long Id);
        public abstract SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_ActInAct(long Id, int UpdatedBy);
        public abstract PagedList<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_Delete(long Id, int DeletedBy);
    }
}
