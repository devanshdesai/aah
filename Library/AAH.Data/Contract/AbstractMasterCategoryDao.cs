﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractMasterCategoryDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractMasterCategory> MasterCategory_Upsert(AbstractMasterCategory abstractMasterCategory);
        public abstract SuccessResult<AbstractMasterCategory> MasterCategory_ById(long Id);
        public abstract SuccessResult<AbstractMasterCategory> MasterCategory_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractMasterCategory> MasterCategory_All(PageParam pageParam, string Search);
    }
}
