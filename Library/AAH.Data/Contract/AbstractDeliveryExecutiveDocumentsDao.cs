﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractDeliveryExecutiveDocumentsDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Upsert(AbstractDeliveryExecutiveDocuments abstractDeliveryExecutiveDocuments);
        public abstract PagedList<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_ByDeliveryExecutiveId(PageParam pageparam ,string Search, long DeliveryExecutiveId);
        public abstract SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Verify(long Id, long AdminId);
    }
}
