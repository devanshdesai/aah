﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractTicketDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractTicket> Ticket_Upsert(AbstractTicket abstractTicket);
        public abstract SuccessResult<AbstractTicket> Ticket_ById(long Id);
        public abstract SuccessResult<AbstractTicket> Ticket_ActInAct(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractTicket> Ticket_ChangeStatus(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractTicket> Ticket_ChangePlatform(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractTicket> Ticket_ChangeType(long Id, long UpdatedBy);
        public abstract PagedList<AbstractTicket> Ticket_All(PageParam pageParam, string Search);
    }
}
