﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class AbstractDeliveryExecutiveCheckInCheckOutDao : AbstractBaseDao

    {
        public abstract SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_Upsert(AbstractDeliveryExecutiveCheckInCheckOut abstractDeliveryExecutiveCheckInCheckOut);
        public abstract SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_ById(long Id);
        public abstract PagedList<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId, string ActionFormDate, string ActionToDate, long TypeId);
    }
}
