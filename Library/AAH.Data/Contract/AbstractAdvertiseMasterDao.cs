﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractAdvertiseMasterDao: AbstractBaseDao
    {
        public abstract SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_Upsert(AbstractAdvertiseMaster abstractAdvertiseMaster);
        public abstract SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_ById(int Id);
        public abstract SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_ActInAct(int Id, int UpdatedBy);
        public abstract PagedList<AbstractAdvertiseMaster> AdvertiseMaster_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_Delete(int Id, int DeletedBy);

       
    }
}
