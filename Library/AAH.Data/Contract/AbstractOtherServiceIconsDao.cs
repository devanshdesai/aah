﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Data.Contract
{
    public abstract class  AbstractOtherServiceIconsDao: AbstractBaseDao

    {
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Upsert(AbstractOtherServiceIcons abstractOtherServiceIcons);
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ById(long Id);
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ActInAct(long Id, int UpdatedBy);
        public abstract PagedList<AbstractOtherServiceIcons> OtherServiceIcons_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Delete(long Id, int DeletedBy);
    }
}
