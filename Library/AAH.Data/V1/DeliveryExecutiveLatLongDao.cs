﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryExecutiveLatLongDao : AbstractDeliveryExecutiveLatLongDao
    {
        public override PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_ByDeliveryExecutiveId(PageParam pageparam, string Search,
            long DeliveryExecutiveId, string ActionFormDate, string ActionToDate, string ActionFormTime, string ActionToTime)
        {
            PagedList<AbstractDeliveryExecutiveLatLong> Address = new PagedList<AbstractDeliveryExecutiveLatLong>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageparam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageparam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ActionFormDate", ActionFormDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToDate", ActionToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionFormTime", ActionFormTime, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToTime", ActionToTime, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveLatLong_ByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<DeliveryExecutiveLatLong>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        } 
        
        public override PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_ByRequestId(PageParam pageparam, string Search, long RequestId)
        {
            PagedList<AbstractDeliveryExecutiveLatLong> Address = new PagedList<AbstractDeliveryExecutiveLatLong>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageparam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageparam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveLatLong_ByRequestId, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<DeliveryExecutiveLatLong>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }

        public override SuccessResult<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_Insert(string Json)
        {
            SuccessResult<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong = null;
            var param = new DynamicParameters();

            param.Add("@Json", Json, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveLatLong_Insert, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveLatLong = task.Read<SuccessResult<AbstractDeliveryExecutiveLatLong>>().SingleOrDefault();
                DeliveryExecutiveLatLong.Item = task.Read<DeliveryExecutiveLatLong>().SingleOrDefault();
            }

            return DeliveryExecutiveLatLong;
        }


        public override PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_Map()
        {
            PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong = new PagedList<AbstractDeliveryExecutiveLatLong>();

            var param = new DynamicParameters();

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveLatLong_Map, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveLatLong.Values.AddRange(task.Read<DeliveryExecutiveLatLong>());
                DeliveryExecutiveLatLong.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutiveLatLong;
        }



        public override PagedList<AbstractOnboarderLatLong> OnboarderLatLong_ByAdminId(PageParam pageparam, string Search,
            long AdminId, string ActionFormDate, string ActionToDate, string ActionFormTime, string ActionToTime)
        {
            PagedList<AbstractOnboarderLatLong> Address = new PagedList<AbstractOnboarderLatLong>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageparam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageparam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AdminId", AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ActionFormDate", ActionFormDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToDate", ActionToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionFormTime", ActionFormTime, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToTime", ActionToTime, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OnboarderLatLong_ByAdminId, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<OnboarderLatLong>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }

        public override SuccessResult<AbstractOnboarderLatLong> OnboarderLatLong_Insert(string Json)
        {
            SuccessResult<AbstractOnboarderLatLong> DeliveryExecutiveLatLong = null;
            var param = new DynamicParameters();

            param.Add("@Json", Json, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OnboarderLatLong_Insert, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveLatLong = task.Read<SuccessResult<AbstractOnboarderLatLong>>().SingleOrDefault();
                DeliveryExecutiveLatLong.Item = task.Read<OnboarderLatLong>().SingleOrDefault();
            }

            return DeliveryExecutiveLatLong;
        }

    }
}
