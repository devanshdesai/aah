﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AspNetUsersDao_Token
    {
        public SuccessResult<AbstractUsers> AspNetUsers_VerifyUsernameEmail(AbstractUsers abstractAspNetUsers)
        {
            SuccessResult<AbstractUsers> users = null;
            var param = new DynamicParameters();

            param.Add("@MobileNumber", abstractAspNetUsers.MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractAspNetUsers.DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", abstractAspNetUsers.UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", abstractAspNetUsers.IMEI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Otp", abstractAspNetUsers.OTP, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Id", abstractAspNetUsers.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_VerifyOtp, param, commandType: CommandType.StoredProcedure);
                users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                users.Item = task.Read<Users>().SingleOrDefault();
            }

            return users;
        }

        //public SuccessResult<AbstractUsers> AspNetUsers_FailedCountUpsert(DatabaseContext context, AbstractUsers aspNetUsers)
        //{
        //    SuccessResult<AbstractUsers> successResult = new SuccessResult<AbstractUsers>();

        //    //using (var context = new DatabaseContext())
        //    {
        //        if (aspNetUsers.Id != 0)
        //        {
        //            var obj = (from user in context.AspNetUsers
        //                       where user.Id == aspNetUsers.Id
        //                       select user).SingleOrDefault();

        //            context.Entry(obj).State = EntityState.Modified;
        //            obj.AccessFailedCount = aspNetUsers.AccessFailedCount;
        //            obj.LockoutEndDateUtc = aspNetUsers.LockoutEndDateUtc;

        //            successResult.Item = obj;
        //        }

        //        context.SaveChanges();
        //    }

        //    successResult.Code = 200;
        //    successResult.Message = "User Details Saved Successfully";

        //    return successResult;
        //}
    }
}
