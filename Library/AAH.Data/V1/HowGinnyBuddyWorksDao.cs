﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class HowGinnyBuddyWorksDao : AbstractHowGinnyBuddyWorksDao
    {

        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_Upsert(AbstractHowGinnyBuddyWorks AbstractHowGinnyBuddyWorks)
        {
            SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractHowGinnyBuddyWorks.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@URL", AbstractHowGinnyBuddyWorks.URL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Priority", AbstractHowGinnyBuddyWorks.Priority, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractHowGinnyBuddyWorks.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractHowGinnyBuddyWorks.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HowGinnyBuddyWorks_Upsert, param, commandType: CommandType.StoredProcedure);
                HowGinnyBuddyWorks = task.Read<SuccessResult<AbstractHowGinnyBuddyWorks>>().SingleOrDefault();
                HowGinnyBuddyWorks.Item = task.Read<HowGinnyBuddyWorks>().SingleOrDefault();
            }

            return HowGinnyBuddyWorks;
        }

        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_ById(long Id)
        {
            SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HowGinnyBuddyWorks_ById, param, commandType: CommandType.StoredProcedure);
                HowGinnyBuddyWorks = task.Read<SuccessResult<AbstractHowGinnyBuddyWorks>>().SingleOrDefault();
                HowGinnyBuddyWorks.Item = task.Read<HowGinnyBuddyWorks>().SingleOrDefault();
            }

            return HowGinnyBuddyWorks;
        }
        public override PagedList<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_All(PageParam pageParam, string search)
        {
            PagedList<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks = new PagedList<AbstractHowGinnyBuddyWorks>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HowGinnyBuddyWorks_All, param, commandType: CommandType.StoredProcedure);
                HowGinnyBuddyWorks.Values.AddRange(task.Read<HowGinnyBuddyWorks>());
                HowGinnyBuddyWorks.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return HowGinnyBuddyWorks;
        }

        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_ActInAct(long Id, int UpdatedBy)
        {
            SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HowGinnyBuddyWorks_ActInAct, param, commandType: CommandType.StoredProcedure);
                HowGinnyBuddyWorks = task.Read<SuccessResult<AbstractHowGinnyBuddyWorks>>().SingleOrDefault();
                HowGinnyBuddyWorks.Item = task.Read<HowGinnyBuddyWorks>().SingleOrDefault();
            }

            return HowGinnyBuddyWorks;
        }

        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_Delete(long Id, int DeletedBy)
        {
            SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HowGinnyBuddyWorks_Delete, param, commandType: CommandType.StoredProcedure);
                HowGinnyBuddyWorks = task.Read<SuccessResult<AbstractHowGinnyBuddyWorks>>().SingleOrDefault();
                HowGinnyBuddyWorks.Item = task.Read<HowGinnyBuddyWorks>().SingleOrDefault();
            }

            return HowGinnyBuddyWorks;
        }
    }
}
