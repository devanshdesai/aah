﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class TicketDao : AbstractTicketDao
    {

        public override SuccessResult<AbstractTicket> Ticket_Upsert(AbstractTicket AbstractTicket)
        {
            SuccessResult<AbstractTicket> Ticket = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractTicket.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Title", AbstractTicket.Title, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", AbstractTicket.CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractTicket.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", AbstractTicket.RequestId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PlatFormId", AbstractTicket.PlatFormId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId", AbstractTicket.StatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TypeId", AbstractTicket.TypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractTicket.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractTicket.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Ticket_Upsert, param, commandType: CommandType.StoredProcedure);
                Ticket = task.Read<SuccessResult<AbstractTicket>>().SingleOrDefault();
                Ticket.Item = task.Read<Ticket>().SingleOrDefault();
            }

            return Ticket;
        }

        

        public override SuccessResult<AbstractTicket> Ticket_ById(long Id)
        {
            SuccessResult<AbstractTicket> Ticket = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Ticket_ById, param, commandType: CommandType.StoredProcedure);
                Ticket = task.Read<SuccessResult<AbstractTicket>>().SingleOrDefault();
                Ticket.Item = task.Read<Ticket>().SingleOrDefault();
            }

            return Ticket;
        }
        public override PagedList<AbstractTicket> Ticket_All(PageParam pageParam, string search)
        {
            PagedList<AbstractTicket> Ticket = new PagedList<AbstractTicket>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Ticket_All, param, commandType: CommandType.StoredProcedure);
                Ticket.Values.AddRange(task.Read<Ticket>());
                Ticket.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Ticket;
        }

        public override SuccessResult<AbstractTicket> Ticket_ActInAct(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractTicket> Ticket = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Ticket_ActInAct, param, commandType: CommandType.StoredProcedure);
                Ticket = task.Read<SuccessResult<AbstractTicket>>().SingleOrDefault();
                Ticket.Item = task.Read<Ticket>().SingleOrDefault();
            }

            return Ticket;
        }


        public override SuccessResult<AbstractTicket> Ticket_ChangeStatus(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractTicket> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Ticket_ChangeStatus, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractTicket>>().SingleOrDefault();
                Address.Item = task.Read<Ticket>().SingleOrDefault();
            }

            return Address;
        } 
        public override SuccessResult<AbstractTicket> Ticket_ChangePlatform(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractTicket> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Ticket_ChangePlatform, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractTicket>>().SingleOrDefault();
                Address.Item = task.Read<Ticket>().SingleOrDefault();
            }

            return Address;
        } 
        public override SuccessResult<AbstractTicket> Ticket_ChangeType(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractTicket> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Ticket_ChangeType, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractTicket>>().SingleOrDefault();
                Address.Item = task.Read<Ticket>().SingleOrDefault();
            }

            return Address;
        }

    }

}
