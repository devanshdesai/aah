﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AgencyVsAreaDao : AbstractAgencyVsAreaDao
    {

        public override SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Upsert(AbstractAgencyVsArea AbstractAgencyVsArea)
        {
            SuccessResult<AbstractAgencyVsArea> AgencyVsArea = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAgencyVsArea.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AgencyId", AbstractAgencyVsArea.AgencyId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AreaId", AbstractAgencyVsArea.AreaId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Assigned", AbstractAgencyVsArea.Assigned, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAgencyVsArea.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAgencyVsArea.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyVsArea_Upsert, param, commandType: CommandType.StoredProcedure);
                AgencyVsArea = task.Read<SuccessResult<AbstractAgencyVsArea>>().SingleOrDefault();
                AgencyVsArea.Item = task.Read<AgencyVsArea>().SingleOrDefault();
            }

            return AgencyVsArea;
        }

        public override SuccessResult<AbstractAgencyVsArea> AfterAgencyVsArea_Complete(long DeliveryExecutiveId , long AgencyVsAreaId)
        {
            SuccessResult<AbstractAgencyVsArea> AgencyVsArea = null;
            var param = new DynamicParameters();

            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AgencyVsAreaId", AgencyVsAreaId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                //var task = con.QueryMultiple(SQLConfig.AfterAgencyVsArea_Complete, param, commandType: CommandType.StoredProcedure);
                //AgencyVsArea = task.Read<SuccessResult<AbstractAgencyVsArea>>().SingleOrDefault();
                //AgencyVsArea.Item = task.Read<AgencyVsArea>().SingleOrDefault();
            }

            return AgencyVsArea;
        }

       
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_ById(int Id)
        {
            PagedList<AbstractAgencyVsArea> AgencyVsArea = new PagedList<AbstractAgencyVsArea>();

            var param = new DynamicParameters();
            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyVsArea_ById, param, commandType: CommandType.StoredProcedure);
                AgencyVsArea.Values.AddRange(task.Read<AgencyVsArea>());
                AgencyVsArea.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AgencyVsArea;
        }
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAgencyVsArea> AgencyVsArea = new PagedList<AbstractAgencyVsArea>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyVsArea_All, param, commandType: CommandType.StoredProcedure);
                AgencyVsArea.Values.AddRange(task.Read<AgencyVsArea>());
                AgencyVsArea.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AgencyVsArea;
        }
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_byAgencyId(PageParam pageParam, string search,long AgencyId)
        {
            PagedList<AbstractAgencyVsArea> AgencyVsArea = new PagedList<AbstractAgencyVsArea>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AgencyId", AgencyId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyVsArea_byAgencyId, param, commandType: CommandType.StoredProcedure);
                AgencyVsArea.Values.AddRange(task.Read<AgencyVsArea>());
                AgencyVsArea.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AgencyVsArea;
        }
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_AssignedDE(PageParam pageParam, long AgencyId, long MasterAreaId)
        {
            PagedList<AbstractAgencyVsArea> AgencyVsArea = new PagedList<AbstractAgencyVsArea>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AgencyId", AgencyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MasterAreaId", MasterAreaId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyVsArea_AssignedDE, param, commandType: CommandType.StoredProcedure);
                AgencyVsArea.Values.AddRange(task.Read<AgencyVsArea>());
                AgencyVsArea.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AgencyVsArea;
        }

        public override SuccessResult<AbstractAgencyVsArea> AgencyVsArea_ActInAct(int Id, int UpdatedBy)
        {
            SuccessResult<AbstractAgencyVsArea> AgencyVsArea = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyVsArea_ActInAct, param, commandType: CommandType.StoredProcedure);
                AgencyVsArea = task.Read<SuccessResult<AbstractAgencyVsArea>>().SingleOrDefault();
                AgencyVsArea.Item = task.Read<AgencyVsArea>().SingleOrDefault();
            }

            return AgencyVsArea;
        }


        public override SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Delete(int Id, int AreaId)
        {
            SuccessResult<AbstractAgencyVsArea> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AreaId", AreaId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyVsArea_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAgencyVsArea>>().SingleOrDefault();
                Address.Item = task.Read<AgencyVsArea>().SingleOrDefault();
            }

            return Address;
        }

    }

}
