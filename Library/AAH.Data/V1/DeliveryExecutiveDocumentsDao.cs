﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryExecutiveDocumentsDao : AbstractDeliveryExecutiveDocumentsDao
    {
        public override PagedList<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_ByDeliveryExecutiveId(PageParam pageparam, string Search, long DeliveryExecutiveId)
        {
            PagedList<AbstractDeliveryExecutiveDocuments> Address = new PagedList<AbstractDeliveryExecutiveDocuments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageparam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageparam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveDocuments_ByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<DeliveryExecutiveDocuments>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }

        public override SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Upsert(AbstractDeliveryExecutiveDocuments AbstractDeliveryExecutiveDocuments)
        {
            SuccessResult<AbstractDeliveryExecutiveDocuments> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractDeliveryExecutiveDocuments.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", AbstractDeliveryExecutiveDocuments.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminId", AbstractDeliveryExecutiveDocuments.AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DocumentTypeId", AbstractDeliveryExecutiveDocuments.DocumentTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DocumentUrl", AbstractDeliveryExecutiveDocuments.DocumentUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveDocuments_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractDeliveryExecutiveDocuments>>().SingleOrDefault();
                Address.Item = task.Read<DeliveryExecutiveDocuments>().SingleOrDefault();
            }

            return Address;
        }
        public override SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractDeliveryExecutiveDocuments> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveDocuments_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractDeliveryExecutiveDocuments>>().SingleOrDefault();
                Address.Item = task.Read<DeliveryExecutiveDocuments>().SingleOrDefault();
            }

            return Address;
        }
        public override SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Verify(long Id, long AdminId)
        {
            SuccessResult<AbstractDeliveryExecutiveDocuments> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminId", AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveDocuments_Verify, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractDeliveryExecutiveDocuments>>().SingleOrDefault();
                Address.Item = task.Read<DeliveryExecutiveDocuments>().SingleOrDefault();
            }

            return Address;
        }
    }
 }
