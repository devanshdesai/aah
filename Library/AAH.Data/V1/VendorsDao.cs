﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class VendorsDao : AbstractVendorsDao
    {
        public override SuccessResult<AbstractVendors> Vendors_Upsert(AbstractVendors AbstractVendors)
        {
            SuccessResult<AbstractVendors> items = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractVendors.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FirstName", AbstractVendors.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", AbstractVendors.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopName", AbstractVendors.ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GujaratiShopName", AbstractVendors.GujaratiShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@HindiShopName", AbstractVendors.HindiShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PrimaryPhoneNumber", AbstractVendors.PrimaryPhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AlternatePhoneNumber", AbstractVendors.AlternatePhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Location", AbstractVendors.Location, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AadharCardNumber", AbstractVendors.AadharCardNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AadharImage", AbstractVendors.AadharImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ElectionIDNumber", AbstractVendors.ElectionIDNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ElectionIDImage", AbstractVendors.ElectionIDImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ThelaImages", AbstractVendors.ThelaImages, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerProfile", AbstractVendors.ShopOwnerProfile, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Items", AbstractVendors.Items, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Lat", AbstractVendors.Lat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Long", AbstractVendors.Long, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsDeliveryBoy", AbstractVendors.IsDeliveryBoy, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsReadyForCommition", AbstractVendors.IsReadyForCommition, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedLat", AbstractVendors.CreatedLat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedLong", AbstractVendors.CreatedLong, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedLat", AbstractVendors.UpdatedLat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedLong", AbstractVendors.UpdatedLong, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractVendors.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractVendors.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ReferenceCode", AbstractVendors.ReferenceCode, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendors_Upsert, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                items.Item = task.Read<Vendors>().SingleOrDefault();
            }

            return items;
        }

        public override SuccessResult<AbstractVendorsThelaImages> VendorsThelaImages_Upsert(AbstractVendorsThelaImages AbstractVendorsThelaImages)
        {
            SuccessResult<AbstractVendorsThelaImages> VendorsThelaImages = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractVendorsThelaImages.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ImagesUrl", AbstractVendorsThelaImages.ImagesUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsPrimary", AbstractVendorsThelaImages.IsPrimary, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@VendorId", AbstractVendorsThelaImages.VendorId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsThelaImages_Upsert, param, commandType: CommandType.StoredProcedure);
                VendorsThelaImages = task.Read<SuccessResult<AbstractVendorsThelaImages>>().SingleOrDefault();
                VendorsThelaImages.Item = task.Read<VendorsThelaImages>().SingleOrDefault();
            }

            return VendorsThelaImages;
        }

        public override PagedList<AbstractVendors> Vendors_All(PageParam pageParam, string search)
        {
            PagedList<AbstractVendors> items = new PagedList<AbstractVendors>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendors_All, param, commandType: CommandType.StoredProcedure);
                items.Values.AddRange(task.Read<Vendors>());
                items.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return items;
        }

        public override PagedList<AbstractVendors> Vendors_ByAdminId(PageParam pageParam,string search, long AdminId)
        {
            PagedList<AbstractVendors> items = new PagedList<AbstractVendors>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AdminId", AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendors_ByAdminId, param, commandType: CommandType.StoredProcedure);
                items.Values.AddRange(task.Read<Vendors>());
                items.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return items;
        }


        public override SuccessResult<AbstractVendors> Vendors_ById(long Id)
        {
            SuccessResult<AbstractVendors> items = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendors_ById, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                items.Item = task.Read<Vendors>().SingleOrDefault();
            }

            return items;
        }

        public override SuccessResult<AbstractVendors> Vendor_SendOTP(string MobileNumber,string DeviceToken,long OTP)
        {
            SuccessResult<AbstractVendors> Vendor = null;
            var param = new DynamicParameters();

            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OTP", OTP, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendor_SendOTP, param, commandType: CommandType.StoredProcedure);
                Vendor = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                Vendor.Item = task.Read<Vendors>().SingleOrDefault();
            }
            return Vendor;
        }

        public override SuccessResult<AbstractVendors> Vendor_VerifyOtp(long Otp, long Id)
        {
            SuccessResult<AbstractVendors> Vendor = null;
            var param = new DynamicParameters();

            param.Add("@Otp", Otp, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendor_VerifyOtp, param, commandType: CommandType.StoredProcedure);
                Vendor = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                Vendor.Item = task.Read<Vendors>().SingleOrDefault();
            }
            return Vendor;
        }

        public override SuccessResult<AbstractVendors> Vendors_ActInAct(long Id)
        {
            SuccessResult<AbstractVendors> items = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendors_ActInAct, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                items.Item = task.Read<Vendors>().SingleOrDefault();
            }

            return items;
        }

        public override SuccessResult<AbstractVendors> VendorsThelaImages_Delete(string Url, long vendorId)
        {
            SuccessResult<AbstractVendors> items = null;
            var param = new DynamicParameters();

            param.Add("@vendorId", vendorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Url", Url, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsThelaImages_Delete, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                items.Item = task.Read<Vendors>().SingleOrDefault();
            }

            return items;
        }


        public override SuccessResult<AbstractVendors> Vendors_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractVendors> items = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendors_Delete, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                items.Item = task.Read<Vendors>().SingleOrDefault();
            }

            return items;
        }

        // Vendors Vs Items

        public override SuccessResult<AbstractVendorsVsItems> VendorsVsItems_Upsert(AbstractVendorsVsItems abstractVendorsVsItems)
        {
            SuccessResult<AbstractVendorsVsItems> items = null;
            var param = new DynamicParameters();

            param.Add("@VendorsId", abstractVendorsVsItems.VendorsId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ItemsIds", abstractVendorsVsItems.ItemsIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractVendorsVsItems.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsItems_Upsert, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendorsVsItems>>().SingleOrDefault();
                items.Item = task.Read<VendorsVsItems>().SingleOrDefault();
            }

            return items;
        }

        public override SuccessResult<AbstractVendorsVsItems> VendorsVsItems_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractVendorsVsItems> items = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsItems_Delete, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendorsVsItems>>().SingleOrDefault();
                items.Item = task.Read<VendorsVsItems>().SingleOrDefault();
            }

            return items;
        }
         public override SuccessResult<AbstractVendorsVsItems> VendorsVsItems_ActInAct(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractVendorsVsItems> items = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsItems_ActInAct, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendorsVsItems>>().SingleOrDefault();
                items.Item = task.Read<VendorsVsItems>().SingleOrDefault();
            }

            return items;
        }
        public override SuccessResult<AbstractVendors> Vendor_IsOnline(long VendorId, int IsOnline)
        {
            SuccessResult<AbstractVendors> items = null;
            var param = new DynamicParameters();

            param.Add("@VendorId", VendorId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsOnline", IsOnline, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendor_IsOnline, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendors>>().SingleOrDefault();
                items.Item = task.Read<Vendors>().SingleOrDefault();
            }

            return items;
        }

        public override PagedList<AbstractVendorsVsItems> VendorsVsItems_ByVendorsId(PageParam pageParam, string Search, long VendorsId)
        {
            PagedList<AbstractVendorsVsItems> items = new PagedList<AbstractVendorsVsItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@VendorsId", VendorsId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsItems_ByVendorsId, param, commandType: CommandType.StoredProcedure);
                items.Values.AddRange(task.Read<VendorsVsItems>());
                items.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return items;
        }
        //Vendors Vs References
        public override SuccessResult<AbstractVendorsVsReferences> VendorsVsReferences_Upsert(AbstractVendorsVsReferences abstractVendorsVsReferences)
        {
            SuccessResult<AbstractVendorsVsReferences> items = null;
            var param = new DynamicParameters();

            param.Add("@ShopName", abstractVendorsVsReferences.ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerNumber", abstractVendorsVsReferences.ShopOwnerNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@VendorsId", abstractVendorsVsReferences.VendorsId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractVendorsVsReferences.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsReferences_Upsert, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendorsVsReferences>>().SingleOrDefault();
                items.Item = task.Read<VendorsVsReferences>().SingleOrDefault();
            }

            return items;
        }
        public override SuccessResult<AbstractVendorsVsReferences> VendorsVsReferences_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractVendorsVsReferences> items = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsReferences_Delete, param, commandType: CommandType.StoredProcedure);
                items = task.Read<SuccessResult<AbstractVendorsVsReferences>>().SingleOrDefault();
                items.Item = task.Read<VendorsVsReferences>().SingleOrDefault();
            }

            return items;
        }
        public override PagedList<AbstractVendorsVsReferences> VendorsVsReferences_ByVendorsId(PageParam pageParam, string Search, long VendorsId)
        {
            PagedList<AbstractVendorsVsReferences> items = new PagedList<AbstractVendorsVsReferences>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@VendorsId", VendorsId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsReferences_ByVendorsId, param, commandType: CommandType.StoredProcedure);
                items.Values.AddRange(task.Read<VendorsVsReferences>());
                items.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return items;
        }
    }

}
