﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class MasterPayTypeDao : AbstractMasterPayTypeDao
    {

        public override SuccessResult<AbstractMasterPayType> MasterPayType_Upsert(AbstractMasterPayType AbstractMasterPayType)
        {
            SuccessResult<AbstractMasterPayType> MasterPayType = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractMasterPayType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractMasterPayType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractMasterPayType.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OriginalPrice", AbstractMasterPayType.OriginalPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@DiscountPrice", AbstractMasterPayType.DiscountPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractMasterPayType.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractMasterPayType.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPayType_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterPayType = task.Read<SuccessResult<AbstractMasterPayType>>().SingleOrDefault();
                MasterPayType.Item = task.Read<MasterPayType>().SingleOrDefault();
            }

            return MasterPayType;
        }

        public override SuccessResult<AbstractMasterPayType> MasterPayType_ById(long Id)
        {
            SuccessResult<AbstractMasterPayType> MasterPayType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPayType_ById, param, commandType: CommandType.StoredProcedure);
                MasterPayType = task.Read<SuccessResult<AbstractMasterPayType>>().SingleOrDefault();
                MasterPayType.Item = task.Read<MasterPayType>().SingleOrDefault();
            }

            return MasterPayType;
        }
        public override PagedList<AbstractMasterPayType> MasterPayType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterPayType> MasterPayType = new PagedList<AbstractMasterPayType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPayType_All, param, commandType: CommandType.StoredProcedure);
                MasterPayType.Values.AddRange(task.Read<MasterPayType>());
                MasterPayType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterPayType;
        }

        public override SuccessResult<AbstractMasterPayType> MasterPayType_ActInAct(long Id, int UpdatedBy)
        {
            SuccessResult<AbstractMasterPayType> MasterPayType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPayType_ActInAct, param, commandType: CommandType.StoredProcedure);
                MasterPayType = task.Read<SuccessResult<AbstractMasterPayType>>().SingleOrDefault();
                MasterPayType.Item = task.Read<MasterPayType>().SingleOrDefault();
            }

            return MasterPayType;
        }


        public override SuccessResult<AbstractMasterPayType> MasterPayType_Delete(long Id, int DeletedBy)
        {
            SuccessResult<AbstractMasterPayType> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPayType_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractMasterPayType>>().SingleOrDefault();
                Address.Item = task.Read<MasterPayType>().SingleOrDefault();
            }

            return Address;
        }

    }
}
