﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AgencyMasterDao : AbstractAgencyMasterDao
    {

        
        public override SuccessResult<AbstractAgencyMaster> AgencyMaster_Upsert(AbstractAgencyMaster AbstractAgencyMaster)
        {
            SuccessResult<AbstractAgencyMaster> AgencyMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAgencyMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractAgencyMaster.Name, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                AgencyMaster = task.Read<SuccessResult<AbstractAgencyMaster>>().SingleOrDefault();
                AgencyMaster.Item = task.Read<AgencyMaster>().SingleOrDefault();
            }

            return AgencyMaster;
        }

        public override SuccessResult<AbstractAgencyMaster> AgencyMaster_ById(long Id)
        {
            SuccessResult<AbstractAgencyMaster> AgencyMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyMaster_ById, param, commandType: CommandType.StoredProcedure);
                AgencyMaster = task.Read<SuccessResult<AbstractAgencyMaster>>().SingleOrDefault();
                AgencyMaster.Item = task.Read<AgencyMaster>().SingleOrDefault();
            }

            return AgencyMaster;
        }
        public override PagedList<AbstractAgencyMaster> AgencyMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAgencyMaster> AgencyMaster = new PagedList<AbstractAgencyMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AgencyMaster_All, param, commandType: CommandType.StoredProcedure);
                AgencyMaster.Values.AddRange(task.Read<AgencyMaster>());
                AgencyMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AgencyMaster;
        }

       
    }
}
