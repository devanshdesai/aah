﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class CustomersNotificationDao : AbstractCustomersNotificationDao
    {
        public override SuccessResult<AbstractCustomersNotification> CustomersNotification_Upsert(AbstractCustomersNotification abstractCustomersNotification)
        {
            SuccessResult<AbstractCustomersNotification> CustomersNotification = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCustomersNotification.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Description", abstractCustomersNotification.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCustomersNotification.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomersNotification_Upsert, param, commandType: CommandType.StoredProcedure);
                CustomersNotification = task.Read<SuccessResult<AbstractCustomersNotification>>().SingleOrDefault();
                CustomersNotification.Item = task.Read<CustomersNotification>().SingleOrDefault();
            }

            return CustomersNotification;
        }
    }
}
