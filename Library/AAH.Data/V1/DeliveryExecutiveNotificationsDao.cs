﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryExecutiveNotificationsDao : AbstractDeliveryExecutiveNotificationsDao
    {


        public override SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_Insert(AbstractDeliveryExecutiveNotifications abstractDeliveryExecutiveNotifications)
        {
            SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDeliveryExecutiveNotifications.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", abstractDeliveryExecutiveNotifications.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", abstractDeliveryExecutiveNotifications.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Description", abstractDeliveryExecutiveNotifications.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDeliveryExecutiveNotifications.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveNotifications_Insert, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveNotifications = task.Read<SuccessResult<AbstractDeliveryExecutiveNotifications>>().SingleOrDefault();
                DeliveryExecutiveNotifications.Item = task.Read<DeliveryExecutiveNotifications>().SingleOrDefault();
            }

            return DeliveryExecutiveNotifications;
        }

        public override PagedList<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_ByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent)
        {
            PagedList<AbstractDeliveryExecutiveNotifications> Users = new PagedList<AbstractDeliveryExecutiveNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ActionFormDate", ActionFormDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToDate", ActionToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsSent", IsSent, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveNotifications_ByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<DeliveryExecutiveNotifications>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }

        public override PagedList<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_ByRequestId(PageParam pageParam, long RequestId)
        {
            PagedList<AbstractDeliveryExecutiveNotifications> Users = new PagedList<AbstractDeliveryExecutiveNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveNotifications_ByRequestId, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<DeliveryExecutiveNotifications>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }

        public override SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_RequestStatusUpdate(long Id, long StatusId)
        {
            SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveNotifications_RequestStatusUpdate, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveNotifications = task.Read<SuccessResult<AbstractDeliveryExecutiveNotifications>>().SingleOrDefault();
                DeliveryExecutiveNotifications.Item = task.Read<DeliveryExecutiveNotifications>().SingleOrDefault();
            }

            return DeliveryExecutiveNotifications;
        }
    }
}
