﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryExecutiveCheckInCheckOutDao : AbstractDeliveryExecutiveCheckInCheckOutDao
    {
        public override PagedList<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId
            (PageParam pageParam, long DeliveryExecutiveId, string ActionFormDate, string ActionToDate, long TypeId)
        {
            PagedList<AbstractDeliveryExecutiveCheckInCheckOut> Address = new PagedList<AbstractDeliveryExecutiveCheckInCheckOut>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ActionFormDate", ActionFormDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToDate", ActionToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TypeId", TypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<DeliveryExecutiveCheckInCheckOut>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }

        public override SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_ById(long Id)
        {
            SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveCheckInCheckOut_ById, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveCheckInCheckOut = task.Read<SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut>>().SingleOrDefault();
                DeliveryExecutiveCheckInCheckOut.Item = task.Read<DeliveryExecutiveCheckInCheckOut>().SingleOrDefault();
            }

            return DeliveryExecutiveCheckInCheckOut;
        }
        public override SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_Upsert(AbstractDeliveryExecutiveCheckInCheckOut AbstractDeliveryExecutiveCheckInCheckOut)
        {
            SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractDeliveryExecutiveCheckInCheckOut.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", AbstractDeliveryExecutiveCheckInCheckOut.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminId", AbstractDeliveryExecutiveCheckInCheckOut.AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TypeId", AbstractDeliveryExecutiveCheckInCheckOut.TypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsAdministration", AbstractDeliveryExecutiveCheckInCheckOut.IsAdministration, dbType: DbType.Boolean, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveCheckInCheckOut_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut>>().SingleOrDefault();
                Address.Item = task.Read<DeliveryExecutiveCheckInCheckOut>().SingleOrDefault();
            }

            return Address;
        }
    }
}
