﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class RequestsDelieveryExecutiveTrackerDao : AbstractRequestsDelieveryExecutiveTrackerDao
    {
        public override PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId(PageParam pageParam, string Search, long DeliveryExecutiveId)
        {
            PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker = new PagedList<AbstractRequestsDelieveryExecutiveTracker>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                RequestsDelieveryExecutiveTracker.Values.AddRange(task.Read<RequestsDelieveryExecutiveTracker>());
                RequestsDelieveryExecutiveTracker.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return RequestsDelieveryExecutiveTracker;
        }

        public override PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker_ByRequestId(PageParam pageParam, string Search, long RequestId)
        {
            PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker = new PagedList<AbstractRequestsDelieveryExecutiveTracker>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestsDelieveryExecutiveTracker_ByRequestId, param, commandType: CommandType.StoredProcedure);
                RequestsDelieveryExecutiveTracker.Values.AddRange(task.Read<RequestsDelieveryExecutiveTracker>());
                RequestsDelieveryExecutiveTracker.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return RequestsDelieveryExecutiveTracker;
        }
    }
}
