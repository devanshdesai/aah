﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryLimitDao : AbstractDeliveryLimitDao
    {

        public override SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Upsert(AbstractDeliveryLimit AbstractDeliveryLimit)
        {
            SuccessResult<AbstractDeliveryLimit> DeliveryLimit = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractDeliveryLimit.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Title", AbstractDeliveryLimit.Title, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractDeliveryLimit.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageURL", AbstractDeliveryLimit.ImageURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractDeliveryLimit.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractDeliveryLimit.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryLimit_Upsert, param, commandType: CommandType.StoredProcedure);
                DeliveryLimit = task.Read<SuccessResult<AbstractDeliveryLimit>>().SingleOrDefault();
                DeliveryLimit.Item = task.Read<DeliveryLimit>().SingleOrDefault();
            }

            return DeliveryLimit;
        }

        public override SuccessResult<AbstractDeliveryLimit> DeliveryLimit_ById(long Id)
        {
            SuccessResult<AbstractDeliveryLimit> DeliveryLimit = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryLimit_ById, param, commandType: CommandType.StoredProcedure);
                DeliveryLimit = task.Read<SuccessResult<AbstractDeliveryLimit>>().SingleOrDefault();
                DeliveryLimit.Item = task.Read<DeliveryLimit>().SingleOrDefault();
            }

            return DeliveryLimit;
        }
        public override PagedList<AbstractDeliveryLimit> DeliveryLimit_All(PageParam pageParam, string search)
        {
            PagedList<AbstractDeliveryLimit> DeliveryLimit = new PagedList<AbstractDeliveryLimit>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryLimit_All, param, commandType: CommandType.StoredProcedure);
                DeliveryLimit.Values.AddRange(task.Read<DeliveryLimit>());
                DeliveryLimit.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryLimit;
        }


        public override SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Delete(long Id, int DeletedBy)
        {
            SuccessResult<AbstractDeliveryLimit> DeliveryLimit = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryLimit_Delete, param, commandType: CommandType.StoredProcedure);
                DeliveryLimit = task.Read<SuccessResult<AbstractDeliveryLimit>>().SingleOrDefault();
                DeliveryLimit.Item = task.Read<DeliveryLimit>().SingleOrDefault();
            }

            return DeliveryLimit;
        }
    }
}
