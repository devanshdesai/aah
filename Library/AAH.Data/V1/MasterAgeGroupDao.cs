﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class MasterAgeGroupDao : AbstractMasterAgeGroupDao
    {

        public override SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_Upsert(AbstractMasterAgeGroup AbstractMasterAgeGroup)
        {
            SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractMasterAgeGroup.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractMasterAgeGroup.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DisplayName", AbstractMasterAgeGroup.DisplayName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractMasterAgeGroup.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractMasterAgeGroup.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterAgeGroup_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterAgeGroup = task.Read<SuccessResult<AbstractMasterAgeGroup>>().SingleOrDefault();
                MasterAgeGroup.Item = task.Read<MasterAgeGroup>().SingleOrDefault();
            }

            return MasterAgeGroup;
        }

        public override SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_ById(long Id)
        {
            SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterAgeGroup_ById, param, commandType: CommandType.StoredProcedure);
                MasterAgeGroup = task.Read<SuccessResult<AbstractMasterAgeGroup>>().SingleOrDefault();
                MasterAgeGroup.Item = task.Read<MasterAgeGroup>().SingleOrDefault();
            }

            return MasterAgeGroup;
        }
        public override PagedList<AbstractMasterAgeGroup> MasterAgeGroup_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterAgeGroup> MasterAgeGroup = new PagedList<AbstractMasterAgeGroup>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterAgeGroup_All, param, commandType: CommandType.StoredProcedure);
                MasterAgeGroup.Values.AddRange(task.Read<MasterAgeGroup>());
                MasterAgeGroup.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterAgeGroup;
        }


        public override SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_Delete(long Id, int DeletedBy)
        {
            SuccessResult<AbstractMasterAgeGroup> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterAgeGroup_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractMasterAgeGroup>>().SingleOrDefault();
                Address.Item = task.Read<MasterAgeGroup>().SingleOrDefault();
            }

            return Address;
        }

    }
}
