﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class RequestsStatusTrackerDao : AbstractRequestsStatusTrackerDao
    {
        public override PagedList<AbstractRequestsStatusTracker> RequestsStatusTracker_ByRequestId(PageParam pageParam, string Search, long RequestId)
        {
            PagedList<AbstractRequestsStatusTracker> Users = new PagedList<AbstractRequestsStatusTracker>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestsStatusTracker_ByRequestId, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<RequestsStatusTracker>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }
    }
}
