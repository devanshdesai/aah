﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AdvertisePreferredCategoryDao : AbstractAdvertisePreferredCategoryDao
    {

       
        public override SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Upsert(AbstractAdvertisePreferredCategory AbstractAdvertisePreferredCategory)
        {
            SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAdvertisePreferredCategory.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CategoryId", AbstractAdvertisePreferredCategory.CategoryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AdvertiseMasterId", AbstractAdvertisePreferredCategory.AdvertiseMasterId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAdvertisePreferredCategory.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAdvertisePreferredCategory.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertisePreferredCategory_Upsert, param, commandType: CommandType.StoredProcedure);
                AdvertisePreferredCategory = task.Read<SuccessResult<AbstractAdvertisePreferredCategory>>().SingleOrDefault();
                AdvertisePreferredCategory.Item = task.Read<AdvertisePreferredCategory>().SingleOrDefault();
            }

            return AdvertisePreferredCategory;
        }


        public override SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_ById(int Id)
        {
            SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertisePreferredCategory_ById, param, commandType: CommandType.StoredProcedure);
                AdvertisePreferredCategory = task.Read<SuccessResult<AbstractAdvertisePreferredCategory>>().SingleOrDefault();
                AdvertisePreferredCategory.Item = task.Read<AdvertisePreferredCategory>().SingleOrDefault();
            }

            return AdvertisePreferredCategory;
        }
        public override PagedList<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory = new PagedList<AbstractAdvertisePreferredCategory>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertisePreferredCategory_All, param, commandType: CommandType.StoredProcedure);
                AdvertisePreferredCategory.Values.AddRange(task.Read<AdvertisePreferredCategory>());
                AdvertisePreferredCategory.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdvertisePreferredCategory;
        }


        public override SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Delete(int Id, int DeletedBy)
        {
            SuccessResult<AbstractAdvertisePreferredCategory> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertisePreferredCategory_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAdvertisePreferredCategory>>().SingleOrDefault();
                Address.Item = task.Read<AdvertisePreferredCategory>().SingleOrDefault();
            }

            return Address;
        }

       

    }
}
