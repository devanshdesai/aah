﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AddressTypeDao : AbstractAddressTypeDao
    {
        public override SuccessResult<AbstractAddressType> AddressType_Upsert(AbstractAddressType AbstractAddressType)
        {
            SuccessResult<AbstractAddressType> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAddressType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractAddressType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAddressType.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAddressType.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddressType_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAddressType>>().SingleOrDefault();
                Address.Item = task.Read<AddressType>().SingleOrDefault();
            }

            return Address;
        }

        public override PagedList<AbstractAddressType> AddressType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAddressType> Address = new PagedList<AbstractAddressType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddressType_All, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<AddressType>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }


        public override SuccessResult<AbstractAddressType> AddressType_ById(long Id)
        {
            SuccessResult<AbstractAddressType> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddressType_ById, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAddressType>>().SingleOrDefault();
                Address.Item = task.Read<AddressType>().SingleOrDefault();
            }

            return Address;
        }


        public override SuccessResult<AbstractAddressType> AddressType_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractAddressType> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddressType_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAddressType>>().SingleOrDefault();
                Address.Item = task.Read<AddressType>().SingleOrDefault();
            }

            return Address;
        }
    }

}
