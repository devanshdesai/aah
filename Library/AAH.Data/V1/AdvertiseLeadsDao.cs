﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AdvertiseLeadsDao : AbstractAdvertiseLeadsDao
    {

       
        public override SuccessResult<AbstractAdvertiseLeads> AdvertiseLeads_Upsert(AbstractAdvertiseLeads AbstractAdvertiseLeads)
        {
            SuccessResult<AbstractAdvertiseLeads> AdvertiseLeads = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAdvertiseLeads.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdvertiseId", AbstractAdvertiseLeads.AdvertiseId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", AbstractAdvertiseLeads.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", AbstractAdvertiseLeads.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", AbstractAdvertiseLeads.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsSoftLeads", AbstractAdvertiseLeads.IsSoftLeads, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsConvertToHardLeads", AbstractAdvertiseLeads.IsConvertToHardLeads, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAdvertiseLeads.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAdvertiseLeads.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseLeads_Upsert, param, commandType: CommandType.StoredProcedure);
                AdvertiseLeads = task.Read<SuccessResult<AbstractAdvertiseLeads>>().SingleOrDefault();
                AdvertiseLeads.Item = task.Read<AdvertiseLeads>().SingleOrDefault();
            }

            return AdvertiseLeads;
        }

        
        public override PagedList<AbstractAdvertiseLeads> AdvertiseLeads_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAdvertiseLeads> AdvertiseLeads = new PagedList<AbstractAdvertiseLeads>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseLeads_All, param, commandType: CommandType.StoredProcedure);
                AdvertiseLeads.Values.AddRange(task.Read<AdvertiseLeads>());
                AdvertiseLeads.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdvertiseLeads;
        } 
        
        public override PagedList<AbstractAdvertiseLeads> AdvertiseLeads_ByRequestId(PageParam pageParam, string search,long AdvertiseId)
        {
            PagedList<AbstractAdvertiseLeads> AdvertiseLeads = new PagedList<AbstractAdvertiseLeads>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AdvertiseId", AdvertiseId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseLeads_ByRequestId, param, commandType: CommandType.StoredProcedure);
                AdvertiseLeads.Values.AddRange(task.Read<AdvertiseLeads>());
                AdvertiseLeads.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdvertiseLeads;
        }

      
    }
}
