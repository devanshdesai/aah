﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class MasterCategoryDao : AbstractMasterCategoryDao
    {
        public override SuccessResult<AbstractMasterCategory> MasterCategory_Upsert(AbstractMasterCategory AbstractMasterCategory)
        {
            SuccessResult<AbstractMasterCategory> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractMasterCategory.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractMasterCategory.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractMasterCategory.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractMasterCategory.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCategory_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractMasterCategory>>().SingleOrDefault();
                Address.Item = task.Read<MasterCategory>().SingleOrDefault();
            }

            return Address;
        }

        public override PagedList<AbstractMasterCategory> MasterCategory_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterCategory> Address = new PagedList<AbstractMasterCategory>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCategory_All, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<MasterCategory>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }


        public override SuccessResult<AbstractMasterCategory> MasterCategory_ById(long Id)
        {
            SuccessResult<AbstractMasterCategory> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCategory_ById, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractMasterCategory>>().SingleOrDefault();
                Address.Item = task.Read<MasterCategory>().SingleOrDefault();
            }

            return Address;
        }


        public override SuccessResult<AbstractMasterCategory> MasterCategory_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractMasterCategory> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCategory_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractMasterCategory>>().SingleOrDefault();
                Address.Item = task.Read<MasterCategory>().SingleOrDefault();
            }

            return Address;
        }
    }

}
