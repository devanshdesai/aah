﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class UsersLatLongDao : AbstractUsersLatLongDao
    {
       
        public override PagedList<AbstractUsersLatLong> UsersLatLong_ByUserId(PageParam pageParam, string search, long UserId)
        {
            PagedList<AbstractUsersLatLong> UsersLatLong = new PagedList<AbstractUsersLatLong>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersLatLong_ByUserId, param, commandType: CommandType.StoredProcedure);
                UsersLatLong.Values.AddRange(task.Read<UsersLatLong>());
                UsersLatLong.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UsersLatLong;
        }public override PagedList<AbstractUsersLatLong> UsersLatLong_ByRequestId(PageParam pageParam, string search, long RequestId)
        {
            PagedList<AbstractUsersLatLong> UsersLatLong = new PagedList<AbstractUsersLatLong>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersLatLong_ByRequestId, param, commandType: CommandType.StoredProcedure);
                UsersLatLong.Values.AddRange(task.Read<UsersLatLong>());
                UsersLatLong.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UsersLatLong;
        }
        
        public override SuccessResult<AbstractUsersLatLong> UsersLatLong_Insert(string Json)
        {
            SuccessResult<AbstractUsersLatLong> UsersLatLong = null;
            var param = new DynamicParameters();

            param.Add("@Json", Json, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersLatLong_Insert, param, commandType: CommandType.StoredProcedure);
                UsersLatLong = task.Read<SuccessResult<AbstractUsersLatLong>>().SingleOrDefault();
                UsersLatLong.Item = task.Read<AbstractUsersLatLong>().SingleOrDefault();
            }

            return UsersLatLong;
        }

    }
}
