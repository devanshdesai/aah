﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class SurveyDao : AbstractSurveyDao
    {

        
        public override SuccessResult<AbstractSurvey> Survey_Insert(AbstractSurvey AbstractSurvey)
        {
            SuccessResult<AbstractSurvey> Survey = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractSurvey.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", AbstractSurvey.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AgeGroup", AbstractSurvey.AgeGroup, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Pincode", AbstractSurvey.Pincode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NoOfFamilyMember", AbstractSurvey.NoOfFamilyMember, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Username", AbstractSurvey.Username, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@HomeTown", AbstractSurvey.HomeTown, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Country", AbstractSurvey.Country, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@State", AbstractSurvey.State, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", AbstractSurvey.City, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Area", AbstractSurvey.Area, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractSurvey.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractSurvey.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Survey_Insert, param, commandType: CommandType.StoredProcedure);
                Survey = task.Read<SuccessResult<AbstractSurvey>>().SingleOrDefault();
                Survey.Item = task.Read<Survey>().SingleOrDefault();
            }

            return Survey;
        }

        
        public override PagedList<AbstractSurvey> Survey_All(PageParam pageParam, string search)
        {
            PagedList<AbstractSurvey> Survey = new PagedList<AbstractSurvey>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Survey_All, param, commandType: CommandType.StoredProcedure);
                Survey.Values.AddRange(task.Read<Survey>());
                Survey.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Survey;
        }

        
    }
}
