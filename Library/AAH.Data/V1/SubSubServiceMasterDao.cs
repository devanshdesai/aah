﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class SubSubServiceMasterDao : AbstractSubSubServiceMasterDao
    {
        public override SuccessResult<AbstractSubSubServiceMaster> SubSubServiceMaster_Upsert(AbstractSubSubServiceMaster AbstractSubSubServiceMaster)
        {
            SuccessResult<AbstractSubSubServiceMaster> SubService = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractSubSubServiceMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubServiceMasterId", AbstractSubSubServiceMaster.SubServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubSubServiceName", AbstractSubSubServiceMaster.SubSubServiceName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Icon", AbstractSubSubServiceMaster.Icon, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Link", AbstractSubSubServiceMaster.Link, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractSubSubServiceMaster.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractSubSubServiceMaster.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractSubSubServiceMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubSubServiceMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                SubService = task.Read<SuccessResult<AbstractSubSubServiceMaster>>().SingleOrDefault();
                SubService.Item = task.Read<SubSubServiceMaster>().SingleOrDefault();
            }

            return SubService;
        }

         public override SuccessResult<AbstractSubSubServiceMaster> SubSubServiceMaster_ById(long Id)
        {
            SuccessResult<AbstractSubSubServiceMaster> SubSubServiceMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubSubServiceMaster_ById, param, commandType: CommandType.StoredProcedure);
                SubSubServiceMaster = task.Read<SuccessResult<AbstractSubSubServiceMaster>>().SingleOrDefault();
                SubSubServiceMaster.Item = task.Read<SubSubServiceMaster>().SingleOrDefault();
            }

            return SubSubServiceMaster;
        }
        public override PagedList<AbstractSubSubServiceMaster> SubSubServiceMaster_BySubServiceMasterId(PageParam pageParam, string search, long SubServiceMasterId)
        {
            PagedList<AbstractSubSubServiceMaster> SubService = new PagedList<AbstractSubSubServiceMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SubServiceMasterId", SubServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubSubServiceMaster_BySubServiceMasterId, param, commandType: CommandType.StoredProcedure);
                SubService.Values.AddRange(task.Read<SubSubServiceMaster>());
                SubService.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return SubService;
        }

   
    }
}
