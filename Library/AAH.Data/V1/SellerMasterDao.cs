﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class SellerMasterDao : AbstractSellerMasterDao
    {

        public override SuccessResult<AbstractSellerMaster> SellerMaster_Upsert(AbstractSellerMaster AbstractSellerMaster)
        {
            SuccessResult<AbstractSellerMaster> SellerMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractSellerMaster.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RequestId", AbstractSellerMaster.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractSellerMaster.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Lat", AbstractSellerMaster.Lat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Long", AbstractSellerMaster.Long, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PicUrl", AbstractSellerMaster.PicUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OwnerName", AbstractSellerMaster.OwnerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ContactNumber", AbstractSellerMaster.ContactNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractSellerMaster.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractSellerMaster.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SellerMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                SellerMaster = task.Read<SuccessResult<AbstractSellerMaster>>().SingleOrDefault();
                SellerMaster.Item = task.Read<SellerMaster>().SingleOrDefault();
            }

            return SellerMaster;
        }

    }
}
