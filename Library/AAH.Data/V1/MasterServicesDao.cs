﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class MasterServicesDao : AbstractMasterServicesDao
    {
        public override SuccessResult<AbstractMasterServices> MasterServices_Upsert(AbstractMasterServices abstractMasterServices)
        {
            SuccessResult<AbstractMasterServices> masterServices = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterServices.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceNameEnglish", abstractMasterServices.ServiceNameEnglish, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceNameGujarati", abstractMasterServices.ServiceNameGujarati, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceNameHindi", abstractMasterServices.ServiceNameHindi, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractMasterServices.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractMasterServices.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServices_Upsert, param, commandType: CommandType.StoredProcedure);
                masterServices = task.Read<SuccessResult<AbstractMasterServices>>().SingleOrDefault();
                masterServices.Item = task.Read<MasterServices>().SingleOrDefault();
            }

            return masterServices;
        }

        public override PagedList<AbstractMasterServices> MasterServices_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterServices> masterServices = new PagedList<AbstractMasterServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServices_All, param, commandType: CommandType.StoredProcedure);
                masterServices.Values.AddRange(task.Read<MasterServices>());
                masterServices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return masterServices;
        }


        public override SuccessResult<AbstractMasterServices> MasterServices_ById(long Id)
        {
            SuccessResult<AbstractMasterServices> masterServices = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServices_ById, param, commandType: CommandType.StoredProcedure);
                masterServices = task.Read<SuccessResult<AbstractMasterServices>>().SingleOrDefault();
                masterServices.Item = task.Read<MasterServices>().SingleOrDefault();
            }

            return masterServices;
        }


        public override SuccessResult<AbstractMasterServices> MasterServices_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractMasterServices> masterServices = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServices_Delete, param, commandType: CommandType.StoredProcedure);
                masterServices = task.Read<SuccessResult<AbstractMasterServices>>().SingleOrDefault();
                masterServices.Item = task.Read<MasterServices>().SingleOrDefault();
            }

            return masterServices;
        }
    }

}
