﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AdminDao : AbstractAdminDao
    {

        public override bool Admin_SignOut(long Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Admin_SignOut, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }
            return result;

        }
        public override SuccessResult<AbstractAdmin> Admin_SignIn(string Email, string Password)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_SignIn, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
        public override SuccessResult<AbstractAdmin> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OldPassword", OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
        public override SuccessResult<AbstractAdmin> Admin_Upsert(AbstractAdmin AbstractAdmin)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAdmin.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractAdmin.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", AbstractAdmin.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", AbstractAdmin.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", AbstractAdmin.MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Gender", AbstractAdmin.Gender, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AdminTypeId", AbstractAdmin.AdminTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAdmin.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAdmin.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AreaMasterId", AbstractAdmin.AreaMasterId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GSTNumber", AbstractAdmin.GSTNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressProof", AbstractAdmin.AddressProof, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OwnerName", AbstractAdmin.OwnerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PANNumber", AbstractAdmin.PANNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PANNumberImage", AbstractAdmin.PANNumberImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ManagerName", AbstractAdmin.ManagerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ManagerContactNumber", AbstractAdmin.ManagerContactNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Target", AbstractAdmin.Target, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Salary", AbstractAdmin.Salary, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Penalty", AbstractAdmin.Penalty, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Commision", AbstractAdmin.Commision, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NoOfDE", AbstractAdmin.NoOfDE, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceCharge", AbstractAdmin.ServiceCharge, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ThelaValaCharge", AbstractAdmin.ThelaValaCharge, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdCharge", AbstractAdmin.AdCharge, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CommitmentAmtMP", AbstractAdmin.CommitmentAmtMP, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CommitmentAgencyDow", AbstractAdmin.CommitmentAgencyDow, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CommitmentAgencyServ", AbstractAdmin.CommitmentAgencyServ, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@AreaId", AbstractAdmin.AreaId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Upsert, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }

        public override SuccessResult<AbstractAdmin> Admin_ById(long Id)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ById, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
        public override SuccessResult<AbstractAdmin> Admin_Dashboard(long Id)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Dashboard, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
        public override PagedList<AbstractAdmin> Admin_All(PageParam pageParam, string Search, string Name, string Email, string MobileNumber, long AdminTypeId, long IsActive)
        {
            PagedList<AbstractAdmin> Admin = new PagedList<AbstractAdmin>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Name", Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AdminTypeId", AdminTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_All, param, commandType: CommandType.StoredProcedure);
                Admin.Values.AddRange(task.Read<Admin>());
                Admin.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Admin;
        }
        public override PagedList<AbstractAdmin> DownloadingStaff_TodayTarget(PageParam pageParam, string Fromdate, string Todate, long Id)
        {
            PagedList<AbstractAdmin> Admin = new PagedList<AbstractAdmin>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Fromdate", Fromdate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Todate", Todate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DownloadingStaff_TodayTarget, param, commandType: CommandType.StoredProcedure);
                Admin.Values.AddRange(task.Read<Admin>());
                Admin.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Admin;
        }
        public override PagedList<AbstractAdmin> OnBoardingStaff_TodayTarget(PageParam pageParam, string Fromdate, string Todate, long Id)
        {
            PagedList<AbstractAdmin> Admin = new PagedList<AbstractAdmin>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Fromdate", Fromdate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Todate", Todate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OnBoardingStaff_TodayTarget, param, commandType: CommandType.StoredProcedure);
                Admin.Values.AddRange(task.Read<Admin>());
                Admin.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Admin;
        }
        public override SuccessResult<AbstractAdmin> Admin_ActInAct(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ActInAct, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }

        // Admin Type

        public override PagedList<AbstractAdminType> AdminType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAdminType> AdminType = new PagedList<AbstractAdminType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminType_All, param, commandType: CommandType.StoredProcedure);
                AdminType.Values.AddRange(task.Read<AdminType>());
                AdminType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminType;
        }

        public override PagedList<AbstractAreaVsStaff> Vendor_ValidArea(PageParam pageParam, string Search, long StaffId)
        {
            PagedList<AbstractAreaVsStaff> AdminType = new PagedList<AbstractAreaVsStaff>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@StaffId", StaffId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Vendor_ValidArea, param, commandType: CommandType.StoredProcedure);
                AdminType.Values.AddRange(task.Read<AreaVsStaff>());
                AdminType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminType;
        }
        public override PagedList<AbstractAdmin> GetAgency_ByAreaId(PageParam pageParam, string Search, string AreaId)
        {
            PagedList<AbstractAdmin> AdminType = new PagedList<AbstractAdmin>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AreaId", AreaId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetAgency_ByAreaId, param, commandType: CommandType.StoredProcedure);
                AdminType.Values.AddRange(task.Read<Admin>());
                AdminType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminType;
        }
        public override PagedList<AbstractAdmin> GetAgency_ByIds(string Ids)
        {
            PagedList<AbstractAdmin> AdminType = new PagedList<AbstractAdmin>();

            var param = new DynamicParameters();
            param.Add("@AgencyId", Ids, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetAgency_ByIds, param, commandType: CommandType.StoredProcedure);
                AdminType.Values.AddRange(task.Read<Admin>());
                AdminType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminType;
        }

        public override SuccessResult<AbstractAdmin> GetAvailableDE_ByAgencyId(string Id, long AreaId = 0)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AreaId", AreaId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetAvailableDE_ByAgencyId, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
        public override SuccessResult<AbstractAdmin> AreaVsStaff_ById(long Id)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaVsStaff_ById, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
       
        public override PagedList<AbstractAdmin> AreaVsStaff_All(PageParam pageParam, string Search, long StaffId)
        {
            PagedList<AbstractAdmin> AdminType = new PagedList<AbstractAdmin>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@StaffId", StaffId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaVsStaff_All, param, commandType: CommandType.StoredProcedure);
                AdminType.Values.AddRange(task.Read<Admin>());
                AdminType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminType;
        }
        public override SuccessResult<AbstractAdmin> AreaVsStaff_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaVsStaff_Delete, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
        public override SuccessResult<AbstractAdmin> AreaVsStaff_Upsert(AbstractAreaVsStaff AbstractAreaVsStaff)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAreaVsStaff.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AreaId", AbstractAreaVsStaff.AreaId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@StaffId", AbstractAreaVsStaff.StaffId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAreaVsStaff.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAreaVsStaff.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaVsStaff_Upsert, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }

    }
}
