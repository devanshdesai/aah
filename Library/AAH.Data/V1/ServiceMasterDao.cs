﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class ServiceMasterDao : AbstractServiceMasterDao
    {
        public override SuccessResult<AbstractServiceMaster> ServiceMaster_Upsert(AbstractServiceMaster AbstractServiceMaster)
        {
            SuccessResult<AbstractServiceMaster> Service = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractServiceMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceName", AbstractServiceMaster.ServiceName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Icon", AbstractServiceMaster.Icon, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractServiceMaster.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractServiceMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                Service = task.Read<SuccessResult<AbstractServiceMaster>>().SingleOrDefault();
                Service.Item = task.Read<ServiceMaster>().SingleOrDefault();
            }

            return Service;
        }

        public override SuccessResult<AbstractServiceMaster> ServiceMaster_ById(long Id)
        {
            SuccessResult<AbstractServiceMaster> ServiceMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceMaster_ById, param, commandType: CommandType.StoredProcedure);
                ServiceMaster = task.Read<SuccessResult<AbstractServiceMaster>>().SingleOrDefault();
                ServiceMaster.Item = task.Read<ServiceMaster>().SingleOrDefault();
            }

            return ServiceMaster;
        }
        public override PagedList<AbstractServiceMaster> ServiceMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractServiceMaster> Service = new PagedList<AbstractServiceMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceMaster_All, param, commandType: CommandType.StoredProcedure);
                Service.Values.AddRange(task.Read<ServiceMaster>());
                Service.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Service;
        }

        public override SuccessResult<AbstractServiceMaster> ServiceMaster_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractServiceMaster> Service = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceMaster_Delete, param, commandType: CommandType.StoredProcedure);
                Service = task.Read<SuccessResult<AbstractServiceMaster>>().SingleOrDefault();
                Service.Item = task.Read<ServiceMaster>().SingleOrDefault();
            }

            return Service;
        }
    }
}
