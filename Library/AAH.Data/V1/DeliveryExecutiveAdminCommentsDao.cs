﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryExecutiveAdminCommentsDao : AbstractDeliveryExecutiveAdminCommentsDao
    {

        public override SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_Upsert(AbstractDeliveryExecutiveAdminComments AbstractDeliveryExecutiveAdminComments)
        {
            SuccessResult<AbstractDeliveryExecutiveAdminComments> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractDeliveryExecutiveAdminComments.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", AbstractDeliveryExecutiveAdminComments.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminId", AbstractDeliveryExecutiveAdminComments.AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Comment", AbstractDeliveryExecutiveAdminComments.Comment, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractDeliveryExecutiveAdminComments.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractDeliveryExecutiveAdminComments.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveAdminComments_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractDeliveryExecutiveAdminComments>>().SingleOrDefault();
                Address.Item = task.Read<DeliveryExecutiveAdminComments>().SingleOrDefault();
            }

            return Address;
        }

        public override PagedList<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_All(PageParam pageParam, string search, long DeliveryExecutiveId, long AdminId)
        {
            PagedList<AbstractDeliveryExecutiveAdminComments> Address = new PagedList<AbstractDeliveryExecutiveAdminComments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminId", AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveAdminComments_All, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<DeliveryExecutiveAdminComments>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }

        public override SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_ById(long Id)
        {
            SuccessResult<AbstractDeliveryExecutiveAdminComments> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveAdminComments_ById, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractDeliveryExecutiveAdminComments>>().SingleOrDefault();
                Address.Item = task.Read<DeliveryExecutiveAdminComments>().SingleOrDefault();
            }

            return Address;
        }

        public override SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractDeliveryExecutiveAdminComments> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveAdminComments_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractDeliveryExecutiveAdminComments>>().SingleOrDefault();
                Address.Item = task.Read<DeliveryExecutiveAdminComments>().SingleOrDefault();
            }

            return Address;
        }

    }
}
