﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class OtherServiceIconsDao : AbstractOtherServiceIconsDao
    {

        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Upsert(AbstractOtherServiceIcons AbstractOtherServiceIcons)
        {
            SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractOtherServiceIcons.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@URL", AbstractOtherServiceIcons.URL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractOtherServiceIcons.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Priority", AbstractOtherServiceIcons.Priority, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractOtherServiceIcons.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractOtherServiceIcons.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OtherServiceIcons_Upsert, param, commandType: CommandType.StoredProcedure);
                OtherServiceIcons = task.Read<SuccessResult<AbstractOtherServiceIcons>>().SingleOrDefault();
                OtherServiceIcons.Item = task.Read<OtherServiceIcons>().SingleOrDefault();
            }

            return OtherServiceIcons;
        }

        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ById(long Id)
        {
            SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OtherServiceIcons_ById, param, commandType: CommandType.StoredProcedure);
                OtherServiceIcons = task.Read<SuccessResult<AbstractOtherServiceIcons>>().SingleOrDefault();
                OtherServiceIcons.Item = task.Read<OtherServiceIcons>().SingleOrDefault();
            }

            return OtherServiceIcons;
        }
        public override PagedList<AbstractOtherServiceIcons> OtherServiceIcons_All(PageParam pageParam, string search)
        {
            PagedList<AbstractOtherServiceIcons> OtherServiceIcons = new PagedList<AbstractOtherServiceIcons>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OtherServiceIcons_All, param, commandType: CommandType.StoredProcedure);
                OtherServiceIcons.Values.AddRange(task.Read<OtherServiceIcons>());
                OtherServiceIcons.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return OtherServiceIcons;
        }

        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ActInAct(long Id, int UpdatedBy)
        {
            SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OtherServiceIcons_ActInAct, param, commandType: CommandType.StoredProcedure);
                OtherServiceIcons = task.Read<SuccessResult<AbstractOtherServiceIcons>>().SingleOrDefault();
                OtherServiceIcons.Item = task.Read<OtherServiceIcons>().SingleOrDefault();
            }

            return OtherServiceIcons;
        }

        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Delete(long Id, int DeletedBy)
        {
            SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OtherServiceIcons_Delete, param, commandType: CommandType.StoredProcedure);
                OtherServiceIcons = task.Read<SuccessResult<AbstractOtherServiceIcons>>().SingleOrDefault();
                OtherServiceIcons.Item = task.Read<OtherServiceIcons>().SingleOrDefault();
            }

            return OtherServiceIcons;
        }
    }
}
