﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class UserNotificationsDao : AbstractUserNotificationsDao
    {


        public override SuccessResult<AbstractUserNotifications> UserNotifications_Insert(AbstractUserNotifications abstractUserNotifications)
        {
            SuccessResult<AbstractUserNotifications> UserNotifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserNotifications.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserNotifications.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", abstractUserNotifications.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Description", abstractUserNotifications.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserNotifications.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_Insert, param, commandType: CommandType.StoredProcedure);
                UserNotifications = task.Read<SuccessResult<AbstractUserNotifications>>().SingleOrDefault();
                UserNotifications.Item = task.Read<UserNotifications>().SingleOrDefault();
            }

            return UserNotifications;
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent)
        {
            PagedList<AbstractUserNotifications> Users = new PagedList<AbstractUserNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ActionFormDate", ActionFormDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToDate", ActionToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsSent", IsSent, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_ByUserId, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<UserNotifications>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByRequestId(PageParam pageParam, long RequestId, long UserId)
        {
            PagedList<AbstractUserNotifications> Users = new PagedList<AbstractUserNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_ByRequestId, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<UserNotifications>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }
        
    }
}
