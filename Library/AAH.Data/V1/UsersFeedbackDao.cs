﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class UsersFeedbackDao : AbstractUsersFeedbackDao
    {
        public override SuccessResult<AbstractUsersFeedback> UsersFeedback_Insert(AbstractUsersFeedback AbstractUsersFeedback)
        {
            SuccessResult<AbstractUsersFeedback> UsersFeedback = null;
            var param = new DynamicParameters();

            param.Add("@UsersId", AbstractUsersFeedback.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractUsersFeedback.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", AbstractUsersFeedback.RequestId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Rettings", AbstractUsersFeedback.Rettings, dbType: DbType.Int32, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersFeedback_Insert, param, commandType: CommandType.StoredProcedure);
                UsersFeedback = task.Read<SuccessResult<AbstractUsersFeedback>>().SingleOrDefault();
                UsersFeedback.Item = task.Read<UsersFeedback>().SingleOrDefault();
            }

            return UsersFeedback;
        }
       
        public override PagedList<AbstractUsersFeedback> UsersFeedback_All(PageParam pageParam, string search, string FromDate, string ToDate,long UsersId)
        {
            PagedList<AbstractUsersFeedback> UsersFeedback = new PagedList<AbstractUsersFeedback>();

            var param = new DynamicParameters();

            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FromDate", FromDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToDate", ToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UsersId", UsersId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersFeedback_All, param, commandType: CommandType.StoredProcedure);
                UsersFeedback.Values.AddRange(task.Read<UsersFeedback>());
                UsersFeedback.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UsersFeedback;
        }
    }
 }
