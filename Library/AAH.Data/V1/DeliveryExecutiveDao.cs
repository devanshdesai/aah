﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryExecutiveDao : AbstractDeliveryExecutiveDao
    {

        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_ByDeliveryExecutiveId(PageParam pageParam, string search, long DeliveryExecutiveId)
        {
            PagedList<AbstractDeliveryExecutive> UserDevices = new PagedList<AbstractDeliveryExecutive>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveDevices_ByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                UserDevices.Values.AddRange(task.Read<DeliveryExecutive>());
                UserDevices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserDevices;
        }

        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_Upsert(AbstractDeliveryExecutive AbstractDeliveryExecutive)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractDeliveryExecutive.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FirstName", AbstractDeliveryExecutive.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", AbstractDeliveryExecutive.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MiddleName", AbstractDeliveryExecutive.MiddleName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProfileURL", AbstractDeliveryExecutive.ProfileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Addressline1", AbstractDeliveryExecutive.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Addressline2", AbstractDeliveryExecutive.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AadharCardImage", AbstractDeliveryExecutive.AadharCardImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Pincode", AbstractDeliveryExecutive.Pincode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AadharCardNumber", AbstractDeliveryExecutive.AadharCardNumber, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CityMasterId", AbstractDeliveryExecutive.CityMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StateMasterId", AbstractDeliveryExecutive.StateMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CountryMasterId", AbstractDeliveryExecutive.CountryMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Latitude", AbstractDeliveryExecutive.Latitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Longitude", AbstractDeliveryExecutive.Longitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Gender", AbstractDeliveryExecutive.Gender, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractDeliveryExecutive.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractDeliveryExecutive.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AgencyId", AbstractDeliveryExecutive.AgencyId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterAreaId", AbstractDeliveryExecutive.MasterAreaId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", AbstractDeliveryExecutive.MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentName", AbstractDeliveryExecutive.ParentName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentNumber", AbstractDeliveryExecutive.ParentNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PhotoIdProof", AbstractDeliveryExecutive.PhotoIdProof, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AgreementCopy", AbstractDeliveryExecutive.AgreementCopy, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DrivingLicenceNumber", AbstractDeliveryExecutive.DrivingLicenceNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AreaIds", AbstractDeliveryExecutive.AreaIds, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_Upsert, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_ActInAct(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_ActInAct, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsMarketingPerson(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_IsMarketingPerson, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }

        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_UpdateLatLong(long Id, string Latitude, string Longitude)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Latitude", Latitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Longitude", Longitude, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_UpdateLatLong, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsApproved(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_IsApproved, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsAvailable(long DeliveryExecutiveId, int IsAvailable)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsAvailable", IsAvailable, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_IsAvailable, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_ById(long Id)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_ById, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }
        public override SuccessResult<AbstractDeliveryExecutive> Dashboard_ById(long Id)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Dashboard_ById, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }

        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_Logout(string DeviceToken, string UDID, string IMEI)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", IMEI, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_Logout, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }

        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_VerifyOtp(long Id, string DeviceToken, long Otp,string DeviceType, string UDID, string IMEI)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", IMEI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Otp", Otp, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeviceType", DeviceType, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_VerifyOtp, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }

        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_SendOTP(string MobileNumber, string DeviceToken, string DeviceType, string UDID, string IMEI, long OTP)
        {
            SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceType", DeviceType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", IMEI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OTP", OTP, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_SendOTP, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive = task.Read<SuccessResult<AbstractDeliveryExecutive>>().SingleOrDefault();
                DeliveryExecutive.Item = task.Read<DeliveryExecutive>().SingleOrDefault();
            }

            return DeliveryExecutive;
        }


        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_All(PageParam pageParam, string search, string UserName,
            string MobileNumber, int Gender, long IsActive, long IsApproved, long IsAvailable, long loginId =0, long AgencyId = 0, long MasterAreaId = 0)
        {
            PagedList<AbstractDeliveryExecutive> DeliveryExecutive = new PagedList<AbstractDeliveryExecutive>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserName", UserName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Gender", Gender, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsApproved", IsApproved, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsAvailable", IsAvailable, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LoginId", loginId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AgencyId", AgencyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MasterAreaId", MasterAreaId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_All, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive.Values.AddRange(task.Read<DeliveryExecutive>());
                DeliveryExecutive.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutive;
        }
        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_ByAgencyId(PageParam pageParam, string search, long AgencyId)
        {
            PagedList<AbstractDeliveryExecutive> DeliveryExecutive = new PagedList<AbstractDeliveryExecutive>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            param.Add("@AgencyId", AgencyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
         

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_ByAgencyId, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive.Values.AddRange(task.Read<DeliveryExecutive>());
                DeliveryExecutive.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutive;
        }
        public override PagedList<AbstractDeliveryExecutive> Requests_NotAssignedDeliveryExecutive_All(PageParam pageParam, long RequestId)
        {
            PagedList<AbstractDeliveryExecutive> DeliveryExecutive = new PagedList<AbstractDeliveryExecutive>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_NotAssignedDeliveryExecutive_All, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive.Values.AddRange(task.Read<DeliveryExecutive>());
                DeliveryExecutive.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutive;
        }
        public override PagedList<AbstractDeliveryExecutive> GetDeliveryExecutiveCountByAddressId(string AddressId)
        {
            PagedList<AbstractDeliveryExecutive> DeliveryExecutive = new PagedList<AbstractDeliveryExecutive>();

            var param = new DynamicParameters();
            param.Add("@AddressId", AddressId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetDeliveryExecutiveCountByAddressId, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive.Values.AddRange(task.Read<DeliveryExecutive>());
                DeliveryExecutive.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutive;
        }

        // DE Feedback
        public override SuccessResult<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback_Insert(AbstractDeliveryExecutiveFeedback AbstractDeliveryExecutiveFeedback)
        {
            SuccessResult<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback = null;
            var param = new DynamicParameters();

            param.Add("@DeliveryExecutiveId", AbstractDeliveryExecutiveFeedback.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractDeliveryExecutiveFeedback.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", AbstractDeliveryExecutiveFeedback.RequestId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Rettings", AbstractDeliveryExecutiveFeedback.Rettings, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveFeedback_Insert, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveFeedback = task.Read<SuccessResult<AbstractDeliveryExecutiveFeedback>>().SingleOrDefault();
                DeliveryExecutiveFeedback.Item = task.Read<DeliveryExecutiveFeedback>().SingleOrDefault();
            }

            return DeliveryExecutiveFeedback;
        }

        public override PagedList<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback_All(PageParam pageParam, string search, string FromDate, string ToDate, long DeliveryExecutiveId)
        {
            PagedList<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback = new PagedList<AbstractDeliveryExecutiveFeedback>();

            var param = new DynamicParameters();

            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FromDate", FromDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToDate", ToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveFeedback_All, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveFeedback.Values.AddRange(task.Read<DeliveryExecutiveFeedback>());
                DeliveryExecutiveFeedback.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutiveFeedback;
        }
        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_Reports(PageParam pageParam, string search)
        {
            PagedList<AbstractDeliveryExecutive> DeliveryExecutive = new PagedList<AbstractDeliveryExecutive>();

            var param = new DynamicParameters();

            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutive_Reports, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutive.Values.AddRange(task.Read<DeliveryExecutive>());
                DeliveryExecutive.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutive;
        }
    }
}
