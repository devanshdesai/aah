﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DeliveryExecutiveHealthDao : AbstractDeliveryExecutiveHealthDao
    {

       
        public override SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ById(long Id)
        {
            SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveHealth_ById, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveHealth = task.Read<SuccessResult<AbstractDeliveryExecutiveHealth>>().SingleOrDefault();
                DeliveryExecutiveHealth.Item = task.Read<DeliveryExecutiveHealth>().SingleOrDefault();
            }

            return DeliveryExecutiveHealth;
        }
        public override PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_All(PageParam pageParam, string search)
        {
            PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth = new PagedList<AbstractDeliveryExecutiveHealth>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveHealth_All, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveHealth.Values.AddRange(task.Read<DeliveryExecutiveHealth>());
                DeliveryExecutiveHealth.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutiveHealth;
        }
        public override PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ByUserId(PageParam pageParam, string search,int UserId,int DeliveryExecutiveId)
        {
            PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth = new PagedList<AbstractDeliveryExecutiveHealth>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveHealth_ByUserId, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveHealth.Values.AddRange(task.Read<DeliveryExecutiveHealth>());
                DeliveryExecutiveHealth.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DeliveryExecutiveHealth;
        }
        public override SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ByDeliveryExecutiveId(long Id)
        {
            SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveHealth_ByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveHealth = task.Read<SuccessResult<AbstractDeliveryExecutiveHealth>>().SingleOrDefault();
                DeliveryExecutiveHealth.Item = task.Read<DeliveryExecutiveHealth>().SingleOrDefault();
            }

            return DeliveryExecutiveHealth;
        }
        public override SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_Upsert(AbstractDeliveryExecutiveHealth AbstractDeliveryExecutiveHealth)
        {
            SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractDeliveryExecutiveHealth.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", AbstractDeliveryExecutiveHealth.DeliveryExecutiveId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", AbstractDeliveryExecutiveHealth.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@VaccineName", AbstractDeliveryExecutiveHealth.VaccineName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Dose", AbstractDeliveryExecutiveHealth.Dose, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsVaccinated", AbstractDeliveryExecutiveHealth.IsVaccinated, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Temperature", AbstractDeliveryExecutiveHealth.Temperature, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", AbstractDeliveryExecutiveHealth.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractDeliveryExecutiveHealth.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractDeliveryExecutiveHealth.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveHealth_Upsert, param, commandType: CommandType.StoredProcedure);
                DeliveryExecutiveHealth = task.Read<SuccessResult<AbstractDeliveryExecutiveHealth>>().SingleOrDefault();
                DeliveryExecutiveHealth.Item = task.Read<DeliveryExecutiveHealth>().SingleOrDefault();
            }

            return DeliveryExecutiveHealth;
        }


    }
}
