﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class RequestsDao : AbstractRequestsDao
    {
        public override SuccessResult<AbstractRequests> Requests_Insert(AbstractRequests AbstractRequests)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@UserId", AbstractRequests.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceMasterId", AbstractRequests.ServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubServiceMasterId", AbstractRequests.SubServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubSubServiceMasterId", AbstractRequests.SubSubServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AmountRequested", AbstractRequests.AmountRequested, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@AudioUrl", AbstractRequests.AudioUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressId", AbstractRequests.AddressId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", AbstractRequests.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", AbstractRequests.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Latitude", AbstractRequests.Latitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Longitude", AbstractRequests.Longitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserDescription", AbstractRequests.UserDescription, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PayType", AbstractRequests.PayType, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_Insert, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override PagedList<AbstractRequestcomments> Requestcomments_ByRequestId(PageParam pageParam, string Search, long RequestId)
        {
            PagedList<AbstractRequestcomments> Requests = new PagedList<AbstractRequestcomments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requestcomments_ByRequestId, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requestcomments>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

        public override SuccessResult<AbstractRequestcomments> Requestcomments_Insert(AbstractRequestcomments abstractRequestcomments)
        {
            SuccessResult<AbstractRequestcomments> Requests = null;
            var param = new DynamicParameters();

            param.Add("@AdminId", abstractRequestcomments.AdminId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", abstractRequestcomments.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Description", abstractRequestcomments.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requestcomments_Insert, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequestcomments>>().SingleOrDefault();
                Requests.Item = task.Read<Requestcomments>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequests> Requests_AssignDeliveryExecutive(AbstractRequests AbstractRequests)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@RequestId", AbstractRequests.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", AbstractRequests.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AssignedBy", AbstractRequests.AssignedBy, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsAssignedByAdmin", AbstractRequests.IsAssignedByAdmin, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_AssignDeliveryExecutive, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }
            return Requests;
        }


        public override SuccessResult<AbstractRequests> Requests_UpdateByAdmin(long RequestedId, long ServiceMasterTypeId, long MasterPayTypeId, decimal AmountRequested, long DeliveryExecutiveId)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@RequestedId", RequestedId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceMasterTypeId", ServiceMasterTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MasterPayTypeId", MasterPayTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AmountRequested", AmountRequested, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_UpdateByAdmin, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }
            return Requests;
        }

        public override SuccessResult<AbstractRequests> Request_UpdateStatus(long RequestId, long StatusId, long DeliveryExecutiveId, decimal AmountGiven,decimal RequestedAmount, long OTP = 0, string DECancleReason = "")
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OTP", OTP, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AmountGiven", AmountGiven, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@RequestedAmount", RequestedAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@DECancleReason", DECancleReason, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Request_UpdateStatus, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }
            return Requests;
        }

        public override SuccessResult<AbstractRequests> Requests_ById(long Id, long LoginId = 0)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LoginId", LoginId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_ById, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequests> Requests_UpdateIsSettledWithAgency(long AgencyId, string FromDate, string ToDate, 
            long DeliveryExecutiveId, int IsSettledWithAgency)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@AgencyId", AgencyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FromDate", FromDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToDate", ToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsSettledWithAgency", IsSettledWithAgency, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_UpdateIsSettledWithAgency, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequests> Requests_UpdateIsSettledWithAdmin(long Id, int IsSettledWithAdmin)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsSettledWithAdmin", IsSettledWithAdmin, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_UpdateIsSettledWithAdmin, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequests> Requests_UpdateByDeliveryExecutive(AbstractRequests abstractRequests)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@UserId", abstractRequests.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MasterCategoryId", abstractRequests.MasterCategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SellerMasterId", abstractRequests.SellerMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PayType", abstractRequests.PayType, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", abstractRequests.Type, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestsId", abstractRequests.RequestsId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PolyLine", abstractRequests.PolyLine, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_UpdateByDeliveryExecutive, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }


        public override SuccessResult<AbstractRequests> DeliveryExecutiveRequestAmount_Update(long Id, decimal DeliveryExecutiveRequestAmount)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveRequestAmount", DeliveryExecutiveRequestAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DeliveryExecutiveRequestAmount_Update, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequests> IsApprovedByUser_Update(long Id, int IsApprovedByUser)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsApprovedByUser", IsApprovedByUser, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.IsApprovedByUser_Update, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override PagedList<AbstractRequests> Requests_All(PageParam pageParam, string Search,
            long UserId, long DeliveryExecutiveId, long ServiceMasterId, long SubServiceMasterId, long StatusId, long loginId =0, long Type = 0,long FilterType = 0)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceMasterId", ServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubServiceMasterId", SubServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LoginId", loginId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FilterType", FilterType, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_All, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

        public override PagedList<AbstractRequests> RequestsStatus_All(PageParam pageParam, string Search,long UserId, long StatusId)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestsStatus_All, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

        public override PagedList<AbstractRequests> GetRequestByDeliveryExecutiveId(PageParam pageParam, string Search = "",
            long DeliveryExecutiveId = 0, long StatusId = 0)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetRequestByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

        public override PagedList<AbstractRequests> RequestByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestByUserId, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }


        public override PagedList<AbstractRequests> RequestByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestByDeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

        public override PagedList<AbstractRequests> Requests_Attended(PageParam pageParam, string ActionFormDate, string ActionToDate)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ActionFormDate", ActionFormDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ActionToDate", ActionToDate, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_Attended, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

        public override PagedList<AbstractRequests> Requests_Urgent(PageParam pageParam, long UserId, long DeliveryExecutiveId)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_Urgent, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

        public override SuccessResult<AbstractRequests> Review_ByUser(long Id, decimal RatingByDeliveryExecutive, string ReviewByDeliveryExecutive)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RatingByDeliveryExecutive", RatingByDeliveryExecutive, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ReviewByDeliveryExecutive", ReviewByDeliveryExecutive, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection    (Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Review_ByUser, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequests> Review_ByDeliveryExecutive(long Id, decimal RatingByUser,
            string ReviewByUser)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RatingByUser", RatingByUser, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ReviewByUser", ReviewByUser, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Review_ByDeliveryExecutive, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequestsCount> Requests_ByStatusCount()
        {
            SuccessResult<AbstractRequestsCount> Requests = null;
            var param = new DynamicParameters();

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_ByStatusCount, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequestsCount>>().SingleOrDefault();
                Requests.Item = task.Read<RequestsCount>().SingleOrDefault();
            }

            return Requests;
        }

        public override SuccessResult<AbstractRequestsReachTime> Requests_ReachTime(long Id)
        {
            SuccessResult<AbstractRequestsReachTime> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_ReachTime, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequestsReachTime>>().SingleOrDefault();
                Requests.Item = task.Read<RequestsReachTime>().SingleOrDefault();
            }

            return Requests;
        }


        public override SuccessResult<AbstractRequests> Requests_UpdateEstimatedETA(long Id, DateTime EstimatedETA)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EstimatedETA", EstimatedETA, dbType: DbType.DateTime, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Requests_UpdateEstimatedETA, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }
        public override SuccessResult<AbstractRequests> Update_TipAmount(long Id, bool IsGiveTip, decimal TipAmount)
        {
            SuccessResult<AbstractRequests> Requests = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsGiveTip", IsGiveTip, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@TipAmount", TipAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Update_TipAmount, param, commandType: CommandType.StoredProcedure);
                Requests = task.Read<SuccessResult<AbstractRequests>>().SingleOrDefault();
                Requests.Item = task.Read<Requests>().SingleOrDefault();
            }

            return Requests;
        }


        public override PagedList<AbstractRequests> Report_All(PageParam pageParam, string search, string FromDate, string ToDate, long AgencyId,
            long DeliveryExecutiveId = 0, long IsSettled = 0)
        {
            PagedList<AbstractRequests> Requests = new PagedList<AbstractRequests>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FromDate", FromDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToDate", ToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AgencyId", AgencyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsSettled", IsSettled, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Report_All, param, commandType: CommandType.StoredProcedure);
                Requests.Values.AddRange(task.Read<Requests>());
                Requests.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Requests;
        }

    }
}
