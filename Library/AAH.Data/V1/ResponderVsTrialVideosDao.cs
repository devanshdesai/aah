﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class ResponderVsTrialVideosDao : AbstractResponderVsTrialVideosDao
    {

        public override SuccessResult<AbstractResponderVsTrialVideos> ResponderVsTrialVideos_Upsert(AbstractResponderVsTrialVideos AbstractResponderVsTrialVideos)
        {
            SuccessResult<AbstractResponderVsTrialVideos> ResponderVsTrialVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractResponderVsTrialVideos.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ResponderId", AbstractResponderVsTrialVideos.ResponderId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@VideoId", AbstractResponderVsTrialVideos.VideoId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractResponderVsTrialVideos.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractResponderVsTrialVideos.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ResponderVsTrialVideos_Upsert, param, commandType: CommandType.StoredProcedure);
                ResponderVsTrialVideos = task.Read<SuccessResult<AbstractResponderVsTrialVideos>>().SingleOrDefault();
                ResponderVsTrialVideos.Item = task.Read<ResponderVsTrialVideos>().SingleOrDefault();
            }

            return ResponderVsTrialVideos;
        }

    }
}
