﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class CategoryVsDeliveryExecutiveDao : AbstractCategoryVsDeliveryExecutiveDao
    {
        public override SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_Upsert(AbstractCategoryVsDeliveryExecutive AbstractCategoryVsDeliveryExecutive)
        {
            SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractCategoryVsDeliveryExecutive.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", AbstractCategoryVsDeliveryExecutive.DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryMasterIdStr", AbstractCategoryVsDeliveryExecutive.CategoryMasterIdStr, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", AbstractCategoryVsDeliveryExecutive.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", AbstractCategoryVsDeliveryExecutive.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractCategoryVsDeliveryExecutive.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractCategoryVsDeliveryExecutive.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CategoryVsDeliveryExecutive_Upsert, param, commandType: CommandType.StoredProcedure);
                CategoryVsDeliveryExecutive = task.Read<SuccessResult<AbstractCategoryVsDeliveryExecutive>>().SingleOrDefault();
                CategoryVsDeliveryExecutive.Item = task.Read<CategoryVsDeliveryExecutive>().SingleOrDefault();
            }

            return CategoryVsDeliveryExecutive;
        }

        public override PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_RequestId(PageParam pageParam, string search,long RequestId)
        {
            PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive = new PagedList<AbstractCategoryVsDeliveryExecutive>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CategoryVsDeliveryExecutive_RequestId, param, commandType: CommandType.StoredProcedure);
                CategoryVsDeliveryExecutive.Values.AddRange(task.Read<CategoryVsDeliveryExecutive>());
                CategoryVsDeliveryExecutive.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CategoryVsDeliveryExecutive;
        }

        public override PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_DeliveryExecutiveId(PageParam pageParam, string search, long DeliveryExecutiveId)
        {
            PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive = new PagedList<AbstractCategoryVsDeliveryExecutive>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CategoryVsDeliveryExecutive_DeliveryExecutiveId, param, commandType: CommandType.StoredProcedure);
                CategoryVsDeliveryExecutive.Values.AddRange(task.Read<CategoryVsDeliveryExecutive>());
                CategoryVsDeliveryExecutive.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CategoryVsDeliveryExecutive;
        }

        public override SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CategoryVsDeliveryExecutive_Delete, param, commandType: CommandType.StoredProcedure);
                CategoryVsDeliveryExecutive = task.Read<SuccessResult<AbstractCategoryVsDeliveryExecutive>>().SingleOrDefault();
                CategoryVsDeliveryExecutive.Item = task.Read<CategoryVsDeliveryExecutive>().SingleOrDefault();
            }

            return CategoryVsDeliveryExecutive;
        }
    }

}
