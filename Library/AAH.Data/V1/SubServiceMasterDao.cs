﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class SubServiceMasterDao : AbstractSubServiceMasterDao
    {
        public override SuccessResult<AbstractSubServiceMaster> SubServiceMaster_Upsert(AbstractSubServiceMaster AbstractSubServiceMaster)
        {
            SuccessResult<AbstractSubServiceMaster> SubService = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractSubServiceMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceMasterId", AbstractSubServiceMaster.ServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubServiceName", AbstractSubServiceMaster.SubServiceName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Icon", AbstractSubServiceMaster.Icon, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Link", AbstractSubServiceMaster.Link, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractSubServiceMaster.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractSubServiceMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubServiceMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                SubService = task.Read<SuccessResult<AbstractSubServiceMaster>>().SingleOrDefault();
                SubService.Item = task.Read<SubServiceMaster>().SingleOrDefault();
            }

            return SubService;
        }

         public override SuccessResult<AbstractSubServiceMaster> SubServiceMaster_ById(long Id)
        {
            SuccessResult<AbstractSubServiceMaster> SubServiceMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubServiceMaster_ById, param, commandType: CommandType.StoredProcedure);
                SubServiceMaster = task.Read<SuccessResult<AbstractSubServiceMaster>>().SingleOrDefault();
                SubServiceMaster.Item = task.Read<SubServiceMaster>().SingleOrDefault();
            }

            return SubServiceMaster;
        }
        public override PagedList<AbstractSubServiceMaster> SubServiceMaster_ByServiceMasterId(PageParam pageParam, string search, long ServiceMasterId)
        {
            PagedList<AbstractSubServiceMaster> SubService = new PagedList<AbstractSubServiceMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceMasterId", ServiceMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubServiceMaster_ByServiceMasterId, param, commandType: CommandType.StoredProcedure);
                SubService.Values.AddRange(task.Read<SubServiceMaster>());
                SubService.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return SubService;
        }

        public override SuccessResult<AbstractSubServiceMaster> SubServiceMaster_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractSubServiceMaster> SubService = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubServiceMaster_Delete, param, commandType: CommandType.StoredProcedure);
                SubService = task.Read<SuccessResult<AbstractSubServiceMaster>>().SingleOrDefault();
                SubService.Item = task.Read<SubServiceMaster>().SingleOrDefault();
            }

            return SubService;
        }
    }
}
