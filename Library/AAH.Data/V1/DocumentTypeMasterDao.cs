﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class DocumentTypeMasterDao : AbstractDocumentTypeMasterDao
    {
        public override PagedList<AbstractDocumentTypeMaster> DocumentTypeMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractDocumentTypeMaster> DocumentTypeMaster = new PagedList<AbstractDocumentTypeMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DocumentTypeMaster_All, param, commandType: CommandType.StoredProcedure);
                DocumentTypeMaster.Values.AddRange(task.Read<DocumentTypeMaster>());
                DocumentTypeMaster.TotalRecords = task.Read<int>().SingleOrDefault();
            }
            return DocumentTypeMaster;
        }

    }
}
