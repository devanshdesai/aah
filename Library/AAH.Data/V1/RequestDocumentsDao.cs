﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class RequestDocumentsDao : AbstractRequestDocumentsDao
    {
        public override SuccessResult<AbstractRequestDocuments> RequestDocuments_Insert(AbstractRequestDocuments abstractRequestDocuments)
        {
            SuccessResult<AbstractRequestDocuments> RequestDocuments = null;
            var param = new DynamicParameters();

            param.Add("@RequestId", abstractRequestDocuments.RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DocumentURL", abstractRequestDocuments.DocumentURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsUploadedByDeliveryExecutive", abstractRequestDocuments.IsUploadedByDeliveryExecutive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsUploadedByUser", abstractRequestDocuments.IsUploadedByUser, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractRequestDocuments.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestDocuments_Insert, param, commandType: CommandType.StoredProcedure);
                RequestDocuments = task.Read<SuccessResult<AbstractRequestDocuments>>().SingleOrDefault();
                RequestDocuments.Item = task.Read<RequestDocuments>().SingleOrDefault();
            }

            return RequestDocuments;
        }

        public override SuccessResult<AbstractRequestDocuments> RequestDocuments_ById(long Id)
        {
            SuccessResult<AbstractRequestDocuments> RequestDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestDocuments_ById, param, commandType: CommandType.StoredProcedure);
                RequestDocuments = task.Read<SuccessResult<AbstractRequestDocuments>>().SingleOrDefault();
                RequestDocuments.Item = task.Read<RequestDocuments>().SingleOrDefault();
            }

            return RequestDocuments;
        }

        public override PagedList<AbstractRequestDocuments> RequestDocuments_ByRequestId(PageParam pageParam, long RequestId)
        {
            PagedList<AbstractRequestDocuments> RequestDocuments = new PagedList<AbstractRequestDocuments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RequestId", RequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestDocuments_ByRequestId, param, commandType: CommandType.StoredProcedure);
                RequestDocuments.Values.AddRange(task.Read<RequestDocuments>());
                RequestDocuments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return RequestDocuments;
        }

        public override SuccessResult<AbstractRequestDocuments> RequestDocuments_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractRequestDocuments> RequestDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RequestDocuments_Delete, param, commandType: CommandType.StoredProcedure);
                RequestDocuments = task.Read<SuccessResult<AbstractRequestDocuments>>().SingleOrDefault();
                RequestDocuments.Item = task.Read<RequestDocuments>().SingleOrDefault();
            }

            return RequestDocuments;
        }
    }
}
