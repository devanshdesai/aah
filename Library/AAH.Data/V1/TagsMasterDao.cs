﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class TagsMasterDao : AbstractTagsMasterDao
    {
        public override PagedList<AbstractTagsMaster> TagsMaster_All(PageParam pageparam, string Search)
        {
            PagedList<AbstractTagsMaster> TagsMaster = new PagedList<AbstractTagsMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageparam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageparam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TagsMaster_All, param, commandType: CommandType.StoredProcedure);
                TagsMaster.Values.AddRange(task.Read<TagsMaster>());
                TagsMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TagsMaster;
        }
    }
}
