﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class TicketCommentsDao : AbstractTicketCommentsDao
    {

        
        

        public override SuccessResult<AbstractTicketComments> TicketComments_ById(long Id)
        {
            SuccessResult<AbstractTicketComments> TicketComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TicketComments_ById, param, commandType: CommandType.StoredProcedure);
                TicketComments = task.Read<SuccessResult<AbstractTicketComments>>().SingleOrDefault();
                TicketComments.Item = task.Read<TicketComments>().SingleOrDefault();
            }

            return TicketComments;
        } 
        public override SuccessResult<AbstractTicketComments> TicketComments_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractTicketComments> TicketComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TicketComments_Delete, param, commandType: CommandType.StoredProcedure);
                TicketComments = task.Read<SuccessResult<AbstractTicketComments>>().SingleOrDefault();
                TicketComments.Item = task.Read<TicketComments>().SingleOrDefault();
            }

            return TicketComments;
        } 
        
        public override SuccessResult<AbstractTicketComments> TicketComments_Upsert(AbstractTicketComments abstractTicketComments)
        {
            SuccessResult<AbstractTicketComments> TicketComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTicketComments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TicketId", abstractTicketComments.TicketId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Description", abstractTicketComments.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractTicketComments.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractTicketComments.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TicketComments_Upsert, param, commandType: CommandType.StoredProcedure);
                TicketComments = task.Read<SuccessResult<AbstractTicketComments>>().SingleOrDefault();
                TicketComments.Item = task.Read<TicketComments>().SingleOrDefault();
            }

            return TicketComments;
        }
        public override PagedList<AbstractTicketComments> TicketComments_All(PageParam pageParam, string search)
        {
            PagedList<AbstractTicketComments> TicketComments = new PagedList<AbstractTicketComments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TicketComments_All, param, commandType: CommandType.StoredProcedure);
                TicketComments.Values.AddRange(task.Read<TicketComments>());
                TicketComments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TicketComments;
        }public override PagedList<AbstractTicketComments> TicketComments_ByTicketId(PageParam pageParam, string search,long TicketId)
        {
            PagedList<AbstractTicketComments> TicketComments = new PagedList<AbstractTicketComments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TicketId", TicketId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TicketComments_ByTicketId, param, commandType: CommandType.StoredProcedure);
                TicketComments.Values.AddRange(task.Read<TicketComments>());
                TicketComments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TicketComments;
        }

        public override SuccessResult<AbstractTicketComments> TicketComments_ActInAct(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractTicketComments> TicketComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TicketComments_ActInAct, param, commandType: CommandType.StoredProcedure);
                TicketComments = task.Read<SuccessResult<AbstractTicketComments>>().SingleOrDefault();
                TicketComments.Item = task.Read<TicketComments>().SingleOrDefault();
            }

            return TicketComments;
        }


       

    }

}
