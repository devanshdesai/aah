﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class TrialVideosDao : AbstractTrialVideosDao
    {

        public override SuccessResult<AbstractTrialVideos> TrialVideos_Upsert(AbstractTrialVideos AbstractTrialVideos)
        {
            SuccessResult<AbstractTrialVideos> TrialVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractTrialVideos.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@NoOfTimes", AbstractTrialVideos.NoOfTimes, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Priority", AbstractTrialVideos.Priority, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@URL", AbstractTrialVideos.URL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Duration", AbstractTrialVideos.Duration, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractTrialVideos.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractTrialVideos.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrialVideos_Upsert, param, commandType: CommandType.StoredProcedure);
                TrialVideos = task.Read<SuccessResult<AbstractTrialVideos>>().SingleOrDefault();
                TrialVideos.Item = task.Read<TrialVideos>().SingleOrDefault();
            }

            return TrialVideos;
        }

        public override SuccessResult<AbstractTrialVideos> AfterTrialVideos_Complete(long DeliveryExecutiveId , long TrialVideosId)
        {
            SuccessResult<AbstractTrialVideos> TrialVideos = null;
            var param = new DynamicParameters();

            param.Add("@DeliveryExecutiveId", DeliveryExecutiveId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TrialVideosId", TrialVideosId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AfterTrialVideos_Complete, param, commandType: CommandType.StoredProcedure);
                TrialVideos = task.Read<SuccessResult<AbstractTrialVideos>>().SingleOrDefault();
                TrialVideos.Item = task.Read<TrialVideos>().SingleOrDefault();
            }

            return TrialVideos;
        }

        public override SuccessResult<AbstractTrialVideos> TrialVideos_ById(int Id)
        {
            SuccessResult<AbstractTrialVideos> TrialVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrialVideos_ById, param, commandType: CommandType.StoredProcedure);
                TrialVideos = task.Read<SuccessResult<AbstractTrialVideos>>().SingleOrDefault();
                TrialVideos.Item = task.Read<TrialVideos>().SingleOrDefault();
            }

            return TrialVideos;
        }
        public override PagedList<AbstractTrialVideos> TrialVideos_All(PageParam pageParam, string search)
        {
            PagedList<AbstractTrialVideos> TrialVideos = new PagedList<AbstractTrialVideos>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrialVideos_All, param, commandType: CommandType.StoredProcedure);
                TrialVideos.Values.AddRange(task.Read<TrialVideos>());
                TrialVideos.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TrialVideos;
        }

        public override SuccessResult<AbstractTrialVideos> TrialVideos_ActInAct(int Id, int UpdatedBy)
        {
            SuccessResult<AbstractTrialVideos> TrialVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrialVideos_ActInAct, param, commandType: CommandType.StoredProcedure);
                TrialVideos = task.Read<SuccessResult<AbstractTrialVideos>>().SingleOrDefault();
                TrialVideos.Item = task.Read<TrialVideos>().SingleOrDefault();
            }

            return TrialVideos;
        }


        public override SuccessResult<AbstractTrialVideos> TrialVideos_Delete(int Id, int DeletedBy)
        {
            SuccessResult<AbstractTrialVideos> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrialVideos_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractTrialVideos>>().SingleOrDefault();
                Address.Item = task.Read<TrialVideos>().SingleOrDefault();
            }

            return Address;
        }

    }

}
