﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AreaMasterDao : AbstractAreaMasterDao
    {

        public override SuccessResult<AbstractAreaMaster> AreaMaster_Upsert(AbstractAreaMaster AbstractAreaMaster)
        {
            SuccessResult<AbstractAreaMaster> AreaMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAreaMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PinCode", AbstractAreaMaster.PinCode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractAreaMaster.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CityMasterId", AbstractAreaMaster.CityMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StateMasterId", AbstractAreaMaster.StateMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ConvinienceDiscount", AbstractAreaMaster.ConvinienceDiscount, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ConvinienceCharge", AbstractAreaMaster.ConvinienceCharge, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@GeoFencing", AbstractAreaMaster.GeoFencing, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MaxNoOfExecutives", AbstractAreaMaster.MaxNoOfExecutives, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MinNoOfExecutives", AbstractAreaMaster.MinNoOfExecutives, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@GeoFencing", AbstractAreaMaster.GeoFencing, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAreaMaster.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAreaMaster.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                AreaMaster = task.Read<SuccessResult<AbstractAreaMaster>>().SingleOrDefault();
                AreaMaster.Item = task.Read<AreaMaster>().SingleOrDefault();
            }

            return AreaMaster;
        }

        public override SuccessResult<AbstractAreaMaster> AreaMaster_ById(long Id)
        {
            SuccessResult<AbstractAreaMaster> AreaMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaMaster_ById, param, commandType: CommandType.StoredProcedure);
                AreaMaster = task.Read<SuccessResult<AbstractAreaMaster>>().SingleOrDefault();
                AreaMaster.Item = task.Read<AreaMaster>().SingleOrDefault();
            }

            return AreaMaster;
        }
        public override PagedList<AbstractAreaMaster> AreaMaster_All(PageParam pageParam, string search, long LoginId=0, long Type = 0)
        {
            PagedList<AbstractAreaMaster> AreaMaster = new PagedList<AbstractAreaMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LoginId", LoginId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaMaster_All, param, commandType: CommandType.StoredProcedure);
                AreaMaster.Values.AddRange(task.Read<AreaMaster>());
                AreaMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AreaMaster;
        }
        public override PagedList<AbstractAreaMaster> AreaMaster_ByAgencyId(PageParam pageParam, string search, long AgencyId = 0)
        {
            PagedList<AbstractAreaMaster> AreaMaster = new PagedList<AbstractAreaMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AgencyId", AgencyId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaMaster_ByAgencyId, param, commandType: CommandType.StoredProcedure);
                AreaMaster.Values.AddRange(task.Read<AreaMaster>());
                AreaMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AreaMaster;
        }


        public override SuccessResult<AbstractAreaMaster> AreaMaster_Delete(long Id, int DeletedBy)
        {
            SuccessResult<AbstractAreaMaster> AreaMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaMaster_Delete, param, commandType: CommandType.StoredProcedure);
                AreaMaster = task.Read<SuccessResult<AbstractAreaMaster>>().SingleOrDefault();
                AreaMaster.Item = task.Read<AreaMaster>().SingleOrDefault();
            }

            return AreaMaster;
        }

        public override SuccessResult<AbstractAreaMaster> AreaMaster_ActInAct(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractAreaMaster> AreaMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaMaster_ActInAct, param, commandType: CommandType.StoredProcedure);
                AreaMaster = task.Read<SuccessResult<AbstractAreaMaster>>().SingleOrDefault();
                AreaMaster.Item = task.Read<AreaMaster>().SingleOrDefault();
            }

            return AreaMaster;
        }
        public override SuccessResult<AbstractAreaMaster> AreaMaster_AppUnApp(long Id, long UpdatedBy, int Type = 0)
        {
            SuccessResult<AbstractAreaMaster> AreaMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AreaMaster_IsApprove, param, commandType: CommandType.StoredProcedure);
                AreaMaster = task.Read<SuccessResult<AbstractAreaMaster>>().SingleOrDefault();
                AreaMaster.Item = task.Read<AreaMaster>().SingleOrDefault();
            }

            return AreaMaster;
        }
    }
}
