﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class UsersDao : AbstractUsersDao
    {

        public override PagedList<AbstractUserDevices> UserDevices_ByUserId(PageParam pageParam, string search, long UserId)
        {
            PagedList<AbstractUserDevices> UserDevices = new PagedList<AbstractUserDevices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserDevices_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserDevices.Values.AddRange(task.Read<UserDevices>());
                UserDevices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserDevices;
        }

        public override PagedList<AbstractUserDevices> UserDevices_ClearDeviceToken(PageParam pageParam, string search, long UserId)
        {
            PagedList<AbstractUserDevices> UserDevices = new PagedList<AbstractUserDevices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserDevices_ClearDeviceToken, param, commandType: CommandType.StoredProcedure);
                UserDevices.Values.AddRange(task.Read<UserDevices>());
                UserDevices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserDevices;
        }

        public override SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers AbstractUsers)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractUsers.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FirstName", AbstractUsers.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", AbstractUsers.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProfileURL", AbstractUsers.ProfileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", AbstractUsers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Gender", AbstractUsers.Gender, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractUsers.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_ActInAct(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ActInAct, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_IsPayable(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_IsPayable, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_IsBlacklist(long Id, long UpdatedBy)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_IsBlacklist, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_ById(long Id)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ById, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_Logout(string DeviceToken, string UDID, string IMEI)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", IMEI, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Logout, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_VerifyOtp(long Id, string DeviceToken, long Otp, string UDID, string IMEI)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", IMEI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Otp", Otp, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_VerifyOtp, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_SendOTP(string MobileNumber, string DeviceToken, string DeviceType, string UDID, string IMEI, long OTP)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceType", DeviceType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", IMEI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OTP", OTP, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_SendOTP, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }


        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, string UserName, string MobileNumber, 
            string Email, int Gender, long IsActive,long DEId)
        {
            PagedList<AbstractUsers> Users = new PagedList<AbstractUsers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserName", UserName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Gender", Gender, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DEId", DEId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_All, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<Users>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_ReferenceCode(long UserId, string ReferenceCode)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ReferenceCode", ReferenceCode, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ReferenceCode, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractUsers> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Address.Item = task.Read<Users>().SingleOrDefault();
            }

            return Address;
        }
    }
 }
