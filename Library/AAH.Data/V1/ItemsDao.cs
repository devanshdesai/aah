﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class ItemsDao : AbstractItemsDao
    {
        public override SuccessResult<AbstractItems> Items_Upsert(AbstractItems AbstractItems)
        {
            SuccessResult<AbstractItems> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractItems.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractItems.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GujaratiName", AbstractItems.GujaratiName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@HindiName", AbstractItems.HindiName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageUrl", AbstractItems.ImageUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractItems.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractItems.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Items_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractItems>>().SingleOrDefault();
                Address.Item = task.Read<Items>().SingleOrDefault();
            }

            return Address;
        }

        public override PagedList<AbstractItems> Items_All(PageParam pageParam, string search)
        {
            PagedList<AbstractItems> Address = new PagedList<AbstractItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Items_All, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<Items>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }
        public override PagedList<AbstractItems> AllItems(PageParam pageParam, string search)
        {
            PagedList<AbstractItems> Address = new PagedList<AbstractItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AllItems, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<Items>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }

        public override PagedList<AbstractItems> SearchItems_All(PageParam pageParam, string search,long UserId)
        {
            PagedList<AbstractItems> Address = new PagedList<AbstractItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SearchItems_All, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<Items>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }
         public override PagedList<AbstractItems> VendorsVsItems_AddItemsList(PageParam pageParam, string search,long VendorsId)
        {
            PagedList<AbstractItems> items = new PagedList<AbstractItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@VendorsId", VendorsId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.VendorsVsItems_AddItemsList, param, commandType: CommandType.StoredProcedure);
                items.Values.AddRange(task.Read<Items>());
                items.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return items;
        }

        public override SuccessResult<AbstractItems> Items_ById(long Id)
        {
            SuccessResult<AbstractItems> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Items_ById, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractItems>>().SingleOrDefault();
                Address.Item = task.Read<Items>().SingleOrDefault();
            }

            return Address;
        }

        public override SuccessResult<AbstractItems> Items_ActInAct(long Id)
        {
            SuccessResult<AbstractItems> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Items_ActInAct, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractItems>>().SingleOrDefault();
                Address.Item = task.Read<Items>().SingleOrDefault();
            }

            return Address;
        }


        public override SuccessResult<AbstractItems> Items_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractItems> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Items_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractItems>>().SingleOrDefault();
                Address.Item = task.Read<Items>().SingleOrDefault();
            }

            return Address;
        }
    }

}
