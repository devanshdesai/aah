﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using Dapper;

namespace AAH.Data.V1
{
    public class AdvertiseMasterDao : AbstractAdvertiseMasterDao
    {

        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_Upsert(AbstractAdvertiseMaster AbstractAdvertiseMaster)
        {
            SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractAdvertiseMaster.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CompanyName", AbstractAdvertiseMaster.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@HelpDocUrl", AbstractAdvertiseMaster.HelpDocUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Link", AbstractAdvertiseMaster.Link, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", AbstractAdvertiseMaster.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", AbstractAdvertiseMaster.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                AdvertiseMaster = task.Read<SuccessResult<AbstractAdvertiseMaster>>().SingleOrDefault();
                AdvertiseMaster.Item = task.Read<AdvertiseMaster>().SingleOrDefault();
            }

            return AdvertiseMaster;
        }

        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_ById(int Id)
        {
            SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseMaster_ById, param, commandType: CommandType.StoredProcedure);
                AdvertiseMaster = task.Read<SuccessResult<AbstractAdvertiseMaster>>().SingleOrDefault();
                AdvertiseMaster.Item = task.Read<AdvertiseMaster>().SingleOrDefault();
            }

            return AdvertiseMaster;
        }
        public override PagedList<AbstractAdvertiseMaster> AdvertiseMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAdvertiseMaster> AdvertiseMaster = new PagedList<AbstractAdvertiseMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseMaster_All, param, commandType: CommandType.StoredProcedure);
                AdvertiseMaster.Values.AddRange(task.Read<AdvertiseMaster>());
                AdvertiseMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdvertiseMaster;
        }

        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_ActInAct(int Id, int UpdatedBy)
        {
            SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseMaster_ActInAct, param, commandType: CommandType.StoredProcedure);
                AdvertiseMaster = task.Read<SuccessResult<AbstractAdvertiseMaster>>().SingleOrDefault();
                AdvertiseMaster.Item = task.Read<AdvertiseMaster>().SingleOrDefault();
            }

            return AdvertiseMaster;
        }


        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_Delete(int Id, int DeletedBy)
        {
            SuccessResult<AbstractAdvertiseMaster> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdvertiseMaster_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAdvertiseMaster>>().SingleOrDefault();
                Address.Item = task.Read<AdvertiseMaster>().SingleOrDefault();
            }

            return Address;
        }

    }
}
