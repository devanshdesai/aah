﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AAH.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        #region Users
        public const string Users_Upsert = "Users_Upsert";
        public const string Users_ActInAct = "Users_ActInAct";
        public const string Users_IsBlacklist = "Users_IsBlacklist";
        public const string Users_IsPayable = "Users_IsPayable";
        public const string Users_Logout = "Users_Logout";
        public const string Users_SendOTP = "Users_SendOTP";
        public const string Users_VerifyOtp = "Users_VerifyOtp";
        public const string Users_All = "Users_All";
        public const string UserDevices_ByUserId = "UserDevices_ByUserId";
        public const string UserDevices_ClearDeviceToken = "UserDevices_ClearDeviceToken";
        public const string Users_Delete = "Users_Delete";
        #endregion



        #region Survey
        public const string Survey_Insert = "Survey_Insert";
        public const string Survey_All = "Survey_All";
        #endregion

        #region RequestDocuments
        public const string RequestDocuments_Insert = "RequestDocuments_Insert";
        public const string RequestDocuments_ById = "RequestDocuments_ById";
        public const string RequestDocuments_ByRequestId = "RequestDocuments_ByRequestId";
        public const string RequestDocuments_Delete = "RequestDocuments_Delete";
        #endregion

        #region AddressMaster
        public const string AddressMaster_Upsert = "AddressMaster_Upsert";
        public const string AddressMaster_All = "AddressMaster_All";
        public const string AddressMaster_ById = "AddressMaster_ById";
        public const string AddressDefaultById = "AddressDefaultById";
        public const string AddressMaster_ActInAct = "AddressMaster_ActInAct";
        public const string AddressMaster_Delete = "AddressMaster_Delete";
        public const string AddressMaster_ByUserId = "AddressMaster_ByUserId";
        public const string Users_ById = "Users_ById";
        public const string SelectedAddress_Insert = "SelectedAddress_Insert";
        public const string SelectedAddress_ByUserId = "SelectedAddress_ByUserId";
        public const string Users_ReferenceCode = "Users_ReferenceCode";
        #endregion

        #region AddressType
        public const string AddressType_Upsert = "AddressType_Upsert";
        public const string AddressType_All = "AddressType_All";
        public const string AddressType_ById = "AddressType_ById";
        public const string AddressType_Delete = "AddressType_Delete";
       
        #endregion

        #region CityMaster
        public const string CityMaster_ByStateMasterId = "CityMaster_ByStateMasterId";
        #endregion

        #region StateMaster
        public const string StateMaster_ByCountryMasterId = "StateMaster_ByCountryMasterId";
        #endregion

        #region CountryMaster
        public const string CountryMaster_All = "CountryMaster_All";
        #endregion

        #region ServiceMaster
        public const string ServiceMaster_Upsert = "ServiceMaster_Upsert";
        public const string ServiceMaster_All = "ServiceMaster_All";
        public const string ServiceMaster_Delete = "ServiceMaster_Delete";
        public const string ServiceMaster_ById = "ServiceMaster_ById";
        #endregion

        #region SubServiceMaster
        public const string SubServiceMaster_Upsert = "SubServiceMaster_Upsert";
        public const string SubServiceMaster_ByServiceMasterId = "SubServiceMaster_ByServiceMasterId";
        public const string SubServiceMaster_Delete = "SubServiceMaster_Delete";
        public const string SubServiceMaster_ById = "SubServiceMaster_ById";
        #endregion

        #region SubSubServiceMaster
        public const string SubSubServiceMaster_Upsert = "SubSubServiceMaster_Upsert";
        public const string SubSubServiceMaster_BySubServiceMasterId = "SubSubServiceMaster_BySubServiceMasterId";
        public const string SubSubServiceMaster_ById = "SubSubServiceMaster_ById";
        #endregion

        #region AgencyMaster
        public const string AgencyMaster_Upsert = "AgencyMaster_Upsert";
        public const string AgencyMaster_ById = "AgencyMaster_ById";
        public const string AgencyMaster_All = "AgencyMaster_All";
        #endregion

        #region DeliveryExecutive
        public const string DeliveryExecutive_Upsert = "DeliveryExecutive_Upsert";
        public const string DeliveryExecutive_ActInAct = "DeliveryExecutive_ActInAct";
        public const string Dashboard_ById = "Dashboard_ById";
        public const string DeliveryExecutive_ById = "DeliveryExecutive_ById";
        public const string DeliveryExecutive_IsApproved = "DeliveryExecutive_IsApproved";
        public const string DeliveryExecutive_IsAvailable = "DeliveryExecutive_IsAvailable";
        public const string DeliveryExecutive_IsMarketingPerson = "DeliveryExecutive_IsMarketingPerson";
        public const string DeliveryExecutive_Logout = "DeliveryExecutive_Logout";
        public const string DeliveryExecutive_SendOTP = "DeliveryExecutive_SendOTP";
        public const string DeliveryExecutive_VerifyOtp = "DeliveryExecutive_VerifyOtp";
        public const string DeliveryExecutive_All = "DeliveryExecutive_All";
        public const string DeliveryExecutive_Reports = "DeliveryExecutive_Reports";
        public const string DeliveryExecutiveDevices_ByDeliveryExecutiveId = "DeliveryExecutiveDevices_ByDeliveryExecutiveId";
        public const string DeliveryExecutive_ByAgencyId = "DeliveryExecutive_ByAgencyId";
        #endregion
        
        #region DeliveryExecutive
        public const string AdvertiseLeads_Upsert = "AdvertiseLeads_Upsert";
        public const string AdvertiseLeads_All = "AdvertiseLeads_All";
        public const string AdvertiseLeads_ByRequestId = "AdvertiseLeads_ByRequestId";
        #endregion

        #region DeliveryExecutiveDocuments
        public const string DeliveryExecutiveDocuments_ByDeliveryExecutiveId = "DeliveryExecutiveDocuments_ByDeliveryExecutiveId";
        public const string DeliveryExecutiveDocuments_Upsert = "DeliveryExecutiveDocuments_Upsert";
        public const string DeliveryExecutiveDocuments_Delete = "DeliveryExecutiveDocuments_Delete";
        public const string DeliveryExecutiveDocuments_Verify = "DeliveryExecutiveDocuments_Verify";
        #endregion

        #region DeliveryExecutive
        public const string DocumentTypeMaster_All = "DocumentTypeMaster_All";
        public const string DeliveryExecutive_UpdateLatLong = "DeliveryExecutive_UpdateLatLong";

        #endregion

        #region Admin
        public const string Admin_SignOut = "Admin_SignOut";
        public const string Admin_SignIn = "Admin_SignIn";
        public const string Admin_ChangePassword = "Admin_ChangePassword";
        public const string Admin_Upsert = "Admin_Upsert";
        public const string Admin_ById = "Admin_ById";
        public const string Admin_All = "Admin_All";
        public const string Admin_ActInAct = "Admin_ActInAct";
        public const string Admin_Dashboard = "Admin_Dashboard";
        public const string DownloadingStaff_TodayTarget = "DownloadingStaff_TodayTarget";
        public const string OnBoardingStaff_TodayTarget = "OnBoardingStaff_TodayTarget";
        public const string AreaVsStaff_ById = "AreaVsStaff_ById";
        public const string AreaVsStaff_All = "AreaVsStaff_All";
        public const string AreaVsStaff_Delete = "AreaVsStaff_Delete";
        public const string AreaVsStaff_Upsert = "AreaVsStaff_Upsert";
        #endregion

        #region HowGinnyBuddyWorks
        public const string HowGinnyBuddyWorks_Upsert = "HowGinnyBuddyWorks_Upsert";
        public const string HowGinnyBuddyWorks_ById = "HowGinnyBuddyWorks_ById";
        public const string HowGinnyBuddyWorks_All = "HowGinnyBuddyWorks_All";
        public const string HowGinnyBuddyWorks_ActInAct = "HowGinnyBuddyWorks_ActInAct";
        public const string HowGinnyBuddyWorks_Delete = "HowGinnyBuddyWorks_Delete";

        #endregion 
        
        #region MasterPayType
        public const string MasterPayType_Upsert = "MasterPayType_Upsert";
        public const string MasterPayType_ById = "MasterPayType_ById";
        public const string MasterPayType_All = "MasterPayType_All";
        public const string MasterPayType_ActInAct = "MasterPayType_ActInAct";
        public const string MasterPayType_Delete = "MasterPayType_Delete";
        #endregion

        #region MasterPayType
        public const string Ticket_Upsert = "Ticket_Upsert";
        public const string Ticket_ById = "Ticket_ById";
        public const string Ticket_All = "Ticket_All";
        public const string Ticket_ActInAct = "Ticket_ActInAct";
        public const string Ticket_ChangeStatus = "Ticket_ChangeStatus";
        public const string Ticket_ChangeType = "Ticket_ChangeType";
        public const string Ticket_ChangePlatform = "Ticket_ChangePlatform";
        #endregion 
        
        #region MasterPayType
        public const string TicketComments_ById = "TicketComments_ById";
        public const string TicketComments_All = "TicketComments_All";
        public const string TicketComments_ActInAct = "TicketComments_ActInAct";
        public const string TicketComments_ByTicketId = "TicketComments_ByTicketId";
        public const string TicketComments_Delete = "TicketComments_Delete";
        public const string TicketComments_Upsert = "TicketComments_Upsert";
        #endregion

        #region MasterPayType
        public const string MasterAgeGroup_Upsert = "MasterAgeGroup_Upsert";
        public const string MasterAgeGroup_ById = "MasterAgeGroup_ById";
        public const string MasterAgeGroup_All = "MasterAgeGroup_All";
        public const string MasterAgeGroup_Delete = "MasterAgeGroup_Delete";
        #endregion

        #region OtherServiceIcons_Delete
        public const string OtherServiceIcons_Upsert = "OtherServiceIcons_Upsert";
        public const string OtherServiceIcons_ById = "OtherServiceIcons_ById";
        public const string OtherServiceIcons_All = "OtherServiceIcons_All";
        public const string OtherServiceIcons_ActInAct = "OtherServiceIcons_ActInAct";
        public const string OtherServiceIcons_Delete = "OtherServiceIcons_Delete";

        #endregion

        #region AdminType
        public const string AdminType_All = "AdminType_All";
        public const string Vendor_ValidArea = "Vendor_ValidArea";
        public const string GetAgency_ByAreaId = "GetAgency_ByAreaId";
        public const string GetAgency_ByIds = "GetAgency_ByIds";
        public const string GetAvailableDE_ByAgencyId = "GetAvailableDE_ByAgencyId";
        #endregion

        #region DeliveryExecutiveAdminComments
        public const string DeliveryExecutiveAdminComments_Upsert = "DeliveryExecutiveAdminComments_Upsert";
        public const string DeliveryExecutiveAdminComments_All = "DeliveryExecutiveAdminComments_All";
        public const string DeliveryExecutiveAdminComments_ById = "DeliveryExecutiveAdminComments_ById";
        public const string DeliveryExecutiveAdminComments_Delete = "DeliveryExecutiveAdminComments_Delete";
        #endregion
        
        #region DeliveryExecutiveCheckInCheckOut
        public const string DeliveryExecutiveCheckInCheckOut_Upsert = "DeliveryExecutiveCheckInCheckOut_Upsert";
        public const string DeliveryExecutiveCheckInCheckOut_ById = "DeliveryExecutiveCheckInCheckOut_ById";
        public const string DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId = "DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId";
        #endregion
        
        #region DeliveryExecutiveLatLong
        public const string DeliveryExecutiveLatLong_ByDeliveryExecutiveId = "DeliveryExecutiveLatLong_ByDeliveryExecutiveId";
        public const string DeliveryExecutiveLatLong_ByRequestId = "DeliveryExecutiveLatLong_ByRequestId";
        public const string DeliveryExecutiveLatLong_Insert = "DeliveryExecutiveLatLong_Insert";
        public const string DeliveryExecutiveLatLong_Map = "DeliveryExecutiveLatLong_Map";

        public const string OnboarderLatLong_ByAdminId = "OnboarderLatLong_ByAdminId";
        public const string OnboarderLatLong_Insert = "OnboarderLatLong_Insert";
        #endregion

        #region LookupStatus
        public const string LookupStatus_All = "LookupStatus_All ";
        #endregion

        #region UsersLatLong
        public const string UsersLatLong_ByUserId = "UsersLatLong_ByUserId";
        public const string UsersLatLong_ByRequestId = "UsersLatLong_ByRequestId";
        public const string UsersLatLong_Insert = "UsersLatLong_Insert";
        #endregion

        # region UserNotifications
        public const string UserNotifications_Insert = "UserNotifications_Insert";
        public const string UserNotifications_ByUserId = "UserNotifications_ByUserId";
        public const string UserNotifications_ByRequestId = "UserNotifications_ByRequestId";
        
        #endregion

        #region DeliveryExecutiveNotifications
        public const string DeliveryExecutiveNotifications_Insert = "DeliveryExecutiveNotifications_Insert";
        public const string DeliveryExecutiveNotifications_ByDeliveryExecutiveId = "DeliveryExecutiveNotifications_ByDeliveryExecutiveId";
        public const string DeliveryExecutiveNotifications_ByRequestId = "DeliveryExecutiveNotifications_ByRequestId";
        public const string DeliveryExecutiveNotifications_RequestStatusUpdate = "DeliveryExecutiveNotifications_RequestStatusUpdate";
        #endregion

        #region Requests
        public const string Requests_Insert = "Requests_Insert";
        public const string Requests_AssignDeliveryExecutive = "Requests_AssignDeliveryExecutive";
        public const string Request_UpdateStatus = "Request_UpdateStatus";
        public const string Requests_ById = "Requests_ById";
        public const string Requests_UpdateByDeliveryExecutive = "Requests_UpdateByDeliveryExecutive";
        public const string Requests_All = "Requests_All";
        public const string Requestcomments_ByRequestId = "Requestcomments_ByRequestId";
        public const string Requestcomments_Insert = "Requestcomments_Insert";
        public const string GetRequestByDeliveryExecutiveId = "DeliveryExecutiveVsRequests_All";
        public const string RequestByDeliveryExecutiveId = "RequestByDeliveryExecutiveId";
        public const string RequestByUserId = "RequestByUserId";
        public const string Review_ByUser = "Review_ByUser";
        public const string Review_ByDeliveryExecutive = "Review_ByDeliveryExecutive";
        public const string Requests_Urgent = "Requests_Urgent";
        public const string Requests_Attended = "Requests_Attended";
        public const string Requests_ByStatusCount = "Requests_ByStatusCount";
        public const string Requests_ReachTime = "Requests_ReachTime";
        public const string DeliveryExecutiveRequestAmount_Update = "DeliveryExecutiveRequestAmount_Update";
        public const string IsApprovedByUser_Update = "IsApprovedByUser_Update";
        public const string Requests_NotAssignedDeliveryExecutive_All = "Requests_NotAssignedDeliveryExecutive_All";
        public const string GetDeliveryExecutiveCountByAddressId = "GetDeliveryExecutiveCountByAddressId";
        public const string Requests_UpdateEstimatedETA = "Requests_UpdateEstimatedETA";
        public const string Requests_UpdateByAdmin = "Requests_UpdateByAdmin";
        public const string RequestsStatus_All = "RequestsStatus_All";
        public const string Requests_UpdateIsSettledWithAgency = "Requests_UpdateIsSettledWithAgency";
        public const string Requests_UpdateIsSettledWithAdmin = "Requests_UpdateIsSettledWithAdmin";
        public const string Update_TipAmount = "Update_TipAmount";
        public const string Report_All = "Report_All";
        #endregion

        #region RequestsStatusTracker
        public const string RequestsStatusTracker_ByRequestId = "RequestsStatusTracker_ByRequestId";
        #endregion

        #region RequestsDelieveryExecutiveTracker
        public const string RequestsDelieveryExecutiveTracker_ByRequestId = "RequestsDelieveryExecutiveTracker_ByRequestId";
        public const string RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId = "RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId";
        #endregion
        
        #region TagsMaster
        public const string TagsMaster_All = "TagsMaster_All ";
        #endregion

        #region DeliveryExecutiveHealth
        public const string DeliveryExecutiveHealth_Upsert = "DeliveryExecutiveHealth_Upsert";
        public const string DeliveryExecutiveHealth_All = "DeliveryExecutiveHealth_All";
        public const string DeliveryExecutiveHealth_ByUserId = "DeliveryExecutiveHealth_ByUserId";
        public const string DeliveryExecutiveHealth_ByDeliveryExecutiveId = "DeliveryExecutiveHealth_ByDeliveryExecutiveId";
        public const string DeliveryExecutiveHealth_ById = "DeliveryExecutiveHealth_ById";
        #endregion

        #region UsersFeedback
        public const string UsersFeedback_Insert = "UsersFeedback_Insert";
        public const string UsersFeedback_All = "UsersFeedback_All";
        #endregion

        #region DeliveryExecutiveFeedback
        public const string DeliveryExecutiveFeedback_Insert = "DeliveryExecutiveFeedback_Insert";
        public const string DeliveryExecutiveFeedback_All = "DeliveryExecutiveFeedback_All";
        #endregion

        #region MasterCategory
        public const string MasterCategory_Upsert = "MasterCategory_Upsert";
        public const string MasterCategory_All = "MasterCategory_All";
        public const string MasterCategory_ById = "MasterCategory_ById";
        public const string MasterCategory_Delete = "MasterCategory_Delete";
        #endregion

        #region CategoryVsDeliveryExecutive
        public const string CategoryVsDeliveryExecutive_Upsert = "CategoryVsDeliveryExecutive_Upsert";
        public const string CategoryVsDeliveryExecutive_RequestId = "CategoryVsDeliveryExecutive_RequestId";
        public const string CategoryVsDeliveryExecutive_DeliveryExecutiveId = "CategoryVsDeliveryExecutive_DeliveryExecutiveId";
        public const string CategoryVsDeliveryExecutive_Delete = "CategoryVsDeliveryExecutive_Delete";
        #endregion

        #region SellerMaster
        public const string SellerMaster_Upsert = "SellerMaster_Upsert";
        #endregion

        #region AdvertiseMaster
        public const string AdvertiseMaster_Upsert = "AdvertiseMaster_Upsert";
        public const string AdvertiseMaster_All = "AdvertiseMaster_All";
        public const string AdvertiseMaster_ById = "AdvertiseMaster_ById";
        public const string AdvertiseMaster_Delete = "AdvertiseMaster_Delete";
        public const string AdvertiseMaster_ActInAct = "AdvertiseMaster_ActInAct";
        #endregion

        #region AdvertisePreferredCategory
        public const string AdvertisePreferredCategory_Upsert = "AdvertisePreferredCategory_Upsert";
        public const string AdvertisePreferredCategory_All = "AdvertisePreferredCategory_All";
        public const string AdvertisePreferredCategory_ById = "AdvertisePreferredCategory_ById";
        public const string AdvertisePreferredCategory_Delete = "AdvertisePreferredCategory_Delete";
        //public const string MasterCategoryA_All = "MasterCategoryA_All";
        //public const string AdvertiseMasterA_All = "AdvertiseMasterA_All";
        #endregion

        #region TrialVideos
        public const string AfterTrialVideos_Complete = "AfterTrialVideos_Complete";
        public const string TrialVideos_Upsert = "TrialVideos_Upsert";
        public const string TrialVideos_All = "TrialVideos_All";
        public const string TrialVideos_ById = "TrialVideos_ById";
        public const string TrialVideos_Delete = "TrialVideos_Delete";
        public const string TrialVideos_ActInAct = "TrialVideos_ActInAct";
        #endregion

        #region DeliveryLimit
        public const string DeliveryLimit_Upsert = "DeliveryLimit_Upsert";
        public const string DeliveryLimit_All = "DeliveryLimit_All";
        public const string DeliveryLimit_ById = "DeliveryLimit_ById";
        public const string DeliveryLimit_Delete = "DeliveryLimit_Delete";
        #endregion

        #region ResponderVsTrialVideos
        public const string ResponderVsTrialVideos_Upsert = "ResponderVsTrialVideos_Upsert";
        #endregion

        #region AreaMaster
        public const string AreaMaster_Upsert = "AreaMaster_Upsert";
        public const string AreaMaster_All = "AreaMaster_All";
        public const string AreaMaster_ByAgencyId = "AreaMaster_ByAgencyId";
        public const string AreaMaster_ById = "AreaMaster_ById";
        public const string AreaMaster_Delete = "AreaMaster_Delete";
        public const string AreaMaster_ActInAct = "AreaMaster_ActInAct";
        public const string AreaMaster_IsApprove = "AreaMaster_IsApprove";
        #endregion
        
        #region AgencyVsArea
        public const string AgencyVsArea_Upsert = "AgencyVsArea_Upsert";
        public const string AgencyVsArea_All = "AgencyVsArea_All";
        public const string AgencyVsArea_AssignedDE = "AgencyVsArea_AssignedDE";
        public const string AgencyVsArea_ByAgencyId = "AgencyVsArea_ByAgencyId";
        public const string AgencyVsArea_ById = "AgencyVsArea_ById";
        public const string AgencyVsArea_Delete = "AgencyVsArea_Delete";
        public const string AgencyVsArea_ActInAct = "AgencyVsArea_ActInAct";
        public const string AgencyVsArea_IsApprove = "AgencyVsArea_IsApprove";
        public const string AgencyVsArea_byAgencyId = "AgencyVsArea_byAgencyId";
        #endregion

        #region MasterServices
        public const string MasterServices_Upsert = "MasterServices_Upsert";
        public const string MasterServices_ById = "MasterServices_ById";
        public const string MasterServices_Delete = "MasterServices_Delete";
        public const string MasterServices_All = "MasterServices_All";
        #endregion

        public const string CustomersNotification_Upsert = "CustomersNotification_Upsert";


        //Chai Nasta ===============================
        #region Vendors
        public const string Vendors_Upsert = "Vendors_Upsert";
        public const string VendorsThelaImages_Upsert = "VendorsThelaImages_Upsert";
        public const string Vendors_All = "Vendors_All";
        public const string Vendors_ByAdminId = "Vendors_ByAdminId";
        public const string Vendors_ById = "Vendors_ById";
        public const string Vendor_SendOTP = "Vendor_SendOTP";
        public const string Vendor_VerifyOtp = "Vendor_VerifyOtp";
        public const string Vendors_Delete = "Vendors_Delete";
        public const string Vendors_ActInAct = "Vendors_ActInAct";
        public const string VendorsThelaImages_Delete = "VendorsThelaImages_Delete";
        public const string VendorsVsItems_Upsert = "VendorsVsItems_Upsert";
        public const string VendorsVsItems_Delete = "VendorsVsItems_Delete";
        public const string VendorsVsItems_ByVendorsId = "VendorsVsItems_ByVendorsId";
        public const string VendorsVsItems_ActInAct = "VendorsVsItems_ActInAct";
        public const string Vendor_IsOnline = "Vendor_IsOnline";
        public const string VendorsVsReferences_Upsert = "VendorsVsReferences_Upsert";
        public const string VendorsVsReferences_Delete = "VendorsVsReferences_Delete";
        public const string VendorsVsReferences_ByVendorsId = "VendorsVsReferences_ByVendorsId";
        #endregion

        #region Items
        public const string Items_Upsert = "Items_Upsert";
        public const string Items_All = "Items_All";
        public const string AllItems = "AllItems";
        public const string SearchItems_All = "SearchItems_All";
        public const string Items_ById = "Items_ById";
        public const string Items_Delete = "Items_Delete";
        public const string Items_ActInAct = "Items_ActInAct";
        public const string VendorsVsItems_AddItemsList = "VendorsVsItems_AddItemsList";
        #endregion
    }
}
