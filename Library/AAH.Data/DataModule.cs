﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AAH.Data
{
    using Autofac;
    using AAH.Data.Contract;

    //using AAH.Data.Contract;


    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<V1.UsersDao>().As<AbstractUsersDao>().InstancePerDependency();
            builder.RegisterType<V1.TicketDao>().As<AbstractTicketDao>().InstancePerDependency();
            builder.RegisterType<V1.TicketCommentsDao>().As<AbstractTicketCommentsDao>().InstancePerDependency();
            builder.RegisterType<V1.AdvertiseLeadsDao>().As<AbstractAdvertiseLeadsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterAgeGroupDao>().As<AbstractMasterAgeGroupDao>().InstancePerDependency();
            builder.RegisterType<V1.SurveyDao>().As<AbstractSurveyDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryLimitDao>().As<AbstractDeliveryLimitDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterPayTypeDao>().As<AbstractMasterPayTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.OtherServiceIconsDao>().As<AbstractOtherServiceIconsDao>().InstancePerDependency();
            builder.RegisterType<V1.HowGinnyBuddyWorksDao>().As<AbstractHowGinnyBuddyWorksDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveHealthDao>().As<AbstractDeliveryExecutiveHealthDao>().InstancePerDependency();
            builder.RegisterType<V1.ResponderVsTrialVideosDao>().As<AbstractResponderVsTrialVideosDao>().InstancePerDependency();
            builder.RegisterType<V1.TrialVideosDao>().As<AbstractTrialVideosDao>().InstancePerDependency();
            builder.RegisterType<V1.AdvertisePreferredCategoryDao>().As<AbstractAdvertisePreferredCategoryDao>().InstancePerDependency();
            builder.RegisterType<V1.AdvertiseMasterDao>().As<AbstractAdvertiseMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.SellerMasterDao>().As<AbstractSellerMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.AddressTypeDao>().As<AbstractAddressTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.RequestDocumentsDao>().As<AbstractRequestDocumentsDao>().InstancePerDependency();
            builder.RegisterType<V1.AddressMasterDao>().As<AbstractAddressMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.CityMasterDao>().As<AbstractCityMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.StateMasterDao>().As<AbstractStateMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.CountryMasterDao>().As<AbstractCountryMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.ServiceMasterDao>().As<AbstractServiceMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.SubServiceMasterDao>().As<AbstractSubServiceMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveDao>().As<AbstractDeliveryExecutiveDao>().InstancePerDependency();
            builder.RegisterType<V1.AdminDao>().As<AbstractAdminDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveAdminCommentsDao>().As<AbstractDeliveryExecutiveAdminCommentsDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveDao>().As<AbstractDeliveryExecutiveDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveCheckInCheckOutDao>().As<AbstractDeliveryExecutiveCheckInCheckOutDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveLatLongDao>().As<AbstractDeliveryExecutiveLatLongDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupStatusDao>().As<AbstractLookupStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveDocumentsDao>().As<AbstractDeliveryExecutiveDocumentsDao>().InstancePerDependency();
            builder.RegisterType<V1.DocumentTypeMasterDao>().As<AbstractDocumentTypeMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.UsersLatLongDao>().As<AbstractUsersLatLongDao>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsDao>().As<AbstractUserNotificationsDao>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveNotificationsDao>().As<AbstractDeliveryExecutiveNotificationsDao>().InstancePerDependency();
            builder.RegisterType<V1.RequestsDao>().As<AbstractRequestsDao>().InstancePerDependency();
            builder.RegisterType<V1.RequestsStatusTrackerDao>().As<AbstractRequestsStatusTrackerDao>().InstancePerDependency();
            builder.RegisterType<V1.RequestsDelieveryExecutiveTrackerDao>().As<AbstractRequestsDelieveryExecutiveTrackerDao>().InstancePerDependency();
            builder.RegisterType<V1.SubSubServiceMasterDao>().As<AbstractSubSubServiceMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.TagsMasterDao>().As<AbstractTagsMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.UsersFeedbackDao>().As<AbstractUsersFeedbackDao>().InstancePerDependency();
            builder.RegisterType<V1.AgencyMasterDao>().As<AbstractAgencyMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterCategoryDao>().As<AbstractMasterCategoryDao>().InstancePerDependency();
            builder.RegisterType<V1.CategoryVsDeliveryExecutiveDao>().As<AbstractCategoryVsDeliveryExecutiveDao>().InstancePerDependency();
            builder.RegisterType<V1.AreaMasterDao>().As<AbstractAreaMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.AgencyVsAreaDao>().As<AbstractAgencyVsAreaDao>().InstancePerDependency();
            builder.RegisterType<V1.CustomersNotificationDao>().As<AbstractCustomersNotificationDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterServicesDao>().As<AbstractMasterServicesDao>().InstancePerDependency();

            //Chai Nasta ===============================================

            builder.RegisterType<V1.VendorsDao>().As<AbstractVendorsDao>().InstancePerDependency();
            builder.RegisterType<V1.ItemsDao>().As<AbstractItemsDao>().InstancePerDependency();

            base.Load(builder);
        }
    }
}
