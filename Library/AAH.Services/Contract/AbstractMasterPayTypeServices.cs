﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractMasterPayTypeServices
    {
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_Upsert(AbstractMasterPayType abstractMasterPayType);
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_ById(long Id);
        public abstract PagedList<AbstractMasterPayType> MasterPayType_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_Delete(long Id, int DeletedBy);
        public abstract SuccessResult<AbstractMasterPayType> MasterPayType_ActInAct(long Id, int UpdatedBy);
    }
}
