﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractUsersFeedbackServices
    {
        public abstract SuccessResult<AbstractUsersFeedback> UsersFeedback_Insert(AbstractUsersFeedback abstractUsersFeedback);

        public abstract PagedList<AbstractUsersFeedback> UsersFeedback_All(PageParam pageParam, string search, string FromDate, string ToDate,long UsersId);


    }
}
