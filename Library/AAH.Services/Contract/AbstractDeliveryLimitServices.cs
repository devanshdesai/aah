﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractDeliveryLimitServices
    {
        public abstract SuccessResult<AbstractDeliveryLimit> DeliveryLimit_ById(long Id);
        public abstract PagedList<AbstractDeliveryLimit> DeliveryLimit_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Upsert(AbstractDeliveryLimit abstractDeliveryLimit);
        public abstract SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Delete(long Id, int DeletedBy);
    }
}
