﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractRequestDocumentsServices
    {
        public abstract SuccessResult<AbstractRequestDocuments> RequestDocuments_Insert(AbstractRequestDocuments abstractRequestDocuments);
        public abstract SuccessResult<AbstractRequestDocuments> RequestDocuments_ById(long Id);
        public abstract PagedList<AbstractRequestDocuments> RequestDocuments_ByRequestId(PageParam pageParam, long RequestId);
        public abstract SuccessResult<AbstractRequestDocuments> RequestDocuments_Delete(long Id, long DeletedBy);
    }
}
