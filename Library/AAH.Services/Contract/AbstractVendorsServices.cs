﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractVendorsServices
    {
        public abstract SuccessResult<AbstractVendors> Vendors_Upsert(AbstractVendors abstractVendors);
        public abstract SuccessResult<AbstractVendorsThelaImages> VendorsThelaImages_Upsert(AbstractVendorsThelaImages abstractVendorsThelaImages);
        public abstract SuccessResult<AbstractVendors> Vendors_ById(long Id);
        public abstract SuccessResult<AbstractVendors> Vendor_SendOTP(string MobileNumber, string DeviceToken, long OTP);
        public abstract SuccessResult<AbstractVendors> Vendor_VerifyOtp(long Otp, long Id);
        public abstract SuccessResult<AbstractVendors> Vendors_ActInAct(long Id);
        public abstract SuccessResult<AbstractVendors> VendorsThelaImages_Delete(string Url, long vendorId);
        public abstract SuccessResult<AbstractVendors> Vendors_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractVendors> Vendors_All(PageParam pageParam, string Search);
        public abstract PagedList<AbstractVendors> Vendors_ByAdminId(PageParam pageParam, string search, long AdminId);

        // Vendors Vs Items
        public abstract SuccessResult<AbstractVendorsVsItems> VendorsVsItems_Upsert(AbstractVendorsVsItems abstractVendorsVsItems);
        public abstract SuccessResult<AbstractVendorsVsItems> VendorsVsItems_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractVendorsVsItems> VendorsVsItems_ActInAct(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractVendors> Vendor_IsOnline(long VendorId, int IsOnline);
        public abstract PagedList<AbstractVendorsVsItems> VendorsVsItems_ByVendorsId(PageParam pageParam, string Search, long VendorsId);
        //Vendors Vs References
        public abstract SuccessResult<AbstractVendorsVsReferences> VendorsVsReferences_Upsert(AbstractVendorsVsReferences abstractVendorsVsReferences);
        public abstract SuccessResult<AbstractVendorsVsReferences> VendorsVsReferences_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractVendorsVsReferences> VendorsVsReferences_ByVendorsId(PageParam pageParam, string Search, long VendorsId);

    }
}
