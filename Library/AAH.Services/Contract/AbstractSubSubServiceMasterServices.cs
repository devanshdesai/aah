﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractSubSubServiceMasterServices
    {
        public abstract SuccessResult<AbstractSubSubServiceMaster> SubSubServiceMaster_Upsert(AbstractSubSubServiceMaster abstractSubSubServiceMaster);
        public abstract PagedList<AbstractSubSubServiceMaster> SubSubServiceMaster_BySubServiceMasterId(PageParam pageParam, string search, long SubServiceMasterId);
        public abstract SuccessResult<AbstractSubSubServiceMaster> SubSubServiceMaster_ById(long Id);

    }
}
