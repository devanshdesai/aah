﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractDocumentTypeMasterServices
    {
        public abstract PagedList<AbstractDocumentTypeMaster> DocumentTypeMaster_All(PageParam pageParam, string search);
    }
}
