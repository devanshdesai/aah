﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractAgencyMasterServices
    {
        public abstract SuccessResult<AbstractAgencyMaster> AgencyMaster_Upsert(AbstractAgencyMaster abstractAgencyMaster);
        public abstract SuccessResult<AbstractAgencyMaster> AgencyMaster_ById(long Id);
        public abstract PagedList<AbstractAgencyMaster> AgencyMaster_All(PageParam pageParam, string Search);
    }
}
