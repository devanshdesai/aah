﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractMasterServicesServices
    {
        public abstract SuccessResult<AbstractMasterServices> MasterServices_Upsert(AbstractMasterServices abstractMasterServices);
        public abstract PagedList<AbstractMasterServices> MasterServices_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractMasterServices> MasterServices_ById(long Id);
        public abstract SuccessResult<AbstractMasterServices> MasterServices_Delete(long Id, long DeletedBy);
    }
}
