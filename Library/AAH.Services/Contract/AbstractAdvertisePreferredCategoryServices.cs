﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractAdvertisePreferredCategoryServices
    {
        public abstract SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Upsert(AbstractAdvertisePreferredCategory abstractAdvertisePreferredCategory);
        public abstract SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_ById(int Id);
        public abstract PagedList<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Delete(int Id, int DeletedBy);
    }
}
