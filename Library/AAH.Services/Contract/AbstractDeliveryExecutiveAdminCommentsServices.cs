﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractDeliveryExecutiveAdminCommentsServices
    {
        public abstract SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_Upsert(AbstractDeliveryExecutiveAdminComments abstractDeliveryExecutiveAdminComments);
        public abstract PagedList<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_All(PageParam pageParam, string search, long DeliveryExecutiveId, long AdminId);
        public abstract SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_ById(long Id);
        public abstract SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_Delete(long Id, long DeletedBy);
    }
}
