﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractUsersServices
    {
        public abstract SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers);
        public abstract SuccessResult<AbstractUsers> Users_SendOTP(string MobileNumber, string DeviceToken, string DeviceType, string UDID, string IMEI, long OTP);
        public abstract SuccessResult<AbstractUsers> Users_Logout(string DeviceToken, string UDID, string IMEI);
        public abstract SuccessResult<AbstractUsers> Users_VerifyOtp(long Id, string DeviceToken, long Otp, string UDID, string IMEI);
        public abstract SuccessResult<AbstractUsers> Users_ById(long Id);
        public abstract SuccessResult<AbstractUsers> Users_ActInAct(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractUsers> Users_IsPayable(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractUsers> Users_IsBlacklist(long Id, long UpdatedBy);
        public abstract PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, string UserName, string MobileNumber, string Email, int Gender, long IsActive,long DEId);
        public abstract PagedList<AbstractUserDevices> UserDevices_ByUserId(PageParam pageParam, string search, long UserId);
        public abstract PagedList<AbstractUserDevices> UserDevices_ClearDeviceToken(PageParam pageParam, string search, long UserId);
        public abstract SuccessResult<AbstractUsers> Users_ReferenceCode(long UserId, string ReferenceCode);
        public abstract SuccessResult<AbstractUsers> Users_Delete(long Id, long DeletedBy);
    }
}
