﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractAgencyVsAreaServices
    {
        public abstract SuccessResult<AbstractAgencyVsArea> AfterAgencyVsArea_Complete(long DeliveryExecutiveId , long AgencyVsAreaId);

        public abstract SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Upsert(AbstractAgencyVsArea abstractAgencyVsArea);
        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_ById(int Id);
        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_AssignedDE(PageParam pageParam, long AgencyId, long MasterAreaId);
        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractAgencyVsArea> AgencyVsArea_byAgencyId(PageParam pageParam, string search,long AgencyId);
        public abstract SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Delete(int Id, int AreaId);
        public abstract SuccessResult<AbstractAgencyVsArea> AgencyVsArea_ActInAct(int Id, int UpdatedBy);
    }
}
