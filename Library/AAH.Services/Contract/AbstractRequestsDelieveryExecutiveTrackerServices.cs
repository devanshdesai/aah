﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractRequestsDelieveryExecutiveTrackerServices
    {
        public abstract PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId(PageParam pageParam, string Search, long DeliveryExecutiveId);
        public abstract PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker_ByRequestId(PageParam pageParam, string Search, long RequestId);
    }
}
