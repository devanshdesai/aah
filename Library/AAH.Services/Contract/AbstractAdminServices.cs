﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractAdminServices
    {
        public abstract bool Admin_SignOut(long Id);
        public abstract SuccessResult<AbstractAdmin> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword);
        public abstract SuccessResult<AbstractAdmin> Admin_SignIn(string Email, string Password);
        public abstract SuccessResult<AbstractAdmin> Admin_ById(long Id);
        public abstract SuccessResult<AbstractAdmin> Admin_Dashboard(long Id);

        public abstract PagedList<AbstractAdmin> Admin_All(PageParam pageParam, string Search, string Name, string Email, string MobileNumber, long AdminTypeId, long IsActive);
        public abstract PagedList<AbstractAdmin> DownloadingStaff_TodayTarget(PageParam pageParam, string Fromdate, string Todate, long Id);
        public abstract PagedList<AbstractAdmin> OnBoardingStaff_TodayTarget(PageParam pageParam, string Fromdate, string Todate, long Id);
        public abstract SuccessResult<AbstractAdmin> Admin_Upsert(AbstractAdmin abstractAdmin);
        public abstract SuccessResult<AbstractAdmin> Admin_ActInAct(long Id, long UpdatedBy);

        // Admin Type
        public abstract PagedList<AbstractAdminType> AdminType_All(PageParam pageParam, string Search);
        public abstract PagedList<AbstractAdmin> GetAgency_ByAreaId(PageParam pageParam, string Search, string AreaId);
        public abstract PagedList<AbstractAreaVsStaff> Vendor_ValidArea(PageParam pageParam, string Search, long StaffId);
        public abstract PagedList<AbstractAdmin> GetAgency_ByIds(String Ids);
        public abstract SuccessResult<AbstractAdmin> GetAvailableDE_ByAgencyId(string Id, long AreaId = 0);

        public abstract PagedList<AbstractAdmin> AreaVsStaff_All(PageParam pageParam, string search, long StaffId);
        public abstract SuccessResult<AbstractAdmin> AreaVsStaff_Upsert(AbstractAreaVsStaff abstractAreaVsStaff);
        public abstract SuccessResult<AbstractAdmin> AreaVsStaff_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractAdmin> AreaVsStaff_ById(long Id);

    }
}
