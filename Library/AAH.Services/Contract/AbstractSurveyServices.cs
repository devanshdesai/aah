﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractSurveyServices
    {
        public abstract PagedList<AbstractSurvey> Survey_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractSurvey> Survey_Insert(AbstractSurvey abstractSurvey);

    }
}
