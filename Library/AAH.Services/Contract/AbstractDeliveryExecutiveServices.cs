﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractDeliveryExecutiveServices
    {
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_Upsert(AbstractDeliveryExecutive abstractDeliveryExecutive);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_SendOTP(string MobileNumber, string DeviceToken, string DeviceType, string UDID, string IMEI, long OTP);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_Logout(string DeviceToken, string UDID, string IMEI);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_VerifyOtp(long Id, string DeviceToken, long Otp, string DeviceType, string UDID, string IMEI);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_ActInAct(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsMarketingPerson(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractDeliveryExecutive> Dashboard_ById(long Id);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_ById(long Id);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_UpdateLatLong(long Id, string Latitude, string Longitude);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsApproved(long Id, long UpdatedBy);
        public abstract SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsAvailable(long DeliveryExecutiveId, int IsAvailable);
        public abstract PagedList<AbstractDeliveryExecutive> DeliveryExecutive_All(PageParam pageParam, string search, string UserName, string MobileNumber, int Gender, long IsActive, long IsApproved, long IsAvailable, long loginId = 0, long AgencyId = 0, long MasterAreaId = 0);
        public abstract PagedList<AbstractDeliveryExecutive> DeliveryExecutive_ByAgencyId(PageParam pageParam, string search, long AgencyId);
        public abstract PagedList<AbstractDeliveryExecutive> GetDeliveryExecutiveCountByAddressId(string AddressId);
        public abstract PagedList<AbstractDeliveryExecutive> Requests_NotAssignedDeliveryExecutive_All(PageParam pageParam, long RequestId);
        public abstract PagedList<AbstractDeliveryExecutive> DeliveryExecutive_ByDeliveryExecutiveId(PageParam pageParam, string search, long DeliveryExecutiveId);
        public abstract SuccessResult<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback_Insert(AbstractDeliveryExecutiveFeedback abstractDeliveryExecutiveFeedback);
        public abstract PagedList<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback_All(PageParam pageParam, string search, string FromDate, string ToDate, long DeliveryExecutiveId);
        public abstract PagedList<AbstractDeliveryExecutive> DeliveryExecutive_Reports(PageParam pageParam, string search);

    }
}
