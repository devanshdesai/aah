﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractServiceMasterServices
    {
        public abstract SuccessResult<AbstractServiceMaster> ServiceMaster_Upsert(AbstractServiceMaster abstractServiceMaster);
        public abstract PagedList<AbstractServiceMaster> ServiceMaster_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractServiceMaster> ServiceMaster_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractServiceMaster> ServiceMaster_ById(long Id);
    }
}
