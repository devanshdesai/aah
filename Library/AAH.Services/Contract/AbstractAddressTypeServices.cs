﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractAddressTypeServices
    {
        public abstract SuccessResult<AbstractAddressType> AddressType_Upsert(AbstractAddressType abstractAddressType);
        public abstract PagedList<AbstractAddressType> AddressType_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractAddressType> AddressType_ById(long Id);
        public abstract SuccessResult<AbstractAddressType> AddressType_Delete(long Id, long DeletedBy);
    }
}
