﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractMasterAgeGroupServices
    {
        public abstract SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_Upsert(AbstractMasterAgeGroup abstractMasterAgeGroup);
        public abstract SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_ById(long Id);
        public abstract PagedList<AbstractMasterAgeGroup> MasterAgeGroup_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_Delete(long Id, int DeletedBy);
    }
}
