﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractTrialVideosServices
    {
        public abstract SuccessResult<AbstractTrialVideos> AfterTrialVideos_Complete(long DeliveryExecutiveId , long TrialVideosId);

        public abstract SuccessResult<AbstractTrialVideos> TrialVideos_Upsert(AbstractTrialVideos abstractTrialVideos);
        public abstract SuccessResult<AbstractTrialVideos> TrialVideos_ById(int Id);
        public abstract PagedList<AbstractTrialVideos> TrialVideos_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractTrialVideos> TrialVideos_Delete(int Id, int DeletedBy);
        public abstract SuccessResult<AbstractTrialVideos> TrialVideos_ActInAct(int Id, int UpdatedBy);
    }
}
