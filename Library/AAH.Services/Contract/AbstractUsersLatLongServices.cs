﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractUsersLatLongServices
    {
        public abstract PagedList<AbstractUsersLatLong> UsersLatLong_ByUserId(PageParam pageparam, string Search, long UserId);
        public abstract PagedList<AbstractUsersLatLong> UsersLatLong_ByRequestId(PageParam pageparam, string Search, long RequestId);
        public abstract SuccessResult<AbstractUsersLatLong> UsersLatLong_Insert(string Json);

    }
}
