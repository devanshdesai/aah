﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractMasterCategoryServices
    {
        public abstract SuccessResult<AbstractMasterCategory> MasterCategory_Upsert(AbstractMasterCategory abstractMasterCategory);
        public abstract PagedList<AbstractMasterCategory> MasterCategory_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractMasterCategory> MasterCategory_ById(long Id);
        public abstract SuccessResult<AbstractMasterCategory> MasterCategory_Delete(long Id, long DeletedBy);
    }
}
