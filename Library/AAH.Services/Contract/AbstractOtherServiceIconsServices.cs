﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
namespace AAH.Services.Contract
{
    public abstract class AbstractOtherServiceIconsServices
    {
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ById(long Id);
        public abstract PagedList<AbstractOtherServiceIcons> OtherServiceIcons_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Upsert(AbstractOtherServiceIcons abstractOtherServiceIcons);
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ActInAct(long Id, int UpdatedBy);
        public abstract SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Delete(long Id, int DeletedBy);
    }
}
