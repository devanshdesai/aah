﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;

namespace AAH.Services.Contract
{
    public abstract class AbstractCategoryVsDeliveryExecutiveServices
    {
        public abstract SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_Upsert(AbstractCategoryVsDeliveryExecutive abstractCategoryVsDeliveryExecutive);
        public abstract SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_RequestId(PageParam pageParam, string Search, long RequestId);
        public abstract PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_DeliveryExecutiveId(PageParam pageParam, string Search, long DeliveryExecutiveId);
    }
}
