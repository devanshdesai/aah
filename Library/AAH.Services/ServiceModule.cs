﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AAH.Services
{
    using Autofac;
    using Data;
    using AAH.Services.Contract;
    
    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DataModule>();
            builder.RegisterType<V1.UsersServices>().As<AbstractUsersServices>().InstancePerDependency();
            builder.RegisterType<V1.TicketServices>().As<AbstractTicketServices>().InstancePerDependency();
            builder.RegisterType<V1.TicketCommentsServices>().As<AbstractTicketCommentsServices>().InstancePerDependency();
            builder.RegisterType<V1.AdvertiseLeadsServices>().As<AbstractAdvertiseLeadsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterAgeGroupServices>().As<AbstractMasterAgeGroupServices>().InstancePerDependency();
            builder.RegisterType<V1.SurveyServices>().As<AbstractSurveyServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryLimitServices>().As<AbstractDeliveryLimitServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterPayTypeServices>().As<AbstractMasterPayTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.OtherServiceIconsServices>().As<AbstractOtherServiceIconsServices>().InstancePerDependency();
            builder.RegisterType<V1.HowGinnyBuddyWorksServices>().As<AbstractHowGinnyBuddyWorksServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveHealthServices>().As<AbstractDeliveryExecutiveHealthServices>().InstancePerDependency();
            builder.RegisterType<V1.ResponderVsTrialVideosServices>().As<AbstractResponderVsTrialVideosServices>().InstancePerDependency();
            builder.RegisterType<V1.TrialVideosServices>().As<AbstractTrialVideosServices>().InstancePerDependency();
            builder.RegisterType<V1.AdvertisePreferredCategoryServices>().As<AbstractAdvertisePreferredCategoryServices>().InstancePerDependency();
            builder.RegisterType<V1.AdvertiseMasterServices>().As<AbstractAdvertiseMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.SellerMasterServices>().As<AbstractSellerMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.AddressTypeServices>().As<AbstractAddressTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.RequestDocumentsServices>().As<AbstractRequestDocumentsServices>().InstancePerDependency();
            builder.RegisterType<V1.AddressMasterServices>().As<AbstractAddressMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.CityMasterServices>().As<AbstractCityMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.StateMasterServices>().As<AbstractStateMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.CountryMasterServices>().As<AbstractCountryMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.ServiceMasterServices>().As<AbstractServiceMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.SubServiceMasterServices>().As<AbstractSubServiceMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveServices>().As<AbstractDeliveryExecutiveServices>().InstancePerDependency();
            builder.RegisterType<V1.DocumentTypeMasterServices>().As<AbstractDocumentTypeMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.AdminServices>().As<AbstractAdminServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveAdminCommentsServices>().As<AbstractDeliveryExecutiveAdminCommentsServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveDocumentsServices>().As<AbstractDeliveryExecutiveDocumentsServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveCheckInCheckOutServices>().As<AbstractDeliveryExecutiveCheckInCheckOutServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveLatLongServices>().As<AbstractDeliveryExecutiveLatLongServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupStatusServices>().As<AbstractLookupStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.UsersLatLongServices>().As<AbstractUsersLatLongServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveServices>().As<AbstractDeliveryExecutiveServices>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsServices>().As<AbstractUserNotificationsServices>().InstancePerDependency();
            builder.RegisterType<V1.DeliveryExecutiveNotificationsServices>().As<AbstractDeliveryExecutiveNotificationsServices>().InstancePerDependency();
            builder.RegisterType<V1.RequestsServices>().As<AbstractRequestsServices>().InstancePerDependency();
            builder.RegisterType<V1.RequestsStatusTrackerServices>().As<AbstractRequestsStatusTrackerServices>().InstancePerDependency();
            builder.RegisterType<V1.RequestsDelieveryExecutiveTrackerServices>().As<AbstractRequestsDelieveryExecutiveTrackerServices>().InstancePerDependency();
            builder.RegisterType<V1.SubSubServiceMasterServices>().As<AbstractSubSubServiceMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.TagsMasterServices>().As<AbstractTagsMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.UsersFeedbackServices>().As<AbstractUsersFeedbackServices>().InstancePerDependency();
            builder.RegisterType<V1.AgencyMasterServices>().As<AbstractAgencyMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterCategoryServices>().As<AbstractMasterCategoryServices>().InstancePerDependency();
            builder.RegisterType<V1.CategoryVsDeliveryExecutiveServices>().As<AbstractCategoryVsDeliveryExecutiveServices>().InstancePerDependency();
            builder.RegisterType<V1.AreaMasterServices>().As<AbstractAreaMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomersNotificationServices>().As<AbstractCustomersNotificationServices>().InstancePerDependency();
            builder.RegisterType<V1.AgencyVsAreaServices>().As<AbstractAgencyVsAreaServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterServicesServices>().As<AbstractMasterServicesServices>().InstancePerDependency();

            //Chai Nasta ===========================================

            builder.RegisterType<V1.VendorsServices>().As<AbstractVendorsServices>().InstancePerDependency();
            builder.RegisterType<V1.ItemsServices>().As<AbstractItemsServices>().InstancePerDependency();

            base.Load(builder);
        }
    }
}
