﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AdvertisePreferredCategoryServices : AbstractAdvertisePreferredCategoryServices
    {
        private AbstractAdvertisePreferredCategoryDao abstractAdvertisePreferredCategoryDao;
        public AdvertisePreferredCategoryServices(AbstractAdvertisePreferredCategoryDao abstractAdvertisePreferredCategoryDao)
        {
            this.abstractAdvertisePreferredCategoryDao = abstractAdvertisePreferredCategoryDao;
        }
        public override SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Upsert(AbstractAdvertisePreferredCategory abstractAdvertisePreferredCategory)
        {
            return this.abstractAdvertisePreferredCategoryDao.AdvertisePreferredCategory_Upsert(abstractAdvertisePreferredCategory);
        }
        public override SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_ById(int Id)
        {
            return this.abstractAdvertisePreferredCategoryDao.AdvertisePreferredCategory_ById(Id);
        }
        public override PagedList<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_All(PageParam pageParam, string search)
        {
            return this.abstractAdvertisePreferredCategoryDao.AdvertisePreferredCategory_All(pageParam, search);
        }
        public override SuccessResult<AbstractAdvertisePreferredCategory> AdvertisePreferredCategory_Delete(int Id, int DeletedBy)
        {
            return this.abstractAdvertisePreferredCategoryDao.AdvertisePreferredCategory_Delete(Id, DeletedBy);
        }
    }
}