﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class MasterAgeGroupServices : AbstractMasterAgeGroupServices
    {
        private AbstractMasterAgeGroupDao abstractMasterAgeGroupDao;
        public MasterAgeGroupServices(AbstractMasterAgeGroupDao abstractMasterAgeGroupDao)
        {
            this.abstractMasterAgeGroupDao = abstractMasterAgeGroupDao;
        }

        public override SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_Upsert(AbstractMasterAgeGroup abstractMasterAgeGroup)
        {
            return this.abstractMasterAgeGroupDao.MasterAgeGroup_Upsert(abstractMasterAgeGroup);
        }
        public override SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_ById(long Id)
        {
            return this.abstractMasterAgeGroupDao.MasterAgeGroup_ById(Id);
        }
        public override PagedList<AbstractMasterAgeGroup> MasterAgeGroup_All(PageParam pageParam, string search)
        {
            return this.abstractMasterAgeGroupDao.MasterAgeGroup_All(pageParam, search);
        }
        public override SuccessResult<AbstractMasterAgeGroup> MasterAgeGroup_Delete(long Id, int DeletedBy)
        {
            return this.abstractMasterAgeGroupDao.MasterAgeGroup_Delete(Id, DeletedBy);
        }
    }
}