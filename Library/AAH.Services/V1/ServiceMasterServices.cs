﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class ServiceMasterServices : AbstractServiceMasterServices
    {
        private AbstractServiceMasterDao abstractServiceMasterDao;

        public ServiceMasterServices(AbstractServiceMasterDao abstractServiceMasterDao)
        {
            this.abstractServiceMasterDao = abstractServiceMasterDao;
        }
        public override SuccessResult<AbstractServiceMaster> ServiceMaster_Delete(long Id, long DeletedBy)
        {
            return this.abstractServiceMasterDao.ServiceMaster_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractServiceMaster> ServiceMaster_ById(long Id)
        {
            return this.abstractServiceMasterDao.ServiceMaster_ById(Id);
        }
        public override PagedList<AbstractServiceMaster> ServiceMaster_All(PageParam pageParam, string search)
        {
            return this.abstractServiceMasterDao.ServiceMaster_All(pageParam, search);
        }
        public override SuccessResult<AbstractServiceMaster> ServiceMaster_Upsert(AbstractServiceMaster abstractServiceMaster)
        {
            return this.abstractServiceMasterDao.ServiceMaster_Upsert(abstractServiceMaster);
        }
    }
}