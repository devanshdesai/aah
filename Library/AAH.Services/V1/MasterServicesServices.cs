﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class MasterServicesServices : AbstractMasterServicesServices
    {
        private AbstractMasterServicesDao abstractMasterServicesDao;

        public MasterServicesServices(AbstractMasterServicesDao abstractMasterServicesDao)
        {
            this.abstractMasterServicesDao = abstractMasterServicesDao;
        }
        
        public override SuccessResult<AbstractMasterServices> MasterServices_ById(long Id)
        {
            return this.abstractMasterServicesDao.MasterServices_ById(Id);
        }
        public override SuccessResult<AbstractMasterServices> MasterServices_Delete(long Id, long DeletedBy)
        {
            return this.abstractMasterServicesDao.MasterServices_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractMasterServices> MasterServices_All(PageParam pageParam, string search)
        {
            return this.abstractMasterServicesDao.MasterServices_All(pageParam, search);
        }
        public override SuccessResult<AbstractMasterServices> MasterServices_Upsert(AbstractMasterServices abstractMasterServices)
        {
            return this.abstractMasterServicesDao.MasterServices_Upsert(abstractMasterServices);
        }
    }

    
}