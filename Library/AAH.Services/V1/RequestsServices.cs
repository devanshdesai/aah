﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class RequestsServices : AbstractRequestsServices
    {
        private AbstractRequestsDao abstractRequestsDao;

        public RequestsServices(AbstractRequestsDao abstractRequestsDao)
        {
            this.abstractRequestsDao = abstractRequestsDao;
        }
        public override SuccessResult<AbstractRequestcomments> Requestcomments_Insert(AbstractRequestcomments abstractRequestcomments)
        {
            return this.abstractRequestsDao.Requestcomments_Insert(abstractRequestcomments);
        }
        public override SuccessResult<AbstractRequests> Requests_Insert(AbstractRequests abstractRequests)
        {
            return this.abstractRequestsDao.Requests_Insert(abstractRequests);
        }

        public override PagedList<AbstractRequestcomments> Requestcomments_ByRequestId(PageParam pageParam, string Search, long RequestId)
        {
            return this.abstractRequestsDao.Requestcomments_ByRequestId(pageParam, Search, RequestId);
        }

        public override SuccessResult<AbstractRequests> Requests_AssignDeliveryExecutive(AbstractRequests abstractRequests)
        {
            return this.abstractRequestsDao.Requests_AssignDeliveryExecutive(abstractRequests);
        }

        public override PagedList<AbstractRequests> Requests_Attended(PageParam pageParam, string ActionFormDate, string ActionToDate)
        {
            return this.abstractRequestsDao.Requests_Attended(pageParam, ActionFormDate, ActionToDate);
        }

        public override SuccessResult<AbstractRequests> Request_UpdateStatus(long RequestId, long StatusId, long DeliveryExecutiveId, decimal AmountGiven,decimal RequestedAmount, long OTP = 0, string DECancleReason = "")
        {
            return this.abstractRequestsDao.Request_UpdateStatus(RequestId, StatusId, DeliveryExecutiveId, AmountGiven, RequestedAmount,OTP, DECancleReason);
        }
        public override SuccessResult<AbstractRequests> Requests_ById(long Id, long LoginId = 0)
        {
            return this.abstractRequestsDao.Requests_ById(Id,LoginId);
        }

        public override SuccessResult<AbstractRequests> Requests_UpdateIsSettledWithAgency(long AgencyId, string FromDate, string ToDate, long DeliveryExecutiveId, int IsSettledWithAgency)
        {
            return this.abstractRequestsDao.Requests_UpdateIsSettledWithAgency(AgencyId, FromDate, ToDate, DeliveryExecutiveId, IsSettledWithAgency);
        }

        public override SuccessResult<AbstractRequests> Requests_UpdateIsSettledWithAdmin(long Id, int IsSettledWithAdmin)
        {
            return this.abstractRequestsDao.Requests_UpdateIsSettledWithAdmin(Id, IsSettledWithAdmin);
        }

        public override SuccessResult<AbstractRequests> Requests_UpdateByAdmin(long RequestedId, long ServiceMasterTypeId, long MasterPayTypeId, decimal AmountRequested, long DeliveryExecutiveId)
        {
            return this.abstractRequestsDao.Requests_UpdateByAdmin(RequestedId, ServiceMasterTypeId, MasterPayTypeId, AmountRequested, DeliveryExecutiveId);
        }
         
        public override SuccessResult<AbstractRequests> Requests_UpdateByDeliveryExecutive(AbstractRequests abstractRequests)
        {
            return this.abstractRequestsDao.Requests_UpdateByDeliveryExecutive(abstractRequests);
        }

        public override SuccessResult<AbstractRequests> DeliveryExecutiveRequestAmount_Update(long Id, decimal DeliveryExecutiveRequestAmount)
        {
            return this.abstractRequestsDao.DeliveryExecutiveRequestAmount_Update(Id, DeliveryExecutiveRequestAmount);
        }

        public override SuccessResult<AbstractRequests> IsApprovedByUser_Update(long Id, int IsApprovedByUser)
        {
            return this.abstractRequestsDao.IsApprovedByUser_Update(Id, IsApprovedByUser);
        }

        public override PagedList<AbstractRequests> Requests_All(PageParam pageParam, string Search,
            long UserId, long DeliveryExecutiveId, long ServiceMasterId, long SubServiceMasterId, long StatusId, long loginId =0, long Type = 0,long FilterType = 0)
        {
            return this.abstractRequestsDao.Requests_All(pageParam, Search, 
                UserId, DeliveryExecutiveId, ServiceMasterId, SubServiceMasterId, StatusId,loginId, Type, FilterType);
        }
        
        public override PagedList<AbstractRequests> RequestsStatus_All(PageParam pageParam, string Search, long UserId,long StatusId)
        {
            return this.abstractRequestsDao.RequestsStatus_All(pageParam, Search,UserId, StatusId);
        }

        public override PagedList<AbstractRequests> GetRequestByDeliveryExecutiveId(PageParam pageParam, string Search = "",
            long DeliveryExecutiveId = 0, long StatusId = 0)
        {
            return this.abstractRequestsDao.GetRequestByDeliveryExecutiveId(pageParam, Search, 
                 DeliveryExecutiveId,StatusId);
        }
        public override PagedList<AbstractRequests> RequestByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractRequestsDao.RequestByUserId(pageParam, UserId);
        }
        public override PagedList<AbstractRequests> RequestByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId)
        {
            return this.abstractRequestsDao.RequestByDeliveryExecutiveId(pageParam, DeliveryExecutiveId);
        }
        public override SuccessResult<AbstractRequests> Review_ByUser(long Id, decimal RatingByUser, string ReviewByUser)
        {
            return this.abstractRequestsDao.Review_ByUser(Id, RatingByUser, ReviewByUser);
        }
        public override SuccessResult<AbstractRequests> Review_ByDeliveryExecutive(long Id, decimal RatingByDeliveryExecutive,
            string ReviewByDeliveryExecutive)
        {
            return this.abstractRequestsDao.Review_ByDeliveryExecutive(Id, RatingByDeliveryExecutive, ReviewByDeliveryExecutive);
        }
        public override PagedList<AbstractRequests> Requests_Urgent(PageParam pageParam, long UserId, long DeliveryExecutiveId)
        {
            return this.abstractRequestsDao.Requests_Urgent(pageParam, UserId, DeliveryExecutiveId);
        }
        public override SuccessResult<AbstractRequestsCount> Requests_ByStatusCount()
        {
            return this.abstractRequestsDao.Requests_ByStatusCount();
        }
        public override SuccessResult<AbstractRequestsReachTime> Requests_ReachTime(long Id)
        {
            return this.abstractRequestsDao.Requests_ReachTime(Id);
        }

        public override SuccessResult<AbstractRequests> Requests_UpdateEstimatedETA(long Id, DateTime EstimatedETA)
        {
            return this.abstractRequestsDao.Requests_UpdateEstimatedETA(Id, EstimatedETA);
        }
        public override SuccessResult<AbstractRequests> Update_TipAmount(long Id, bool IsGiveTip, decimal TipAmount)
        {
            return this.abstractRequestsDao.Update_TipAmount(Id, IsGiveTip,TipAmount);
        }
        public override PagedList<AbstractRequests> Report_All(PageParam pageParam, string search, string FromDate, string ToDate, long AgencyId, long DeliveryExecutiveId = 0, long IsSettled = 0)
        {
            return this.abstractRequestsDao.Report_All(pageParam, search, FromDate, ToDate, AgencyId, DeliveryExecutiveId, IsSettled);
        }
    }

}