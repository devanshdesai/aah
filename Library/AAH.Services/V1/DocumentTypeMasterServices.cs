﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;
    
namespace AAH.Services.V1
{
    public class DocumentTypeMasterServices : AbstractDocumentTypeMasterServices
    {
        private AbstractDocumentTypeMasterDao abstractDocumentTypeMasterDao;

        public DocumentTypeMasterServices(AbstractDocumentTypeMasterDao abstractDocumentTypeMasterDao)
        {
            this.abstractDocumentTypeMasterDao = abstractDocumentTypeMasterDao;
        }

        public override PagedList<AbstractDocumentTypeMaster> DocumentTypeMaster_All(PageParam pageParam, string search)
        {
            return this.abstractDocumentTypeMasterDao.DocumentTypeMaster_All(pageParam, search);
        }
    }
}
