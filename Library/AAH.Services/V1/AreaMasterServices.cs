﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AreaMasterServices : AbstractAreaMasterServices
    {
        private AbstractAreaMasterDao abstractAreaMasterDao;
        public AreaMasterServices(AbstractAreaMasterDao abstractAreaMasterDao)
        {
            this.abstractAreaMasterDao = abstractAreaMasterDao;
        }
        
        public override SuccessResult<AbstractAreaMaster> AreaMaster_ById(long Id)
        {
            return this.abstractAreaMasterDao.AreaMaster_ById(Id);
        }
        public override PagedList<AbstractAreaMaster> AreaMaster_All(PageParam pageParam, string search, long LoginId =0, long Type = 0)
        {
            return this.abstractAreaMasterDao.AreaMaster_All(pageParam, search,LoginId, Type);
        }
        public override PagedList<AbstractAreaMaster> AreaMaster_ByAgencyId(PageParam pageParam, string search, long AgencyId = 0)
        {
            return this.abstractAreaMasterDao.AreaMaster_ByAgencyId(pageParam, search, AgencyId);
        }
        public override SuccessResult<AbstractAreaMaster> AreaMaster_Upsert(AbstractAreaMaster abstractAreaMaster)
        {
            return this.abstractAreaMasterDao.AreaMaster_Upsert(abstractAreaMaster);
        }
        public override SuccessResult<AbstractAreaMaster> AreaMaster_Delete(long Id, int DeletedBy)
        {
            return this.abstractAreaMasterDao.AreaMaster_Delete(Id, DeletedBy);
        } 
        public override SuccessResult<AbstractAreaMaster> AreaMaster_ActInAct(long Id, long UpdatedBy)
        {
            return this.abstractAreaMasterDao.AreaMaster_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractAreaMaster> AreaMaster_AppUnApp(long Id, long UpdatedBy, int Type = 0)
        {
            return this.abstractAreaMasterDao.AreaMaster_AppUnApp(Id, UpdatedBy,Type);
        }
    }
}