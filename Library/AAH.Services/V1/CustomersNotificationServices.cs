﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class CustomersNotificationServices : AbstractCustomersNotificationServices
    {
        private AbstractCustomersNotificationDao abstractCustomersNotificationDao;

        public CustomersNotificationServices(AbstractCustomersNotificationDao abstractCustomersNotificationDao)
        {
            this.abstractCustomersNotificationDao = abstractCustomersNotificationDao;
        }
        
        public override SuccessResult<AbstractCustomersNotification> CustomersNotification_Upsert(AbstractCustomersNotification abstractCustomersNotification)
        {
            return this.abstractCustomersNotificationDao.CustomersNotification_Upsert(abstractCustomersNotification);
        }
    }

    
}