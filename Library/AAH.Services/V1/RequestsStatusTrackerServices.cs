﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class RequestsStatusTrackerServices : AbstractRequestsStatusTrackerServices
    {
        private AbstractRequestsStatusTrackerDao abstractRequestsStatusTrackerDao;

        public RequestsStatusTrackerServices(AbstractRequestsStatusTrackerDao abstractRequestsStatusTrackerDao)
        {
            this.abstractRequestsStatusTrackerDao = abstractRequestsStatusTrackerDao;
        }
        
        public override PagedList<AbstractRequestsStatusTracker> RequestsStatusTracker_ByRequestId(PageParam pageParam, string Search, long RequestId)
        {
            return this.abstractRequestsStatusTrackerDao.RequestsStatusTracker_ByRequestId(pageParam, Search, RequestId);
        }
    }

}