﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class UsersFeedbackServices : AbstractUsersFeedbackServices
    {
        private AbstractUsersFeedbackDao abstractUsersFeedbackDao;

        public UsersFeedbackServices(AbstractUsersFeedbackDao abstractUsersFeedbackDao)
        {
            this.abstractUsersFeedbackDao = abstractUsersFeedbackDao;
        }
       
        public override SuccessResult<AbstractUsersFeedback> UsersFeedback_Insert(AbstractUsersFeedback abstractUsersFeedback)
        {
            return this.abstractUsersFeedbackDao.UsersFeedback_Insert(abstractUsersFeedback);
        }
       
        public override PagedList<AbstractUsersFeedback> UsersFeedback_All(PageParam pageParam, string search, string FromDate, string ToDate,long UsersId)
        {
            return this.abstractUsersFeedbackDao.UsersFeedback_All(pageParam, search, FromDate, ToDate, UsersId);
        }
    }

}