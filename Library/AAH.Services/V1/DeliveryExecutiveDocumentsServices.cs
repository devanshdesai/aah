﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryExecutiveDocumentsServices : AbstractDeliveryExecutiveDocumentsServices
    {
        private AbstractDeliveryExecutiveDocumentsDao abstractDeliveryExecutiveDocumentsDao;

        public DeliveryExecutiveDocumentsServices(AbstractDeliveryExecutiveDocumentsDao abstractDeliveryExecutiveDocumentsDao)
        {
            this.abstractDeliveryExecutiveDocumentsDao = abstractDeliveryExecutiveDocumentsDao;
        }

        public override PagedList<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_ByDeliveryExecutiveId(PageParam pageparam, string Search, long DeliveryExecutiveId)
        {
            return abstractDeliveryExecutiveDocumentsDao.DeliveryExecutiveDocuments_ByDeliveryExecutiveId(pageparam, Search, DeliveryExecutiveId);
        }
        public override SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Verify(long Id, long AdminId)
        {
            return this.abstractDeliveryExecutiveDocumentsDao.DeliveryExecutiveDocuments_Verify(Id, AdminId);
        }
        public override SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Upsert(AbstractDeliveryExecutiveDocuments abstractDeliveryExecutiveDocuments)
        {
            return this.abstractDeliveryExecutiveDocumentsDao.DeliveryExecutiveDocuments_Upsert(abstractDeliveryExecutiveDocuments);
        }
        public override SuccessResult<AbstractDeliveryExecutiveDocuments> DeliveryExecutiveDocuments_Delete(long Id, long DeletedBy)
        {
            return this.abstractDeliveryExecutiveDocumentsDao.DeliveryExecutiveDocuments_Delete(Id, DeletedBy);
        }
    }

}