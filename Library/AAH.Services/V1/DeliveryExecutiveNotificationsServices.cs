﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryExecutiveNotificationsServices : AbstractDeliveryExecutiveNotificationsServices
    {
        private AbstractDeliveryExecutiveNotificationsDao abstractDeliveryExecutiveNotificationsDao;

        public DeliveryExecutiveNotificationsServices(AbstractDeliveryExecutiveNotificationsDao abstractDeliveryExecutiveNotificationsDao)
        {
            this.abstractDeliveryExecutiveNotificationsDao = abstractDeliveryExecutiveNotificationsDao;
        }
        public override SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_Insert(AbstractDeliveryExecutiveNotifications abstractDeliveryExecutiveNotifications)
        {
            return this.abstractDeliveryExecutiveNotificationsDao.DeliveryExecutiveNotifications_Insert(abstractDeliveryExecutiveNotifications);
        }
        
        public override PagedList<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_ByDeliveryExecutiveId(PageParam pageParam, long DeliveryExecutiveId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent)
        {
            return this.abstractDeliveryExecutiveNotificationsDao.DeliveryExecutiveNotifications_ByDeliveryExecutiveId(pageParam, DeliveryExecutiveId, RequestId, ActionFormDate, ActionToDate, IsSent);
        }

        public override PagedList<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_ByRequestId(PageParam pageParam, long RequestId)
        {
            return this.abstractDeliveryExecutiveNotificationsDao.DeliveryExecutiveNotifications_ByRequestId(pageParam, RequestId);
        }
        public override SuccessResult<AbstractDeliveryExecutiveNotifications> DeliveryExecutiveNotifications_RequestStatusUpdate(long Id, long StatusId)
        {
            return this.abstractDeliveryExecutiveNotificationsDao.DeliveryExecutiveNotifications_RequestStatusUpdate(Id, StatusId);
        }
    }
}