﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class CategoryVsDeliveryExecutiveServices : AbstractCategoryVsDeliveryExecutiveServices
    {
        private AbstractCategoryVsDeliveryExecutiveDao abstractCategoryVsDeliveryExecutiveDao;

        public CategoryVsDeliveryExecutiveServices(AbstractCategoryVsDeliveryExecutiveDao abstractCategoryVsDeliveryExecutiveDao)
        {
            this.abstractCategoryVsDeliveryExecutiveDao = abstractCategoryVsDeliveryExecutiveDao;
        }
        
        public override SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_Delete(long Id, long DeletedBy)
        {
            return this.abstractCategoryVsDeliveryExecutiveDao.CategoryVsDeliveryExecutive_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_RequestId(PageParam pageParam, string search , long RequestId)
        {
            return this.abstractCategoryVsDeliveryExecutiveDao.CategoryVsDeliveryExecutive_RequestId(pageParam, search, RequestId);
        }
        public override PagedList<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_DeliveryExecutiveId(PageParam pageParam, string search, long DeliveryExecutiveId)
        {
            return this.abstractCategoryVsDeliveryExecutiveDao.CategoryVsDeliveryExecutive_DeliveryExecutiveId(pageParam, search, DeliveryExecutiveId);
        }
        public override SuccessResult<AbstractCategoryVsDeliveryExecutive> CategoryVsDeliveryExecutive_Upsert(AbstractCategoryVsDeliveryExecutive abstractCategoryVsDeliveryExecutive)
        {
            return this.abstractCategoryVsDeliveryExecutiveDao.CategoryVsDeliveryExecutive_Upsert(abstractCategoryVsDeliveryExecutive);
        }
    }

    
}