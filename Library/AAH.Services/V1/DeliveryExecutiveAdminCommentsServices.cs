﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryExecutiveAdminCommentsServices : AbstractDeliveryExecutiveAdminCommentsServices
    {
        private AbstractDeliveryExecutiveAdminCommentsDao abstractDeliveryExecutiveAdminCommentsDao;
        public DeliveryExecutiveAdminCommentsServices(AbstractDeliveryExecutiveAdminCommentsDao abstractDeliveryExecutiveAdminCommentsDao)
        {
            this.abstractDeliveryExecutiveAdminCommentsDao = abstractDeliveryExecutiveAdminCommentsDao;
        }

        public override SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_Upsert(AbstractDeliveryExecutiveAdminComments abstractDeliveryExecutiveAdminComments)
        {
            return this.abstractDeliveryExecutiveAdminCommentsDao.DeliveryExecutiveAdminComments_Upsert(abstractDeliveryExecutiveAdminComments);
        }

        public override PagedList<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_All(PageParam pageParam, string search, long DeliveryExecutiveId, long AdminId)
        {
            return this.abstractDeliveryExecutiveAdminCommentsDao.DeliveryExecutiveAdminComments_All(pageParam, search, DeliveryExecutiveId, AdminId);
        }

        public override SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_ById(long Id)
        {
            return this.abstractDeliveryExecutiveAdminCommentsDao.DeliveryExecutiveAdminComments_ById(Id);
        }

        public override SuccessResult<AbstractDeliveryExecutiveAdminComments> DeliveryExecutiveAdminComments_Delete(long Id, long DeletedBy)
        {
            return this.abstractDeliveryExecutiveAdminCommentsDao.DeliveryExecutiveAdminComments_Delete(Id, DeletedBy);
        }

    }
}