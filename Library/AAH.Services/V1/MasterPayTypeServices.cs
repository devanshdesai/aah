﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class MasterPayTypeServices : AbstractMasterPayTypeServices
    {
        private AbstractMasterPayTypeDao abstractMasterPayTypeDao;
        public MasterPayTypeServices(AbstractMasterPayTypeDao abstractMasterPayTypeDao)
        {
            this.abstractMasterPayTypeDao = abstractMasterPayTypeDao;
        }

        public override SuccessResult<AbstractMasterPayType> MasterPayType_Upsert(AbstractMasterPayType abstractMasterPayType)
        {
            return this.abstractMasterPayTypeDao.MasterPayType_Upsert(abstractMasterPayType);
        }
        public override SuccessResult<AbstractMasterPayType> MasterPayType_ById(long Id)
        {
            return this.abstractMasterPayTypeDao.MasterPayType_ById(Id);
        }
        public override PagedList<AbstractMasterPayType> MasterPayType_All(PageParam pageParam, string search)
        {
            return this.abstractMasterPayTypeDao.MasterPayType_All(pageParam, search);
        }
        public override SuccessResult<AbstractMasterPayType> MasterPayType_ActInAct(long Id, int UpdatedBy)
        {
            return this.abstractMasterPayTypeDao.MasterPayType_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractMasterPayType> MasterPayType_Delete(long Id, int DeletedBy)
        {
            return this.abstractMasterPayTypeDao.MasterPayType_Delete(Id, DeletedBy);
        }
    }
}