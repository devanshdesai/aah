﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Data.V1;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using Microsoft.ApplicationInsights;

namespace AAH.Services.V1
{
    public class AspNetUsersServices_Token
    {
        private AspNetUsersDao_Token aspNetUsersDao_Token = new AspNetUsersDao_Token();
        TelemetryClient client = new TelemetryClient();

        public async Task<SuccessResult<AbstractUsers>> AspNetUsers_VerifyUsernameEmail(AbstractUsers abstractAspNetUsers)
        {
            var result = aspNetUsersDao_Token.AspNetUsers_VerifyUsernameEmail(abstractAspNetUsers);

            // If user is deactivated then do not allow to generate token.
            if (result != null && result.Item != null && result.Item.IsActive != 1)
            {
                result.Item = null;
                result.Code = 400;
                result.Message = "User not found !";
                return result;
            }
            
            return result;
        }
    }
}