﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryExecutiveCheckInCheckOutServices : AbstractDeliveryExecutiveCheckInCheckOutServices
    {
        private AbstractDeliveryExecutiveCheckInCheckOutDao abstractDeliveryExecutiveCheckInCheckOutDao;

        public DeliveryExecutiveCheckInCheckOutServices(AbstractDeliveryExecutiveCheckInCheckOutDao abstractDeliveryExecutiveCheckInCheckOutDao)
        {
            this.abstractDeliveryExecutiveCheckInCheckOutDao = abstractDeliveryExecutiveCheckInCheckOutDao;
        }
        public override SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_ById(long Id)
        {
            return this.abstractDeliveryExecutiveCheckInCheckOutDao.DeliveryExecutiveCheckInCheckOut_ById(Id);
        }
        public override PagedList<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId(PageParam pageParam,
            long DeliveryExecutiveId, string ActionFormDate, string ActionToDate, long TypeId)
        {
            return this.abstractDeliveryExecutiveCheckInCheckOutDao.DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId(pageParam, 
                DeliveryExecutiveId, ActionFormDate, ActionToDate, TypeId);
        }

        public override SuccessResult<AbstractDeliveryExecutiveCheckInCheckOut> DeliveryExecutiveCheckInCheckOut_Upsert(AbstractDeliveryExecutiveCheckInCheckOut abstractDeliveryExecutiveCheckInCheckOut)
        {
            return this.abstractDeliveryExecutiveCheckInCheckOutDao.DeliveryExecutiveCheckInCheckOut_Upsert(abstractDeliveryExecutiveCheckInCheckOut);
        }
    }
}
