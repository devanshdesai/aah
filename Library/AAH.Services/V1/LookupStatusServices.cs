﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class LookupStatusServices : AbstractLookupStatusServices
    {
        private AbstractLookupStatusDao abstractLookupStatusDao;

        public LookupStatusServices(AbstractLookupStatusDao abstractLookupStatusDao)
        {
            this.abstractLookupStatusDao = abstractLookupStatusDao;
        }

        public override PagedList<AbstractLookupStatus> LookupStatus_All(PageParam pageParam, string search)
        {
            return this.abstractLookupStatusDao.LookupStatus_All(pageParam, search);
        }
    }
}
