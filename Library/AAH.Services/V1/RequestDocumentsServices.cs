﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;


namespace AAH.Services.V1
{
    public class RequestDocumentsServices : AbstractRequestDocumentsServices
    {
        private AbstractRequestDocumentsDao abstractRequestDocumentsDao;

        public RequestDocumentsServices(AbstractRequestDocumentsDao abstractRequestDocumentsDao)
        {
            this.abstractRequestDocumentsDao = abstractRequestDocumentsDao;
        }
        public override SuccessResult<AbstractRequestDocuments> RequestDocuments_Insert(AbstractRequestDocuments abstractRequestDocuments)
        {
            return this.abstractRequestDocumentsDao.RequestDocuments_Insert(abstractRequestDocuments);
        }
        public override SuccessResult<AbstractRequestDocuments> RequestDocuments_ById(long Id)
        {
            return this.abstractRequestDocumentsDao.RequestDocuments_ById(Id);
        }
        public override PagedList<AbstractRequestDocuments> RequestDocuments_ByRequestId(PageParam pageParam, long RequestId)
        {
            return this.abstractRequestDocumentsDao.RequestDocuments_ByRequestId(pageParam, RequestId);
        }
        public override SuccessResult<AbstractRequestDocuments> RequestDocuments_Delete(long Id, long DeletedBy)
        {
            return this.abstractRequestDocumentsDao.RequestDocuments_Delete(Id, DeletedBy);
        }
        
       
    }
}
