﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AdvertiseLeadsServices : AbstractAdvertiseLeadsServices
    {
        private AbstractAdvertiseLeadsDao abstractAdvertiseLeadsDao;
        public AdvertiseLeadsServices(AbstractAdvertiseLeadsDao abstractAdvertiseLeadsDao)
        {
            this.abstractAdvertiseLeadsDao = abstractAdvertiseLeadsDao;
        }

        
        public override PagedList<AbstractAdvertiseLeads> AdvertiseLeads_All(PageParam pageParam, string search)
        {
            return this.abstractAdvertiseLeadsDao.AdvertiseLeads_All(pageParam, search);
        }
        public override PagedList<AbstractAdvertiseLeads> AdvertiseLeads_ByRequestId(PageParam pageParam, string search,long AdvertiseId)
        {
            return this.abstractAdvertiseLeadsDao.AdvertiseLeads_ByRequestId(pageParam, search, AdvertiseId);
        }
        public override SuccessResult<AbstractAdvertiseLeads> AdvertiseLeads_Upsert(AbstractAdvertiseLeads abstractAdvertiseLeads)
        {
            return this.abstractAdvertiseLeadsDao.AdvertiseLeads_Upsert(abstractAdvertiseLeads);
        }
       
    }
}