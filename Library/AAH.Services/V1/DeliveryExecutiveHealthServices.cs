﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryExecutiveHealthServices : AbstractDeliveryExecutiveHealthServices
    {
        private AbstractDeliveryExecutiveHealthDao abstractDeliveryExecutiveHealthDao;
        public DeliveryExecutiveHealthServices(AbstractDeliveryExecutiveHealthDao abstractDeliveryExecutiveHealthDao)
        {
            this.abstractDeliveryExecutiveHealthDao = abstractDeliveryExecutiveHealthDao;
        }

        
        public override SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ById(long Id)
        {
            return this.abstractDeliveryExecutiveHealthDao.DeliveryExecutiveHealth_ById(Id);
        }
        public override SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ByDeliveryExecutiveId(long Id)
        {
            return this.abstractDeliveryExecutiveHealthDao.DeliveryExecutiveHealth_ByDeliveryExecutiveId(Id);
        }
        public override PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_All(PageParam pageParam, string search)
        {
            return this.abstractDeliveryExecutiveHealthDao.DeliveryExecutiveHealth_All(pageParam, search);
        }
        public override PagedList<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_ByUserId(PageParam pageParam, string search,int UserId,int DeliveryExecutiveId)
        {
            return this.abstractDeliveryExecutiveHealthDao.DeliveryExecutiveHealth_ByUserId(pageParam, search, UserId, DeliveryExecutiveId);
        }
        public override SuccessResult<AbstractDeliveryExecutiveHealth> DeliveryExecutiveHealth_Upsert(AbstractDeliveryExecutiveHealth abstractDeliveryExecutiveHealth)
        {
            return this.abstractDeliveryExecutiveHealthDao.DeliveryExecutiveHealth_Upsert(abstractDeliveryExecutiveHealth);
        }

    }
}