﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class RequestsDelieveryExecutiveTrackerServices : AbstractRequestsDelieveryExecutiveTrackerServices
    {
        private AbstractRequestsDelieveryExecutiveTrackerDao abstractRequestsDelieveryExecutiveTrackerDao;

        public RequestsDelieveryExecutiveTrackerServices(AbstractRequestsDelieveryExecutiveTrackerDao abstractRequestsDelieveryExecutiveTrackerDao)
        {
            this.abstractRequestsDelieveryExecutiveTrackerDao = abstractRequestsDelieveryExecutiveTrackerDao;
        }
        
        public override PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId(PageParam pageParam, string Search, long DeliveryExecutiveId)
        {
            return this.abstractRequestsDelieveryExecutiveTrackerDao.RequestsDelieveryExecutiveTracker_ByDeliveryExecutiveId(pageParam, Search, DeliveryExecutiveId);
        }

        public override PagedList<AbstractRequestsDelieveryExecutiveTracker> RequestsDelieveryExecutiveTracker_ByRequestId(PageParam pageParam, string Search, long RequestId)
        {
            return this.abstractRequestsDelieveryExecutiveTrackerDao.RequestsDelieveryExecutiveTracker_ByRequestId(pageParam, Search, RequestId);
        }

    }

}