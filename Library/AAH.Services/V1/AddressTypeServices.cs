﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AddressTypeServices : AbstractAddressTypeServices
    {
        private AbstractAddressTypeDao abstractAddressTypeDao;

        public AddressTypeServices(AbstractAddressTypeDao abstractAddressTypeDao)
        {
            this.abstractAddressTypeDao = abstractAddressTypeDao;
        }
        
        public override SuccessResult<AbstractAddressType> AddressType_ById(long Id)
        {
            return this.abstractAddressTypeDao.AddressType_ById(Id);
        }
        public override SuccessResult<AbstractAddressType> AddressType_Delete(long Id, long DeletedBy)
        {
            return this.abstractAddressTypeDao.AddressType_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractAddressType> AddressType_All(PageParam pageParam, string search)
        {
            return this.abstractAddressTypeDao.AddressType_All(pageParam, search);
        }
        public override SuccessResult<AbstractAddressType> AddressType_Upsert(AbstractAddressType abstractAddressType)
        {
            return this.abstractAddressTypeDao.AddressType_Upsert(abstractAddressType);
        }
    }

    
}