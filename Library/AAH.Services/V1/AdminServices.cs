﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AdminServices : AbstractAdminServices
    {
        private AbstractAdminDao abstractAdminDao;
        public AdminServices(AbstractAdminDao abstractAdminDao)
        {
            this.abstractAdminDao = abstractAdminDao;
        }

        public override bool Admin_SignOut(long Id)
        {
            return this.abstractAdminDao.Admin_SignOut(Id);
        }
        public override SuccessResult<AbstractAdmin> Admin_SignIn(string Email, string Password)
        {
            return this.abstractAdminDao.Admin_SignIn(Email, Password);
        }
        public override SuccessResult<AbstractAdmin> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            return this.abstractAdminDao.Admin_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
        }

        public override SuccessResult<AbstractAdmin> Admin_ById(long Id)
        {
            return this.abstractAdminDao.Admin_ById(Id);
        }
        public override SuccessResult<AbstractAdmin> Admin_Dashboard(long Id)
        {
            return this.abstractAdminDao.Admin_Dashboard(Id);
        }
        public override PagedList<AbstractAdmin> Admin_All(PageParam pageParam, string Search, string Name, string Email, string MobileNumber, long AdminTypeId, long IsActive)
        {
            return this.abstractAdminDao.Admin_All(pageParam, Search, Name,Email,MobileNumber,AdminTypeId,IsActive);
        }
        public override PagedList<AbstractAdmin> DownloadingStaff_TodayTarget(PageParam pageParam, string Fromdate, string Todate, long Id)
        {
            return this.abstractAdminDao.DownloadingStaff_TodayTarget(pageParam, Fromdate, Todate, Id); 
        }
        public override PagedList<AbstractAdmin> OnBoardingStaff_TodayTarget(PageParam pageParam, string Fromdate, string Todate, long Id)
        {
            return this.abstractAdminDao.OnBoardingStaff_TodayTarget(pageParam, Fromdate, Todate, Id);
        }
        public override SuccessResult<AbstractAdmin> Admin_Upsert(AbstractAdmin abstractAdmin)
        {
            return this.abstractAdminDao.Admin_Upsert(abstractAdmin);
        }
        public override SuccessResult<AbstractAdmin> Admin_ActInAct(long Id, long UpdatedBy)
        {
            return this.abstractAdminDao.Admin_ActInAct(Id, UpdatedBy);
        }

        // Admin Type
        public override PagedList<AbstractAdminType> AdminType_All(PageParam pageParam, string search)
        {
            return this.abstractAdminDao.AdminType_All(pageParam, search);
        }
        public override PagedList<AbstractAdmin> GetAgency_ByAreaId(PageParam pageParam, string Search, string AreaId)
        {
            return this.abstractAdminDao.GetAgency_ByAreaId(pageParam, Search, AreaId);
        }
        public override PagedList<AbstractAreaVsStaff> Vendor_ValidArea(PageParam pageParam, string Search, long StaffId)
        {
            return this.abstractAdminDao.Vendor_ValidArea(pageParam, Search, StaffId);
        }
        public override PagedList<AbstractAdmin> GetAgency_ByIds(string Ids)
        {
            return this.abstractAdminDao.GetAgency_ByIds(Ids);
        }
        public override SuccessResult<AbstractAdmin> GetAvailableDE_ByAgencyId(string Id, long AreaId = 0)
        {
            return this.abstractAdminDao.GetAvailableDE_ByAgencyId(Id,AreaId);
        }

        public override SuccessResult<AbstractAdmin> AreaVsStaff_Upsert(AbstractAreaVsStaff abstractAreaVsStaff)
        {
            return this.abstractAdminDao.AreaVsStaff_Upsert(abstractAreaVsStaff);
        }
        public override PagedList<AbstractAdmin> AreaVsStaff_All(PageParam pageParam, string search,long StaffId)
        {
            return this.abstractAdminDao.AreaVsStaff_All(pageParam, search, StaffId);
        }
        public override SuccessResult<AbstractAdmin> AreaVsStaff_Delete(long Id, long DeletedBy)
        {
            return this.abstractAdminDao.AreaVsStaff_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractAdmin> AreaVsStaff_ById(long Id)
        {
            return this.abstractAdminDao.AreaVsStaff_ById(Id);
        }
    }
}