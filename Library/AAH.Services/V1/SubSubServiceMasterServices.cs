﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class SubSubServiceMasterServices : AbstractSubSubServiceMasterServices
    {
        private AbstractSubSubServiceMasterDao abstractSubSubServiceMasterDao;

        public SubSubServiceMasterServices(AbstractSubSubServiceMasterDao abstractSubSubServiceMasterDao)
        {
            this.abstractSubSubServiceMasterDao = abstractSubSubServiceMasterDao;
        }
        public override SuccessResult<AbstractSubSubServiceMaster> SubSubServiceMaster_ById(long Id)
        {
            return this.abstractSubSubServiceMasterDao.SubSubServiceMaster_ById(Id);
        }
       
        public override PagedList<AbstractSubSubServiceMaster> SubSubServiceMaster_BySubServiceMasterId(PageParam pageParam, string search, long SubServiceMasterId)
        {
            return this.abstractSubSubServiceMasterDao.SubSubServiceMaster_BySubServiceMasterId(pageParam, search, SubServiceMasterId);
        }
        public override SuccessResult<AbstractSubSubServiceMaster> SubSubServiceMaster_Upsert(AbstractSubSubServiceMaster abstractSubSubServiceMaster)
        {
            return this.abstractSubSubServiceMasterDao.SubSubServiceMaster_Upsert(abstractSubSubServiceMaster);
        }
    }
}