﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AgencyMasterServices : AbstractAgencyMasterServices
    {
        private AbstractAgencyMasterDao abstractAgencyMasterDao;
        public AgencyMasterServices(AbstractAgencyMasterDao abstractAgencyMasterDao)
        {
            this.abstractAgencyMasterDao = abstractAgencyMasterDao;
        }

       
        public override SuccessResult<AbstractAgencyMaster> AgencyMaster_ById(long Id)
        {
            return this.abstractAgencyMasterDao.AgencyMaster_ById(Id);
        }
        public override PagedList<AbstractAgencyMaster> AgencyMaster_All(PageParam pageParam, string Search)
        {
            return this.abstractAgencyMasterDao.AgencyMaster_All(pageParam, Search);
        }
        public override SuccessResult<AbstractAgencyMaster> AgencyMaster_Upsert(AbstractAgencyMaster abstractAgencyMaster)
        {
            return this.abstractAgencyMasterDao.AgencyMaster_Upsert(abstractAgencyMaster);
        }
        
    }
}