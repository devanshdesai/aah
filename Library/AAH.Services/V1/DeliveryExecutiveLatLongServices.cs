﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryExecutiveLatLongServices : AbstractDeliveryExecutiveLatLongServices
    {
        private AbstractDeliveryExecutiveLatLongDao abstractDeliveryExecutiveLatLongDao;

        public DeliveryExecutiveLatLongServices(AbstractDeliveryExecutiveLatLongDao abstractDeliveryExecutiveLatLongDao)
        {
            this.abstractDeliveryExecutiveLatLongDao = abstractDeliveryExecutiveLatLongDao;
        }
        public override PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_ByDeliveryExecutiveId(PageParam pageParam, string search, 
            long DeliveryExecutiveId, string ActionFormDate, string ActionToDate, string ActionFormTime, string ActionToTime)
        {
            return this.abstractDeliveryExecutiveLatLongDao.DeliveryExecutiveLatLong_ByDeliveryExecutiveId(pageParam, search, DeliveryExecutiveId,
                ActionFormDate, ActionToDate, ActionFormTime, ActionToTime);
        }
        public override PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_ByRequestId(PageParam pageParam, string search, long RequestId)
        {
            return this.abstractDeliveryExecutiveLatLongDao.DeliveryExecutiveLatLong_ByRequestId(pageParam, search, RequestId);
        }
        public override PagedList<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_Map()
        {
            return this.abstractDeliveryExecutiveLatLongDao.DeliveryExecutiveLatLong_Map();
        }
        public override SuccessResult<AbstractDeliveryExecutiveLatLong> DeliveryExecutiveLatLong_Insert(string Json)
        {
            return this.abstractDeliveryExecutiveLatLongDao.DeliveryExecutiveLatLong_Insert(Json);
        }

        public override PagedList<AbstractOnboarderLatLong> OnboarderLatLong_ByAdminId(PageParam pageParam, string search,
            long AdminId, string ActionFormDate, string ActionToDate, string ActionFormTime, string ActionToTime)
        {
            return this.abstractDeliveryExecutiveLatLongDao.OnboarderLatLong_ByAdminId(pageParam, search, AdminId,
                ActionFormDate, ActionToDate, ActionFormTime, ActionToTime);
        }
        public override SuccessResult<AbstractOnboarderLatLong> OnboarderLatLong_Insert(string Json)
        {
            return this.abstractDeliveryExecutiveLatLongDao.OnboarderLatLong_Insert(Json);
        }
    }
   }

