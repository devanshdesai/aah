﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class HowGinnyBuddyWorksServices : AbstractHowGinnyBuddyWorksServices
    {
        private AbstractHowGinnyBuddyWorksDao abstractHowGinnyBuddyWorksDao;
        public HowGinnyBuddyWorksServices(AbstractHowGinnyBuddyWorksDao abstractHowGinnyBuddyWorksDao)
        {
            this.abstractHowGinnyBuddyWorksDao = abstractHowGinnyBuddyWorksDao;
        }
        
        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_ById(long Id)
        {
            return this.abstractHowGinnyBuddyWorksDao.HowGinnyBuddyWorks_ById(Id);
        }
        public override PagedList<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_All(PageParam pageParam, string search)
        {
            return this.abstractHowGinnyBuddyWorksDao.HowGinnyBuddyWorks_All(pageParam, search);
        }
        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_Upsert(AbstractHowGinnyBuddyWorks abstractHowGinnyBuddyWorks)
        {
            return this.abstractHowGinnyBuddyWorksDao.HowGinnyBuddyWorks_Upsert(abstractHowGinnyBuddyWorks);
        }
        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_ActInAct(long Id, int UpdatedBy)
        {
            return this.abstractHowGinnyBuddyWorksDao.HowGinnyBuddyWorks_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractHowGinnyBuddyWorks> HowGinnyBuddyWorks_Delete(long Id, int DeletedBy)
        {
            return this.abstractHowGinnyBuddyWorksDao.HowGinnyBuddyWorks_Delete(Id, DeletedBy);
        }
    }
}