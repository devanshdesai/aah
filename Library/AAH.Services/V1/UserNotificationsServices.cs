﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class UserNotificationsServices : AbstractUserNotificationsServices
    {
        private AbstractUserNotificationsDao abstractUserNotificationsDao;

        public UserNotificationsServices(AbstractUserNotificationsDao abstractUserNotificationsDao)
        {
            this.abstractUserNotificationsDao = abstractUserNotificationsDao;
        }
        public override SuccessResult<AbstractUserNotifications> UserNotifications_Insert(AbstractUserNotifications abstractUserNotifications)
        {
            return this.abstractUserNotificationsDao.UserNotifications_Insert(abstractUserNotifications);
        }
        
        public override PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ByUserId(pageParam, UserId, RequestId, ActionFormDate, ActionToDate, IsSent);
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByRequestId(PageParam pageParam, long RequestId, long UserId)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ByRequestId(pageParam, RequestId, UserId);
        }
        
    }
}