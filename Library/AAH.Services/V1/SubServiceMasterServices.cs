﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class SubServiceMasterServices : AbstractSubServiceMasterServices
    {
        private AbstractSubServiceMasterDao abstractSubServiceMasterDao;

        public SubServiceMasterServices(AbstractSubServiceMasterDao abstractSubServiceMasterDao)
        {
            this.abstractSubServiceMasterDao = abstractSubServiceMasterDao;
        }
        public override SuccessResult<AbstractSubServiceMaster> SubServiceMaster_ById(long Id)
        {
            return this.abstractSubServiceMasterDao.SubServiceMaster_ById(Id);
        }
        public override SuccessResult<AbstractSubServiceMaster> SubServiceMaster_Delete(long Id, long DeletedBy)
        {
            return this.abstractSubServiceMasterDao.SubServiceMaster_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractSubServiceMaster> SubServiceMaster_ByServiceMasterId(PageParam pageParam, string search, long ServiceMasterId)
        {
            return this.abstractSubServiceMasterDao.SubServiceMaster_ByServiceMasterId(pageParam, search, ServiceMasterId);
        }
        public override SuccessResult<AbstractSubServiceMaster> SubServiceMaster_Upsert(AbstractSubServiceMaster abstractSubServiceMaster)
        {
            return this.abstractSubServiceMasterDao.SubServiceMaster_Upsert(abstractSubServiceMaster);
        }
    }
}