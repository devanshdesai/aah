﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class MasterCategoryServices : AbstractMasterCategoryServices
    {
        private AbstractMasterCategoryDao abstractMasterCategoryDao;

        public MasterCategoryServices(AbstractMasterCategoryDao abstractMasterCategoryDao)
        {
            this.abstractMasterCategoryDao = abstractMasterCategoryDao;
        }
        
        public override SuccessResult<AbstractMasterCategory> MasterCategory_ById(long Id)
        {
            return this.abstractMasterCategoryDao.MasterCategory_ById(Id);
        }
        public override SuccessResult<AbstractMasterCategory> MasterCategory_Delete(long Id, long DeletedBy)
        {
            return this.abstractMasterCategoryDao.MasterCategory_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractMasterCategory> MasterCategory_All(PageParam pageParam, string search)
        {
            return this.abstractMasterCategoryDao.MasterCategory_All(pageParam, search);
        }
        public override SuccessResult<AbstractMasterCategory> MasterCategory_Upsert(AbstractMasterCategory abstractMasterCategory)
        {
            return this.abstractMasterCategoryDao.MasterCategory_Upsert(abstractMasterCategory);
        }
    }

}