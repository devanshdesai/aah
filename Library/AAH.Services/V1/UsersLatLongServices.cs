﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class UsersLatLongServices : AbstractUsersLatLongServices
    {
        private AbstractUsersLatLongDao abstractUsersLatLongDao;

        public UsersLatLongServices(AbstractUsersLatLongDao abstractUsersLatLongDao)
        {
            this.abstractUsersLatLongDao = abstractUsersLatLongDao;
        }
        public override PagedList<AbstractUsersLatLong> UsersLatLong_ByUserId(PageParam pageparam, string Search, long UserId)
        {
            return this.abstractUsersLatLongDao.UsersLatLong_ByUserId(pageparam, Search, UserId);
        }
        public override PagedList<AbstractUsersLatLong> UsersLatLong_ByRequestId(PageParam pageParam, string search, long RequestId)
        {
            return this.abstractUsersLatLongDao.UsersLatLong_ByRequestId(pageParam, search, RequestId);
        }
        public override SuccessResult<AbstractUsersLatLong> UsersLatLong_Insert(string Json)
        {
            return this.abstractUsersLatLongDao.UsersLatLong_Insert(Json);
        }
    }
   }

