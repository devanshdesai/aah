﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryExecutiveServices : AbstractDeliveryExecutiveServices
    {
        private AbstractDeliveryExecutiveDao abstractDeliveryExecutiveDao;

        public DeliveryExecutiveServices(AbstractDeliveryExecutiveDao abstractDeliveryExecutiveDao)
        {
            this.abstractDeliveryExecutiveDao = abstractDeliveryExecutiveDao;
        }

        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_ByDeliveryExecutiveId(PageParam pageParam, string search, long DeliveryExecutiveId)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_ByDeliveryExecutiveId(pageParam, search, DeliveryExecutiveId);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_Upsert(AbstractDeliveryExecutive abstractDeliveryExecutive)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_Upsert(abstractDeliveryExecutive);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_ActInAct(long Id, long DeletedBy)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_ActInAct(Id, DeletedBy);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsMarketingPerson(long Id, long DeletedBy)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_IsMarketingPerson(Id, DeletedBy);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_UpdateLatLong(long Id, string Latitude, string Longitude)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_UpdateLatLong(Id, Latitude, Longitude);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsApproved(long Id, long DeletedBy)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_IsApproved(Id, DeletedBy);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_IsAvailable(long DeliveryExecutiveId, int IsAvailable)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_IsAvailable(DeliveryExecutiveId, IsAvailable);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_ById(long Id)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_ById(Id);
        }
         public override SuccessResult<AbstractDeliveryExecutive> Dashboard_ById(long Id)
        {
            return this.abstractDeliveryExecutiveDao.Dashboard_ById(Id);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_Logout(string DeviceToken, string UDID, string IMEI)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_Logout(DeviceToken, UDID, IMEI);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_VerifyOtp(long Id, string DeviceToken, long Otp,string DeviceType, string UDID, string IMEI)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_VerifyOtp(Id,DeviceToken, Otp, DeviceType, UDID, IMEI);
        }
        public override SuccessResult<AbstractDeliveryExecutive> DeliveryExecutive_SendOTP(string MobileNumber, string DeviceToken, string DeviceType, string UDID, string IMEI, long OTP)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_SendOTP(MobileNumber, DeviceToken, DeviceType, UDID, IMEI, OTP);
        }
        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_All(PageParam pageParam, string search, string UserName, string MobileNumber, int Gender, long IsActive, long IsApproved, long IsAvailable, long loginId =0, long AgencyId = 0, long MasterAreaId = 0)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_All(pageParam, search, UserName, MobileNumber, Gender, IsActive, IsApproved, IsAvailable,loginId,AgencyId,MasterAreaId);
        }
        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_ByAgencyId(PageParam pageParam, string search, long AgencyId)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_ByAgencyId(pageParam, search,AgencyId);
        }
        public override PagedList<AbstractDeliveryExecutive> GetDeliveryExecutiveCountByAddressId(string AddressId)
        {
            return this.abstractDeliveryExecutiveDao.GetDeliveryExecutiveCountByAddressId(AddressId);
        }
        public override PagedList<AbstractDeliveryExecutive> Requests_NotAssignedDeliveryExecutive_All(PageParam pageParam, long RequestId)
        {
            return this.abstractDeliveryExecutiveDao.Requests_NotAssignedDeliveryExecutive_All(pageParam, RequestId);
        }

        // DE Feedback

        public override SuccessResult<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback_Insert(AbstractDeliveryExecutiveFeedback abstractDeliveryExecutiveFeedback)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutiveFeedback_Insert(abstractDeliveryExecutiveFeedback);
        }
        public override PagedList<AbstractDeliveryExecutiveFeedback> DeliveryExecutiveFeedback_All(PageParam pageParam, string search, string FromDate, string ToDate, long DeliveryExecutiveId)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutiveFeedback_All(pageParam, search, FromDate, ToDate, DeliveryExecutiveId);
        }
        public override PagedList<AbstractDeliveryExecutive> DeliveryExecutive_Reports(PageParam pageParam, string search)
        {
            return this.abstractDeliveryExecutiveDao.DeliveryExecutive_Reports(pageParam, search);
        }
    }
}