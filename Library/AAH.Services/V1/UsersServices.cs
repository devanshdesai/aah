﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class UsersServices : AbstractUsersServices
    {
        private AbstractUsersDao abstractUsersDao;

        public UsersServices(AbstractUsersDao abstractUsersDao)
        {
            this.abstractUsersDao = abstractUsersDao;
        }
        public override PagedList<AbstractUserDevices> UserDevices_ByUserId(PageParam pageParam, string search, long UserId)
        {
            return this.abstractUsersDao.UserDevices_ByUserId(pageParam, search, UserId);
        }
        public override PagedList<AbstractUserDevices> UserDevices_ClearDeviceToken(PageParam pageParam, string search, long UserId)
        {
            return this.abstractUsersDao.UserDevices_ClearDeviceToken(pageParam, search, UserId);
        }
        public override SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_Upsert(abstractUsers);
        }
        public override SuccessResult<AbstractUsers> Users_ActInAct(long Id, long DeletedBy)
        {
            return this.abstractUsersDao.Users_ActInAct(Id, DeletedBy);
        }
        public override SuccessResult<AbstractUsers> Users_IsBlacklist(long Id, long UpdatedBy)
        {
            return this.abstractUsersDao.Users_IsBlacklist(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractUsers> Users_IsPayable(long Id, long UpdatedBy)
        {
            return this.abstractUsersDao.Users_IsPayable(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractUsers> Users_Logout(string DeviceToken, string UDID, string IMEI)
        {
            return this.abstractUsersDao.Users_Logout(DeviceToken, UDID, IMEI);
        }
        public override SuccessResult<AbstractUsers> Users_ById(long Id)
        {
            return this.abstractUsersDao.Users_ById(Id);
        }
        public override SuccessResult<AbstractUsers> Users_VerifyOtp(long Id, string DeviceToken, long Otp, string UDID, string IMEI)
        {
            return this.abstractUsersDao.Users_VerifyOtp(Id,DeviceToken, Otp, UDID, IMEI);
        }
        public override SuccessResult<AbstractUsers> Users_SendOTP(string MobileNumber, string DeviceToken, string DeviceType, string UDID, string IMEI, long OTP)
        {
            return this.abstractUsersDao.Users_SendOTP(MobileNumber, DeviceToken, DeviceType, UDID, IMEI, OTP);
        }
        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, string UserName, string MobileNumber, string Email, int Gender, long IsActive,long DEId)
        {
            return this.abstractUsersDao.Users_All(pageParam, search, UserName, MobileNumber, Email, Gender, IsActive, DEId);
        }
        public override SuccessResult<AbstractUsers> Users_ReferenceCode(long UserId, string ReferenceCode)
        {
            return this.abstractUsersDao.Users_ReferenceCode(UserId, ReferenceCode);
        }
        public override SuccessResult<AbstractUsers> Users_Delete(long Id, long DeletedBy)
        {
            return this.abstractUsersDao.Users_Delete(Id, DeletedBy);
        }
    }

}