﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class SurveyServices : AbstractSurveyServices
    {
        private AbstractSurveyDao abstractSurveyDao;
        public SurveyServices(AbstractSurveyDao abstractSurveyDao)
        {
            this.abstractSurveyDao = abstractSurveyDao;
        }
        public override PagedList<AbstractSurvey> Survey_All(PageParam pageParam, string search)
        {
            return this.abstractSurveyDao.Survey_All(pageParam, search);
        }
        public override SuccessResult<AbstractSurvey> Survey_Insert(AbstractSurvey abstractSurvey)
        {
            return this.abstractSurveyDao.Survey_Insert(abstractSurvey);
        }
    }
}