﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AgencyVsAreaServices : AbstractAgencyVsAreaServices
    {
        private AbstractAgencyVsAreaDao abstractAgencyVsAreaDao;
        public AgencyVsAreaServices(AbstractAgencyVsAreaDao abstractAgencyVsAreaDao)
        {
            this.abstractAgencyVsAreaDao = abstractAgencyVsAreaDao;
        }
        public override SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Upsert(AbstractAgencyVsArea abstractAgencyVsArea)
        {
            return this.abstractAgencyVsAreaDao.AgencyVsArea_Upsert(abstractAgencyVsArea);
        }
        public override SuccessResult<AbstractAgencyVsArea> AfterAgencyVsArea_Complete(long DeliveryExecutiveId, long AgencyVsAreaId)
        {
            return this.abstractAgencyVsAreaDao.AfterAgencyVsArea_Complete(DeliveryExecutiveId, AgencyVsAreaId);
        }
        
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_All(PageParam pageParam, string search)
        {
            return this.abstractAgencyVsAreaDao.AgencyVsArea_All(pageParam, search);
        } 
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_AssignedDE(PageParam pageParam, long AgencyId, long MasterAreaId)
        {
            return this.abstractAgencyVsAreaDao.AgencyVsArea_AssignedDE(pageParam, AgencyId, MasterAreaId);
        }
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_byAgencyId(PageParam pageParam, string search,long AgencyId)
        {
            return this.abstractAgencyVsAreaDao.AgencyVsArea_byAgencyId(pageParam, search, AgencyId);
        }
        public override PagedList<AbstractAgencyVsArea> AgencyVsArea_ById(int Id)
        {
            return this.abstractAgencyVsAreaDao.AgencyVsArea_ById(Id);
        }
        public override SuccessResult<AbstractAgencyVsArea> AgencyVsArea_ActInAct(int Id, int UpdatedBy)
        {
            return this.abstractAgencyVsAreaDao.AgencyVsArea_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractAgencyVsArea> AgencyVsArea_Delete(int Id, int AreaId)
        {
            return this.abstractAgencyVsAreaDao.AgencyVsArea_Delete(Id, AreaId);
        }

    }
}