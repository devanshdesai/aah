﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class ItemsServices : AbstractItemsServices
    {
        private AbstractItemsDao abstractItemsDao;

        public ItemsServices(AbstractItemsDao abstractItemsDao)
        {
            this.abstractItemsDao = abstractItemsDao;
        }
        
        public override SuccessResult<AbstractItems> Items_ById(long Id)
        {
            return this.abstractItemsDao.Items_ById(Id);
        }
        public override SuccessResult<AbstractItems> Items_ActInAct(long Id)
        {
            return this.abstractItemsDao.Items_ActInAct(Id);
        }
        public override SuccessResult<AbstractItems> Items_Delete(long Id, long DeletedBy)
        {
            return this.abstractItemsDao.Items_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractItems> Items_All(PageParam pageParam, string search)
        {
            return this.abstractItemsDao.Items_All(pageParam, search);
        }
        public override PagedList<AbstractItems> AllItems(PageParam pageParam, string search)
        {
            return this.abstractItemsDao.AllItems(pageParam, search);
        }
        public override PagedList<AbstractItems> SearchItems_All(PageParam pageParam, string search,long UserId)
        {
            return this.abstractItemsDao.SearchItems_All(pageParam, search, UserId);
        }
         public override PagedList<AbstractItems> VendorsVsItems_AddItemsList(PageParam pageParam, string search,long VendorsId)
        {
            return this.abstractItemsDao.VendorsVsItems_AddItemsList(pageParam, search, VendorsId);
        }
        public override SuccessResult<AbstractItems> Items_Upsert(AbstractItems abstractItems)
        {
            return this.abstractItemsDao.Items_Upsert(abstractItems);
        }
    }

    
}