﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class DeliveryLimitServices : AbstractDeliveryLimitServices
    {
        private AbstractDeliveryLimitDao abstractDeliveryLimitDao;
        public DeliveryLimitServices(AbstractDeliveryLimitDao abstractDeliveryLimitDao)
        {
            this.abstractDeliveryLimitDao = abstractDeliveryLimitDao;
        }
        
        public override SuccessResult<AbstractDeliveryLimit> DeliveryLimit_ById(long Id)
        {
            return this.abstractDeliveryLimitDao.DeliveryLimit_ById(Id);
        }
        public override PagedList<AbstractDeliveryLimit> DeliveryLimit_All(PageParam pageParam, string search)
        {
            return this.abstractDeliveryLimitDao.DeliveryLimit_All(pageParam, search);
        }
        public override SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Upsert(AbstractDeliveryLimit abstractDeliveryLimit)
        {
            return this.abstractDeliveryLimitDao.DeliveryLimit_Upsert(abstractDeliveryLimit);
        }
        public override SuccessResult<AbstractDeliveryLimit> DeliveryLimit_Delete(long Id, int DeletedBy)
        {
            return this.abstractDeliveryLimitDao.DeliveryLimit_Delete(Id, DeletedBy);
        }
    }
}