﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class TicketCommentsServices : AbstractTicketCommentsServices
    {
        private AbstractTicketCommentsDao abstractTicketCommentsDao;

        public TicketCommentsServices(AbstractTicketCommentsDao abstractTicketCommentsDao)
        {
            this.abstractTicketCommentsDao = abstractTicketCommentsDao;
        }

        public override SuccessResult<AbstractTicketComments> TicketComments_ById(long Id)
        {
            return this.abstractTicketCommentsDao.TicketComments_ById(Id);
        }
        public override SuccessResult<AbstractTicketComments> TicketComments_Delete(long Id, long DeletedBy)
        {
            return this.abstractTicketCommentsDao.TicketComments_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractTicketComments> TicketComments_Upsert(AbstractTicketComments abstractTicketComments)
        {
            return this.abstractTicketCommentsDao.TicketComments_Upsert(abstractTicketComments);
        }

        public override SuccessResult<AbstractTicketComments> TicketComments_ActInAct(long Id, long UpdatedBy)
        {
            return this.abstractTicketCommentsDao.TicketComments_ActInAct(Id, UpdatedBy);
        }
        public override PagedList<AbstractTicketComments> TicketComments_All(PageParam pageParam, string search)
        {
            return this.abstractTicketCommentsDao.TicketComments_All(pageParam, search);
        }
        public override PagedList<AbstractTicketComments> TicketComments_ByTicketId(PageParam pageParam, string search, long TicketId)
        {
            return this.abstractTicketCommentsDao.TicketComments_ByTicketId(pageParam, search, TicketId);
        }
    }
    
}