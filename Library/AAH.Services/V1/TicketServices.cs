﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class TicketServices : AbstractTicketServices
    {
        private AbstractTicketDao abstractTicketDao;

        public TicketServices(AbstractTicketDao abstractTicketDao)
        {
            this.abstractTicketDao = abstractTicketDao;
        }
        
        public override SuccessResult<AbstractTicket> Ticket_ById(long Id)
        {
            return this.abstractTicketDao.Ticket_ById(Id);
        }
        public override SuccessResult<AbstractTicket> Ticket_ChangeStatus(long Id, long UpdatedBy)
        {
            return this.abstractTicketDao.Ticket_ChangeStatus(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractTicket> Ticket_ChangeType(long Id, long UpdatedBy)
        {
            return this.abstractTicketDao.Ticket_ChangeType(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractTicket> Ticket_ChangePlatform(long Id, long UpdatedBy)
        {
            return this.abstractTicketDao.Ticket_ChangePlatform(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractTicket> Ticket_ActInAct(long Id, long UpdatedBy)
        {
            return this.abstractTicketDao.Ticket_ActInAct(Id, UpdatedBy);
        }
        public override PagedList<AbstractTicket> Ticket_All(PageParam pageParam, string search)
        {
            return this.abstractTicketDao.Ticket_All(pageParam, search);
        }
        public override SuccessResult<AbstractTicket> Ticket_Upsert(AbstractTicket abstractTicket)
        {
            return this.abstractTicketDao.Ticket_Upsert(abstractTicket);
        }
    }

    
}