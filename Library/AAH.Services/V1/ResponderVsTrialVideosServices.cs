﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class ResponderVsTrialVideosServices : AbstractResponderVsTrialVideosServices
    {
        private AbstractResponderVsTrialVideosDao abstractResponderVsTrialVideosDao;
        public ResponderVsTrialVideosServices(AbstractResponderVsTrialVideosDao abstractResponderVsTrialVideosDao)
        {
            this.abstractResponderVsTrialVideosDao = abstractResponderVsTrialVideosDao;
        }

        public override SuccessResult<AbstractResponderVsTrialVideos> ResponderVsTrialVideos_Upsert(AbstractResponderVsTrialVideos abstractResponderVsTrialVideos)
        {
            return this.abstractResponderVsTrialVideosDao.ResponderVsTrialVideos_Upsert(abstractResponderVsTrialVideos);
        }
        
    }
}