﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class TrialVideosServices : AbstractTrialVideosServices
    {
        private AbstractTrialVideosDao abstractTrialVideosDao;
        public TrialVideosServices(AbstractTrialVideosDao abstractTrialVideosDao)
        {
            this.abstractTrialVideosDao = abstractTrialVideosDao;
        }
        public override SuccessResult<AbstractTrialVideos> TrialVideos_Upsert(AbstractTrialVideos abstractTrialVideos)
        {
            return this.abstractTrialVideosDao.TrialVideos_Upsert(abstractTrialVideos);
        }
        public override SuccessResult<AbstractTrialVideos> AfterTrialVideos_Complete(long DeliveryExecutiveId, long TrialVideosId)
        {
            return this.abstractTrialVideosDao.AfterTrialVideos_Complete(DeliveryExecutiveId, TrialVideosId);
        }
        public override SuccessResult<AbstractTrialVideos> TrialVideos_ById(int Id)
        {
            return this.abstractTrialVideosDao.TrialVideos_ById(Id);
        }
        public override PagedList<AbstractTrialVideos> TrialVideos_All(PageParam pageParam, string search)
        {
            return this.abstractTrialVideosDao.TrialVideos_All(pageParam, search);
        }
        public override SuccessResult<AbstractTrialVideos> TrialVideos_ActInAct(int Id, int UpdatedBy)
        {
            return this.abstractTrialVideosDao.TrialVideos_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractTrialVideos> TrialVideos_Delete(int Id, int DeletedBy)
        {
            return this.abstractTrialVideosDao.TrialVideos_Delete(Id, DeletedBy);
        }

    }
}