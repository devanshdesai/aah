﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class AdvertiseMasterServices : AbstractAdvertiseMasterServices
    {
        private AbstractAdvertiseMasterDao abstractAdvertiseMasterDao;
        public AdvertiseMasterServices(AbstractAdvertiseMasterDao abstractAdvertiseMasterDao)
        {
            this.abstractAdvertiseMasterDao = abstractAdvertiseMasterDao;
        }

        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_Upsert(AbstractAdvertiseMaster abstractAdvertiseMaster)
        {
            return this.abstractAdvertiseMasterDao.AdvertiseMaster_Upsert(abstractAdvertiseMaster);
        }
        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_ById(int Id)
        {
            return this.abstractAdvertiseMasterDao.AdvertiseMaster_ById(Id);
        }
        public override PagedList<AbstractAdvertiseMaster> AdvertiseMaster_All(PageParam pageParam, string search)
        {
            return this.abstractAdvertiseMasterDao.AdvertiseMaster_All(pageParam, search);
        }
        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_ActInAct(int Id, int UpdatedBy)
        {
            return this.abstractAdvertiseMasterDao.AdvertiseMaster_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractAdvertiseMaster> AdvertiseMaster_Delete(int Id, int DeletedBy)
        {
            return this.abstractAdvertiseMasterDao.AdvertiseMaster_Delete(Id, DeletedBy);
        }
    }
}