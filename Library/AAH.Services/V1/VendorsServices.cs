﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class VendorsServices : AbstractVendorsServices
    {
        private AbstractVendorsDao abstractVendorsDao;

        public VendorsServices(AbstractVendorsDao abstractVendorsDao)
        {
            this.abstractVendorsDao = abstractVendorsDao;
        }
        
        public override SuccessResult<AbstractVendors> Vendors_ById(long Id)
        {
            return this.abstractVendorsDao.Vendors_ById(Id);
        }
        public override SuccessResult<AbstractVendors> Vendor_SendOTP(string MobileNumber, string DeviceToken, long OTP)
        {
            return this.abstractVendorsDao.Vendor_SendOTP(MobileNumber, DeviceToken, OTP);
        }
        public override SuccessResult<AbstractVendors> Vendor_VerifyOtp(long Otp, long Id)
        {
            return this.abstractVendorsDao.Vendor_VerifyOtp(Otp, Id);
        }
        public override SuccessResult<AbstractVendors> Vendors_ActInAct(long Id)
        {
            return this.abstractVendorsDao.Vendors_ActInAct(Id);
        }
        public override SuccessResult<AbstractVendors> VendorsThelaImages_Delete(string Url, long vendorId)
        {
            return this.abstractVendorsDao.VendorsThelaImages_Delete(Url, vendorId);
        }
        public override SuccessResult<AbstractVendors> Vendors_Delete(long Id, long DeletedBy)
        {
            return this.abstractVendorsDao.Vendors_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractVendors> Vendors_All(PageParam pageParam, string search)
        {
            return this.abstractVendorsDao.Vendors_All(pageParam, search);
        }
        public override PagedList<AbstractVendors> Vendors_ByAdminId(PageParam pageParam, string search, long AdminId)
        {
            return this.abstractVendorsDao.Vendors_ByAdminId(pageParam, search,AdminId);
        }
        public override SuccessResult<AbstractVendors> Vendors_Upsert(AbstractVendors abstractVendors)
        {
            return this.abstractVendorsDao.Vendors_Upsert(abstractVendors);
        }
        public override SuccessResult<AbstractVendorsThelaImages> VendorsThelaImages_Upsert(AbstractVendorsThelaImages abstractVendorsThelaImages)
        {
            return this.abstractVendorsDao.VendorsThelaImages_Upsert(abstractVendorsThelaImages);
        }

        // Vendors Vs Items

        public override SuccessResult<AbstractVendorsVsItems> VendorsVsItems_Upsert(AbstractVendorsVsItems abstractVendorsVsItems)
        {
            return this.abstractVendorsDao.VendorsVsItems_Upsert(abstractVendorsVsItems);
        }
        public override SuccessResult<AbstractVendorsVsItems> VendorsVsItems_Delete(long Id, long DeletedBy)
        {
            return this.abstractVendorsDao.VendorsVsItems_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractVendorsVsItems> VendorsVsItems_ActInAct(long Id, long UpdatedBy)
        {
            return this.abstractVendorsDao.VendorsVsItems_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractVendors> Vendor_IsOnline(long VendorId, int IsOnline)
        {
            return this.abstractVendorsDao.Vendor_IsOnline(VendorId, IsOnline);
        }
        public override PagedList<AbstractVendorsVsItems> VendorsVsItems_ByVendorsId(PageParam pageParam, string Search, long VendorsId)
        {
            return this.abstractVendorsDao.VendorsVsItems_ByVendorsId(pageParam, Search, VendorsId);
        }
        //Vendors Vs References
        public override SuccessResult<AbstractVendorsVsReferences> VendorsVsReferences_Upsert(AbstractVendorsVsReferences abstractVendorsVsReferences)
        {
            return this.abstractVendorsDao.VendorsVsReferences_Upsert(abstractVendorsVsReferences);
        }
        public override SuccessResult<AbstractVendorsVsReferences> VendorsVsReferences_Delete(long Id, long DeletedBy)
        {
            return this.abstractVendorsDao.VendorsVsReferences_Delete(Id, DeletedBy);
        }
        public override PagedList<AbstractVendorsVsReferences> VendorsVsReferences_ByVendorsId(PageParam pageParam, string Search, long VendorsId)
        {
            return this.abstractVendorsDao.VendorsVsReferences_ByVendorsId(pageParam, Search, VendorsId);
        }
    }

    
}