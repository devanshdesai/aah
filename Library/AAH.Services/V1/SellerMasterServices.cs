﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class SellerMasterServices : AbstractSellerMasterServices
    {
        private AbstractSellerMasterDao abstractSellerMasterDao;
        public SellerMasterServices(AbstractSellerMasterDao abstractSellerMasterDao)
        {
            this.abstractSellerMasterDao = abstractSellerMasterDao;
        }

        public override SuccessResult<AbstractSellerMaster> SellerMaster_Upsert(AbstractSellerMaster abstractSellerMaster)
        {
            return this.abstractSellerMasterDao.SellerMaster_Upsert(abstractSellerMaster);
        }
       
    }
}