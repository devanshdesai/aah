﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class TagsMasterServices : AbstractTagsMasterServices
    {
        private AbstractTagsMasterDao abstractTagsMasterDao;

        public TagsMasterServices(AbstractTagsMasterDao abstractTagsMasterDao)
        {
            this.abstractTagsMasterDao = abstractTagsMasterDao;
        }

        public override PagedList<AbstractTagsMaster> TagsMaster_All(PageParam pageParam, string search)
        {
            return this.abstractTagsMasterDao.TagsMaster_All(pageParam, search);
        }
    }
}
