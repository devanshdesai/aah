﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Data.Contract;
using AAH.Entities.Contract;
using AAH.Services.Contract;

namespace AAH.Services.V1
{
    public class OtherServiceIconsServices : AbstractOtherServiceIconsServices
    {
        private AbstractOtherServiceIconsDao abstractOtherServiceIconsDao;
        public OtherServiceIconsServices(AbstractOtherServiceIconsDao abstractOtherServiceIconsDao)
        {
            this.abstractOtherServiceIconsDao = abstractOtherServiceIconsDao;
        }
        
        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ById(long Id)
        {
            return this.abstractOtherServiceIconsDao.OtherServiceIcons_ById(Id);
        }
        public override PagedList<AbstractOtherServiceIcons> OtherServiceIcons_All(PageParam pageParam, string search)
        {
            return this.abstractOtherServiceIconsDao.OtherServiceIcons_All(pageParam, search);
        }
        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Upsert(AbstractOtherServiceIcons abstractOtherServiceIcons)
        {
            return this.abstractOtherServiceIconsDao.OtherServiceIcons_Upsert(abstractOtherServiceIcons);
        }
        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_ActInAct(long Id, int UpdatedBy)
        {
            return this.abstractOtherServiceIconsDao.OtherServiceIcons_ActInAct(Id, UpdatedBy);
        }
        public override SuccessResult<AbstractOtherServiceIcons> OtherServiceIcons_Delete(long Id, int DeletedBy)
        {
            return this.abstractOtherServiceIconsDao.OtherServiceIcons_Delete(Id, DeletedBy);
        }
    }
}