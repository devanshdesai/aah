﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractTicket
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public long CustomerId { get; set; }
        public string Description { get; set; }
        public string ReferenceCode { get; set; }
        public string MobileNumber { get; set; }
        public string Name { get; set; }
        public string ProfileURL { get; set; }
        public string Email { get; set; }
        public long RequestId { get; set; }
        public long StatusId { get; set; }
        public long PlatFormId { get; set; }
        public long TypeId { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        
        [NotMapped]
        public string ProfileURLStr => Configurations.AdminEndPoint + ProfileURL;
    }
}

