﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract


{
    public abstract class AbstractTagsMaster 
    {
        public long Id { get; set; }
        public string Name { get; set; }
     
    }
}

