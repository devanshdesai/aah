﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractDeliveryExecutiveLatLong
    {
        public long Id { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public long RequestId { get; set; }
        public string CapturedDate { get; set; }
        public string Json { get; set; }
        public string Latitude { get; set; }
        public string DeliveryExecutiveName { get; set; }
        public string DeliveryExecutiveMobileNumber { get; set; }
        public string Longitude { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        // For map
        public long StatusId { get; set; }
       
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
       }

    public abstract class AbstractOnboarderLatLong
    {
        public long Id { get; set; }
        public long AdminId { get; set; }
        public string AdminName { get; set; }
        public string AdminMobileNumber { get; set; }
        public string CapturedDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

