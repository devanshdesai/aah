﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAH.Entities.Contract
{
    public abstract class AbstractDeliveryExecutive : AbstractDeliveryExecutiveDevices
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long Pincode { get; set; }
        public long CityMasterId { get; set; }
        public string CityName { get; set; }
        public long StateMasterId { get; set; }
        public string StateName { get; set; }
        public long AgencyId { get; set; }
        public long MasterAreaId { get; set; }
        public string AgencyName { get; set; }
        public string MasterAreaName { get; set; }
        public long CountryMasterId { get; set; }
        public long AadharCardNumber { get; set; }
        public string CountryName { get; set; }
        public string AadharCardImage { get; set; }
        public string MobileNumber { get; set; }
        public int Gender { get; set; }
        public long TodayDownloads { get; set; }
        public long ThisMonthDownloads { get; set; }
        public long DailyAvgDownloads { get; set; }
        public long ThisMonthAmount { get; set; }
        public long TodayDelivery { get; set; }
        public int IsActive { get; set; }
        public bool IsSignup { get; set; }
        public bool IsMarketingPerson { get; set; }
        public int IsApproved { get; set; }
        public int IsAvailable { get; set; }
        public string ProfileURL { get; set; }
        public string PhotoIdProof { get; set; }
        public string AgreementCopy { get; set; }
        public string ParentName { get; set; }
        public string ParentNumber { get; set; }
        public string DrivingLicenceNumber { get; set; }
        public string DrivingLicenceImage { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CurrentLat { get; set; }
        public string CurrentLong { get; set; }
        public string TrialVideosUrl { get; set; }
        public string TotalTrialVideosDays { get; set; }
        public string TrialVideosRemainingDays { get; set; }
        public long TrialVideosId { get; set; }
        public bool IsTrialVideos { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; } 
        public decimal DistanceKm { get; set; }
        public string AreaIds { get; set; }
        public HttpPostedFileBase Files { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string ProfileURLStr => Configurations.AdminEndPoint + ProfileURL;
        [NotMapped]
        public string PhotoIdProofStr => Configurations.ClientURL + PhotoIdProof;
        [NotMapped]
        public string AgreementCopyStr => Configurations.ClientURL + AgreementCopy;
        [NotMapped]
        public string AadharCardImageStr => Configurations.ClientURL + AadharCardImage;

        public decimal Ratings { get; set; }
        public DateTime CheckIn { get; set; }
        [NotMapped]
        public string CheckInStr => CheckIn != null ? CheckIn.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public DateTime CheckOut { get; set; }
        [NotMapped]
        public string CheckOutStr => CheckOut != null ? CheckOut.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public string WorkingSince { get; set; }
        public int DeviceCount { get; set; }


        [NotMapped]
        public string AvatarFolder { get; set; }
        [NotMapped]
        public string DocumentPath { get; set; }
        [NotMapped]
        public string Path { get; set; }
    }
}

