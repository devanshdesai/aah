﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
        public abstract class AbstractRequestsStatusTracker
    {
        public long Id { get; set; }
        public long RequestId { get; set; }
        public decimal RequestAmountGiven { get; set; }
        public string RequestUserDescription { get; set; }
        public DateTime RequestAssignedDateTime { get; set; }
        public string RequestReviewByUser { get; set; }
        public long StatusId { get; set; }
        public string StatusName { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        


        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string RequestAssignedDateTimeStr => RequestAssignedDateTime != null ? RequestAssignedDateTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        
    }
}

