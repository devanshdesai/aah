﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractDeliveryExecutiveAdminComments
    {
        public long Id { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public string DeliveryExecutiveFirstName { get; set; }
        public string DeliveryExecutiveMiddleName { get; set; }
        public string DeliveryExecutiveLastName { get; set; }
        public string DeliveryExecutiveMobileNumber { get; set; }
        public long AdminId { get; set; }
        public string AdminName { get; set; }
        public string AdminEmail { get; set; }
        public string AdminMobileNumber { get; set; }
        public int AdminGender { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

