﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract

// CC, UU, DD and IsActive field AbstractBase inherit thashe 
{
    public abstract class AbstractServiceMaster: AbstractBase
    {
        public long Id { get; set; }
        public string ServiceName { get; set; }
        public string Icon { get; set; }
        [NotMapped]
        public string IconStr => Configurations.ClientURL + Icon;

    }
}

