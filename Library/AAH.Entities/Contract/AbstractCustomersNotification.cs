﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractCustomersNotification
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public long IsSend { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
   
}

