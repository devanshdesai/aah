﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAH.Entities.Contract
{
    public abstract class AbstractItems
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string GujaratiName { get; set; }
        public string HindiName { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }
        public string CreatedByName { get; set; }
        public string UpdatedByName { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public HttpPostedFileBase Files { get; set; }

        [NotMapped]
        public string ImageUrlStr => Configurations.AdminURL + ImageUrl;
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
   
}

