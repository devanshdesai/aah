﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractTicketComments
    {
        public long Id { get; set; }
        
        public string Description { get; set; }
        public long TicketId { get; set; }
        
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
    }
}

