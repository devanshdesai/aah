﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract

// CC, UU, DD and IsActive field AbstractBase inherit thashe 
{
    public abstract class AbstractDeliveryExecutiveDevices
    {
        public long DeliveryExecutiveDevicesId { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string UDID { get; set; }
        public string IMEI { get; set; }
        public long OTP { get; set; }
        public DateTime LastLogin { get; set;}
        public int IsVerified { get; set; }
        public DateTime DeliveryExecutiveDevicesCreatedDate { get; set; }
        public DateTime DeliveryExecutiveDevicesUpdatedDate { get; set; }
        public long DeliveryExecutiveDevicesUpdatedBy { get; set; }

        [NotMapped]
        public string DeliveryExecutiveDevicesCreatedDateStr => DeliveryExecutiveDevicesCreatedDate != null ? DeliveryExecutiveDevicesCreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeliveryExecutiveDevicesUpdatedDateStr => DeliveryExecutiveDevicesUpdatedDate != null ? DeliveryExecutiveDevicesUpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string LastLoginStr => LastLogin != null ? LastLogin.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

