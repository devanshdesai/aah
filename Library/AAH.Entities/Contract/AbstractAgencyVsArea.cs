﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractAgencyVsArea
    {
        public int Id { get; set; }
        public int AreaId { get; set; }
        public int AgencyId { get; set; }
        public int Assigned { get; set; }
        public string AreaName { get; set; }
        public string AgencyName { get; set; }
        public string Name { get; set; }
        public string ProfilePic { get; set; }
        public string CreatedByName { get; set; }
        public string MobileNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public int TodayTotalAttended { get; set; }
        public int TotalAttended { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public int DeletedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string ProfilePicStr => Configurations.ClientURL + ProfilePic;

    }
}

