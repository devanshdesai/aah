﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractAreaMaster
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string ConvinienceDiscount { get; set; }
        public string ConvinienceCharge { get; set; }
        public long PinCode { get; set; }
        public long CityMasterId { get; set; }
        public long StateMasterId { get; set; }
        public string GeoFencing { get; set; }
        public bool IsActive { get; set; }
        public int IsApprove { get; set; }
        public int IsApproveByAdmin { get; set; }
        public int IsApproveBySuperAdmin { get; set; }
        public int MaxNoOfExecutives { get; set; }
        public int MinNoOfExecutives { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }

}

