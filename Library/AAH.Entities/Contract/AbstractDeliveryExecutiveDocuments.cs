﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract

// CC, UU, DD and IsActive field AbstractBase inherit thashe 
{
    public abstract class AbstractDeliveryExecutiveDocuments
    {
        public long Id { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public string DeliveryExecutiveName { get; set; }
        public long DocumentTypeId { get; set; }
        public string DocumentTypeName { get; set; }
        public string DocumentUrl { get; set; }
        public int IsVerified { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public long AdminId { get; set; }
        public string AdminName { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DocumentUrlStr => Configurations.ClientURL + DocumentUrl;
    }
}

