﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract

// CC, UU, DD and IsActive field AbstractBase inherit thashe 
{
    public abstract class AbstractUsers : AbstractUserDevices
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileURL { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public int Gender { get; set; }
        public int IsActive { get; set; }
        public bool IsBlacklist { get; set; }
        public bool IsPayable { get; set; }
        public bool IsSignup { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public int CurrentDeviceCount { get; set; }
        public int CurrentAddressCount { get; set; }
        public int CurrentRequestCount { get; set; }
        public int AddressCount { get; set; }
        public int RequestCount { get; set; } 
        public long FavouriteDeliveryExecutiveId { get; set; }
        public long WorstDeliveryExecutiveId { get; set; }
        public string ReferenceCode { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string ProfileURLStr => Configurations.ClientURL + ProfileURL;
        
    }
}

