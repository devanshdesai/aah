﻿using AAH.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractRequestDocuments : AbstractBase
    {
        // CC, UU, DD and IsActive field AbstractBase inherit thashe 

        public long Id { get; set; }
        public long RequestId { get; set; }
        public string DocumentURL { get; set; }
        public bool IsUploadedByDeliveryExecutive { get; set; }
        public bool IsUploadedByUser { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedByMobileNumber { get; set; }

        [NotMapped]
        public string DocumentURLStr => Configurations.ClientURL + DocumentURL;
    }
}
