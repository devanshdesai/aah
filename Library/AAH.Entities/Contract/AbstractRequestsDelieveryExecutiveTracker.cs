﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
        public abstract class AbstractRequestsDelieveryExecutiveTracker
    {
        public long Id { get; set; }
        public long RequestId { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public string DeliveryExecutiveName { get; set; }
        public string MobileNumber { get; set; }
        public DateTime AssignedDateTime { get; set; }
        public string AssignedBy { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string AssignedDateTimeStr => AssignedDateTime != null ? AssignedDateTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        
    }
}

