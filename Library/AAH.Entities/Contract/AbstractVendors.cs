﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractVendors
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ShopName { get; set; }
        public string GujaratiShopName { get; set; }
        public string HindiShopName { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string Location { get; set; }
        public string ThelaImages { get; set; }
        public string AadharCardNumber { get; set; }
        public string AadharImage { get; set; }
        public string ElectionIDNumber { get; set; }
        public string ElectionIDImage { get; set; }
        public string Items { get; set; }
        public bool IsActive { get; set; }
        public int IsOnline { get; set; }
        public bool IsDeleted { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string CreatedLat { get; set; }
        public string CreatedLong { get; set; }
        public string UpdatedLat { get; set; }
        public string UpdatedLong { get; set; }
        public string CreatedName { get; set; }
        public string UpdatedName { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long ReferenceCode { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string ShopOwnerProfile { get; set; }
        public string ThelaImageObj { get; set; }
        public bool IsDeliveryBoy { get; set; }
        public bool IsReadyForCommition { get; set; }
       // public long AreaId { get; set; }


        [NotMapped]
        public string ShopOwnerProfileStr => Configurations.AdminURL + ShopOwnerProfile;
        [NotMapped]
        public string AadharImageStr => Configurations.ClientURL + AadharImage;
        [NotMapped]
        public string ElectionIDImageStr => Configurations.ClientURL + ElectionIDImage;
        [NotMapped]
        public string ThelaImageStr => Configurations.ClientURL + ThelaImages;
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
    public abstract class AbstractVendorsVsReferences
    {
        public long Id { get; set; }
        public long VendorsId { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerNumber { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
    public abstract class AbstractVendorsThelaImages
    {
        public long Id { get; set; }
        public long VendorId { get; set; }
        public string ImagesUrl { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedDate { get; set; }
    }

    public abstract class AbstractVendorsVsItems
    {
        public long Id { get; set; }
        public long VendorsId { get; set; }
        public string VendorFirstName { get; set; }
        public string VendorLastName { get; set; }
        public string VendorShopOwnerProfile { get; set; }
        public long ItemsId { get; set; }
        public string ItemsIds { get; set; }
        public string ItemName { get; set; }
        public string ItemImageUrl { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }

        [NotMapped]
        public string VendorShopOwnerProfileStr => Configurations.ClientURL + VendorShopOwnerProfile;
        [NotMapped]
        public string ItemImageUrlStr => Configurations.ClientURL + ItemImageUrl;
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }

    public class IsPrimaryKey
    {
        public List<AbPrimary> AbPrimaryObj { get; set; }
    }

    public class AbPrimary
    {
        public bool IsPrimary { get; set; }
    }

}

