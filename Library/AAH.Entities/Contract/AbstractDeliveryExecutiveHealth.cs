﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractDeliveryExecutiveHealth
    {
        public long Id { get; set; }
        public int DeliveryExecutiveId { get; set; }
        public int UserId { get; set; }
        public string VaccineName { get; set; }
        public int Dose { get; set; }
        public int IsVaccinated { get; set; }
        public string Temperature { get; set; }
        public DateTime TemperatureCheckAt { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public int DeletedBy { get; set; }
        public string ProfileUrl { get; set; }
        public string MobileNumber { get; set; }
        public string CurrentLat { get; set; }
        public string CurrentLong { get; set; }
        public string DeliveryExecutiveName { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string TemperatureCheckAtStr => TemperatureCheckAt != null ? TemperatureCheckAt.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

