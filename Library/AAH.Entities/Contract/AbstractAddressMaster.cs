﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract

// CC, UU, DD and IsActive field AbstractBase inherit thashe 
{
    public abstract class AbstractAddressMaster : AbstractBase
    {
        public long Id { get; set; }
        public long AddressTypeId { get; set; }
        public long AreaMasterId { get; set; }
        public string AreaName { get; set; }
        public string AddressTypeName { get; set; }
        public string LocationName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long PinCode { get; set; }
        public long CityMasterId { get; set; }
        public string CityName { get; set; }
        public long StateMasterId { get; set; }
        public string StateName { get; set; }
        public long CountryMasterId { get; set; }
        public string CountryName { get; set; }
        public long UserId { get; set; }
        public long AdminId { get; set; }
        public string Latitude { get; set; }
        public string Longitute { get; set; }
        public bool IsActive { get; set; }
        public int IsDefault { get; set; }
        public bool IsSelected { get; set; }
    }

    public abstract class AbstractAddressMasterRequestModel
    {
        public long Id { get; set; }
        public long AddressTypeId { get; set; }
        public long AreaMasterId { get; set; }
        public string LocationName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long UserId { get; set; }
        public long PinCode { get; set; }
        public string Latitude { get; set; }
        public string Longitute { get; set; }
        public int IsDefault { get; set; }

    }

    public abstract class AbstractCityMaster
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long StateMasterId { get; set; }
        public string StateMasterName { get; set; }
    }

    public abstract class AbstractStateMaster
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CountryMasterId { get; set; }
        public string CountryMasterName { get; set; }
    }

    public abstract class AbstractCountryMaster
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public abstract class AbstractSelectedAddress
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long AddressId { get; set; }
        public string LocationName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long PinCode { get; set; }
        public string Latitude { get; set; }
        public string Longitute { get; set; }
        public bool IsNew { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long AreaMasterId { get; set; }
    }

}

