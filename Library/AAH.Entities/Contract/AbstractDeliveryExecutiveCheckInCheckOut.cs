﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract

// CC, UU, DD and IsActive field AbstractBase inherit thashe 
{
    public abstract class AbstractDeliveryExecutiveCheckInCheckOut
    {
        public long Id { get; set; }
        public string DeliveryExecutiveName { get; set; }
        public string AdminName { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public DateTime ActionDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public long TypeId { get; set; }
        public long AdminId { get; set; }
        public long CreatedBy { get; set; }
        public bool IsAdministration { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string ActionDateTimeStr => ActionDateTime != null ? ActionDateTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";

    }
}

