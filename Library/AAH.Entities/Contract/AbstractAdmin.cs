﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractAdmin
    {
        public long Id { get; set; }
        public long Type { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string MobileNumber { get; set; }
        public string AreaMasterId { get; set; }
        public string AreaMasterName { get; set; }
        public string AreaId { get; set; }
        public string AreaName { get; set; }
        public string AreaMasterIds { get; set; }
        public int Gender { get; set; }
        public int AdminTypeId { get; set; }
        public string AdminTypeName { get; set; }
        public bool IsActive { get; set; }
        public string GSTNumber { get; set; }
        public string AddressProof { get; set; }
        public string OwnerName { get; set; }
        public string PANNumber { get; set; }
        public string PANNumberImage { get; set; }
        public string ManagerName { get; set; }
        public string ManagerContactNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime LastLogin { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public long Target { get; set; }
        public long TodayTarget { get; set; }
        public long AchievedTarget { get; set; }
        public long Salary { get; set; }
        public long Royalty { get; set; }
        public long Penalty { get; set; }
        public long Commision { get; set; }
        public long NoOfDE { get; set; }
        public long ServiceCharge { get; set; }
        public long ThelaValaCharge { get; set; }
        public long AdCharge { get; set; }
        public long CommitmentAmtMP { get; set; }
        public long CommitmentAgencyDow { get; set; }
        public long CommitmentAgencyServ { get; set; }
        public long AvailableCount { get; set; }
        public long TotalAvailableCount { get; set; }
        public long MinNoOfExecutives { get; set; }
        public long MaxNoOfExecutives { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string AddressProofStr => Configurations.ClientURL + AddressProof;
        [NotMapped]
        public string PANNumberImageStr => Configurations.ClientURL + PANNumberImage;
        [NotMapped]
        public string AvatarFolder { get; set; }
        [NotMapped]
        public string DocumentPath { get; set; }
        [NotMapped]
        public string Path { get; set; }
    }

    public abstract class AbstractAdminType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
    public abstract class AbstractAreaVsStaff
    {
        public long Id { get; set; }
        public string AreaId { get; set; }
        public long StaffId { get; set; }
        public long AreaMasterId { get; set; }
        public string GeoFencing { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
    }
}

