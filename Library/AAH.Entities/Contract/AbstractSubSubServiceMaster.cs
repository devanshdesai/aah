﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AAH.Entities.Contract

// CC, UU, DD and IsActive field AbstractBase inherit thashe 
{
    public abstract class AbstractSubSubServiceMaster: AbstractBase
    {
        public long Id { get; set; }
        public string SubSubServiceName { get; set; }
        public long SubServiceMasterId { get; set; }
        public string SubServiceName { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string Link { get; set; }
        [NotMapped]
        public string IconStr => Configurations.ClientURL + Icon;
    }
}

