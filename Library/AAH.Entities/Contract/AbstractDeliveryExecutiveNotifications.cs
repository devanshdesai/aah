﻿using AAH;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract
{
    public abstract class AbstractDeliveryExecutiveNotifications
    {
        public long Id { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public string DeliveryExecutiveFirstName { get; set; }
        public string DeliveryExecutiveMiddleName { get; set; }
        public string DeliveryExecutiveLastName { get; set; }
        public string DeliveryExecutiveProfileURL { get; set; }
        public string DeliveryExecutiveMobileNumber { get; set; }
        public long RequestId { get; set; }
        public decimal AmountRequested { get; set; }
        public string RequestedUserDescription { get; set; }
        public DateTime RequestAssignedDateTime { get; set; }
        public string RequestAssignedBy { get; set; }
        public long StatusId { get; set; }
        public string StatusName { get; set; }
        public DateTime ActionDateTime { get; set; }
        public string Description { get; set; }
        public int IsSent { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        [NotMapped]
        public string ActionDateTimeStr => ActionDateTime != null ? ActionDateTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        
    }
}

