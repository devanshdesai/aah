﻿using AAH;
using AAH.Common;
using AAH.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAH.Entities.Contract {
    public abstract class AbstractRequests
    {
        public long Id { get; set; }
        public long UserTotalRequest { get; set; }
        public DateTime EstimatedETA { get; set; }
        public long UserId { get; set; }
        public long OTP { get; set; }
        public long DeliveryExecutiveId { get; set; }
        public long ServiceMasterId { get; set; }
        public long SubServiceMasterId { get; set; }
        public long SubSubServiceMasterId { get; set; }
        public decimal AmountRequested { get; set; }
        public decimal AmountGiven { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public decimal TipAmount { get; set; }
        public string AudioUrl { get; set; }
        public string PolyLine { get; set; }
        public long AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressPinCode { get; set; }
        public string AddressLocationName { get; set; }
        public string AddressLine2 { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string UserDescription { get; set; }
        public DateTime AssignedDateTime { get; set; }
        public string AssignedBy { get; set; }
        public long StatusId { get; set; }
        public long DeliveryExecutiveRequestStatusId { get; set; }
        public decimal RatingByUser { get; set; }
        public decimal RatingByDeliveryExecutive { get; set; }
        public string DeliveryExecutiveRequestStatusName { get; set; }
        public string ReviewByUser { get; set; }
        public string ReviewByDeliveryExecutive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string UserName { get; set; }
        public string UserProfileURL { get; set; }
        public string UserEmail { get; set; }
        public string UserMobileNumber { get; set; }
        public string DeliveryExecutiveName { get; set; }
        public string DeliveryExecutiveProfileURL { get; set; }
        public string DeliveryExecutiveMobileNumber { get; set; }
        public string DeliveryExecutiveRatings { get; set; }
        public string DEAgencyName { get; set; }
        public string ServiceName { get; set; }
        public string SubServiceName { get; set; }
        public string SubSubServiceName { get; set; }
        public string StatusName { get; set; }
        public long RequestId { get; set; }
        public decimal DeliveryExecutiveRequestAmount { get; set; }
        public long IsApprovedByUser { get; set; }
        public string WorkingSince { get; set; }
        public string HelpDocUrl { get; set; }
        public string Link { get; set; }
        public string CompanyName { get; set; }
        public long MasterCategoryId { get; set; }
        public long SellerMasterId { get; set; }
        public long PayType { get; set; }
        public long Type { get; set; }
        public long RequestsId { get; set; }
        public long AdvertiseMasterId { get; set; }
        public string CurrentLat { get; set; }
        public string CurrentLong { get; set; }
        public string AgencyName { get; set; }
        public bool IsConfirmed { get; set; }
        public bool IsConvenienceChargeFree { get; set; }
        public decimal ConvenienceCharge { get; set; }
        public decimal ConvenienceDiscount { get; set; }
        public decimal ProductServiceExpense { get; set; }
        // User Feedback
        public long FeedbackRettings { get; set; }
        public string FeedbackDescription { get; set; }
        // DE Feedback
        public long DEFeedbackRettings { get; set; }
        public string DEFeedbackDescription  { get; set; }
        public string Amount { get; set; }
        public string RequestCount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AmountToAgency { get; set; }
        public string AmountToAdmin { get; set; }
        public string DeliveryExecutiveAmount { get; set; }
        public string GivenAmountToAgency { get; set; }
        public string ShopName { get; set; }
        public string OwnerName { get; set; }
        public string ContactNumber { get; set; }
        public string PicUrl { get; set; }
        public string MasterCategoryName { get; set; }
        public int IsSettledWithAgency { get; set; }
        public int IsSettledWithAdmin { get; set; }

        [NotMapped]
        public long IsAssignedByAdmin { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]

        public string CDate => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]

        public string CTime => CreatedDate != null ? CreatedDate.ToString("hh:mm tt") : "-";
        [NotMapped]

        public string EstimatedETAStr => EstimatedETA != null ? EstimatedETA.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string AssignedDateTimeStr => AssignedDateTime != null ? AssignedDateTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UserProfileURLStr => Configurations.AdminPortalURL + UserProfileURL;
        [NotMapped]
        public string AudioUrlStr => Configurations.AdminPortalURL + AudioUrl;
        [NotMapped]
        public string DeliveryExecutiveProfileURLStr => Configurations.AdminPortalURL + DeliveryExecutiveProfileURL;
        [NotMapped]
        public string DeliveryExecutiveProfileURLStr1 => Configurations.PortalURL + DeliveryExecutiveProfileURL;

        // For insert Lat long
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }

    }
    public abstract class AbstractRequestsCount
    {
        public long NewRequestCount { get; set; }
        public long FindingProvidersCount { get; set; }
        public long RequestAssignedCount { get; set; }
        public long RequestAttendedCount { get; set; }
        public long RequestRejectedByCustomerCount { get; set; }
        public long RequestRejectedByDeliveryExecutiveCount { get; set; }
        public long PitchedCount { get; set; }
        public long AcceptedCount { get; set; }
        public long IgnoredCount { get; set; }
        public long RejectedCount { get; set; }
        public long CashAtHomeCount { get; set; }
        public long SOSCount { get; set; }
        public long OtherServiceCount { get; set; }
        public long CustomerCount { get; set; }
        public long DeliveryexecutiveCount { get; set; }
        public long ActiveDeliveryexecutiveCount { get; set; }
        public long ApprovedDeliveryexecutiveCount { get; set; }
        public long AvailableDeliveryexecutiveCount { get; set; }
        public long NotAttended { get; set; }
        public long TodayNoOfRequest { get; set; }

    } 
    public abstract class AbstractRequestcomments
    {
        public long Id { get; set; }
        public long AdminId { get; set; }
        public long RequestId { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        
    }

    public abstract class AbstractRequestsReachTime
    {
        public string RequestsLatitude { get; set; }
        public string RequestsLongitude { get; set; }
        public string DeliveryExecutiveLatitude { get; set; }
        public string DeliveryExecutiveLongitude { get; set; }
    }
}

