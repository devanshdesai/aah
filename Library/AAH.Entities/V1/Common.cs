﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAH.Entities.Contract;

namespace AAH.Entities.V1
{
    public class Users : AbstractUsers { }
    public class Ticket : AbstractTicket { }
    public class TicketComments : AbstractTicketComments { }
    public class AdvertiseLeads : AbstractAdvertiseLeads { }
    public class MasterAgeGroup : AbstractMasterAgeGroup { }
    public class Survey : AbstractSurvey { }
    public class DeliveryLimit : AbstractDeliveryLimit { }
    public class MasterPayType : AbstractMasterPayType { }
    public class OtherServiceIcons : AbstractOtherServiceIcons { }
    public class HowGinnyBuddyWorks : AbstractHowGinnyBuddyWorks { }
    public class DeliveryExecutiveHealth : AbstractDeliveryExecutiveHealth { }
    public class ResponderVsTrialVideos : AbstractResponderVsTrialVideos { }
    public class TrialVideos : AbstractTrialVideos { }
    public class AdvertisePreferredCategory : AbstractAdvertisePreferredCategory { }
    public class AdvertiseMaster : AbstractAdvertiseMaster { }
    public class SellerMaster : AbstractSellerMaster { }
    public class AddressType : AbstractAddressType { }
    public class RequestDocuments : AbstractRequestDocuments { }
    public class LookupStatus : AbstractLookupStatus { }
    public class DeliveryExecutiveLatLong : AbstractDeliveryExecutiveLatLong { }
    public class AddressMaster : AbstractAddressMaster { }
    public class CityMaster : AbstractCityMaster { }
    public class StateMaster : AbstractStateMaster { }
    public class CountryMaster : AbstractCountryMaster { }
    public class SubServiceMaster : AbstractSubServiceMaster { }
    public class ServiceMaster : AbstractServiceMaster { }
    public class UserDevices : AbstractUserDevices { }
    public class DeliveryExecutiveCheckInCheckOut : AbstractDeliveryExecutiveCheckInCheckOut { }
    public class DeliveryExecutive : AbstractDeliveryExecutive { }
    public class DeliveryExecutiveDevices : AbstractDeliveryExecutiveDevices { }
    public class DocumentTypeMaster : AbstractDocumentTypeMaster { }
    public class Admin : AbstractAdmin { }
    public class AdminType : AbstractAdminType { }
    public class DeliveryExecutiveAdminComments : AbstractDeliveryExecutiveAdminComments { }
    public class DeliveryExecutiveDocuments : AbstractDeliveryExecutiveDocuments { }
    public class UsersLatLong : AbstractUsersLatLong { }
    public class UserNotifications : AbstractUserNotifications { }
    public class DeliveryExecutiveNotifications : AbstractDeliveryExecutiveNotifications { }
    public class Requests : AbstractRequests { }
    public class RequestsStatusTracker : AbstractRequestsStatusTracker { }
    public class RequestsDelieveryExecutiveTracker : AbstractRequestsDelieveryExecutiveTracker { }
    public class RequestsCount : AbstractRequestsCount { }
    public class RequestsReachTime : AbstractRequestsReachTime { }
    public class SubSubServiceMaster : AbstractSubSubServiceMaster { }
    public class TagsMaster : AbstractTagsMaster { }
    public class UsersFeedback : AbstractUsersFeedback { }
    public class DeliveryExecutiveFeedback : AbstractDeliveryExecutiveFeedback { }
    public class AgencyMaster : AbstractAgencyMaster { }
    public class AddressMasterRequestModel : AbstractAddressMasterRequestModel { }
    public class MasterCategory : AbstractMasterCategory { }
    public class CategoryVsDeliveryExecutive : AbstractCategoryVsDeliveryExecutive { }
    public class SelectedAddress : AbstractSelectedAddress { }
    public class AreaMaster : AbstractAreaMaster { }
    public class Requestcomments : AbstractRequestcomments { }
    public class CustomersNotification : AbstractCustomersNotification { }
    public class AgencyVsArea : AbstractAgencyVsArea { }
    public class AreaVsStaff : AbstractAreaVsStaff { }
    public class MasterServices : AbstractMasterServices { }

    //Chai Nasta
    public class Vendors : AbstractVendors { }
    public class VendorsVsReferences : AbstractVendorsVsReferences { }
    public class VendorsThelaImages : AbstractVendorsThelaImages { }
    public class Items : AbstractItems { }
    public class VendorsVsItems : AbstractVendorsVsItems { }
    public class OnboarderLatLong : AbstractOnboarderLatLong { }

}
