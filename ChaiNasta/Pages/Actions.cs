﻿namespace ChaiNasta.Pages
{
    public class Actions
    {
        public const string Signin = "Signin";
        public const string Signout = "Signout";
        public const string Index = "Index";
        public const string VendorAllData = "VendorAllData";
        public const string ItemsAllData = "ItemsAllData";
        public const string ItemDelete = "ItemDelete";
        public const string ItemActInAct = "ItemActInAct";
        public const string SaveItemDetails = "SaveItemDetails";
        public const string Details = "Details";
        public const string VendorDelete = "VendorDelete";
        public const string VendorActInAct = "VendorActInAct";
        public const string ManageItems = "ManageItems";
        public const string VendorProfileDetails = "VendorProfileDetails";
        public const string GetAllItems = "GetAllItems";
        public const string VendorData = "VendorData";
        public const string VendorsThelaImages_Delete = "VendorsThelaImages_Delete";
        public const string VendorsThelaImages_UpdateIsPrimary = "VendorsThelaImages_UpdateIsPrimary";
        public const string VendorsVsReferences_Delete = "VendorsVsReferences_Delete";
        public const string VendorsVsReferences_Upsert = "VendorsVsReferences_Upsert";
        public const string VendorsVsReferences_ByVendorsId = "VendorsVsReferences_ByVendorsId";
        public const string References = "References";

        public const string VendorsVsItems_AddItemsList = "VendorsVsItems_AddItemsList";
        public const string AddItems = "AddItems";
        public const string MyItems = "MyItems";
        public const string VendorsVsItems_Delete = "VendorsVsItems_Delete";
    }
}