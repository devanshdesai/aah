﻿using System.Collections.Generic;

namespace ChaiNasta.Pages
{
    public class Controllers
    {
        public const string Account = "Account";
        public const string Vendors = "Vendors";
        public const string VendorsVsReferences = "VendorsVsReferences";
        public const string Items = "Items";
        public const string Home = "Home";
        public const string Authentication = "Authentication";
        public const string ChaiNasta = "ChaiNasta";

    }
}