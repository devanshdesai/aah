﻿using AAH.Common;
using ChaiNasta.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Common.Paging;
using AAH.Entities.V1;

namespace ChaiNasta.Infrastructure
{

    public class BaseController : Controller
    {
        public BaseController()
        {

        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {

                bool isAllow = false;
                HttpCookie reqCookie = Request.Cookies["ChaiNastaLogin"];
                if (reqCookie != null)
                {
                    ProjectSession.AdminId = Convert.ToInt64(Convert.ToString(reqCookie["Id"]));
                    ProjectSession.AdminName = Convert.ToString(reqCookie["Name"]);
                    ProjectSession.LoginAdminEmail = Convert.ToString(reqCookie["Email"]);
                    ProjectSession.UserType = Convert.ToString(reqCookie["UserType"]);
                }
                else
                {
                    ProjectSession.AdminId = 0;
                }

                string var_sql = "select *,(select Name from AdminType where Id = AdminTypeId) AS AdminTypeName" +
                    " from Admin where  DeletedBy = 0 AND Id = " + ProjectSession.AdminId;

                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dt);

                if (dt.Rows.Count > 0)
                {

                    ProjectSession.AdminName = dt.Rows[0]["Name"].ToString();
                    ProjectSession.LoginAdminEmail = dt.Rows[0]["Email"].ToString();
                    ProjectSession.UserType = dt.Rows[0]["AdminTypeName"].ToString();
                    if (ProjectSession.UserType != "On boarding Staff")
                    {
                        isAllow = false;
                    }
                    else
                    {
                        isAllow = true;
                    }
                }
                else
                {
                    isAllow = false;
                }

                if (!isAllow)
                {
                    filterContext.Result = new RedirectResult("~/Authentication/Signin");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Authentication/Signin");
            }
        }
    }
}