﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Web;
using System.IO;
using static AAHAdmin.Infrastructure.Enums;

namespace AAHAdmin.Controllers
{
    public class AreaMasterController : BaseController
    {
        public readonly AbstractAreaMasterServices abstractAreaMasterServices ;
        public readonly AbstractCityMasterServices abstractCityMasterServices;
        public readonly AbstractStateMasterServices abstractStateMasterServices;
        
        public AreaMasterController
            (AbstractAreaMasterServices abstractAreaMasterServices,
            AbstractCityMasterServices abstractCityMasterServices,
            AbstractStateMasterServices abstractStateMasterServices)
        {
            this.abstractAreaMasterServices = abstractAreaMasterServices;
            this.abstractCityMasterServices = abstractCityMasterServices;
            this.abstractStateMasterServices = abstractStateMasterServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }

        }

        public ActionResult Details(string AID = "MA==")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractAreaMaster abstractPayments = null;
            if (AID != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(AID));
                if (decryptedId > 0)
                {
                    abstractPayments = abstractAreaMasterServices.AreaMaster_ById(decryptedId).Item;
                }
            }
            ViewBag.StateMasterData = StateMasterData();
            ViewBag.CityMasterData = CityMasterData();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View(abstractPayments);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.AreaMaster_Details)]
        public ActionResult AreaMaster_Details(AreaMaster areaMaster)
        {
            try
            {
                SuccessResult<AbstractAreaMaster> result = new SuccessResult<AbstractAreaMaster>();
          
                areaMaster.CreatedBy = ProjectSession.AdminId;
                areaMaster.UpdatedBy = ProjectSession.AdminId;

                result = abstractAreaMasterServices.AreaMaster_Upsert(areaMaster);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.Index, Pages.Controllers.AreaMaster, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception error)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), error);
            }
            return RedirectToAction(Actions.Index, Pages.Controllers.AreaMaster, new { Area = "" });
        }

        [HttpPost]
        public JsonResult AreaMaster_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAreaMasterServices.AreaMaster_All(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.AreaMaster_Delete)]
        public ActionResult AreaMaster_Delete(string Id = "MA==")
        {
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
            var result = abstractAreaMasterServices.AreaMaster_Delete(decryptedId, (int)ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AreaMaster_ActInAct(long Id = 0, int UpdatedBy = 0)
        {
            SuccessResult<AbstractAreaMaster> customerData = new SuccessResult<AbstractAreaMaster>();
            try
            {
                UpdatedBy = ConvertTo.Integer(ProjectSession.AdminId);
                customerData = abstractAreaMasterServices.AreaMaster_ActInAct(Id, UpdatedBy);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult AreaMaster_AppUnApp(long Id = 0, int UpdatedBy = 0)
        {
            SuccessResult<AbstractAreaMaster> customerData = new SuccessResult<AbstractAreaMaster>();
            try
            {
                UpdatedBy = ConvertTo.Integer(ProjectSession.AdminId);
                var type = ProjectSession.UserType == "Super Admin" ? 1 : 0;
                customerData = abstractAreaMasterServices.AreaMaster_AppUnApp(Id, UpdatedBy, type);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> CityMasterData()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var model = abstractCityMasterServices.CityMaster_ByStateMasterId(pageParam, "",0).Values;

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select City", Value = "0" });
            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }
            return items;
        }

        public IList<SelectListItem> StateMasterData()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var model = abstractStateMasterServices.StateMaster_ByCountryMasterId(pageParam, "",0).Values;

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });
            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }
            return items;
        }

    }
}