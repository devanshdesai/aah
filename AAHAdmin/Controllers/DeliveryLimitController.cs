﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Web;
using System.IO;

namespace AAHAdmin.Controllers
{
    public class DeliveryLimitController : BaseController
    {
        public readonly AbstractDeliveryLimitServices abstractDeliveryLimitServices ;

        public DeliveryLimitController(AbstractDeliveryLimitServices abstractDeliveryLimitServices)
        {
            this.abstractDeliveryLimitServices = abstractDeliveryLimitServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult DeliveryLimit_ById(long Id)
        {
            SuccessResult<AbstractDeliveryLimit> successResult = abstractDeliveryLimitServices.DeliveryLimit_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        //DeliveryLimit owner all data get
        [HttpPost]
        public JsonResult DeliveryLimit_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search  = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDeliveryLimitServices.DeliveryLimit_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeliveryLimit_Upsert(HttpPostedFileBase files, DeliveryLimit deliveryLimit)
        {

            SuccessResult<AbstractDeliveryLimit> bannerData = new SuccessResult<AbstractDeliveryLimit>();

            try
            {
                if (deliveryLimit.Id > 0)
                {

                    if (files != null)
                    {
                        string basePath = "doc/DeliveryLimit/" + deliveryLimit.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        deliveryLimit.ImageURL = basePath + fileName;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    }
                    bannerData = abstractDeliveryLimitServices.DeliveryLimit_Upsert(deliveryLimit);
                }

                if (deliveryLimit.Id == 0)
                {
                    string basePath = "doc/DeliveryLimit/" + deliveryLimit.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    deliveryLimit.ImageURL = basePath + fileName;
                    //_abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), banners.BannerUrl);
                    bannerData = abstractDeliveryLimitServices.DeliveryLimit_Upsert(deliveryLimit);
                }

            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }



        public JsonResult DeliveryLimit_Delete(long Id)  //if error delete int deletedBy  
        {
            int DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractDeliveryLimitServices.DeliveryLimit_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

       


    }
}