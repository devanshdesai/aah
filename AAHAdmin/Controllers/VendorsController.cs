﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class VendorsController : BaseController
    {
        public readonly AbstractVendorsServices abstractVendorsServices;
        public readonly AbstractItemsServices abstractItemsServices;
        public readonly AbstractAdminServices abstractAdminServices;
        public VendorsController (AbstractVendorsServices abstractVendorsServices,
            AbstractItemsServices abstractItemsServices,
            AbstractAdminServices abstractAdminServices)
        {
            this.abstractVendorsServices = abstractVendorsServices;
            this.abstractItemsServices = abstractItemsServices;
            this.abstractAdminServices = abstractAdminServices;
        }
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Super Admin" || ProjectSession.UserType == "Admin" || ProjectSession.UserType == "Back office Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Advertising Staff" ||ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public ActionResult Details(string vi = "MA==")
        {
            ViewBag.VendorId = Convert.ToInt32(ConvertTo.Base64Decode(vi));
            if (ProjectSession.UserType == "Super Admin" || ProjectSession.UserType == "Admin" || ProjectSession.UserType == "Back office Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }


        public JsonResult Vendor_ValidArea(string Latitude,string Longitute)
        {
            long StaffId = ProjectSession.AdminId;
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var areaData = abstractAdminServices.Vendor_ValidArea(pageParam, "", StaffId).Values;
            bool isInArea = false;
            for (int i = 0; i < areaData.Count; i++)
            {
                List<double> latlong = new List<double>();
                double l = new double();
                l = Convert.ToDouble(Latitude);
                latlong.Add(l);
                l = Convert.ToDouble(Longitute);
                latlong.Add(l);

                if (areaData[i].GeoFencing != null)
                {
                    var polygonData = JsonConvert.DeserializeObject<List<ARoot>>(areaData[i].GeoFencing);
                    isInArea = inside(polygonData, latlong);

                    if (isInArea)
                    {
                        break;
                    }
                }
            }
            //return AreaMasterId;
            //var result =(AreaMasterId);
            return Json(isInArea, JsonRequestBehavior.AllowGet);
        }
        public bool inside(List<ARoot> vs, List<double> point)
        {

            var x = point[0];
            var y = point[1];

            var inside = false;
            for (int i = 0, j = vs.Count - 1; i < vs.Count; j = i++)
            {
                var xi = vs[i].lat;
                var yi = vs[i].lng;
                var xj = vs[j].lat;
                var yj = vs[j].lng;

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        }
        public class pRoot
        {
            public List<List<double>> MyArray { get; set; }
        }

        public class ARoot
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }
        [HttpPost]
        public JsonResult VendorAllData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractVendorsServices.Vendors_All(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Vendors_ByAdminId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                long AdminId = ProjectSession.AdminId;
                string search = Convert.ToString(requestModel.Search.Value);

                var response = abstractVendorsServices.Vendors_ByAdminId(pageParam,search,AdminId);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.VendorDelete)]
        public ActionResult VendorDelete(string Id = "MA==")
        {
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
            var result = abstractVendorsServices.Vendors_Delete(decryptedId, (int)ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.VendorActInAct)]
        public JsonResult VendorActInAct(long Id = 0)
        {
            var result = abstractVendorsServices.Vendors_ActInAct(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.VendorData)]
        public JsonResult VendorData(long Id = 0)
        {
            var result = abstractVendorsServices.Vendors_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.GetAllItems)]
        public ActionResult GetAllItems(string Search = "")
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.SearchItems_All(pageParam, Search, ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName(Actions.ManageItems)]
        public ActionResult ManageItems(string Search = "")
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.Items_All(pageParam, Search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName(Actions.VendorsVsItems_AddItemsList)]
        public ActionResult VendorsVsItems_AddItemsList(string Search = "", long vendorsId = 0)
        {
           
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.VendorsVsItems_AddItemsList(pageParam, Search, vendorsId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddItems(string itemsId,long vendorsId = 0)
        {
           

            VendorsVsItems items = new VendorsVsItems();
            items.ItemsIds = itemsId;
            items.CreatedBy = ProjectSession.AdminId;
            items.VendorsId = vendorsId;

            var result = abstractVendorsServices.VendorsVsItems_Upsert(items);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult VendorsVsItems_Delete(long Id, long vendorId = 0)
        {
            var result = abstractVendorsServices.VendorsVsItems_Delete(Id, vendorId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.MyItems)]
        public ActionResult MyItems(string Search = "", long vendorsId = 0)
        {
           
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractVendorsServices.VendorsVsItems_ByVendorsId(pageParam, Search, vendorsId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VendorsThelaImages_Delete(string Url, long vendorId)
        {
            SuccessResult<AbstractVendors> result = abstractVendorsServices.VendorsThelaImages_Delete(Url, vendorId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool VendorsThelaImages_UpdateIsPrimary(long Id, long vendorId)
        {
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("VendorsThelaImages_UpdateIsPrimary", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Id", SqlDbType.BigInt).Value = Id;
                    cmd.Parameters.Add("@vendorId", SqlDbType.BigInt).Value = vendorId;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            return true;
        }

        [System.Web.Http.HttpPost]
        public JsonResult VendorProfileDetails(
            HttpPostedFileBase[] uploadFile,
            HttpPostedFileBase ShopOwnerProfile,
            HttpPostedFileBase ElectionIDImage,
            HttpPostedFileBase AadharImage,
            string ImagePrimary = "",
            long Id = 0,
            string FirstName = "",
            string LastName = "",
            string ShopName = "",
            string GujaratiShopName = "",
            string HindiShopName = "",
            string PrimaryPhoneNumber = "",
            string AlternatePhoneNumber = "",
            string AadharCardNumber = "",
            string ElectionIDNumber = "",
            string Location = "",
            string ThelaImages = "",
            string Items = "",
            string Lat = "",
            string Long = "",
            string CreatedLat = "",
            string CreatedLong = "",
            string UpdatedLat = "",
            string UpdatedLong = "",
            bool IsReadyForCommition = false,
            bool IsDeliveryBoy = false
        )
        {
            Vendors vendors = new Vendors();
            vendors.Id = Id;
            vendors.FirstName = FirstName;
            vendors.LastName = LastName;
            vendors.ShopName = ShopName;
            vendors.GujaratiShopName = GujaratiShopName;
            vendors.HindiShopName = HindiShopName;
            vendors.PrimaryPhoneNumber = PrimaryPhoneNumber;
            vendors.AlternatePhoneNumber = AlternatePhoneNumber;
            vendors.Location = Location;
            vendors.ThelaImages = ThelaImages;
            vendors.AadharCardNumber = AadharCardNumber;
            vendors.ElectionIDNumber = ElectionIDNumber;
            vendors.Items = Items;
            vendors.Lat = Lat;
            vendors.Long = Long;
            vendors.IsDeliveryBoy = IsDeliveryBoy;
            vendors.IsReadyForCommition = IsReadyForCommition;
            vendors.CreatedLat = CreatedLat;
            vendors.CreatedLong = CreatedLong;
            vendors.UpdatedLat = UpdatedLat;
            vendors.UpdatedLong = UpdatedLong;
            vendors.CreatedBy = ProjectSession.AdminId;
            vendors.UpdatedBy = ProjectSession.AdminId;
            vendors.ReferenceCode = ProjectSession.AdminId;

            if (ShopOwnerProfile != null)
            {
                string basePath = "assets/docs/ShopOwnerImage/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(ShopOwnerProfile.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                ShopOwnerProfile.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                vendors.ShopOwnerProfile = basePath + fileName;
            }

            if (AadharImage != null)
            {
                string basePath = "assets/docs/AadharImage/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(AadharImage.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                AadharImage.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                vendors.AadharImage = basePath + fileName;
            }

            if (ElectionIDImage != null)
            {
                string basePath = "assets/docs/ElectionIDImage/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(ElectionIDImage.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                ElectionIDImage.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                vendors.ElectionIDImage = basePath + fileName;
            }


            var result = abstractVendorsServices.Vendors_Upsert(vendors);

            if (uploadFile != null)
            {
                IsPrimaryKey IsPrimaryKey = new IsPrimaryKey();

                IsPrimaryKey.AbPrimaryObj = JsonConvert.DeserializeObject<List<AbPrimary>>(Convert.ToString(ImagePrimary));

                for (var i = 0; i < uploadFile.Length; i++)
                {
                    VendorsThelaImages vendorsThelaImages = new VendorsThelaImages();
                    vendorsThelaImages.IsPrimary = IsPrimaryKey.AbPrimaryObj[i].IsPrimary;
                    vendorsThelaImages.VendorId = result.Item.Id;

                    string basePath = "assets/docs/VendorsThelaImages/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(uploadFile[i].FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    uploadFile[i].SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    vendorsThelaImages.ImagesUrl = basePath + fileName;

                    abstractVendorsServices.VendorsThelaImages_Upsert(vendorsThelaImages);
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}