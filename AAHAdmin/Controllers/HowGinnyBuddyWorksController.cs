﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Web;
using System.IO;

namespace AAHAdmin.Controllers
{
    public class HowGinnyBuddyWorksController : BaseController
    {
        public readonly AbstractHowGinnyBuddyWorksServices abstractHowGinnyBuddyWorksServices ;

        public HowGinnyBuddyWorksController(AbstractHowGinnyBuddyWorksServices abstractHowGinnyBuddyWorksServices)
        {
            this.abstractHowGinnyBuddyWorksServices = abstractHowGinnyBuddyWorksServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult HowGinnyBuddyWorks_ById(long Id)
        {
            SuccessResult<AbstractHowGinnyBuddyWorks> successResult = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        //HowGinnyBuddyWorks owner all data get
        [HttpPost]
        public JsonResult HowGinnyBuddyWorks_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search  = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult HowGinnyBuddyWorks_Upsert(HttpPostedFileBase files, HowGinnyBuddyWorks howGinnyBuddyWorks)
        {

            SuccessResult<AbstractHowGinnyBuddyWorks> bannerData = new SuccessResult<AbstractHowGinnyBuddyWorks>();

            try
            {
                if (howGinnyBuddyWorks.Id > 0)
                {

                    if (files != null)
                    {
                        string basePath = "doc/HowGinnyBuddyWorks/" + howGinnyBuddyWorks.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        howGinnyBuddyWorks.URL = basePath + fileName;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    }
                    bannerData = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_Upsert(howGinnyBuddyWorks);
                }

                if (howGinnyBuddyWorks.Id == 0)
                {
                    string basePath = "doc/HowGinnyBuddyWorks/" + howGinnyBuddyWorks.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    howGinnyBuddyWorks.URL = basePath + fileName;
                    //_abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), banners.BannerUrl);
                    bannerData = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_Upsert(howGinnyBuddyWorks);
                }

            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HowGinnyBuddyWorks_Delete(long Id)  //if error delete int deletedBy  
        {
            int DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HowGinnyBuddyWorks_ActInAct(long Id = 0, int UpdatedBy = 0)
        {
            SuccessResult<AbstractHowGinnyBuddyWorks> customerData = new SuccessResult<AbstractHowGinnyBuddyWorks>();
            try
            {
                customerData = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_ActInAct(Id, UpdatedBy);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }


    }
}