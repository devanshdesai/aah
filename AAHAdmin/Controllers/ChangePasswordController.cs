﻿using DataTables.Mvc;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class ChangePasswordController : BaseController
    {
        #region Fields
        private readonly AbstractAdminServices abstractAdminServices;
        #endregion

        #region Ctor
        public ChangePasswordController(AbstractAdminServices abstractAdminServices)
        {
            this.abstractAdminServices = abstractAdminServices;
        }
        #endregion

        #region Methods
        [HttpGet]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            SuccessResult<AbstractAdmin> result = abstractAdminServices.Admin_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);

            var welcome = "Greetings From Ginny Buddy.";
            EmailHelper.SendEmail(welcome, result.Item.Email, result.Item.Name, "Email : " + result.Item.Email + " , Password : " + result.Item.Password + " Information Updated", result.Item.Password);

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        #endregion
    }
}