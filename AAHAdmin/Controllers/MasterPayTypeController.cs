﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;

namespace AAHAdmin.Controllers
{
    public class MasterPayTypeController : BaseController
    {
        public readonly AbstractAgencyMasterServices abstractAgencyMasterServices;
        public readonly AbstractMasterPayTypeServices abstractMasterPayTypeServices;

        public MasterPayTypeController(AbstractAgencyMasterServices abstractAgencyMasterServices, AbstractMasterPayTypeServices abstractMasterPayTypeServices
        )
        {
            this.abstractAgencyMasterServices = abstractAgencyMasterServices;
            this.abstractMasterPayTypeServices = abstractMasterPayTypeServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult MasterPayType_ById(long Id = 0)
        {
            SuccessResult<AbstractMasterPayType> successResult = abstractMasterPayTypeServices.MasterPayType_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MasterPayType_Delete(long Id)  //if error delete int deletedBy  
        {
            int DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractMasterPayTypeServices.MasterPayType_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //MasterPayTypeData_All
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult MasterPayType_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractMasterPayTypeServices.MasterPayType_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MasterPayType_Upsert(long Id = 0, string Name = "",string Description = "",decimal OriginalPrice=0,decimal DiscountPrice = 0,int CreatedBy = 0,int UpdatedBy = 0)
        {
            MasterPayType model = new MasterPayType();
            model.Id = Id;
            model.Name = Name;
            model.Description = Description;
            model.OriginalPrice = OriginalPrice;
            model.DiscountPrice = DiscountPrice;
            model.CreatedBy = CreatedBy;
            model.UpdatedBy = UpdatedBy;
            var result = abstractMasterPayTypeServices.MasterPayType_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MasterPayType_ActInAct(long Id = 0, int UpdatedBy = 0)
        {
            SuccessResult<AbstractMasterPayType> customerData = new SuccessResult<AbstractMasterPayType>();
            try
            {
                customerData = abstractMasterPayTypeServices.MasterPayType_ActInAct(Id, UpdatedBy);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }
    }
}