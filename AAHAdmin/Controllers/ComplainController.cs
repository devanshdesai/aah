﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class ComplainController : BaseController
    {
        public readonly AbstractTicketServices abstractTicketServices;
        public readonly AbstractTicketCommentsServices abstractTicketCommentsServices;
        
        public ComplainController(
            AbstractTicketServices abstractTicketServices,
            AbstractTicketCommentsServices abstractTicketCommentsServices
            )
        {
            this.abstractTicketServices = abstractTicketServices;
            this.abstractTicketCommentsServices = abstractTicketCommentsServices;

        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            //ViewBag.decryptedDIId = Convert.ToInt32(ConvertTo.Base64Decode(DI));
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Details(string vi = "MA==")
        {
            ViewBag.Id = Convert.ToInt32(ConvertTo.Base64Decode(vi));
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        [ActionName(Actions.TicketComments_ById)]
        public JsonResult TicketComments_ById(long Id = 0)
        {
            var result = abstractTicketCommentsServices.TicketComments_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        } 
        [HttpPost]
        [ActionName(Actions.TicketComments_Delete)]
        public JsonResult TicketComments_Delete(long Id = 0)
        {
            var DeletedBy = ProjectSession.AdminId;
            var result = abstractTicketCommentsServices.TicketComments_Delete(Id, DeletedBy);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ActionName(Actions.Ticket_ById)]
        public JsonResult Ticket_ById(long Id = 0)
        {
            var result = abstractTicketServices.Ticket_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Ticket_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractTicketServices.Ticket_All(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.Ticket_ChangeStatus)]
        public JsonResult Ticket_ChangeStatus(long Id)
        {
            var UpdatedBy = ProjectSession.AdminId;
            var result = abstractTicketServices.Ticket_ChangeStatus(Id, UpdatedBy);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.Ticket_ChangePlatform)]
        public JsonResult Ticket_ChangePlatform(long Id)
        {
            var UpdatedBy = ProjectSession.AdminId;
            var result = abstractTicketServices.Ticket_ChangePlatform(Id, UpdatedBy);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ActionName(Actions.Ticket_ChangeType)]
        public JsonResult Ticket_ChangeType(long Id)
        {
            var UpdatedBy = ProjectSession.AdminId;
            var result = abstractTicketServices.Ticket_ChangeType(Id, UpdatedBy);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TicketComments_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractTicketCommentsServices.TicketComments_All(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult TicketComments_ByTicketId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long TicketId=0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                

                var response = abstractTicketCommentsServices.TicketComments_ByTicketId(pageParam, search, TicketId);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.TicketComments_Upsert)]
        public JsonResult TicketComments_Upsert(TicketComments ticketComments)
        {
            var result = abstractTicketCommentsServices.TicketComments_Upsert(ticketComments);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult MasterCategory_Upsert(int Id = 0, string Name = "")
        //{
        //    MasterCategory model = new MasterCategory();
        //    model.Id = Id;
        //    model.Name = Name;
        //    var result = abstractMasterCategoryServices.MasterCategory_Upsert(model);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
    }
}