﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class ReportController : BaseController
    {
        private readonly AbstractRequestsServices abstractRequestsServices= null;
        public readonly AbstractAdminServices abstractAdminServices;
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;

        public ReportController(AbstractRequestsServices abstractRequestsServices, AbstractAdminServices abstractAdminServices,
            AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices)
           
        {
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractAdminServices = abstractAdminServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
        }
        public ActionResult Index()
        {
            ViewBag.GetAgencyAll = GetAgency_All();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public JsonResult Report_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string FromDate = "", string ToDate = "",int AgencyId = 0)
       {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var loginId = ProjectSession.UserType == "Super Admin" ? AgencyId : ProjectSession.AdminId;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractRequestsServices.Report_All(pageParam, search, FromDate, ToDate, loginId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public IList<SelectListItem> GetAgency_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.Admin_All(pageParam, "", "", "", "", 0, 1);
            items.Add(new SelectListItem() { Text = "Select Agency", Value = "" });

            foreach (var master in result.Values)
            {
                if (master.AdminTypeId == 4 && master.IsActive)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
            }

            return items;
        }

        [HttpPost]
        public JsonResult updateIsSettledWithAgency(long Id = 0, int settleId = 0)
        {
            var DEData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(Id);
            string FromDate = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
            string ToDate = DateTime.Now.ToString("yyyy-MM-dd");

            try
            {
                var result = abstractRequestsServices.Requests_UpdateIsSettledWithAgency(DEData.Item.AgencyId, FromDate, ToDate, Id, settleId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}