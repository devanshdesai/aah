﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;

namespace AAHAdmin.Controllers
{
    public class ManageCategoryController : BaseController
    {
        public readonly AbstractAgencyMasterServices abstractAgencyMasterServices;
        public readonly AbstractMasterCategoryServices abstractMasterCategoryServices;

        public ManageCategoryController(AbstractAgencyMasterServices abstractAgencyMasterServices,
            AbstractMasterCategoryServices abstractMasterCategoryServices
        )
        {
            this.abstractAgencyMasterServices = abstractAgencyMasterServices;
            this.abstractMasterCategoryServices = abstractMasterCategoryServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult GetCategoryById(int Id = 0)
        {
            SuccessResult<AbstractMasterCategory> successResult = abstractMasterCategoryServices.MasterCategory_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteCategoryData(int Id = 0)
        {
            SuccessResult<AbstractMasterCategory> successResult = abstractMasterCategoryServices.MasterCategory_Delete(Id , ProjectSession.AdminId);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        //MasterCategoryData_All
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult MasterCategoryData_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractMasterCategoryServices.MasterCategory_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MasterCategory_Upsert(int Id = 0, string Name = "")
        {
            MasterCategory model = new MasterCategory();
            model.Id = Id;
            model.Name = Name;
            var result = abstractMasterCategoryServices.MasterCategory_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}