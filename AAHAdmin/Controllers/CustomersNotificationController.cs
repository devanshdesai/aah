﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Web;
using System.IO;

namespace AAHAdmin.Controllers
{
    public class CustomersNotificationController : BaseController
    {
        public readonly AbstractCustomersNotificationServices abstractCustomersNotificationServices;

        public CustomersNotificationController(AbstractCustomersNotificationServices abstractCustomersNotificationServices)
        {
            this.abstractCustomersNotificationServices = abstractCustomersNotificationServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
         
            else
            {
                return View();
            }
        }

        public JsonResult CustomersNotificationUpsert(string Description = "")
        {
            CustomersNotification model = new CustomersNotification();
            
            model.Id = 0;
            model.Description = Description;
            model.CreatedBy = ProjectSession.AdminId;

            var result = abstractCustomersNotificationServices.CustomersNotification_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}