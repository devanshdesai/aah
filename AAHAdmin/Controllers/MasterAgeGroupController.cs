﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;

namespace AAHAdmin.Controllers
{
    public class MasterAgeGroupController : BaseController
    {
        public readonly AbstractAgencyMasterServices abstractAgencyMasterServices;
        public readonly AbstractMasterAgeGroupServices abstractMasterAgeGroupServices;

        public MasterAgeGroupController(AbstractAgencyMasterServices abstractAgencyMasterServices, AbstractMasterAgeGroupServices abstractMasterAgeGroupServices
        )
        {
            this.abstractAgencyMasterServices = abstractAgencyMasterServices;
            this.abstractMasterAgeGroupServices = abstractMasterAgeGroupServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult MasterAgeGroup_ById(long Id = 0)
        {
            SuccessResult<AbstractMasterAgeGroup> successResult = abstractMasterAgeGroupServices.MasterAgeGroup_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MasterAgeGroup_Delete(long Id)  //if error delete int deletedBy  
        {
            int DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractMasterAgeGroupServices.MasterAgeGroup_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //MasterAgeGroupData_All
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult MasterAgeGroup_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractMasterAgeGroupServices.MasterAgeGroup_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MasterAgeGroup_Upsert(long Id = 0, string Name = "",string DisplayName = "",int CreatedBy = 0,int UpdatedBy = 0)
        {
            MasterAgeGroup model = new MasterAgeGroup();
            model.Id = Id;
            model.Name = Name;
            model.DisplayName = DisplayName;
            model.CreatedBy = CreatedBy;
            model.UpdatedBy = UpdatedBy;
            var result = abstractMasterAgeGroupServices.MasterAgeGroup_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
    }
}