﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Web;
using System.IO;

namespace AAHAdmin.Controllers
{
    public class TrialVideosController : BaseController
    {
        public readonly AbstractTrialVideosServices abstractTrialVideosServices ;

        public TrialVideosController(AbstractTrialVideosServices abstractTrialVideosServices)
        {
            this.abstractTrialVideosServices = abstractTrialVideosServices;
        }

        [ActionName(Actions.Home)]
        public ActionResult Home()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult TrialVideos_ById(int Id)
        {
            SuccessResult<AbstractTrialVideos> successResult = abstractTrialVideosServices.TrialVideos_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        //TrialVideos owner all data get
        [HttpPost]
        public JsonResult TrialVideos_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search  = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractTrialVideosServices.TrialVideos_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult TrialVideos_Upsert(HttpPostedFileBase files, TrialVideos TrialVideos)
        {

            SuccessResult<AbstractTrialVideos> bannerData = new SuccessResult<AbstractTrialVideos>();

            try
            {
                if (TrialVideos.Id > 0)
                {
                    
                    if (files != null)
                    {
                        string basePath = "TrialVideos/" + TrialVideos.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        TrialVideos.URL = basePath + fileName;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    }
                    bannerData = abstractTrialVideosServices.TrialVideos_Upsert(TrialVideos);
                }

                if (TrialVideos.Id == 0)
                {
                    string basePath = "TrialVideos/" + TrialVideos.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    TrialVideos.URL = basePath + fileName;
                    //_abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), banners.BannerUrl);
                    bannerData = abstractTrialVideosServices.TrialVideos_Upsert(TrialVideos);
                }

            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult TrialVideos_Delete(int Id)  //if error delete int deletedBy  
        {
            int DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractTrialVideosServices.TrialVideos_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TrialVideos_ActInAct(int Id = 0, int UpdatedBy = 0)
        {
            SuccessResult<AbstractTrialVideos> customerData = new SuccessResult<AbstractTrialVideos>();
            try
            {
                customerData = abstractTrialVideosServices.TrialVideos_ActInAct(Id, UpdatedBy);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }


    }
}