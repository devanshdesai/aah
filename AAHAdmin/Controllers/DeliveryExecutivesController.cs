﻿using DataTables.Mvc;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using static AAHAdmin.Infrastructure.Enums;

namespace AAHAdmin.Controllers
{
    public class DeliveryExecutivesController : BaseController
    {
        private readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices = null;
        private readonly AbstractDeliveryExecutiveDocumentsServices abstractDeliveryExecutiveDocumentsServices = null;
        private readonly AbstractDeliveryExecutiveCheckInCheckOutServices abstractDeliveryExecutiveCheckInCheckOutServices = null;
        public readonly AbstractRequestsServices abstractRequestsServices = null;
        public readonly AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices = null;
        public readonly AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices = null;
        public readonly AbstractAreaMasterServices abstractAreaMasterServices;
        public readonly AbstractAdminServices abstractAdminServices;

        public DeliveryExecutivesController(AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices,
            AbstractDeliveryExecutiveDocumentsServices abstractDeliveryExecutiveDocumentsServices,
            AbstractDeliveryExecutiveCheckInCheckOutServices abstractDeliveryExecutiveCheckInCheckOutServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices,
            AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices, AbstractAreaMasterServices abstractAreaMasterServices,
            AbstractAdminServices abstractAdminServices)
        {
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
            this.abstractDeliveryExecutiveDocumentsServices = abstractDeliveryExecutiveDocumentsServices;
            this.abstractDeliveryExecutiveCheckInCheckOutServices = abstractDeliveryExecutiveCheckInCheckOutServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractDeliveryExecutiveNotificationsServices = abstractDeliveryExecutiveNotificationsServices;
            this.abstractDeliveryExecutiveLatLongServices = abstractDeliveryExecutiveLatLongServices;
            this.abstractAreaMasterServices = abstractAreaMasterServices;
            this.abstractAdminServices = abstractAdminServices;
        }

        //DeliveryExecutive Index action
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ViewBag.GetAgencyAll = GetAgency_All();
            ViewBag.GetMasterAreaAll = GetMasterArea_All();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Reports()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Details(string DID = "MA==")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractDeliveryExecutive abstractDeliveryExecutive = null;
            if (DID != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(DID));
                if (decryptedId > 0)
                {
                    abstractDeliveryExecutive = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(decryptedId).Item;
                    ViewBag.MasterAreaId = abstractDeliveryExecutive.MasterAreaId;
                }
                else
                {
                    ViewBag.MasterAreaId = 0;

                }
                ViewBag.DeliveryExecutiveId = decryptedId;

            }

            ViewBag.GetAgencyAll = GetAgency_All();
            ViewBag.GetMasterAreaAll = GetMasterArea_All();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View(abstractDeliveryExecutive);
            }
        }

        //public JsonResult DeliveryExecutive_Upsert(long Id = 0, string FName = "", string LName = "", string MobileNumber = "",string AddressLine1 = "", int AgencyId = 0,int MasterAreaId = 0, int CreatedBy = 0, int UpdatedBy = 0)
        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.DeliveryExecutive_Upsert)]
        public ActionResult DeliveryExecutive_Upsert(IEnumerable<HttpPostedFileBase> files, DeliveryExecutive deliveryExecutive)
        {
            DeliveryExecutive model = new DeliveryExecutive();
            model.Id = deliveryExecutive.Id;
            model.FirstName = deliveryExecutive.FirstName;
            model.LastName = deliveryExecutive.LastName;
            model.MobileNumber = deliveryExecutive.MobileNumber;
            model.MasterAreaId = deliveryExecutive.MasterAreaId;
            model.AddressLine1 = deliveryExecutive.AddressLine1;
            model.ParentName = deliveryExecutive.ParentName;
            model.ParentNumber = deliveryExecutive.ParentNumber;
            model.DrivingLicenceNumber = deliveryExecutive.DrivingLicenceNumber;
            model.CreatedBy = ProjectSession.AdminId;
            model.UpdatedBy = ProjectSession.AdminId;

            if (ProjectSession.UserType == "Agency Owner")
            {
                model.AgencyId = ProjectSession.AdminId;
            }
            else if (ProjectSession.UserType == "Super Admin")
            {
                model.AgencyId = deliveryExecutive.AgencyId;
            }
            SuccessResult<AbstractDeliveryExecutive> successResult = new SuccessResult<AbstractDeliveryExecutive>();
            var result = abstractDeliveryExecutiveServices.DeliveryExecutive_Upsert(model);

            if (files != null)
            {
                if (files.Count() > 0 && result.Item != null)
                {
                    for (int i = 0; i < files.Count(); i++)
                    {
                        if (files.ElementAt(i) != null)
                        {
                            model.Path = "doc/DeliveryExecutive/";
                            model.AvatarFolder = Server.MapPath("~/Content/doc/");

                            var file = files.ElementAt(i);
                            string imgName = string.Empty;

                            string basePath = "doc/DeliveryExecutive/" + result.Item.Id.ToString() + "/";
                            string fileName = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + Path.GetFileName(file.FileName);
                            string path = Server.MapPath("~/" + basePath);
                            if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                            {
                                Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                            }
                            file.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                            if (i == 0)
                            {
                                model.ProfileURL = basePath + fileName;
                            }
                            else if (i == 1)
                            {
                                model.PhotoIdProof = basePath + fileName;
                            }
                            else
                            {
                                model.AgreementCopy = basePath + fileName;
                            }
                        }
                        //else
                        //{
                        //    if (model.Id == 0)
                        //    {
                        //        successResult.Code = 400;
                        //        TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "please upload all files.!");
                        //        return RedirectToAction(Actions.Index, Pages.Controllers.DeliveryExecutives, new { Area = "" });
                        //    }
                        //}
                    }

                    //string basePath = "doc/DeliveryExecutive/" + deliveryExecutive.Id.ToString();
                    //string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    //string path = Server.MapPath("~/" + basePath);
                    //if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    //{
                    //    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    //}
                    //files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    //deliveryExecutive.ProfileURL = basePath + fileName;
                    //_abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), banners.BannerUrl);

                    model.Id = result.Item.Id;

                    var rr = abstractDeliveryExecutiveServices.DeliveryExecutive_Upsert(model);
                }
               
            }
           

            if (result != null && result.Code == 200 && result.Item != null)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                return RedirectToAction(Actions.Index, Pages.Controllers.DeliveryExecutives, new { Area = "" });
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
            }
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
            return RedirectToAction(Actions.Index, Pages.Controllers.DeliveryExecutives, new { Area = "" });
            }

            //return Json(rr, JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ViewAllData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string UserName = "",
            string MobileNumber = "", int Gender = 0, long IsActive = 0, long IsApproved = 0, long IsAvailable = 0, long AgencyId = 0, long MasterAreaId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;
                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                long loginId = 0;
                if(ProjectSession.UserType == "Agency Owner")
                {
                    loginId = ProjectSession.AdminId;
                }
                var response = abstractDeliveryExecutiveServices.DeliveryExecutive_All(pageParam, search, UserName, MobileNumber, Gender, IsActive, IsApproved, IsAvailable, loginId, AgencyId, MasterAreaId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        //DeliveryExecutive status actinact
        [HttpPost]
        public JsonResult ActiveInActive(string Id = "MA==")
        {
            long AId = Convert.ToInt64(ConvertTo.Base64Decode(Id));
            SuccessResult<AbstractDeliveryExecutive> result = abstractDeliveryExecutiveServices.DeliveryExecutive_ActInAct(AId, ProjectSession.AdminId);
            //result.Item = null;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeliveryExecutive_IsMarketingPerson(string Id = "MA==")
        {
            long AId = Convert.ToInt64(ConvertTo.Base64Decode(Id));
            SuccessResult<AbstractDeliveryExecutive> result = abstractDeliveryExecutiveServices.DeliveryExecutive_IsMarketingPerson(AId, ProjectSession.AdminId);
            //result.Item = null;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //DeliveryExecutive change status 
        [HttpPost]
        public JsonResult DeliveryExecutive_IsApproved(string Id = "MA==")
        {
            long AId = Convert.ToInt64(ConvertTo.Base64Decode(Id));
            SuccessResult<AbstractDeliveryExecutive> result = abstractDeliveryExecutiveServices.DeliveryExecutive_IsApproved(AId, ProjectSession.AdminId);
            //result.Item = null;
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeliveryExecutive_ByDeliveryExecutiveId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long DeliveryExecutiveId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDeliveryExecutiveServices.DeliveryExecutive_ByDeliveryExecutiveId(pageParam, search, DeliveryExecutiveId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeliveryExecutive_Reports([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDeliveryExecutiveServices.DeliveryExecutive_Reports(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult DeliveryExecutiveDocuments_ByDeliveryExecutiveId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long DeliveryExecutiveId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDeliveryExecutiveDocumentsServices.DeliveryExecutiveDocuments_ByDeliveryExecutiveId(pageParam, search, DeliveryExecutiveId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        //DeliveryExecutive status actinact
        [HttpPost]
        public JsonResult DeliveryExecutiveDocuments_Verify(string Id = "MA==")
        {
            long VId = Convert.ToInt64(ConvertTo.Base64Decode(Id));
            SuccessResult<AbstractDeliveryExecutiveDocuments> result = abstractDeliveryExecutiveDocumentsServices.DeliveryExecutiveDocuments_Verify(VId, ProjectSession.AdminId);
            //result.Item = null;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.Timesheet)]
        public ActionResult Timesheet(string DEId = "MA==")
        {
            ViewBag.DeliveryEId = DEId;
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public JsonResult DeliveryExecutiveCheckInCheckOut([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            string DeliveryExecutiveId = "MA==", string ActionFormDate = "", string ActionToDate = "", long TypeId = 0)
        {

            long DId = Convert.ToInt64(ConvertTo.Base64Decode(DeliveryExecutiveId));
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractDeliveryExecutiveCheckInCheckOutServices.DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId(pageParam,
            DId, ActionFormDate, ActionToDate, TypeId);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetDeliveryExecutiveDetails(string DId = "MA==")
        {
            long Id = Convert.ToInt64(ConvertTo.Base64Decode(DId));

            var result = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        

        [ActionName(Actions.DENotifications)]
        public ActionResult DENotifications(string REId = "MA==")
        {
            ViewBag.DEId = REId;
            ViewBag.GetRequestsAll = GetRequestByDE(REId);
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public IList<SelectListItem> GetRequestByDE(string REId = "MA==")
        {
            List<SelectListItem> items = new List<SelectListItem>();

            long DeId = Convert.ToInt64(ConvertTo.Base64Decode(REId));

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractRequestsServices.RequestByDeliveryExecutiveId(pageParam, DeId);

            items.Add(new SelectListItem() { Text = "All", Value = "0" });

            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Id.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }

        public JsonResult DeliveryExecutiveNotifications_ByDeliveryExecutiveId(
            [ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            string DEId = "MA==", long RequestId = 0, string ActionFormDate = "", string ActionToDate = "", int IsSent = 0)
        {

            long Id = Convert.ToInt64(ConvertTo.Base64Decode(DEId));
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractDeliveryExecutiveNotificationsServices.DeliveryExecutiveNotifications_ByDeliveryExecutiveId(pageParam, Id, RequestId, ActionFormDate, ActionToDate, IsSent);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }

        [ActionName(Actions.DELocations)]
        public ActionResult DELocations(string DEId = "MA==")
        {
            ViewBag.DEId = DEId;
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [ActionName(Actions.OBLocations)]
        public ActionResult OBLocations(string OBId = "MA==")
        {
            ViewBag.OBId = OBId;
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public JsonResult DeliveryExecutiveLatLong_ByDeliveryExecutiveId(
            string DEId = "MA==", string ActionFormDate = "", string ActionToDate = "", string ActionFormTime = "", string ActionToTime = "")
        {

            long Id = Convert.ToInt64(ConvertTo.Base64Decode(DEId));
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractDeliveryExecutiveLatLongServices.DeliveryExecutiveLatLong_ByDeliveryExecutiveId(pageParam, "", Id,
                ActionFormDate, ActionToDate, ActionFormTime, ActionToTime);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(response, JsonRequestBehavior.AllowGet);

        }
        public JsonResult OnboarderLatLong_ByAdminId(
            string OBId = "MA==", string ActionFormDate = "", string ActionToDate = "", string ActionFormTime = "", string ActionToTime = "")
        {

            long Id = Convert.ToInt64(ConvertTo.Base64Decode(OBId));
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractDeliveryExecutiveLatLongServices.OnboarderLatLong_ByAdminId(pageParam, "", Id,
                ActionFormDate, ActionToDate, ActionFormTime, ActionToTime);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(response, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public IList<SelectListItem> GetAgency_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.Admin_All(pageParam, "", "", "", "", 0, 1);
            items.Add(new SelectListItem() { Text = "Select Agency", Value = "" });

            foreach (var master in result.Values)
            {
                if (master.AdminTypeId == 4 && master.IsActive)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
            }

            return items;
        }
        [HttpPost]
        public IList<SelectListItem> GetMasterArea_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;
            var loginId = ProjectSession.UserType == "Super Admin" ? 0 : ProjectSession.AdminId;

            var result = abstractAreaMasterServices.AreaMaster_All(pageParam, "", loginId);
            items.Add(new SelectListItem() { Text = "Select Area", Value = "" });

            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }

        [HttpGet]
        [ActionName(Actions.GetMasterArea_ByAgencyId)]
        public ActionResult GetMasterArea_ByAgencyId(int AgencyId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAreaMasterServices.AreaMaster_ByAgencyId(pageParam, "",AgencyId);


            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}