﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AAHAdmin.Infrastructure.Enums;

namespace AAHAdmin.Controllers
{
    public class ItemsController : BaseController
    {
        public readonly AbstractItemsServices abstractItemsServices;
        public ItemsController(AbstractItemsServices abstractItemsServices)
        {
            this.abstractItemsServices = abstractItemsServices;
        }

        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public ActionResult Details(string IId = "MA==")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractItems abstractItems = null;
            if (IId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(IId));
                if (decryptedId > 0)
                {
                    abstractItems = abstractItemsServices.Items_ById(decryptedId).Item;
                }
            }
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                 return View(abstractItems);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.SaveItemDetails)]
        public ActionResult SaveItemDetails(Items items)
        {
            try
            {
                SuccessResult<AbstractItems> result = new SuccessResult<AbstractItems>();

                items.CreatedBy = ProjectSession.AdminId;
                items.UpdatedBy = ProjectSession.AdminId;

                if (items.Files != null)
                {
                    string basePath = "assets/docs/ItemsUrl/" + items.Id + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(items.Files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    items.Files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    items.ImageUrl = basePath + fileName;
                }
                
                result = abstractItemsServices.Items_Upsert(items);
                
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.Index, Pages.Controllers.Items, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    return RedirectToAction(Actions.Details, Pages.Controllers.Items, new { Area = "" });
                }
            }
            catch (Exception error)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), error);
            }
            return RedirectToAction(Actions.Index, Pages.Controllers.Items, new { Area = "" });
        }

        [HttpPost]
        public JsonResult ItemsAllData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractItemsServices.Items_All(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult AllItems([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractItemsServices.AllItems(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [ActionName(Actions.ItemDelete)]
        public ActionResult ItemDelete(string Id = "MA==")
        {
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
            var result = abstractItemsServices.Items_Delete(decryptedId, (int)ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.ItemActInAct)]
        public JsonResult ItemActInAct(long Id = 0)
        {
            var result = abstractItemsServices.Items_ActInAct(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}