﻿using DataTables.Mvc;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using static AAHAdmin.Infrastructure.Enums;

namespace AAHAdmin.Controllers
{
    public class ChaiNastaVendorsController : BaseController
    {
        public readonly AbstractVendorsServices abstractVendorsServices;
        public readonly AbstractItemsServices abstractItemsServices;
        public ChaiNastaVendorsController(AbstractVendorsServices abstractVendorsServices,
            AbstractItemsServices abstractItemsServices)
        {
            this.abstractVendorsServices = abstractVendorsServices;
            this.abstractItemsServices = abstractItemsServices;
        }

        //DeliveryExecutive Index action

        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Details(string vi = "MA==")
        {
            ViewBag.VendorId = Convert.ToInt32(ConvertTo.Base64Decode(vi));
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public JsonResult ChaiNastaVendorsAllData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractVendorsServices.Vendors_All(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.VendorActInAct)]
        public JsonResult VendorActInAct(long Id = 0)
        {
            var result = abstractVendorsServices.Vendors_ActInAct(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.VendorData)]
        public JsonResult VendorData(long Id = 0)
        {
            var result = abstractVendorsServices.Vendors_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.GetAllItems)]
        public ActionResult GetAllItems(string Search = "")
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.SearchItems_All(pageParam, Search, ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName(Actions.ManageItems)]
        public ActionResult ManageItems(string Search = "")
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.Items_All(pageParam, Search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}