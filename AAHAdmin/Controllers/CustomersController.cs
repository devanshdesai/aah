﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class CustomersController : BaseController
    {
        public readonly AbstractUsersServices abstractUserServices;
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractUserNotificationsServices abstractUserNotificationsServices;
        public readonly AbstractRequestsServices abstractRequestsServices;

        public CustomersController(AbstractUsersServices abstractUserServices, 
            AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractUserNotificationsServices abstractUserNotificationsServices,
            AbstractRequestsServices abstractRequestsServices)
        {
            this.abstractUserServices = abstractUserServices;
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractUserNotificationsServices = abstractUserNotificationsServices;
            this.abstractRequestsServices = abstractRequestsServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index(string DI = "MA==")
        {
            ViewBag.decryptedDIId = Convert.ToInt32(ConvertTo.Base64Decode(DI));
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }


        public JsonResult Users_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string UserName = "",
            string MobileNumber = "", string Email = "", int Gender = 0, long IsActive = 0,long DEId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractUserServices.Users_All(pageParam, search, UserName, MobileNumber, Email, Gender, IsActive, DEId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public JsonResult Users_ActInAct(long Id)
        {
            SuccessResult<AbstractUsers> customerData = new SuccessResult<AbstractUsers>();
            try
            {
                customerData = abstractUserServices.Users_ActInAct(Id, ProjectSession.AdminId);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Users_IsBlacklist(long Id)
        {
            SuccessResult<AbstractUsers> customerData = new SuccessResult<AbstractUsers>();
            try
            {
                customerData = abstractUserServices.Users_IsBlacklist(Id, ProjectSession.AdminId);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Users_IsPayable(long Id)
        {
            SuccessResult<AbstractUsers> customerData = new SuccessResult<AbstractUsers>();
            try
            {
                customerData = abstractUserServices.Users_IsPayable(Id, ProjectSession.AdminId);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult AddressMaster_ByUserId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            long UserId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, search, UserId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UserDevices_ByUserId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            long UserId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractUserServices.UserDevices_ByUserId(pageParam, search, UserId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.UserNotifications)]
        public ActionResult UserNotifications(string REId = "MA==")
        {
            ViewBag.UserEId = REId;
            ViewBag.GetRequestsAll = GetRequestByUser(REId);
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public IList<SelectListItem> GetRequestByUser(string REId = "MA==")
        {
            List<SelectListItem> items = new List<SelectListItem>();

            long UserId = Convert.ToInt64(ConvertTo.Base64Decode(REId));

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractRequestsServices.RequestByUserId(pageParam, UserId);

            items.Add(new SelectListItem() { Text = "All", Value = "0" });

            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Id.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }

        public JsonResult UserNotifications_ByUserId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            string UserId = "MA==", long RequestId = 0, string ActionFormDate = "", string ActionToDate = "", int IsSent = 0)
        {

            long UId = Convert.ToInt64(ConvertTo.Base64Decode(UserId));
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length; 

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractUserNotificationsServices.UserNotifications_ByUserId(pageParam, UId, RequestId, ActionFormDate, ActionToDate, IsSent);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetUsersDetails(string DId = "MA==")
        {
            long Id = Convert.ToInt64(ConvertTo.Base64Decode(DId));

            var result = abstractUserServices.Users_ById(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}