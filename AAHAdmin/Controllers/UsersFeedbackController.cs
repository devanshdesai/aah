﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class UsersFeedbackController : BaseController
    {
        private readonly AbstractUsersFeedbackServices abstractUsersFeedbackServices = null;
       

        public UsersFeedbackController(AbstractUsersFeedbackServices abstractUsersFeedbackServices)
           
        {
            this.abstractUsersFeedbackServices = abstractUsersFeedbackServices;
           
        }
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public JsonResult UsersFeedback_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string FromDate = "", string ToDate = "",int UserId = 0)
       {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractUsersFeedbackServices.UsersFeedback_All(pageParam, search, FromDate, ToDate, UserId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UsersFeedback_Insert( long Id = 0, long UserId = 0,string Description = "" )
        {
            UsersFeedback model = new UsersFeedback();
            model.Id = Id;
            model.UserId = UserId;
            model.Description = Description;
           

            var result = abstractUsersFeedbackServices.UsersFeedback_Insert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}