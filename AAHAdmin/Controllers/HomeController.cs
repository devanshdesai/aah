﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class HomeController : BaseController
    {
        private readonly AbstractRequestsServices abstractRequestsServices = null;
        private readonly AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices = null;
        private readonly AbstractAdminServices abstractAdminServices = null;

        public HomeController(AbstractRequestsServices abstractRequestsServices,
            AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices,
            AbstractAdminServices abstractAdminServices
            )
        {
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractDeliveryExecutiveLatLongServices = abstractDeliveryExecutiveLatLongServices;
            this.abstractAdminServices = abstractAdminServices;
        }

        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult AdminDashboard()
        {
            if (ProjectSession.UserType == "Super Admin" || ProjectSession.UserType == "Admin" || ProjectSession.UserType == "Back office Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult AgencyDashbord()
        {
            if (ProjectSession.UserType == "Super Admin" || ProjectSession.UserType == "Admin" || ProjectSession.UserType == "Back office Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Target_OnBoardingStaff()
        {
            if (ProjectSession.UserType == "Super Admin" || ProjectSession.UserType == "Admin" || ProjectSession.UserType == "Back office Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Target_DownloadingStaff()
        {
            if (ProjectSession.UserType == "Super Admin" || ProjectSession.UserType == "Admin" || ProjectSession.UserType == "Back office Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff")
            {
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult Requests_ByStatusCount()
        {
            SuccessResult<AbstractRequestsCount> result = abstractRequestsServices.Requests_ByStatusCount();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Admin_ById()
        {
            SuccessResult<AbstractAdmin> result = abstractAdminServices.Admin_ById(ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Requests_Urgent(long UserId = 0, long DeliveryExecutiveId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            PagedList<AbstractRequests> result = abstractRequestsServices.Requests_Urgent(pageParam, UserId, DeliveryExecutiveId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeliveryExecutiveLatLong_Map()
        {
            PagedList<AbstractDeliveryExecutiveLatLong> result = abstractDeliveryExecutiveLatLongServices.DeliveryExecutiveLatLong_Map();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Requests_Attended(string ActionFormDate = "", string ActionToDate = "")
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            PagedList<AbstractRequests> result = abstractRequestsServices.Requests_Attended(pageParam, ActionFormDate, ActionToDate);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult Admin_Dashboard()
        {
            long Id = ProjectSession.AdminId;

            SuccessResult<AbstractAdmin> result = abstractAdminServices.Admin_Dashboard(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

       
        [HttpPost]
        public JsonResult OnBoardingStaff_TodayTarget([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Fromdate = "", string Todate = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                long Id = ProjectSession.AdminId;

                var response = abstractAdminServices.OnBoardingStaff_TodayTarget(pageParam, Fromdate, Todate, Id);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DownloadingStaff_TodayTarget([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Fromdate = "", string Todate = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                long Id = ProjectSession.AdminId;

                var response = abstractAdminServices.DownloadingStaff_TodayTarget(pageParam, Fromdate, Todate, Id);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
    }
}