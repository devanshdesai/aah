﻿using DataTables.Mvc;
using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using static AAHAdmin.Infrastructure.Enums;

namespace AAHAdmin.Controllers
{
    public class DEPerformanceController : BaseController
    {
        private readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices = null;
        private readonly AbstractDeliveryExecutiveDocumentsServices abstractDeliveryExecutiveDocumentsServices = null;
        private readonly AbstractDeliveryExecutiveCheckInCheckOutServices abstractDeliveryExecutiveCheckInCheckOutServices = null;
        public readonly AbstractRequestsServices abstractRequestsServices = null;
        public readonly AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices = null;
        public readonly AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices = null;
        public readonly AbstractAreaMasterServices abstractAreaMasterServices;
        public readonly AbstractAdminServices abstractAdminServices;

        public DEPerformanceController(AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices,
            AbstractDeliveryExecutiveDocumentsServices abstractDeliveryExecutiveDocumentsServices,
            AbstractDeliveryExecutiveCheckInCheckOutServices abstractDeliveryExecutiveCheckInCheckOutServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices,
            AbstractDeliveryExecutiveLatLongServices abstractDeliveryExecutiveLatLongServices, AbstractAreaMasterServices abstractAreaMasterServices,
            AbstractAdminServices abstractAdminServices)
        {
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
            this.abstractDeliveryExecutiveDocumentsServices = abstractDeliveryExecutiveDocumentsServices;
            this.abstractDeliveryExecutiveCheckInCheckOutServices = abstractDeliveryExecutiveCheckInCheckOutServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractDeliveryExecutiveNotificationsServices = abstractDeliveryExecutiveNotificationsServices;
            this.abstractDeliveryExecutiveLatLongServices = abstractDeliveryExecutiveLatLongServices;
            this.abstractAreaMasterServices = abstractAreaMasterServices;
            this.abstractAdminServices = abstractAdminServices;
        }

        //DeliveryExecutive Index action
        
        public ActionResult Reports()
        {

            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        
        public JsonResult DeliveryExecutive_Reports([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDeliveryExecutiveServices.DeliveryExecutive_Reports(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        

    }
}