﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class VendorsVsReferencesController : BaseController
    {
        public readonly AbstractVendorsServices abstractVendorsServices;
        public VendorsVsReferencesController(AbstractVendorsServices abstractVendorsServices)

        {
            this.abstractVendorsServices = abstractVendorsServices;

        }
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult References(string ri = "MA==")
        {
            ViewBag.VendorId = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            if (ProjectSession.UserType == "Advertising Staff"|| ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult VendorsVsReferences_Delete(long Id)
        {
            long DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractVendorsServices.VendorsVsReferences_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);


        }
        [HttpPost]
        public JsonResult VendorsVsReferences_Upsert(VendorsVsReferences VendorsVsReferences)
        {
            
                VendorsVsReferences.CreatedBy = ProjectSession.AdminId;
            
            var result = abstractVendorsServices.VendorsVsReferences_Upsert(VendorsVsReferences);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]


       
        public JsonResult VendorsVsReferences_ByVendorsId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search, long VendorsId)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractVendorsServices.VendorsVsReferences_ByVendorsId(pageParam, Search, VendorsId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
    }
}

       


        
        
        