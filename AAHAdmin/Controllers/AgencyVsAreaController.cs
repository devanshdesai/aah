﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Entities.V1;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAHAdmin.Controllers
{
    public class AgencyVsAreaController : BaseController
    {
        public readonly AbstractAgencyMasterServices abstractAgencyMasterServices;
        private readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public readonly AbstractAdminServices abstractAdminServices;
        public readonly AbstractAreaMasterServices abstractAreaMasterServices;
        public readonly AbstractAgencyVsAreaServices abstractAgencyVsAreaServices;

        public AgencyVsAreaController(AbstractAgencyMasterServices abstractAgencyMasterServices,
            AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices, AbstractAgencyVsAreaServices abstractAgencyVsAreaServices,
            AbstractAdminServices abstractAdminServices, AbstractAreaMasterServices abstractAreaMasterServices)
        {
            this.abstractAgencyMasterServices = abstractAgencyMasterServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
            this.abstractAdminServices = abstractAdminServices;
            this.abstractAreaMasterServices = abstractAreaMasterServices;
            this.abstractAgencyVsAreaServices = abstractAgencyVsAreaServices;
        }

        // GET: AgencyVsArea
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult MyArea()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Details(string AID = "MA==")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractAdmin abstractAdmin = null;
            ViewBag.AgencyVsAreaId = 0;
            if (AID != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(AID));
                if (decryptedId > 0)
                {
                    ViewBag.AgencyVsAreaData = abstractAgencyVsAreaServices.AgencyVsArea_ById(decryptedId).Values;
                    ViewBag.AgencyVsAreaId = decryptedId;
                }
            }

            ViewBag.GetAgencyAll = GetAgency_All();
            ViewBag.GetMasterAreaAll = GetMasterArea_All();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
            return View(abstractAdmin);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AgencyVsArea_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAgencyVsAreaServices.AgencyVsArea_All(pageParam, search).Values;

                totalRecord = (int)response.Count;
                filteredRecord = (int)response.Count;

                return Json(new DataTablesResponse(requestModel.Draw, response, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AgencyVsArea_byAgencyId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                long AgencyId = ProjectSession.AdminId;
                var response = abstractAgencyVsAreaServices.AgencyVsArea_byAgencyId(pageParam, search, AgencyId).Values;

                totalRecord = (int)response.Count;
                filteredRecord = (int)response.Count;

                return Json(new DataTablesResponse(requestModel.Draw, response, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AgencyVsArea_AssignedDE([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,long MasterAreaId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                //long AgencyId = ProjectSession.AdminId;
                var response = abstractAgencyVsAreaServices.AgencyVsArea_AssignedDE(pageParam, ProjectSession.AdminId, MasterAreaId);
                
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;
                //totalRecord = (int)response.Count;
                //filteredRecord = (int)response.Count;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public IList<SelectListItem> GetAgency_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.Admin_All(pageParam, "", "", "", "", 0, 1);
            //items.Add(new SelectListItem() { Text = "Select Agency", Value = "" });

            foreach (var master in result.Values)
            {
                if (master.AdminTypeId == 4 && master.IsActive)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
            }

            return items;
        }

        [HttpPost]
        public IList<SelectListItem> GetMasterArea_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAreaMasterServices.AreaMaster_All(pageParam, "");
            items.Add(new SelectListItem() { Text = "Select Area", Value = "" });

            foreach (var master in result.Values)
            {
                if (master.IsApproveByAdmin == 1 && master.IsApproveBySuperAdmin == 1)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
            }

            return items;
        }

        [HttpGet]
        [ActionName(Actions.GetMasterArea_ByAgencyId)]
        public ActionResult GetMasterArea_ByAgencyId(string AgencyId = "")
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.GetAgency_ByAreaId(pageParam, "", AgencyId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [ActionName(Actions.GetAgency_ByAreaId)]
        public ActionResult GetAgency_ByAreaId(int AreaId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAreaMasterServices.AreaMaster_ByAgencyId(pageParam, "", AreaId);


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.GetAgency_ByIds)]
        public ActionResult GetAgency_ByIds(string AgencyId = "")
        {
            var result = abstractAdminServices.GetAgency_ByIds(AgencyId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.GetAvailableDE_ByAgencyId)]
        public ActionResult GetAvailableDE_ByAgencyId(string AgencyId = "",long AreaId = 0)
        {
            var result = abstractAdminServices.GetAvailableDE_ByAgencyId(AgencyId,AreaId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.AgencyVsAreas_Upsert)]
        public ActionResult AgencyVsAreas_Upsert(string StringData = "")
        {
            SuccessResult<AbstractAgencyVsArea> successResult = new SuccessResult<AbstractAgencyVsArea>();
            var selectedData = JsonConvert.DeserializeObject<List<AgencyRoot>>(StringData);
            //var de = abstractAgencyVsAreaServices.AgencyVsArea_Delete(selectedData[0].AgencyId, selectedData[0].AreaId);
            for (int i = 0; i < selectedData.Count; i++)
            {
                if (selectedData[i].Assigned > 0)
                {
                    AbstractAgencyVsArea agencyVsArea = new AgencyVsArea();
                    agencyVsArea.AreaId = selectedData[i].AreaId;
                    agencyVsArea.Assigned = selectedData[i].Assigned;
                    agencyVsArea.AgencyId = selectedData[i].AgencyId;
                    agencyVsArea.Id = selectedData[i].Id;
                    successResult = abstractAgencyVsAreaServices.AgencyVsArea_Upsert(agencyVsArea);
                }
            }
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public class AgencyRoot
        {
            public int Id { get; set; }
            public int AreaId { get; set; }
            public int AgencyId { get; set; }
            public int Assigned { get; set; }
        }
    }
}