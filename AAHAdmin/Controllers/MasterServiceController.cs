﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.IO;

namespace AAHAdmin.Controllers
{
    public class MasterServiceController : BaseController
    {
        public readonly AbstractUsersServices abstractUserServices;
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractUserNotificationsServices abstractUserNotificationsServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractServiceMasterServices abstractServiceMasterServices;
        public readonly AbstractSubServiceMasterServices abstractSubServiceMasterServices;
        public readonly AbstractSubSubServiceMasterServices abstractSubSubServiceMasterServices;

        public MasterServiceController(AbstractUsersServices abstractUserServices, 
            AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractUserNotificationsServices abstractUserNotificationsServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractServiceMasterServices abstractServiceMasterServices,
            AbstractSubServiceMasterServices abstractSubServiceMasterServices,
            AbstractSubSubServiceMasterServices abstractSubSubServiceMasterServices)
        {
            this.abstractUserServices = abstractUserServices;
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractUserNotificationsServices = abstractUserNotificationsServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractServiceMasterServices = abstractServiceMasterServices;
            this.abstractSubServiceMasterServices= abstractSubServiceMasterServices;
            this.abstractSubSubServiceMasterServices = abstractSubSubServiceMasterServices;
        }

        [ActionName(Actions.ServiceMaster)]
        public ActionResult ServiceMaster()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [ActionName(Actions.SubServiceMaster)]
        public ActionResult SubServiceMaster(string SMID = "MA==")
        {
            ViewBag.serviceMasterId = Convert.ToInt64(ConvertTo.Base64Decode(SMID));
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [ActionName(Actions.SubSubServiceMaster)]
        public ActionResult SubSubServiceMaster(string SSMID = "MA==", string SMID = "MA==")
        {
            ViewBag.serviceMasterId = Convert.ToInt64(ConvertTo.Base64Decode(SMID));
            ViewBag.subserviceMasterId = Convert.ToInt64(ConvertTo.Base64Decode(SSMID));
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public JsonResult ServiceMaster_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractServiceMasterServices.ServiceMaster_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        //ServiceMaster insert

        [HttpPost]
        public JsonResult ServiceMasterUpsert(ServiceMaster serviceMaster, HttpPostedFileBase uploadFile)
        {
            if (uploadFile != null)
            {
                string basePath = "Icon/ServiceMaster/" + serviceMaster.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(uploadFile.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                uploadFile.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                serviceMaster.Icon = basePath + fileName;
            }

            if (serviceMaster.Id > 0)
            {
                serviceMaster.UpdatedBy = ProjectSession.AdminId;
            }
            else
            {
                serviceMaster.CreatedBy = ProjectSession.AdminId;
            }


            var result = abstractServiceMasterServices.ServiceMaster_Upsert(serviceMaster);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetServiceMasterDetails(string SMId = "MA==")
        {
            long Id = Convert.ToInt64(ConvertTo.Base64Decode(SMId));
            SuccessResult<AbstractServiceMaster> successResult = abstractServiceMasterServices.ServiceMaster_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SubServiceMaster_ByMasterServiceId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,long ServiceMasterId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                
                var response = abstractSubServiceMasterServices.SubServiceMaster_ByServiceMasterId(pageParam, search, ServiceMasterId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        // SubServiceMaster insert
        [HttpPost]
        public JsonResult SubServiceMasterUpsert(SubServiceMaster SubService, HttpPostedFileBase uploadFile)
        {
            SubServiceMaster model = new SubServiceMaster();
            if (uploadFile != null)
            {
                string basePath = "Icon/SubServiceMaster/" + SubService.ServiceMasterId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(uploadFile.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                uploadFile.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                model.Icon = basePath + fileName;
            }
            else
            {
                model.Icon = SubService.Icon;
            }
            model.Id = SubService.Id;
            model.ServiceMasterId = SubService.ServiceMasterId;
            model.SubServiceName = SubService.SubServiceName;
            model.Link = SubService.Link;
            model.CreatedBy = ProjectSession.AdminId;
            model.UpdatedBy = ProjectSession.AdminId;
            var result = abstractSubServiceMasterServices.SubServiceMaster_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSubServiceMasterDetails(string SSMId = "MA==")
        {
            long Id = Convert.ToInt64(ConvertTo.Base64Decode(SSMId));
            SuccessResult<AbstractSubServiceMaster> successResult = abstractSubServiceMasterServices.SubServiceMaster_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubSubServiceMaster_BySubServiceId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long SubServiceMasterId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var response = abstractSubSubServiceMasterServices.SubSubServiceMaster_BySubServiceMasterId(pageParam, search, SubServiceMasterId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        //SubSubServiceMaster insert
        [HttpPost]
        public JsonResult SubSubServiceMasterUpsert(SubSubServiceMaster SubSubService, HttpPostedFileBase uploadFile)
        {
            SubSubServiceMaster model = new SubSubServiceMaster();
            if (uploadFile != null)
            {
                string basePath = "Icon/SubSubServiceMaster/" + SubSubService.SubServiceMasterId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(uploadFile.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                uploadFile.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                model.Icon = basePath + fileName;
            }
            else
            {
                model.Icon = SubSubService.Icon;
            }
            model.Id = SubSubService.Id;
            model.SubServiceMasterId = SubSubService.SubServiceMasterId;
            model.SubSubServiceName = SubSubService.SubSubServiceName;
            model.Description = SubSubService.Description;
           
            model.Link = SubSubService.Link;
            model.CreatedBy = ProjectSession.AdminId;
            model.UpdatedBy = ProjectSession.AdminId;
            var result = abstractSubSubServiceMasterServices.SubSubServiceMaster_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSubSubServiceMasterDetails(string SSSMId = "MA==")
        {
            long Id = Convert.ToInt64(ConvertTo.Base64Decode(SSSMId));
            SuccessResult<AbstractSubSubServiceMaster> successResult = abstractSubSubServiceMasterServices.SubSubServiceMaster_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

    }
}