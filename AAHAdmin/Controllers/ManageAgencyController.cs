﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Web;
using static AAHAdmin.Infrastructure.Enums;
using System.Linq;
using System.IO;

namespace AAHAdmin.Controllers
{
    public class ManageAgencyController : BaseController
    {
        public readonly AbstractAgencyMasterServices abstractAgencyMasterServices ;
        private readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public readonly AbstractAdminServices abstractAdminServices;
        public readonly AbstractAreaMasterServices abstractAreaMasterServices;

        public ManageAgencyController(AbstractAgencyMasterServices abstractAgencyMasterServices, 
            AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices,
            AbstractAdminServices abstractAdminServices, AbstractAreaMasterServices abstractAreaMasterServices)
        {
            this.abstractAgencyMasterServices = abstractAgencyMasterServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
            this.abstractAdminServices = abstractAdminServices;
            this.abstractAreaMasterServices = abstractAreaMasterServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {

            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public ActionResult Details(string AID = "MA==")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractAdmin abstractAdmin = null;
            if (AID != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(AID));
                if (decryptedId > 0)
                {
                    abstractAdmin = abstractAdminServices.Admin_ById(decryptedId).Item;
                    abstractAdmin.AreaMasterIds = abstractAdmin.AreaMasterId;
                }
            }
            ViewBag.GetUserTypeAll = GetUserType_All();
            ViewBag.GetUserTypeNotAll = GetUserTypeNot_All();
            ViewBag.GetMasterAreaAll = GetMasterArea_All();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                 return View(abstractAdmin);
            }
        }


        [HttpPost]
        public JsonResult GetAgencyById(int Id = 0)
        {
            SuccessResult<AbstractAgencyMaster> successResult = abstractAgencyMasterServices.AgencyMaster_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        //AgencyMaster owner all data get
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Agency_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdminServices.Admin_All(pageParam, search, "", "", "", 4, 1);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.AgencyMaster_Upsert)]
        public ActionResult AgencyMaster_Upsert(IEnumerable<HttpPostedFileBase> files, Admin admin)
        {
            admin.CreatedBy = ProjectSession.AdminId;
            admin.UpdatedBy = ProjectSession.AdminId;
            admin.AdminTypeId = 4;

            if (admin.Id > 0)
            {
                admin.UpdatedBy = ProjectSession.AdminId;
            }
            else
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                var stringChars = new char[8];
                var random = new Random();

                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }

                admin.Password = new String(stringChars);
                admin.CreatedBy = ProjectSession.AdminId;
            }

            var result = abstractAdminServices.Admin_Upsert(admin);

            if (files != null)
            {
                if (files.Count() > 0 && files.ElementAt(0) != null && result.Item != null)
                {
                    admin.Path = "doc/Agency/";
                    admin.AvatarFolder = Server.MapPath("~/Content/doc/");
                    var file = files.ElementAt(0);
                    string basePath = "doc/Agency/" + result.Item.Id.ToString() + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + Path.GetFileName(file.FileName);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    file.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    admin.PANNumberImage = basePath + fileName;
                    admin.Id = result.Item.Id;
                    var rr = abstractAdminServices.Admin_Upsert(admin);
                }

                if (files.Count() > 0 && files.ElementAt(1) != null && result.Item != null)
                {
                    admin.Path = "doc/Agency/";
                    admin.AvatarFolder = Server.MapPath("~/Content/doc/");
                    var file = files.ElementAt(1);
                    string basePath = "doc/Agency/" + result.Item.Id.ToString() + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(file.FileName);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    file.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    admin.AddressProof = basePath + fileName;
                    admin.Id = result.Item.Id;
                    var rr = abstractAdminServices.Admin_Upsert(admin);
                }
            }


            if (result != null && result.Code == 200 && result.Item != null)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                return RedirectToAction(Actions.Index, Pages.Controllers.ManageAgency, new { Area = "" });
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
            }
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashboard", "Home", new { area = "" });
            }
            else
            {
            return RedirectToAction(Actions.Index, Pages.Controllers.ManageAgency, new { Area = "" });
            }
        }


        //[HttpPost]
        //public JsonResult AgencyMaster_Upsert(int Id = 0, string Name = "")
        //{
        //    AgencyMaster model = new AgencyMaster();
        //    model.Id = Id;
        //    model.Name = Name;
        //    var result = abstractAgencyMasterServices.AgencyMaster_Upsert(model);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}


        [HttpPost]
        public IList<SelectListItem> GetUserType_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.AdminType_All(pageParam, "");

            items.Add(new SelectListItem() { Text = "All", Value = "0" });
            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }

        [HttpPost]
        public IList<SelectListItem> GetUserTypeNot_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.AdminType_All(pageParam, "");
            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }

        [HttpPost]
        public IList<SelectListItem> GetMasterArea_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAreaMasterServices.AreaMaster_All(pageParam, "");
            //items.Add(new SelectListItem() { Text = "Select Area", Value = "" });

            foreach (var master in result.Values)
            {
                if (master.IsApproveByAdmin == 1 && master.IsApproveBySuperAdmin == 1)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
            }

            return items;
        }
    }
}