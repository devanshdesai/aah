﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Web;
using System.IO;

namespace AAHAdmin.Controllers
{
    public class OtherServiceIconsController : BaseController
    {
        public readonly AbstractOtherServiceIconsServices abstractOtherServiceIconsServices ;

        public OtherServiceIconsController(AbstractOtherServiceIconsServices abstractOtherServiceIconsServices)
        {
            this.abstractOtherServiceIconsServices = abstractOtherServiceIconsServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult OtherServiceIcons_ById(long Id)
        {
            SuccessResult<AbstractOtherServiceIcons> successResult = abstractOtherServiceIconsServices.OtherServiceIcons_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        //OtherServiceIcons owner all data get
        [HttpPost]
        public JsonResult OtherServiceIcons_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search  = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractOtherServiceIconsServices.OtherServiceIcons_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult OtherServiceIcons_Upsert(HttpPostedFileBase files, OtherServiceIcons otherServiceIcons)
        {

            SuccessResult<AbstractOtherServiceIcons> bannerData = new SuccessResult<AbstractOtherServiceIcons>();
            
            try
            {
                if (otherServiceIcons.Id > 0)
                {

                    if (files != null)
                    {
                        string basePath = "doc/OtherServiceIcons/" + otherServiceIcons.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        otherServiceIcons.URL = basePath + fileName;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    }
                    bannerData = abstractOtherServiceIconsServices.OtherServiceIcons_Upsert(otherServiceIcons);
                }

                if (otherServiceIcons.Id == 0)
                {
                    string basePath = "doc/OtherServiceIcons/" + otherServiceIcons.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    otherServiceIcons.URL = basePath + fileName;
                    //_abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), banners.BannerUrl);
                    bannerData = abstractOtherServiceIconsServices.OtherServiceIcons_Upsert(otherServiceIcons);
                }

            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult OtherServiceIcons_Delete(long Id)  //if error delete int deletedBy  
        {
            int DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractOtherServiceIconsServices.OtherServiceIcons_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OtherServiceIcons_ActInAct(long Id = 0, int UpdatedBy = 0)
        {
            SuccessResult<AbstractOtherServiceIcons> customerData = new SuccessResult<AbstractOtherServiceIcons>();
            try
            {
                customerData = abstractOtherServiceIconsServices.OtherServiceIcons_ActInAct(Id, UpdatedBy);
                if (customerData == null && customerData.Item == null)
                {
                    throw new Exception(customerData.Message);
                }
            }
            catch (Exception ex)
            {
                customerData.Code = 400;
                customerData.Message = ex.Message;
            }
            customerData.Item = null;
            return Json(customerData, JsonRequestBehavior.AllowGet);
        }


    }
}