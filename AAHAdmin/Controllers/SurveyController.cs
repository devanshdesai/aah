﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace AAHAdmin.Controllers
{
    public class SurveyController : BaseController
    {
        public readonly AbstractSurveyServices abstractSurveyServices;
        public readonly AbstractMasterAgeGroupServices abstractMasterAgeGroupServices;
        

        public SurveyController(AbstractSurveyServices abstractSurveyServices, AbstractMasterAgeGroupServices abstractMasterAgeGroupServices)
        {
            this.abstractSurveyServices = abstractSurveyServices;
            this.abstractMasterAgeGroupServices = abstractMasterAgeGroupServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [ActionName(Actions.Success)]
        public ActionResult Success()
        {
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        [ActionName(Actions.Details)]
        public ActionResult Details(string RID = "MA==")
        {
            ViewBag.RequestId = Convert.ToInt64(ConvertTo.Base64Decode(RID));
            ViewBag.MasterAgeGroup_All = MasterAgeGroup_All();

            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public IList<SelectListItem> MasterAgeGroup_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractMasterAgeGroupServices.MasterAgeGroup_All(pageParam, "");
            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }
        public JsonResult SurveyDataGetAll([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractSurveyServices.Survey_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Survey_Insert(Survey survey)
        {
            if (survey.Id > 0)
            {
                survey.UpdatedBy = ProjectSession.AdminId;
            }
            else
            {

                survey.CreatedBy = ProjectSession.AdminId;
            }

            SuccessResult<AbstractSurvey> CustomeResult = new SuccessResult<AbstractSurvey>();

            if (survey.Pincode != 0)
            {
                using (var wc = new HttpClient())
                {
                    List<PincodeRootArray> pincodeRootArray = new List<PincodeRootArray>();

                    wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var path = "https://api.postalpincode.in/pincode/" + ConvertTo.String(survey.Pincode);
                    var AddressrResult = wc.GetStringAsync(path).GetAwaiter().GetResult();

                    object AddressResult = "";
                    pincodeRootArray = JsonConvert.DeserializeObject<List<PincodeRootArray>>(Convert.ToString(AddressrResult));

                    if (pincodeRootArray[0].Status == "Success")
                    {
                        survey.Country = Convert.ToString(pincodeRootArray[0].PostOffice[0].Country);
                        survey.State = Convert.ToString(pincodeRootArray[0].PostOffice[0].State);
                        survey.City = Convert.ToString(pincodeRootArray[0].PostOffice[0].District);
                        survey.Area = Convert.ToString(pincodeRootArray[0].PostOffice[0].Name);
                    }
                    else if (pincodeRootArray[0].Status == "Error")
                    {
                        CustomeResult.Code = 400;
                        CustomeResult.Message = "Pincode is invalid";
                    }
                }
            }
            else
            {
                CustomeResult.Code = 400;
                CustomeResult.Message = "Pincode is required";
            }

             object results = "";

            if (CustomeResult.Code == 400)
            {
                results = CustomeResult;
            }
            else
            {
                results = abstractSurveyServices.Survey_Insert(survey);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}