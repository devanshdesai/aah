﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;

namespace AAHAdmin.Controllers
{
    public class ManageUserController : BaseController
    {
        public readonly AbstractUsersServices abstractUserServices;
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractUserNotificationsServices abstractUserNotificationsServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractServiceMasterServices abstractServiceMasterServices;
        public readonly AbstractSubServiceMasterServices abstractSubServiceMasterServices;
        public readonly AbstractSubSubServiceMasterServices abstractSubSubServiceMasterServices;
        public readonly AbstractAdminServices abstractAdminServices ;
        public readonly AbstractAreaMasterServices abstractAreaMasterServices;

        public ManageUserController(AbstractUsersServices abstractUserServices, 
            AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractUserNotificationsServices abstractUserNotificationsServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractServiceMasterServices abstractServiceMasterServices,
            AbstractSubServiceMasterServices abstractSubServiceMasterServices,
            AbstractSubSubServiceMasterServices abstractSubSubServiceMasterServices,
            AbstractAdminServices abstractAdminServices, AbstractAreaMasterServices abstractAreaMasterServices)
        {
            this.abstractUserServices = abstractUserServices;
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractUserNotificationsServices = abstractUserNotificationsServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractServiceMasterServices = abstractServiceMasterServices;
            this.abstractSubServiceMasterServices= abstractSubServiceMasterServices;
            this.abstractSubSubServiceMasterServices = abstractSubSubServiceMasterServices;
            this.abstractAdminServices = abstractAdminServices;
            this.abstractAreaMasterServices = abstractAreaMasterServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            ViewBag.GetUserTypeAll = GetUserType_All();
            ViewBag.GetUserTypeNotAll = GetUserTypeNot_All();
            ViewBag.GetMasterAreaAll = GetMasterArea_All();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Back office Staff")
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        // User Type = Admin Type 
        
        [HttpPost]
        public IList<SelectListItem> GetUserType_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.AdminType_All(pageParam, "");

            items.Add(new SelectListItem() { Text = "All", Value = "0" });
            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }

        [HttpPost]
        public IList<SelectListItem> GetUserTypeNot_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdminServices.AdminType_All(pageParam, "");
            foreach (var master in result.Values)
            {
                if(master.Name == "Agency Owner")
                {
                    continue;
                }

                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }
            return items;
        }

        public JsonResult Admin_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel , string Name = "",string Email = "",string MobileNumber = "",long AdminTypeId = 0,long IsActive = 2)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdminServices.Admin_All(pageParam, search, Name,Email,MobileNumber,AdminTypeId,IsActive);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Admin_Upsert(Admin admin)
        {
            if (admin.Id > 0)
            {
                admin.UpdatedBy = ProjectSession.AdminId;
            }
            else
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                var stringChars = new char[8];
                var random = new Random();

                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }

                admin.Password = new String(stringChars);
                admin.CreatedBy = ProjectSession.AdminId;
            }

            var result = abstractAdminServices.Admin_Upsert(admin);
            if (result != null && result.Code == 200)
            {
                if (admin.Id > 0)
                {
                    // Update
                    var welcome = "Greetings From Ginny Buddy.";
                    EmailHelper.SendEmail(welcome, result.Item.Email, result.Item.Name, "Email : " + result.Item.Email + " , Password : " + result.Item.Password + " Information Updated", result.Item.Password);

                }
                else
                {
                    // Create
                    var welcome = "Welcome to Ginny Buddy.";
                    EmailHelper.SendEmail(welcome, result.Item.Email, result.Item.Name, "", result.Item.Password);
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        //public JsonResult Admin_Upsert(Admin admin)
        //{
        //    if (admin.Id > 0)
        //    {
        //        admin.UpdatedBy = ProjectSession.AdminId;
        //    }
        //    else
        //    {
        //        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        //        var stringChars = new char[8];
        //        var random = new Random();

        //        for (int i = 0; i < stringChars.Length; i++)
        //        {
        //            stringChars[i] = chars[random.Next(chars.Length)];
        //        }

        //        admin.Password = new String(stringChars);
        //        admin.CreatedBy = ProjectSession.AdminId;
        //    }

        //    var result = abstractAdminServices.Admin_Upsert(admin);
        //    if (result != null && result.Code == 200)
        //    {
        //        if (admin.Id  > 0)
        //        {
        //            // Update
        //            var welcome = "Greetings From Ginny Buddy.";
        //            EmailHelper.SendEmail(welcome, result.Item.Email, result.Item.Name, "Email : " + result.Item.Email + " , Password : " + result.Item.Password +  " Information Updated", result.Item.Password);
                    
        //        }
        //        else
        //        {
        //            // Create
        //            var welcome = "Welcome to Ginny Buddy.";
        //            EmailHelper.SendEmail(welcome, result.Item.Email, result.Item.Name, "", result.Item.Password);
        //        }
        //    }
        //    else
        //    {
        //        throw new Exception(result.Message);
        //    }

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult GetAdminDetails(string SMId = "MA==")
        {
            long Id = Convert.ToInt64(ConvertTo.Base64Decode(SMId));
            SuccessResult<AbstractAdmin> successResult = abstractAdminServices.Admin_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
      

        public JsonResult GetUserDetails(string UId = "MA==")
        {
            long Id = Convert.ToInt64(ConvertTo.Base64Decode(UId));

            var result = abstractUserServices.Users_ById(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetUserDetails(string UId = "MA==")
        //{
        //    long Id = Convert.ToInt64(ConvertTo.Base64Decode(UId));
        //    SuccessResult<AbstractUsers> successResult = abstractUserServices.Users_ById(Id);
        //    return Json(successResult, JsonRequestBehavior.AllowGet);
        //}

        [ActionName(Actions.OBLocations)]
        public ActionResult OBLocations(string OBId = "MA==")
        {
            ViewBag.OBId = OBId;
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Back office Staff")
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        public JsonResult UpdateStatusAdminUsers(long Id)
        {
            SuccessResult<AbstractAdmin> admin = new SuccessResult<AbstractAdmin>();

            try
            {
                admin = abstractAdminServices.Admin_ActInAct(Id, ProjectSession.AdminId);
            }
            catch (Exception ex)
            {
                admin.Code = 400;
                admin.Message = ex.Message;
            }
            admin.Item = null;
            return Json(admin, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public IList<SelectListItem> GetMasterArea_All()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAreaMasterServices.AreaMaster_All(pageParam, "");
            //items.Add(new SelectListItem() { Text = "Select Area", Value = "" });

            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }
        [HttpPost]
        public JsonResult AreaVsStaff_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long StaffId)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdminServices.AreaVsStaff_All(pageParam, search, StaffId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public JsonResult AreaVsStaff_Delete(long Id)
        {
            var result = abstractAdminServices.AreaVsStaff_Delete(Id, ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AreaVsStaff_ById(long Id)
        {
            var result = abstractAdminServices.AreaVsStaff_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AreaVsStaff_Upsert(AreaVsStaff abstractAreaVsStaff)
        {
            if (abstractAreaVsStaff.Id > 0)
            {
                abstractAreaVsStaff.UpdatedBy = ProjectSession.AdminId;
            }
            else
            {
                abstractAreaVsStaff.CreatedBy = ProjectSession.AdminId;
            }
            var result = abstractAdminServices.AreaVsStaff_Upsert(abstractAreaVsStaff);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}