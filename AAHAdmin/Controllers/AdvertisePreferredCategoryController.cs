﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAHAdmin.Infrastructure;
using AAHAdmin.Pages;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;

namespace AAHAdmin.Controllers
{
    public class AdvertisePreferredCategoryController : BaseController
    {
        public readonly AbstractAdvertisePreferredCategoryServices abstractAdvertisePreferredCategoryServices ;
        public readonly AbstractMasterCategoryServices abstractMasterCategoryServices ;
        public readonly AbstractAdvertiseMasterServices abstractAdvertiseMasterServices;
        

        public AdvertisePreferredCategoryController(AbstractAdvertisePreferredCategoryServices abstractAdvertisePreferredCategoryServices,
            AbstractMasterCategoryServices abstractMasterCategoryServices, AbstractAdvertiseMasterServices abstractAdvertiseMasterServices)
        {
            this.abstractAdvertisePreferredCategoryServices = abstractAdvertisePreferredCategoryServices;
            this.abstractMasterCategoryServices = abstractMasterCategoryServices;
            this.abstractAdvertiseMasterServices = abstractAdvertiseMasterServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            ViewBag.MasterCategory = MasterCategory();
            ViewBag.AdvertiseMaster = AdvertiseMaster();
            if (ProjectSession.UserType == "Advertising Staff" || ProjectSession.UserType == "On boarding Staff" || ProjectSession.UserType == "Downloading Staff")
            {
                //RedirectResult("~/Authentication/Signout");
                return RedirectToAction("AdminDashboard", "Home", new { area = "" });
            }
            else if (ProjectSession.UserType == "Agency Owner")
            {
                return RedirectToAction("AgencyDashbord", "Home", new { area = "" });
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public IList<SelectListItem> MasterCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractMasterCategoryServices.MasterCategory_All(pageParam, "");
            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }
        [HttpPost]
        public IList<SelectListItem> AdvertiseMaster()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractAdvertiseMasterServices.AdvertiseMaster_All(pageParam, "");
            foreach (var master in result.Values)
            {
                items.Add(new SelectListItem() { Text = master.CompanyName.ToString(), Value = Convert.ToString(master.Id) });
            }

            return items;
        }
        [HttpPost]
        public JsonResult AdvertisePreferredCategory_ById(int Id = 0)
        {
            SuccessResult<AbstractAdvertisePreferredCategory> successResult = abstractAdvertisePreferredCategoryServices.AdvertisePreferredCategory_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        //AdvertisePreferredCategory owner all data get
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AdvertisePreferredCategory_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Search  = "")
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdvertisePreferredCategoryServices.AdvertisePreferredCategory_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
      
        [HttpPost]
        public JsonResult AdvertisePreferredCategory_Upsert(int Id = 0, int CategoryId = 0,int AdvertiseMasterId = 0)
        {
            AdvertisePreferredCategory model = new AdvertisePreferredCategory();
            model.Id = Id;
            model.CategoryId = CategoryId;
            model.AdvertiseMasterId = AdvertiseMasterId;
            var result = abstractAdvertisePreferredCategoryServices.AdvertisePreferredCategory_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AdvertisePreferredCategory_Delete(int Id)  //if error delete int deletedBy  
        {
            int DeletedBy = (int)ProjectSession.AdminId;

            var result = abstractAdvertisePreferredCategoryServices.AdvertisePreferredCategory_Delete(Id, DeletedBy);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}