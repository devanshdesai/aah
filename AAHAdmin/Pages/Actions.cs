﻿namespace AAHAdmin.Pages
{
    public class Actions
    {
        public const string TrialVideos_Upsert = "TrialVideos_Upsert";
        public const string TrialVideos_All = "TrialVideos_All";
        public const string TrialVideos_ById = "TrialVideos_ById";
        public const string TrialVideos_ActInAct = "TrialVideos_ActInAct";
        public const string TrialVideos_Delete = "TrialVideos_Delete";
        public const string DownloadingStaff_TodayTarget = "DownloadingStaff_TodayTarget";
        public const string Ticket_ChangeStatus = "Ticket_ChangeStatus";
        public const string Ticket_ChangeType = "Ticket_ChangeType";
        public const string Ticket_ChangePlatform = "Ticket_ChangePlatform";
        public const string Ticket_All = "Ticket_All";
        public const string Ticket_ById = "Ticket_ById";
        public const string TicketComments_All = "TicketComments_All";
        public const string TicketComments_ById = "TicketComments_ById";
        public const string TicketComments_Upsert = "TicketComments_Upsert";
        public const string TicketComments_Delete = "TicketComments_Delete";
        public const string TicketComments_ByTicketId = "TicketComments_ByTicketId";
        public const string AgencyDashbord = "AgencyDashbord";
        public const string MyArea = "MyArea";
        public const string AgencyVsArea_byAgencyId = "AgencyVsArea_byAgencyId";
        public const string Vendor_ValidArea = "Vendor_ValidArea";
        
        public const string MasterAgeGroup_Upsert = "MasterAgeGroup_Upsert";
        public const string MasterAgeGroup_All = "MasterAgeGroup_All";
        public const string MasterAgeGroup_ById = "MasterAgeGroup_ById";
        public const string MasterAgeGroup_Delete = "MasterAgeGroup_Delete";
        
        public const string DeliveryLimit_Upsert = "DeliveryLimit_Upsert";
        public const string DeliveryLimit_All = "DeliveryLimit_All";
        public const string DeliveryLimit_ById = "DeliveryLimit_ById";
        public const string DeliveryLimit_Delete = "DeliveryLimit_Delete";

        public const string AreaMaster_Upsert = "AreaMaster_Upsert";
        public const string AreaMaster_All = "AreaMaster_All";
        public const string AreaMaster_ById = "AreaMaster_ById";
        public const string AreaMaster_Delete = "AreaMaster_Delete";
        public const string AreaMaster_ActInAct = "AreaMaster_ActInAct";
        public const string AreaMaster_AppUnApp = "AreaMaster_AppUnApp";
        public const string AreaMaster_Details = "AreaMaster_Details";
        public const string AreaVsStaff_ById = "AreaVsStaff_ById";
        public const string AreaVsStaff_Upsert = "AreaVsStaff_Upsert";

        public const string MasterPayType_Upsert = "MasterPayType_Upsert";
        public const string MasterPayType_All = "MasterPayType_All";
        public const string MasterPayType_ById = "MasterPayType_ById";
        public const string MasterPayType_ActInAct = "MasterPayType_ActInAct";
        public const string MasterPayType_Delete = "MasterPayType_Delete";
        
        public const string OtherServiceIcons_Upsert = "OtherServiceIcons_Upsert";
        public const string OtherServiceIcons_All = "OtherServiceIcons_All";
        public const string OtherServiceIcons_ById = "OtherServiceIcons_ById";
        public const string OtherServiceIcons_ActInAct = "OtherServiceIcons_ActInAct";
        public const string OtherServiceIcons_Delete = "OtherServiceIcons_Delete";

        public const string Signin = "Signin";
        public const string Signout = "Signout";
        public const string Index = "Index";
        public const string Delete = "Delete";
        public const string ChangePassword = "ChangePassword";
        public const string Details = "Details";
        public const string Reports = "Reports";
        public const string updateIsSettledWithAgency = "updateIsSettledWithAgency";
        public const string Requestcomments_Insert = "Requestcomments_Insert";
        public const string updateRequestDetailsByAdmin = "updateRequestDetailsByAdmin";
        public const string UpdateRequest = "UpdateRequest";

        public const string Survey_Insert = "Survey_Insert";
        public const string DeliveryExecutive_Reports = "DeliveryExecutive_Reports";

        public const string ChaiNastaVendorsAllData = "ChaiNastaVendorsAllData";
        public const string VendorActInAct = "VendorActInAct";
        public const string VendorData = "VendorData";
        public const string GetAllItems = "GetAllItems";
        public const string ManageItems = "ManageItems";

        public const string DeliveryExecutiveLatLong_Map = "DeliveryExecutiveLatLong_Map";
        public const string DeliveryExecutive_Upsert = "DeliveryExecutive_Upsert";
        public const string Requests_Attended = "Requests_Attended";
        public const string UpdateStatusAdminUsers = "UpdateStatusAdminUsers";
        public const string Admin_All = "Admin_All";
        public const string Admin_Upsert = "Admin_Upsert";
        public const string GetAdminDetails = "GetAdminDetails";

        public const string Users_All = "Users_All";
        public const string Users_ActInAct = "Users_ActInAct";
        public const string Users_IsBlacklist = "Users_IsBlacklist";
        public const string Users_IsPayable = "Users_IsPayable";
        public const string AddressMaster_ByUserId = "AddressMaster_ByUserId";
        public const string UserDevices_ByUserId = "UserDevices_ByUserId";
        public const string Requests_AssignDeliveryExecutive = "Requests_AssignDeliveryExecutive";
        public const string ViewRequestCommentAllData = "ViewRequestCommentAllData";

        public const string Timesheet = "Timesheet";
        public const string UserNotifications = "UserNotifications";
        public const string ViewAllData = "ViewAllData";
        public const string ActiveInActive = "ActiveInActive";
        public const string DeliveryExecutive_IsApproved = "DeliveryExecutive_IsApproved";
        public const string DeliveryExecutive_ByDeliveryExecutiveId = "DeliveryExecutive_ByDeliveryExecutiveId";
        public const string DeliveryExecutiveDocuments_ByDeliveryExecutiveId = "DeliveryExecutiveDocuments_ByDeliveryExecutiveId";
        public const string DeliveryExecutiveDocuments_Verify = "DeliveryExecutiveDocuments_Verify";
        public const string DeliveryExecutiveCheckInCheckOut_ByDeliveryExecutiveId = "DeliveryExecutiveCheckInCheckOut";
        public const string UserNotifications_ByUserId = "UserNotifications_ByUserId";
        public const string GetDeliveryExecutiveDetails = "GetDeliveryExecutiveDetails";
        public const string GetSubService_All = "GetSubService_All";

        public const string GetUsersDetails = "GetUsersDetails";
        public const string GetRequestsAll = "GetRequestsAll";
        public const string GetRequestByUser = "GetRequestByUser";
        public const string GetRequest_ById = "GetRequest_ById";
        public const string GetRequestDETracker_ByRId = "GetRequestDETracker_ByRId";
        public const string GetRequestStatusTracker_ByRId = "GetRequestStatusTracker_ByRId";

        public const string DeliveryExecutiveNotifications_ByDeliveryExecutiveId = "DeliveryExecutiveNotifications_ByDeliveryExecutiveId";
        public const string ViewRequestAllData = "ViewRequestAllData";
        public const string DENotifications = "DENotifications";
        public const string GetDENotificaiton_ByRId = "GetDENotificaiton_ByRId";
        public const string GetUserNotification_ByRId = "GetUserNotification_ByRId";
        public const string DELocations = "DELocations";
        public const string OBLocations = "OBLocations";
        public const string DeliveryExecutiveLatLong_ByDeliveryExecutiveId = "DeliveryExecutiveLatLong_ByDeliveryExecutiveId";
        public const string Requests_ByStatusCount = "Requests_ByStatusCount";
        public const string Requests_Urgent = "Requests_Urgent";
        public const string ServiceMaster_All = "ServiceMaster_All";
        public const string SubServiceMaster_ByMasterServiceId = "SubServiceMaster_ByMasterServiceId";
        public const string ServiceMaster = "ServiceMaster";
        public const string SubServiceMaster = "SubServiceMaster";
        public const string SubSubServiceMaster_BySubServiceId = "SubSubServiceMaster_BySubServiceId";
        public const string SubSubServiceMaster = "SubSubServiceMaster";
        public const string ServiceMasterUpsert = "ServiceMasterUpsert";
        public const string GetServiceMasterDetails = "GetServiceMasterDetails";
        public const string GetSubSubServiceMasterDetails = "GetSubSubServiceMasterDetails";
        public const string SubSubServiceMasterUpsert = "SubSubServiceMasterUpsert";
        public const string GetSubServiceMasterDetails = "GetSubServiceMasterDetails";
        public const string SubServiceMasterUpsert = "SubServiceMasterUpsert";
        public const string ViewAllRequestDocuments = "ViewAllRequestDocuments";
        public const string DeleteRequestDocuments = "DeleteRequestDocuments";
        public const string ManageUser = "ManageUser";
        public const string UsersFeedback_All = "UsersFeedback_All";
        public const string GetUserTypeNot_All = "GetUserTypeNot_All";

        public const string AgencyMaster_Upsert = "AgencyMaster_Upsert";
        public const string GetAgencyById = "GetAgencyById";
        public const string Agency_All = "Agency_All";

        public const string SaveItemDetails = "SaveItemDetails";
        public const string ItemsAllData = "ItemsAllData";
        public const string AllItems = "AllItems";
        public const string ItemDelete = "ItemDelete";
        public const string ItemActInAct = "ItemActInAct";


        public const string MasterCategoryData_All = "MasterCategoryData_All";
        public const string GetCategoryById = "GetCategoryById";
        public const string MasterCategory_Upsert = "MasterCategory_Upsert";
        public const string DeleteCategoryData = "DeleteCategoryData";

        public const string AdvertiseMaster_Upsert = "AdvertiseMaster_Upsert";
        public const string AdvertiseMaster_All = "AdvertiseMaster_All";
        public const string AdvertiseMaster_ById = "AdvertiseMaster_ById";
        public const string AdvertiseMaster_ActInAct = "AdvertiseMaster_ActInAct";
        public const string AdvertiseMaster_Delete = "AdvertiseMaster_Delete";
        
        public const string AgencyVsArea_Upsert = "AgencyVsArea_Upsert";
        public const string AgencyVsArea_All = "AgencyVsArea_All";
        public const string AgencyVsArea_ById = "AgencyVsArea_ById";
        public const string AgencyVsArea_AssignedDE = "AgencyVsArea_AssignedDE";
        public const string AgencyVsArea_ActInAct = "AgencyVsArea_ActInAct";
        public const string AgencyVsArea_Delete = "AgencyVsArea_Delete";
        
        public const string HowGinnyBuddyWorks_Upsert = "HowGinnyBuddyWorks_Upsert";
        public const string HowGinnyBuddyWorks_All = "HowGinnyBuddyWorks_All";
        public const string HowGinnyBuddyWorks_ById = "HowGinnyBuddyWorks_ById";
        public const string HowGinnyBuddyWorks_ActInAct = "HowGinnyBuddyWorks_ActInAct";
        public const string HowGinnyBuddyWorks_Delete = "HowGinnyBuddyWorks_Delete";

        public const string AdvertisePreferredCategory_Upsert = "AdvertisePreferredCategory_Upsert";
        public const string AdvertisePreferredCategory_All = "AdvertisePreferredCategory_All";
        public const string AdvertisePreferredCategory_ById = "AdvertisePreferredCategory_ById";
        public const string AdvertisePreferredCategory_Delete = "AdvertisePreferredCategory_Delete";
        public const string Home = "Home";

        public const string DeliveryExecutiveData = "DeliveryExecutiveData";
        public const string DeliveryExecutive_IsMarketingPerson = "DeliveryExecutive_IsMarketingPerson";
        public const string RequestDataUpdateAdmin = "RequestDataUpdateAdmin";
        public const string SurveyDataGetAll = "SurveyDataGetAll";
        public const string Success = "Success";


        public const string Report_All = "Report_All";
        public const string GetMasterArea_ByAgencyId = "GetMasterArea_ByAgencyId";
        public const string GetAgency_ByAreaId = "GetAgency_ByAreaId";

        public const string CustomersNotificationUpsert = "CustomersNotificationUpsert";
        public const string OnboarderLatLong_ByAdminId = "OnboarderLatLong_ByAdminId";
        public const string Admin_Dashboard = "Admin_Dashboard";
        public const string AdminDashboard = "AdminDashboard";
        public const string Target_DownloadingStaff = "Target_DownloadingStaff";
        public const string Target_OnBoardingStaff = "Target_OnBoardingStaff";
        public const string OnBoardingStaff_TodayTarget = "OnBoardingStaff_TodayTarget";


        public const string Admin_ById = "Admin_ById";
        public const string GetAgency_ByIds = "GetAgency_ByIds";
        public const string GetAvailableDE_ByAgencyId = "GetAvailableDE_ByAgencyId";
        public const string AgencyVsAreas_Upsert = "AgencyVsAreas_Upsert";

        public const string VendorAllData = "VendorAllData";
        public const string Vendors_ByAdminId = "Vendors_ByAdminId";
       
        public const string VendorDelete = "VendorDelete";
        public const string VendorProfileDetails = "VendorProfileDetails";
        public const string VendorsThelaImages_Delete = "VendorsThelaImages_Delete";
        public const string VendorsThelaImages_UpdateIsPrimary = "VendorsThelaImages_UpdateIsPrimary";
        public const string VendorsVsReferences_Delete = "VendorsVsReferences_Delete";
        public const string VendorsVsReferences_Upsert = "VendorsVsReferences_Upsert";
        public const string VendorsVsReferences_ByVendorsId = "VendorsVsReferences_ByVendorsId";
        public const string References = "References";

        public const string VendorsVsItems_AddItemsList = "VendorsVsItems_AddItemsList";
        public const string AddItems = "AddItems";
        public const string MyItems = "MyItems";
        public const string VendorsVsItems_Delete = "VendorsVsItems_Delete";


        public const string AreaVsStaff_All = "AreaVsStaff_All";

    }
}