﻿using System.Collections.Generic;

namespace AAHAdmin.Pages
{
    public class Controllers
    {
        public const string Items = "Items";
        public const string Account = "Account";
        public const string MasterAgeGroup = "MasterAgeGroup";
        public const string DeliveryLimit = "DeliveryLimit";
        public const string MasterPayType = "MasterPayType";
        public const string OtherServiceIcons = "OtherServiceIcons";
        public const string HowGinnyBuddyWorks = "HowGinnyBuddyWorks";
        public const string TrialVideos = "TrialVideos";
        public const string AdvertiseMaster = "AdvertiseMaster";
        public const string AdvertisePreferredCategory = "AdvertisePreferredCategory";
        public const string Home = "Home";
        public const string Authentication = "Authentication";
        public const string ChangePassword = "ChangePassword";
        public const string Customers = "Customers";
        public const string DeliveryExecutives = "DeliveryExecutives";
        public const string Requests = "Requests";
        public const string MasterService = "MasterService";
        public const string Admin = "Admin";
        public const string ManageUser = "ManageUser";
        public const string UsersFeedback = "UsersFeedback";
        public const string ManageAgency = "ManageAgency";
        public const string ManageCategory = "ManageCategory";
        public const string Survey = "Survey";
        public const string AreaMaster = "AreaMaster";
        public const string Report = "Report";
        public const string CustomersNotification = "CustomersNotification";
        public const string DEPerformance = "DEPerformance";
        public const string ChaiNastaVendors = "ChaiNastaVendors";
        public const string AgencyVsArea = "AgencyVsArea";
        public const string Vendors = "Vendors";
        public const string VendorsVsReferences = "VendorsVsReferences";
        public const string Complain = "Complain";
        public const string AgenacyDeliveryExecutives = "AgenacyDeliveryExecutives";
    }
}