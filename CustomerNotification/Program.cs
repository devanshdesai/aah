﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace CustomerNotification
{
    internal class Program
    {
        public static string ConnStr = "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=AAH;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";

        SqlConnection conn = new SqlConnection(ConnStr);
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {

                    string q = "select * from [dbo].[AllCustomersNotification] where IsSend = 0";
                    SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                    DataTable dt = new DataTable();
                    ad.Fill(dt);



                    if (dt.Rows.Count > 0)
                    {
                        List<string> DeviceT = new List<string>();
                        Console.WriteLine("Getting Connection ...");

                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["Description"].ToString() != "")
                            {
                                string getDT = "select DeviceToken from [dbo].[UserDevices] where DeviceToken IS NOT NULL order by Id desc";
                                SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                                DataTable DEDeviceToken = new DataTable();
                                DT.Fill(DEDeviceToken);
                                DeviceT = new List<string>();

                                if (DEDeviceToken.Rows.Count > 0)
                                {
                                    DeviceT = DEDeviceToken.AsEnumerable()
                                              .Select(r => r.Field<string>("DeviceToken"))
                                              .ToList();

                                    if (DeviceT.Count > 0)
                                    {
                                        SendUserNotification(DeviceT, row["Description"].ToString(), row["Id"].ToString());
                                    }
                                }
                            }
                        }
                    }

                }

                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
            Console.Read();
        }

         public static string SendUserNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = "AAAAJZAGPpE:APA91bH_7g7sqU_ErXU7OAEl56QAX0wtoPnic_JOmJ4ojhY3CQonf_lNgMD7cGZT6CtvzWTAS0ZLHI5OeCzztnejzCGlR09iXiT-VRx4eUEsXAV7HZL_imyYnQyGHk387LhneNesXJCk"; // Something very long
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

       

            var data = new
            {
                registration_ids = DeviceT,
                data = new
                {
                    body = Description,
                    title = "",
                    sound = "default",
                    mutable_contenct = true,
                    badge = 4,
                    data = new { }
                },
                mutable_contenct = true,
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            Conn.Open();
                            string updateIsSent = "UPDATE AllCustomersNotification SET IsSend = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            UpdateDENotificationcmd.ExecuteNonQuery();

                            Conn.Close();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }
            return "";
         }
    }
}
