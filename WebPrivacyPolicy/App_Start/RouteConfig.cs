﻿using System.Web.Mvc;
using System.Web.Routing;


namespace WebPrivacyPolicy
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "PrivacyPolicy", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "WebPrivacyPolicy.Controllers" }
            );
        }
    }
}
