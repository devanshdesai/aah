﻿namespace WebPrivacyPolicy.Pages
{
    public class Actions
    {
        public const string Index = "Index";
        public const string OtherServiceIconsData = "OtherServiceIconsData";
        public const string SendOtp = "SendOtp";
        public const string UsersVerifyOtp = "UsersVerifyOtp";
        public const string GetDataAddress = "GetDataAddress";
        public const string ChooseAddressAdd = "ChooseAddressAdd";
        public const string ChooseAddressData = "ChooseAddressData";
        public const string CreateAddress = "CreateAddress";
        public const string AddressTypeAll = "AddressTypeAll";
        public const string DeletedAddressData = "DeletedAddressData";
        public const string CreateRequestData = "CreateRequestData";
        public const string UserDetailsEdit = "UserDetailsEdit";
        public const string SignIn = "SignIn";
        public const string LogOut = "LogOut";
    }
}