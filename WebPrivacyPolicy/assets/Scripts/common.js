﻿$(document).ready(function () {
    //Prevent Page Reload on all # links
    $("a[href='#']").click(function (e) {
        e.preventDefault();
    });

    //Grid td tooltip
    //$(function () {
    //    setTimeout(function () {
    //        $('table tbody tr td').each(function () {
    //            this.setAttribute('title', $(this).text());
    //        });
    //    }, 1500);
    //});

    //placeholder
    $("[placeholder]").each(function () {
        $(this).attr("data-placeholder", this.placeholder);
        $(this).bind("focus", function () {
            this.placeholder = '';
        });
        $(this).bind("blur", function () {
            this.placeholder = $(this).attr("data-placeholder");
        });
    });

    //First Field Focus
    $(function () {
        setTimeout(function () {
            $(':input:text:enabled:visible:first').focus();
            $('.modal.fade').off('shown.bs.modal').on('shown.bs.modal', function () {
                $(this).find('input:enabled:visible:first').focus();
            });
        }, 200);
    });
});

var baseAPIUrl = "http://localhost:1498/";

function addRequestVerificationToken(data) {
    data.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();
    return data;
}

function getDataTableObj(tableRef_id) {
    return tableRef_id.DataTable();
}

function addRowsToDataTable(rows, dataTableObj) {
    dataTableObj.clear();
    dataTableObj.rows.add(rows);
    dataTableObj.on('order.dt search.dt page.dt', function () {
        dataTableObj.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

//function bindSortingArrow() {
//    $(".dataTable thead th").each(function (i, th) {
//        var html = $(th).html(),
//            cls = $(th).attr('class');
//        if (cls.indexOf('sorting_disabled') == -1) {
//            switch (cls) {
//                case 'sorting_asc':
//                    $(th).html(html + spanAsc); break;
//                case 'sorting_desc':
//                    $(th).html(html + spanDesc); break;
//                default:
//                    $(th).html(html + spanSorting); break;
//            }
//        }
//    });
//}

function ShowMessageToastr(type, message, IsConfirmation, YesResponseMethod, NoResponseMethod) {
    // If message is not confirmation type
    if (IsConfirmation != true) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        if (type == null || type == 'undefined') {
            type = 'success';
        }

        if (message != null && message != 'undefined' || message != '') {
            if (type.toLowerCase() == 'danger') {
                toastr.error(message);
            }
            else if (type == 'success') {
                toastr.success(message);
            }
            else if (type == 'warning') {
                toastr.warning(message);
            }
            else {
                toastr.info(message);
            }

            $('#toast-container').addClass('nopacity');
        }
    } else {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "showMethod": "fadeIn",
            "timeOut": "0",
            "extendedTimeOut": "0",
            "showEasing": "swing",
            "onclick": null,
            "tapToDismiss": false,
            "hideMethod": 'noop',
            allowHtml: true,
            onShown: function (toast) {
                $("#confirmationRevertYes").click(function () {
                    eval(YesResponseMethod);
                    $('.toast').remove();
                });

                $("#confirmationRevertNo").click(function () {
                    eval(NoResponseMethod);
                    $('.toast').remove();
                });
            }
        }

        if (type == null || type == 'undefined') {
            type = 'success';
        }
        if (message != null && message != 'undefined' || message != '') {
            if (type.toLowerCase() == 'danger') {
                toastr.error(message + "<br /><br /><button type='button' id='confirmationRevertYes' style='margin-left:30px;' class='btn btn-primary btn-md'>Yes</button><button type='button' id='confirmationRevertNo' style='padding-left:15px;margin-left:10px;' class='btn btn-default btn-md'>No</button>");
            }
            else if (type == 'success') {
                toastr.success(message + "<br /><br /><button type='button' id='confirmationRevertYes' style='margin-left:30px;' class='btn btn-primary btn-md'>Yes</button><button type='button' id='confirmationRevertNo' style='padding-left:15px;margin-left:10px;' class='btn btn-default btn-md'>No</button>");
            }
            else if (type == 'warning') {
                toastr.warning(message + "<br /><br /><button type='button' id='confirmationRevertYes' style='margin-left:30px;' class='btn btn-primary btn-md'>Yes</button><button type='button' id='confirmationRevertNo' style='padding-left:15px;margin-left:10px' class='btn btn-default btn-md'>No</button>");
            }
            else {
                toastr.info(message + "<br /><br /><button type='button' id='confirmationRevertYes' style='margin-left:30px;' class='btn btn-primary btn-md'>Yes</button><button type='button' id='confirmationRevertNo' style='padding-left:15px;margin-left:10px' class='btn btn-default btn-md'>No</button>");
            }

            $('#toast-container').addClass('nopacity');
        }
    }
}


//// start ajax call start spinner
//function ajaxindicatorstart(text) {
//    if (jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
//        //        jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="../../../asserts/images/ajax-loader.gif"><div>' + text + '</div></div><div class="bg"></div></div>');
//        jQuery('body').append('<div id="resultLoading" style="display:none"><div><img class="spinner_bg" src="' + root + '/resources/images/VRS_logo.jpeg"><img class="img-spinner" src="' + root + '/resources/images/spinner.png"><div></div></div><div class="bg"></div></div>');
//    }

//    jQuery('#resultLoading').css({
//        'width': '100%',
//        'height': '100%',
//        'position': 'fixed',
//        'z-index': '10000000',
//        'top': '0',
//        'left': '0',
//        'right': '0',
//        'bottom': '0',
//        'margin': 'auto'
//    });
//    jQuery('.spinner_bg').css({
//        'position': 'absolute',
//        'left': '110px',
//        'top': '14px',
//        "width": '30px'
//    });
//    jQuery('.img-spinner').css({
//        'width': '60px',
//        'height': '60px',
//        'transition-property': 'transform',
//        'transition-duration': '1s',
//        'animation-name': 'rotate',
//        'animation-duration': '2s',
//        'animation-iteration-count': 'infinite',
//        'animation-timing-function': 'linear'
//    });
//    jQuery('#resultLoading .bg').css({
//        'background': '#000000',
//        'opacity': '0.7',
//        'width': '100%',
//        'height': '100%',
//        'position': 'absolute',
//        'top': '0'
//    });
//    jQuery('#resultLoading>div:first').css({
//        'width': '250px',
//        'height': '75px',
//        'text-align': 'center',
//        'position': 'fixed',
//        'top': '0',
//        'left': '0',
//        'right': '0',
//        'bottom': '0',
//        'margin': 'auto',
//        'font-size': '16px',
//        'z-index': '10',
//        'color': '#ffffff'

//    });
//    jQuery('#resultLoading .bg').height('100%');
//    jQuery('#resultLoading').fadeIn(300);
//    jQuery('body').css('cursor', 'wait');
//}

//// end ajax call spinner
//function ajaxindicatorstop() {
//    jQuery('#resultLoading .bg').height('100%');
//    jQuery('#resultLoading').fadeOut('slow');
//    jQuery('body').css('cursor', 'default');
//}


//// start the ajac call spinner
//jQuery(document).ajaxStart(function () {

//    //show ajax indicator
//    ajaxindicatorstart('please wait...');
//}).ajaxStop(function () {
//    //hide ajax indicator
//    ajaxindicatorstop();
//});

//$(document).ajaxError(function (event, jQXHR, settings, throwerror) {
//    if (jQXHR.status === 401) {
//        window.location.href = "/";
//    }
//});


