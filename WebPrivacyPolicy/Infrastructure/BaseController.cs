﻿using AAH.Common;
using WebPrivacyPolicy.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Common.Paging;
using AAH.Entities.V1;

namespace WebPrivacyPolicy.Infrastructure
{

    public class BaseController : Controller
    {
        public BaseController()
        {

        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {

                bool isAllow = false;
                HttpCookie reqCookie = Request.Cookies["AdminLogin"];
                if (reqCookie != null)
                {
                    ProjectSession.AdminId = Convert.ToInt64(Convert.ToString(reqCookie["Id"]));
                }
                else
                {
                    ProjectSession.AdminId = 0;
                }

                if (ProjectSession.AdminId != 0)
                {
                    isAllow = true;
                }
                else
                {
                    isAllow = false;
                }

                if (!isAllow)
                {
                    filterContext.Result = new RedirectResult("~/Authentication/SignIn");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Authentication/SignIn");
            }
        }
    }
}