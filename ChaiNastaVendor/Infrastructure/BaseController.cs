﻿using AAH.Common;
using ChaiNastaVendor.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Common.Paging;
using AAH.Entities.V1;

namespace ChaiNastaVendor.Infrastructure
{

    public class BaseController : Controller
    {
        public BaseController()
        {

        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {

                HttpCookie reqCookie = Request.Cookies["AdminLogin"];
                
                if (reqCookie != null)
                {
                    ProjectSession.AdminId = Convert.ToInt64(Convert.ToString(reqCookie["Id"]));

                    string var_sql = "select * from Vendors where IsDeleted = 0 and Id = " + ProjectSession.AdminId;
                    SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                    DataTable dt = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                    sda.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            ProjectSession.AdminName = Convert.ToString(Convert.ToString(reqCookie["AdminName"]));
                            ProjectSession.MobileNumber = Convert.ToString(Convert.ToString(reqCookie["MobileNumber"]));
                            ProjectSession.ProfileURL = Convert.ToString(Convert.ToString(reqCookie["ProfileURL"]));
                            return;
                        }
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("~/Authentication/SignIn");
                        return;
                    }
                } 
                else
                {
                    filterContext.Result = new RedirectResult("~/Authentication/SignIn");
                    return;
                }

                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Authentication/SignIn");
            }
        }
    }
}