﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using ChaiNastaVendor.Infrastructure;
using System.Web;
using System.Web.Mvc;
using ChaiNastaVendor.Pages;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System;

namespace ChaiNastaVendor.Controllers
{
    public class DashboardController : BaseController
    {
        public readonly AbstractVendorsServices abstractVendorsServices;
        public DashboardController(AbstractVendorsServices abstractVendorsServices)
        {
            this.abstractVendorsServices = abstractVendorsServices;
        }

        public ActionResult Index(long id = 0)
        {
            ViewBag.VendorsIdData = abstractVendorsServices.Vendors_ById(ProjectSession.AdminId).Item;
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }
            if (id != 0)
            {
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new { Area = "", id = Id });
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        [ActionName(Actions.MyItems)]
        public ActionResult MyItems(string Search = "")
        {
            ViewBag.VendorsIdData = abstractVendorsServices.Vendors_ById(ProjectSession.AdminId).Item;
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractVendorsServices.VendorsVsItems_ByVendorsId(pageParam, Search, Convert.ToInt64(Id));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VendorsVsItems_Delete(long Id)
        {
            var vId = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                vId = reqCookies["Id"].ToString();
            }
            var result = abstractVendorsServices.VendorsVsItems_Delete(Id, Convert.ToInt64(vId));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult VendorsVsItems_ActInAct(long Id)
        {
            var vId = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                vId = reqCookies["Id"].ToString();
            }
            var result = abstractVendorsServices.VendorsVsItems_ActInAct(Id, Convert.ToInt64(vId));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}