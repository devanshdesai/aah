﻿using AAH.Common;
using AAH.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Configuration;

namespace ChaiNastaVendor.Controllers
{
    public class AuthenticationController : Controller
    {
        public readonly AbstractVendorsServices abstractVendorsServices;
        public AuthenticationController(AbstractVendorsServices abstractVendorsServices)
        {
            this.abstractVendorsServices = abstractVendorsServices;
        }
        
        public ActionResult SignIn(string DeviceToken  = "")
        {
            ViewBag.DeviceTokenStr = DeviceToken;
            return View();
        }
        public JsonResult SendOtp(string MobileNumber = "" , string DeviceToken = "")
        {
            string otp = Convert.ToString(new Random().Next(111111, 999999).ToString("D6"));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).
                Replace("#mobile#", MobileNumber.ToString()).Replace("#otp#", otp).ToString());
            request.Method = "GET";
            request.ContentType = "application/json";

            if (MobileNumber == "1234512345")
            {
                otp = "123456";
            }

            var result = abstractVendorsServices.Vendor_SendOTP(MobileNumber, DeviceToken,Convert.ToInt64(otp));

            if (result.Code == 200)
            {
                try
                {
                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                    using (StreamReader responseReader = new StreamReader(webStream))
                    {
                        string response = responseReader.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("-----------------");
                    Console.Out.WriteLine(e.Message);
                }

                    Session.Clear();
                    ProjectSession.AdminId = result.Item.Id;
                    HttpCookie cookie = new HttpCookie("SendOTP");
                    cookie.Values.Add("Id", result.Item.Id.ToString());
                    cookie.Expires = DateTime.Now.AddYears(100);
                    Response.Cookies.Add(cookie);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerifyOtp(long Otp = 0)
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["SendOTP"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            var result = abstractVendorsServices.Vendor_VerifyOtp(Otp, Convert.ToInt64(Id));

            if(result.Code == 200)
            {
                Session.Clear();
                HttpCookie cookie = new HttpCookie("AdminLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("AdminName", result.Item.FirstName.ToString() + " " + result.Item.LastName.ToString());
                cookie.Values.Add("ProfileURL", result.Item.ShopOwnerProfile.ToString());
                cookie.Values.Add("MobileNumber", result.Item.PrimaryPhoneNumber.ToString());
                cookie.Values.Add("IsOnline", result.Item.IsOnline.ToString());

                cookie.Expires = DateTime.Now.AddYears(100);
                Response.Cookies.Add(cookie);

                ProjectSession.AdminId = result.Item.Id;
                ProjectSession.AdminName = result.Item.FirstName + " " + result.Item.LastName;
                ProjectSession.ProfileURL = result.Item.ShopOwnerProfile;
                ProjectSession.MobileNumber = result.Item.PrimaryPhoneNumber;
                ProjectSession.IsOnline = result.Item.IsOnline;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            if (Request.Cookies["AdminLogin"] != null)
            {
                var c = new HttpCookie("AdminLogin");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }

            if (Request.Cookies["SendOTP"] != null)
            {
                var c = new HttpCookie("SendOTP");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }

            return Json("", JsonRequestBehavior.AllowGet);

        }

    }
}