﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using ChaiNastaVendor.Infrastructure;
using System;
using System.Web.Mvc;
using ChaiNastaVendor.Pages;
using AAH.Entities.V1;
using static ChaiNastaVendor.Infrastructure.Enums;
using System.IO;
using System.Web;

namespace ChaiNastaVendor.Controllers
{
    public class ProfilesController : BaseController
    {
        public readonly AbstractVendorsServices abstractVendorsServices;
        public readonly AbstractItemsServices abstractItemsServices;
        public ProfilesController(AbstractVendorsServices abstractVendorsServices,
            AbstractItemsServices abstractItemsServices)
        {
            this.abstractVendorsServices = abstractVendorsServices;
            this.abstractItemsServices = abstractItemsServices;
        }

        public ActionResult Index()
        {
            ViewBag.VendorsIdData = abstractVendorsServices.Vendors_ById(ProjectSession.AdminId).Item;
            return View();
        }

        public ActionResult EditProfile()
        {
            ViewBag.VendorsIdData = abstractVendorsServices.Vendors_ById(ProjectSession.AdminId).Item;
            return View();
        }

        [HttpPost]
        [ActionName(Actions.VendorData)]
        public JsonResult VendorData()
        {
            var result = abstractVendorsServices.Vendors_ById(ProjectSession.AdminId);
            result.Item.ShopOwnerProfile = Configurations.AdminUrl + result.Item.ShopOwnerProfile;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult updateOnline(int IsOnline = 0)
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            try
            {
                var result = abstractVendorsServices.Vendor_IsOnline(Convert.ToInt64(Id), IsOnline);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}