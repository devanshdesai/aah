﻿using AAH.Common.Paging;
using AAH.Services.Contract;
using ChaiNastaVendor.Infrastructure;
using System.Web;
using System.Web.Mvc;
using ChaiNastaVendor.Pages;
using AAH.Entities.V1;
using System;
using AAH.Common;

namespace ChaiNastaVendor.Controllers
{
    public class AddItemsController : BaseController
    {
        public readonly AbstractVendorsServices abstractVendorsServices;
        public readonly AbstractItemsServices abstractItemsServices;
        public AddItemsController(AbstractVendorsServices abstractVendorsServices,
            AbstractItemsServices abstractItemsServices)
        {
            this.abstractVendorsServices = abstractVendorsServices;
            this.abstractItemsServices = abstractItemsServices;
        }

        public ActionResult Index()
        {
            ViewBag.VendorsIdData = abstractVendorsServices.Vendors_ById(ProjectSession.AdminId).Item;
            return View();
        }

        [HttpGet]
        [ActionName(Actions.ManageItems)]
        public ActionResult ManageItems(string Search = "")
        {
            ViewBag.VendorsIdData = abstractVendorsServices.Vendors_ById(ProjectSession.AdminId).Item;
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.Items_All(pageParam, Search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName(Actions.VendorsVsItems_AddItemsList)]
        public ActionResult VendorsVsItems_AddItemsList(string Search = "")
       {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.VendorsVsItems_AddItemsList(pageParam, Search,Convert.ToInt64(Id));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddItems(string itemsId)
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            VendorsVsItems items = new VendorsVsItems();
            items.ItemsIds = itemsId;
            items.CreatedBy = Convert.ToInt64(Id);
            items.VendorsId = Convert.ToInt64(Id);

            var result = abstractVendorsServices.VendorsVsItems_Upsert(items);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}