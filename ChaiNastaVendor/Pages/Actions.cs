﻿namespace ChaiNastaVendor.Pages
{
    public class Actions
    {
        public const string Index = "Index";
        public const string SignIn = "SignIn";
        public const string LogOut = "LogOut";
        public const string SendOtp = "SendOtp";
        public const string VerifyOtp = "VerifyOtp";
        public const string EditProfile = "EditProfile";
        public const string Ongoing = "Ongoing";
        public const string Completed = "Completed";
        public const string Rejected = "Rejected";
        public const string Confirmed = "Confirmed";
        public const string UpsertDeliveryExecutiveDetails = "UpsertDeliveryExecutiveDetails";
        public const string UpdateRequest = "UpdateRequest";
        public const string Feedback = "Feedback";
        public const string CreateFeedbackData = "CreateFeedbackData";
        public const string SellerMasterUpsert = "SellerMasterUpsert";
        public const string updateRequestByDeliveryExecutive = "updateRequestByDeliveryExecutive";
        public const string updateServiceTypeRequest = "updateServiceTypeRequest";
        public const string deliveryExecutiveUpdateLatLong = "deliveryExecutiveUpdateLatLong";
        public const string updateOnline = "updateOnline";
        public const string updateIsSettledWithAgency = "updateIsSettledWithAgency";
        public const string VendorData = "VendorData";
        public const string MyItems = "MyItems";
        public const string VendorsVsItems_Delete = "VendorsVsItems_Delete";
        public const string ManageItems = "ManageItems";
        public const string AddItems = "AddItems";
        public const string VendorsVsItems_AddItemsList = "VendorsVsItems_AddItemsList";
        public const string VendorsVsItems_ActInAct = "VendorsVsItems_ActInAct";
    }
}