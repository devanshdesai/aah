﻿using System.Collections.Generic;

namespace GinnyBuddyResponderApp.Pages
{
    public class Controllers
    {
        public const string Dashboard = "Dashboard";
        public const string Authentication = "Authentication";
        public const string Notifications = "Notifications";
        public const string Profiles = "Profiles";
        public const string Request = "Request";
    }
}