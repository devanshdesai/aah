﻿const TOGGLE = document.querySelector('#toggle')
const AUDIO = document.querySelector('audio')
const RECORDTOGGLEACTION = document.querySelector('recordToggleaction')

let recorder
const RECORD = () => {
    const toggleRecording = async () => {
        if (!recorder) {
            AUDIO.removeAttribute('src')
            const CHUNKS = []
            const MEDIA_STREAM = await window.navigator.mediaDevices.getUserMedia({
                audio: true
            })
            recorder = new MediaRecorder(MEDIA_STREAM)
            recorder.ondataavailable = event => {
                //TOGGLE.innerText = 'Start Recording'
                $("#recordToggleaction").html("")
                $("#recordToggleaction").append(`<i class="dripicons-microphone fs-20 text-danger align-middle d-flex"></i>`)
                $("#toggle").hide();
                $("#recordAudio").show();

                recorder = null
                CHUNKS.push(event.data)
                const AUDIO_BLOB = new Blob(CHUNKS, { type: "audio/mp3" })
                AUDIO.setAttribute('src', window.URL.createObjectURL(AUDIO_BLOB))
            }
            //TOGGLE.innerText = 'Stop Recording'
            $("#recordToggleaction").html("")
            $("#recordToggleaction").append(`<i class="dripicons-media-pause fs-20 text-danger align-middle d-flex"></i>`)
            recorder.start()
        } else {
            recorder.stop()
        }
    }
    toggleRecording()
}
TOGGLE.addEventListener('click', RECORD)