﻿using System.Web.Mvc;
using System.Web.Routing;


namespace GinnyBuddyResponderApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{Id}",
                defaults: new { controller = "Dashboard", action = "Index", Id = UrlParameter.Optional },
                namespaces: new[] { "GinnyBuddyResponderApp.Controllers" }
            );
        }
    }
}
