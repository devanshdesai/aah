﻿using AAH.Common;
using GinnyBuddyResponderApp.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Common.Paging;
using AAH.Entities.V1;

namespace GinnyBuddyResponderApp.Infrastructure
{

    public class BaseController : Controller
    {
        public BaseController()
        {

        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {

                HttpCookie reqCookie = Request.Cookies["AdminLogin"];
                if (reqCookie != null)
                {
                    ProjectSession.AdminId = Convert.ToInt64(Convert.ToString(reqCookie["Id"]));
                    ProjectSession.AdminName = Convert.ToString(Convert.ToString(reqCookie["AdminName"]));
                    ProjectSession.MobileNumber = Convert.ToString(Convert.ToString(reqCookie["MobileNumber"]));
                    ProjectSession.ProfileURL = Convert.ToString(Convert.ToString(reqCookie["ProfileURL"]));
                    ProjectSession.IsAvailable = Convert.ToInt32(Convert.ToString(reqCookie["IsAvailable"]));
                    ProjectSession.AreaName = Convert.ToString(Convert.ToString(reqCookie["AreaName"]));
                } 
                else
                {
                    filterContext.Result = new RedirectResult("~/Authentication/SignIn");
                    return;
                }

                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Authentication/SignIn");
            }
        }
    }
}