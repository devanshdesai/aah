﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyResponderApp.Infrastructure;
using System.Web;
using System.Web.Mvc;
using GinnyBuddyResponderApp.Pages;
using System.Data.SqlClient;
using System.Data;
using System.Text;

namespace GinnyBuddyResponderApp.Controllers
{
    public class DashboardController : BaseController
    {
        public readonly AbstractOtherServiceIconsServices abstractOtherServiceIconsServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public DashboardController(AbstractOtherServiceIconsServices abstractOtherServiceIconsServices,
            AbstractAddressMasterServices abstractAddressMasterServices,
             AbstractRequestsServices abstractRequestsServices,
             AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices)
        {
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractOtherServiceIconsServices = abstractOtherServiceIconsServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
        }

        public ActionResult Index(long id = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.RequestsData = abstractRequestsServices.GetRequestByDeliveryExecutiveId(pageParam, "", ProjectSession.AdminId, 3);
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }
            if (id == 0)
            {
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new { Area = "", id = Id });
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public JsonResult Dashboard_ById()
        {
            long Id = ProjectSession.AdminId;
            var result = abstractDeliveryExecutiveServices.Dashboard_ById(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}