﻿using AAH.Common;
using AAH.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using System.IO;
using GinnyBuddyResponderApp.Infrastructure;
using AAH.Common.Paging;
using AAH.Entities.V1;

namespace GinnyBuddyResponderApp.Controllers
{
    public class RequestController : BaseController
    {
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractRequestDocumentsServices abstractRequestDocumentsServices;
        public readonly AbstractMasterCategoryServices abstractMasterCategoryServices;
        public readonly AbstractSellerMasterServices abstractSellerMasterServices;
        public RequestController(AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractMasterCategoryServices abstractMasterCategoryServices,
            AbstractSellerMasterServices abstractSellerMasterServices,
            AbstractRequestDocumentsServices abstractRequestDocumentsServices)
        {
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractMasterCategoryServices = abstractMasterCategoryServices;
            this.abstractSellerMasterServices = abstractSellerMasterServices;
            this.abstractRequestDocumentsServices = abstractRequestDocumentsServices;
        }
        
        public ActionResult New()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.RequestsData = abstractRequestsServices.GetRequestByDeliveryExecutiveId(pageParam, "", ProjectSession.AdminId, 2);
            return View();
        }

        public ActionResult Completed()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.RequestsData = abstractRequestsServices.GetRequestByDeliveryExecutiveId(pageParam, "", ProjectSession.AdminId, 4);
            return View();
        }

        public ActionResult Rejected()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.RequestsData = abstractRequestsServices.GetRequestByDeliveryExecutiveId(pageParam, "", ProjectSession.AdminId, 6);
            return View();
        }

        public ActionResult RequestDetails(long ri = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.RequestsIdData = abstractRequestsServices.Requests_ById(ri,ProjectSession.AdminId);
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.RequestsIdDocument = abstractRequestDocumentsServices.RequestDocuments_ByRequestId(pageParam, ri);
            ViewBag.MasterCategory = abstractMasterCategoryServices.MasterCategory_All(pageParam, "");
            return View();
        }

        public ActionResult Feedback(long rId = 0)
        {
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.RequestId = rId;
            return View();
        }

        [HttpPost]
        public JsonResult CreateFeedbackData(string description = "", int rating = 0, long requestId = 0)
        {
            DeliveryExecutiveFeedback abstractUsersFeedbackModel = new DeliveryExecutiveFeedback();
            abstractUsersFeedbackModel.DeliveryExecutiveId = ProjectSession.AdminId;
            abstractUsersFeedbackModel.Description = description;
            abstractUsersFeedbackModel.RequestId = requestId;
            abstractUsersFeedbackModel.Rettings = rating;

            var result = abstractDeliveryExecutiveServices.DeliveryExecutiveFeedback_Insert(abstractUsersFeedbackModel);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateRequest(long reId = 0, long statusId = 0, long amountGiven = 0, long requestedAmount = 0, long otp = 0, string DECancleReason = "")
        {
            try
            {
                var reqData = abstractRequestsServices.Requests_ById(reId, ProjectSession.AdminId);
                if (reqData.Item.StatusId == statusId)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }

                if (statusId == 3)
                {
                    otp = Convert.ToInt64(new Random().Next(1111, 9999).ToString("D4"));
                }

                var result = abstractRequestsServices.Request_UpdateStatus(reId, statusId, 
                    ProjectSession.AdminId, Convert.ToDecimal(amountGiven), Convert.ToDecimal(requestedAmount), otp, DECancleReason);

                //PushNotification.Main();

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SellerMasterUpsert(SellerMaster sellerMaster, HttpPostedFileBase uploadFile)
        {
            if (uploadFile != null)
            {
                string basePath = "SellerMaster/" + sellerMaster.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(uploadFile.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                uploadFile.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                sellerMaster.PicUrl = basePath + fileName;
            }
            var result = abstractSellerMasterServices.SellerMaster_Upsert(sellerMaster);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateRequestByDeliveryExecutive(Requests requests)
        {
            requests.RequestsId = requests.Id;
            var result = abstractRequestsServices.Requests_UpdateByDeliveryExecutive(requests);
            if (result.Code == 200)
            {
                var result2 = abstractRequestsServices.Request_UpdateStatus(requests.RequestsId, requests.StatusId,
                       ProjectSession.AdminId, Convert.ToDecimal(requests.AmountGiven), Convert.ToDecimal(requests.AmountRequested));

                //PushNotification.Main();
                var result3 = abstractDeliveryExecutiveServices.DeliveryExecutive_UpdateLatLong(requests.DeliveryExecutiveId, requests.CurrentLat, requests.CurrentLong);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            //else { }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult deliveryExecutiveUpdateLatLong(string Lat = "", string Long = "", long Id = 0)
        {
            try
            {
                var result = abstractDeliveryExecutiveServices.DeliveryExecutive_UpdateLatLong(Id, Lat, Long);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult updateOnline(int isAvailabel = 0)
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            try
            {
                var result = abstractDeliveryExecutiveServices.DeliveryExecutive_IsAvailable(Convert.ToInt64(Id), isAvailabel);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult updateIsSettledWithAgency()
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            var DEData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(Convert.ToInt64(Id));
            string FromDate = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
            string ToDate = DateTime.Now.ToString("yyyy-MM-dd");

            try
            {
                var result = abstractRequestsServices.Requests_UpdateIsSettledWithAgency(DEData.Item.AgencyId, FromDate, ToDate, Convert.ToInt64(Id), 2);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}