﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using GinnyBuddyResponderApp.Infrastructure;
using System;
using System.Web.Mvc;
using GinnyBuddyResponderApp.Pages;
using AAH.Entities.V1;
using static GinnyBuddyResponderApp.Infrastructure.Enums;
using System.IO;
using System.Web;

namespace GinnyBuddyResponderApp.Controllers
{
    public class ProfilesController : BaseController
    {
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public ProfilesController(AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices)
        {
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
        }

        public ActionResult Index()
        {   
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.AddressTypeData = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, "", ProjectSession.AdminId);
            ViewBag.RequestsData = abstractRequestsServices.Requests_All(pageParam, "", ProjectSession.AdminId, 0, 0, 0, 0);
            ViewBag.UserData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            return View();
        }

        public ActionResult EditProfile()
        {
            AbstractDeliveryExecutive abstractDeliveryExecutive = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            return View(abstractDeliveryExecutive);
        }

        //Create Admin Data

        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.UpsertDeliveryExecutiveDetails)]
        public ActionResult UpsertDeliveryExecutiveDetails(DeliveryExecutive deliveryExecutive)
        {
            try
            {
                if(deliveryExecutive.Id == 0)
                {
                    deliveryExecutive.CreatedBy = ProjectSession.AdminId;
                } else
                {
                    deliveryExecutive.UpdatedBy = ProjectSession.AdminId;
                }

                if (deliveryExecutive.Files != null)
                {
                    string basePath = "DeliveryExecutiveProfile/" + deliveryExecutive.Id + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(deliveryExecutive.Files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    deliveryExecutive.Files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    deliveryExecutive.ProfileURL = basePath + fileName;
                }

                SuccessResult<AbstractDeliveryExecutive> result = new SuccessResult<AbstractDeliveryExecutive>();
                result = abstractDeliveryExecutiveServices.DeliveryExecutive_Upsert(deliveryExecutive);

                Session.Clear();
                HttpCookie cookie = new HttpCookie("AdminLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("AdminName", result.Item.FirstName.ToString() + " " + result.Item.LastName.ToString());
                cookie.Values.Add("MobileNumber", result.Item.MobileNumber.ToString());
                cookie.Values.Add("ProfileURL", result.Item.ProfileURLStr.ToString());
                cookie.Values.Add("IsAvailable", result.Item.IsAvailable.ToString());
                cookie.Expires = DateTime.Now.AddYears(100);
                Response.Cookies.Add(cookie);

                ProjectSession.AdminId = result.Item.Id;
                ProjectSession.AdminName = result.Item.FirstName + " " + result.Item.LastName;
                ProjectSession.MobileNumber = result.Item.MobileNumber;
                ProjectSession.ProfileURL = result.Item.ProfileURLStr;
                ProjectSession.IsAvailable = result.Item.IsAvailable;

                if (result != null && result.Code == 200 && result.Item != null)
                {
                    return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new { Area = "", id = result.Item.Id });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception error)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), error);
            }

            return RedirectToAction(Actions.EditProfile, Pages.Controllers.Profiles, new { Area = "" });
        }
    }
}