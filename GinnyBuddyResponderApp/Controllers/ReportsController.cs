﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyResponderApp.Infrastructure;
using System;
using System.Web;
using System.Web.Mvc;

namespace GinnyBuddyResponderApp.Controllers
{
    public class ReportsController : BaseController
    {
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public ReportsController(AbstractRequestsServices abstractRequestsServices, 
            AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices)
        {
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
        }

        public ActionResult Index()
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            var DEData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId);
            var FromDate = DateTime.Today.AddDays(-1);
            string ToDate = DateTime.Now.ToString("yyyy-MM-dd");
            ViewBag.ReportsData = abstractRequestsServices.Report_All(pageParam, "", FromDate.ToString("yyyy-MM-dd"), ToDate, DEData.Item.AgencyId, Convert.ToInt64(Id), 1);
            return View();
        }

        [HttpPost]
        public JsonResult updateIsSettledWithAgency()
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            var DEData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId);
            string FromDate = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
            string ToDate = DateTime.Now.ToString("yyyy-MM-dd");

            try
            {
                var result = abstractRequestsServices.Requests_UpdateIsSettledWithAgency(DEData.Item.AgencyId, FromDate, ToDate, Convert.ToInt64(Id), 2);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}