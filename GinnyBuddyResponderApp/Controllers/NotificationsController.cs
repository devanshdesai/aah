﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyResponderApp.Infrastructure;
using System.Web.Mvc;

namespace GinnyBuddyResponderApp.Controllers
{
    public class NotificationsController : BaseController
    {
        public readonly AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices;
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public NotificationsController(AbstractDeliveryExecutiveNotificationsServices abstractDeliveryExecutiveNotificationsServices,
            AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices)
        {
            this.abstractDeliveryExecutiveNotificationsServices = abstractDeliveryExecutiveNotificationsServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
        }

        public ActionResult Index(int ri = 0)
        {
            ViewBag.ri = ri;
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.DeliveryExecutiveIdData = abstractDeliveryExecutiveServices.DeliveryExecutive_ById(ProjectSession.AdminId).Item;
            ViewBag.NotificationsData = abstractDeliveryExecutiveNotificationsServices.DeliveryExecutiveNotifications_ByDeliveryExecutiveId(pageParam,ProjectSession.AdminId,0,"","",0);
            return View();
        }
    }
}