﻿using AAH.Common;
using GinnyBuddyApp.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using AAH.Common.Paging;
using AAH.Entities.V1;

namespace GinnyBuddyApp.Infrastructure
{

    public class BaseController : Controller
    {
        public BaseController()
        {

        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {

                HttpCookie reqCookie = Request.Cookies["AdminLogin"];
                if (reqCookie != null)
                {
                    ProjectSession.AdminId = Convert.ToInt64(Convert.ToString(reqCookie["Id"]));
                    string var_sql = "select * from Users where DeletedBy = 0 and Id = " + ProjectSession.AdminId;
                    SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                    DataTable dt = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                    sda.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["FirstName"].ToString() != "")
                            {
                                //return;
                            }
                            else
                            {
                                if(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Profiles")
                                {

                                }
                                else
                                {
                                    filterContext.Result = new RedirectResult("~/Profiles/EditProfile");
                                }
                                return;
                            }
                        }
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("~/Authentication/SignIn");
                        return;
                    }

                } 
                else
                {
                    filterContext.Result = new RedirectResult("~/Authentication/SignIn");
                    return;
                }

                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Authentication/SignIn");
            }
        }
    }
}