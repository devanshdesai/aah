﻿namespace GinnyBuddyApp.Pages
{
    public class Actions
    {
        public const string Index = "Index";
        public const string OtherServiceIconsData = "OtherServiceIconsData";
        public const string SendOtp = "SendOtp";
        public const string UsersVerifyOtp = "UsersVerifyOtp";
        public const string GetDataAddress = "GetDataAddress";
        public const string ChooseAddressAdd = "ChooseAddressAdd";
        public const string ChooseAddressData = "ChooseAddressData";
        public const string CreateAddressMy = "CreateAddressMy";
        public const string AddressTypeAll = "AddressTypeAll";
        public const string DeletedAddressData = "DeletedAddressData";
        public const string CreateRequestData = "CreateRequestData";
        public const string UserDetailsEdit = "UserDetailsEdit";
        public const string SignIn = "SignIn";
        public const string LogOut = "LogOut";
        public const string ReferenceCode = "ReferenceCode";
        public const string CreateFeedbackData = "CreateFeedbackData";
        public const string DeliveryExecutiveServices = "DeliveryExecutiveServices";
        public const string GetAllItems = "GetAllItems";
        public const string getDECountByAddress = "getDECountByAddress";
        public const string VendorProfile = "VendorProfile";
        public const string VendorProfileDetails = "VendorProfileDetails";
        public const string ManageItems = "ManageItems";
        public const string Delete = "Delete";
        public const string EditProfile = "EditProfile";
    }
}