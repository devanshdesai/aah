﻿using System.Collections.Generic;

namespace GinnyBuddyApp.Pages
{
    public class Controllers
    {
        public const string CreateRequest = "CreateRequest";
        public const string Dashboard = "Dashboard";
        public const string CreateAddress = "CreateAddress";
        public const string Authentication = "Authentication";
        public const string Notifications = "Notifications";
        public const string Profiles = "Profiles";
        public const string ChaiNasta = "ChaiNasta";
    }
}