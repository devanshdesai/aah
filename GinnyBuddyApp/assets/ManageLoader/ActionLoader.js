﻿$("#HomeMenu").click("on", function () {
    $("#HomeMenu").html("");
    $("#HomeMenu").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#RequestsMenu").click("on", function () {
    $("#RequestsMenu").html("");
    $("#RequestsMenu").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#EditAddress").click("on", function () {
    $("#EditAddress").html("");
    $("#EditAddress").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#ProfileMenu").click("on", function () {
    $("#ProfileMenu").html("");
    $("#ProfileMenu").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#NotificationMenu").click("on", function () {
    $("#NotificationMenu").html("");
    $("#NotificationMenu").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#ManageAddressBack").click("on", function () {
    $("#ManageAddressBack").html("");
    $("#ManageAddressBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#CreateAddressBack").click("on", function () {
    $("#CreateAddressBack").html("");
    $("#CreateAddressBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#infoCircle").click("on", function () {
    $("#infoCircle").html("");
    $("#infoCircle").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#EditProfile").click("on", function () {
    $("#EditProfile").html("");
    $("#EditProfile").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#RequestsBack").click("on", function () {
    $("#RequestsBack").html("");
    $("#RequestsBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" style="margin-right:5px;" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#ProfileBack").click("on", function () {
    $("#ProfileBack").html("");
    $("#ProfileBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" style="margin-right:5px;" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#NotificationsBack").click("on", function () {
    $("#NotificationsBack").html("");
    $("#NotificationsBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" style="margin-right:5px;" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#EditProfileBack").click("on", function () {
    $("#EditProfileBack").html("");
    $("#EditProfileBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" style="margin-right:5px;" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#AddressBack").click("on", function () {
    $("#AddressBack").html("");
    $("#AddressBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" style="margin-right:5px;" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#CreateRequestBack").click("on", function () {
    $("#CreateRequestBack").html("");
    $("#CreateRequestBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" style="margin-right:5px;" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

$("#RequestsDetailsBack").click("on", function () {
    $("#RequestsDetailsBack").html("");
    $("#RequestsDetailsBack").append(`<div class="spinner-border text-primary height-1rem width-1rem" style="margin-right:5px;" role="status">
                                 <span class="sr-only">Loading...</span>
                               </div>`);
});

//$("#profileBackarrow").click("on", function () {
//    $("#profileBackarrow").html("");
//    $("#profileBackarrow").append(`<div class="spinner-border text-primary height-1rem width-1rem" role="status">
//                                 <span class="sr-only">Loading...</span>
//                               </div>`);
//});