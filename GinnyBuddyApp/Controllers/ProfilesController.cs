﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Entities.Contract;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System;
using System.Web.Mvc;
using GinnyBuddyApp.Pages;
using AAH.Entities.V1;
using static GinnyBuddyApp.Infrastructure.Enums;

namespace GinnyBuddyApp.Controllers
{
    public class ProfilesController : BaseController
    {
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractUsersServices abstractUsersServices;
        public readonly AbstractUsersFeedbackServices abstractUsersFeedbackServices;
        int redirectId = 0;
        public ProfilesController(AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractUsersFeedbackServices abstractUsersFeedbackServices,
            AbstractUsersServices abstractUsersServices)
        {
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractUsersFeedbackServices = abstractUsersFeedbackServices;
            this.abstractUsersServices = abstractUsersServices;
        }

        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.AddressTypeData = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, "", ProjectSession.AdminId);
            ViewBag.RequestsData = abstractRequestsServices.Requests_All(pageParam, "", ProjectSession.AdminId, 0, 0, 0, 0);
            ViewBag.FeedbackData = abstractUsersFeedbackServices.UsersFeedback_All(pageParam, "", "", "", ProjectSession.AdminId);
            ViewBag.UserData = abstractUsersServices.Users_ById(ProjectSession.AdminId).Item;
            return View();
        }

        public ActionResult EditProfile()
        {
            AbstractUsers abstractUsers = null;
            abstractUsers = abstractUsersServices.Users_ById(ProjectSession.AdminId).Item;
            ViewBag.Name = abstractUsers.FirstName;
            return View(abstractUsers);
        }

        //Create Admin Data
        [HttpPost]
        [ValidateInput(true)]
        [ActionName(Actions.UserDetailsEdit)]
        public ActionResult UserDetailsEdit(Users users)
        {
            try
            {
                AbstractUsers abstractUsers = null;
                abstractUsers = abstractUsersServices.Users_ById(ProjectSession.AdminId).Item;
                users.UpdatedBy = ProjectSession.AdminId;
                SuccessResult<AbstractUsers> result = new SuccessResult<AbstractUsers>();
                result = abstractUsersServices.Users_Upsert(users);
                if (result.Code == 200)
                {
                    if (abstractUsers.FirstName == null || abstractUsers.FirstName == "")
                    {
                        redirectId = 1;
                    }
                    else
                    {
                        redirectId = 2;
                    }

                    if (redirectId == 1)
                    {
                        return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new { Area = "" });
                    }
                    else
                    {
                        return RedirectToAction(Actions.Index, Pages.Controllers.Profiles, new { Area = "" });
                    }
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception error)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), error);
            }

            return RedirectToAction(Actions.EditProfile, Pages.Controllers.Profiles);
        }
    }
}