﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace GinnyBuddyApp.Controllers
{
    public class DashboardController : BaseController
    {
        public readonly AbstractOtherServiceIconsServices abstractOtherServiceIconsServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public readonly AbstractMasterServicesServices abstractMasterServicesServices;
        public DashboardController(AbstractOtherServiceIconsServices abstractOtherServiceIconsServices,
            AbstractAddressMasterServices abstractAddressMasterServices,
             AbstractRequestsServices abstractRequestsServices,
             AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices,
             AbstractMasterServicesServices abstractMasterServicesServices)
        {
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractOtherServiceIconsServices = abstractOtherServiceIconsServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
            this.abstractMasterServicesServices = abstractMasterServicesServices;
        }

        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.RequestsData = abstractRequestsServices.Requests_All(pageParam, "", ProjectSession.AdminId, 0, 0, 0, 20);
            ViewBag.MasterServicesData = abstractMasterServicesServices.MasterServices_All(pageParam, "");
            getDECountByAddress();
            return View();
        }
        [HttpPost]
        public JsonResult getDECountByAddress()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            var result = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, "", ProjectSession.AdminId).Values;
            if (result.Count > 0)
            {
                var ss = result.Where(x => x.IsDefault == 1).ToList();
                ViewBag.DCount = abstractDeliveryExecutiveServices.GetDeliveryExecutiveCountByAddressId(ss[0].Id.ToString());
            }
            else
            {
                ViewBag.DCount = null;
            }

            return Json(ViewBag.DCount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OtherServiceIconsData()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            var result = abstractOtherServiceIconsServices.OtherServiceIcons_All(pageParam,"");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeliveryExecutiveServices()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            var result = abstractRequestsServices.Requests_All(pageParam, "", ProjectSession.AdminId, 0, 0, 0, 20);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}