﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AAH.Entities.V1;
using Newtonsoft.Json;

namespace GinnyBuddyApp.Controllers
{
    public class CreateAddressController : BaseController
    {
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractAddressTypeServices abstractAddressTypeServices;
        private readonly AbstractAreaMasterServices abstractAreaMasterServices;
        public CreateAddressController(AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractAddressTypeServices abstractAddressTypeServices, AbstractAreaMasterServices abstractAreaMasterServices)
        {
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractAddressTypeServices = abstractAddressTypeServices;
            this.abstractAreaMasterServices = abstractAreaMasterServices;
        }


        public ActionResult Index(int ri = 0)
        {
            ViewBag.ri = ri;
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.AdressTypeData = abstractAddressTypeServices.AddressType_All(pageParam, "");
            return View();
        }

        [HttpPost]
        public JsonResult ManageAddressId(int Id = 0)
        {
            //var result = abstractAddressMasterServices.AddressDefaultById(Convert.ToInt32(ProjectSession.AdminId), Id);
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            var result = abstractAddressMasterServices.AddressMaster_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ManageAddress()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.AddressTypeData = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, "", ProjectSession.AdminId);
            return View();
        }

        [HttpPost]
        public JsonResult CreateAddressMy(AddressMasterRequestModel addressMaster)
        {

            addressMaster.UserId = ProjectSession.AdminId;

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var areaData = abstractAreaMasterServices.AreaMaster_All(pageParam, "").Values;
            var isInArea = false;
            for (int i = 0; i < areaData.Count; i++)
            {
                if (areaData[i].IsActive)
                {
                    List<double> latlong = new List<double>();
                    double l = new double();
                    l = Convert.ToDouble(addressMaster.Latitude);
                    latlong.Add(l);
                    l = Convert.ToDouble(addressMaster.Longitute);
                    latlong.Add(l);

                    if (areaData[i].GeoFencing != null)
                    {
                        var polygonData = JsonConvert.DeserializeObject<List<ARoot>>(areaData[i].GeoFencing);
                        isInArea = inside(polygonData, latlong);

                        if (isInArea)
                        {
                            addressMaster.AreaMasterId = areaData[i].Id;
                        }
                    }
                }
            }

            var result = abstractAddressMasterServices.AddressMaster_Upsert(addressMaster);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeletedAddressData(long AddressId = 0)
        {
            var result = abstractAddressMasterServices.AddressMaster_Delete(AddressId, ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public bool inside(List<ARoot> vs, List<double> point)
        {

            var x = point[0];
            var y = point[1];

            var inside = false;
            for (int i = 0, j = vs.Count - 1; i < vs.Count; j = i++)
            {
                var xi = vs[i].lat;
                var yi = vs[i].lng;
                var xj = vs[j].lat;
                var yj = vs[j].lng;

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        }

        //public bool inside(pRoot vs, List<double> point)
        //{

        //    var x = point[0];
        //    var y = point[1];

        //    var inside = false;
        //    for (int i = 0, j = vs.MyArray.Count - 1; i < vs.MyArray.Count; j = i++)
        //    {
        //        var xi = vs.MyArray[i][0];
        //        var yi = vs.MyArray[i][1];
        //        var xj = vs.MyArray[j][0];
        //        var yj = vs.MyArray[j][1];

        //        var intersect = ((yi > y) != (yj > y))
        //            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        //        if (intersect) inside = !inside;
        //    }

        //    return inside;
        //}
        public class pRoot
        {
            public List<List<double>> MyArray { get; set; }
        }

        public class ARoot
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }
    }
}