﻿using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System.Web.Mvc;
using GinnyBuddyApp.Pages;
using AAH.Common;
using AAH.Entities.V1;

namespace GinnyBuddyApp.Controllers
{
    public class ChaiNastaController : BaseController
    {
        public readonly AbstractItemsServices abstractItemsServices;
        public readonly AbstractVendorsServices abstractVendorsServices;
        public ChaiNastaController(AbstractItemsServices abstractItemsServices,
            AbstractVendorsServices abstractVendorsServices)
        {
            this.abstractItemsServices = abstractItemsServices;
            this.abstractVendorsServices = abstractVendorsServices;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult VendorProfile()
        {
            return View();
        }


        [HttpGet]
        [ActionName(Actions.GetAllItems)]
        public ActionResult GetAllItems(string Search = "")
        {
            PageParam pageParam = new PageParam();  
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.SearchItems_All(pageParam,Search, ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName(Actions.ManageItems)]
        public ActionResult ManageItems(string Search = "")
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10000;
            var result = abstractItemsServices.Items_All(pageParam, Search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult VendorProfileDetails(
        //    long Id = 0,
        //    string VendorName = "",
        //    string CabinName = "",
        //    string PrimaryPhoneNumber = "", 
        //    string AlternatePhoneNumber = "",
        //    string CameraPhoto = "",
        //    string Location = "",
        //    string CabinImage = "",
        //    string Items = "",
        //    string Timings = "",
        //    string Lat = "",
        //    string Long = ""
        //)
        //{
        //    Vendors vendors = new Vendors();
        //    vendors.Id = Id;
        //    vendors.VendorName = VendorName;
        //    vendors.CabinName = CabinName;
        //    vendors.PrimaryPhoneNumber = PrimaryPhoneNumber;
        //    vendors.AlternatePhoneNumber = AlternatePhoneNumber;
        //    vendors.CameraPhoto = CameraPhoto;
        //    vendors.Location = Location;
        //    vendors.CabinImage = CabinImage;
        //    vendors.Items = Items;
        //    vendors.Timings = Timings;
        //    vendors.Lat = Lat;
        //    vendors.Long = Long;
        //    vendors.CreatedBy = ProjectSession.AdminId;
        //    vendors.UpdatedBy = ProjectSession.AdminId;

        //    var result = abstractVendorsServices.Vendors_Upsert(vendors);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}


    }
}