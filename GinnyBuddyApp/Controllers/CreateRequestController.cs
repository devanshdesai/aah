﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAH.Entities.V1;
using System.IO;
using System.Data;

namespace GinnyBuddyApp.Controllers
{
    public class CreateRequestController : BaseController
    {
        public readonly AbstractAddressMasterServices abstractAddressMasterServices;
        public readonly AbstractMasterPayTypeServices abstractMasterPayTypeServices;
        public readonly AbstractRequestsServices abstractRequestsServices;
        public readonly AbstractRequestDocumentsServices abstractRequestDocumentsServices;
        public readonly AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices;
        public readonly string ConnStr = "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=AAH;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        public readonly AbstractUsersFeedbackServices abstractUsersFeedbackServices;

        public CreateRequestController(AbstractAddressMasterServices abstractAddressMasterServices,
            AbstractMasterPayTypeServices abstractMasterPayTypeServices,
            AbstractRequestsServices abstractRequestsServices,
            AbstractRequestDocumentsServices abstractRequestDocumentsServices,
            AbstractUsersFeedbackServices abstractUsersFeedbackServices,
            AbstractDeliveryExecutiveServices abstractDeliveryExecutiveServices)
        {
            this.abstractAddressMasterServices = abstractAddressMasterServices;
            this.abstractMasterPayTypeServices = abstractMasterPayTypeServices;
            this.abstractRequestsServices = abstractRequestsServices;
            this.abstractRequestDocumentsServices = abstractRequestDocumentsServices;
            this.abstractDeliveryExecutiveServices = abstractDeliveryExecutiveServices;
            this.abstractUsersFeedbackServices = abstractUsersFeedbackServices;
        }

        public ActionResult Index(string service = "", long ritype = 0)
        {
            ViewBag.SelectedService = Convert.ToString(ConvertTo.Base64Decode(service));
            ViewBag.ServiceType = ritype;
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.MasterPayTypeData = abstractMasterPayTypeServices.MasterPayType_All(pageParam, "");


            var response = abstractAddressMasterServices.SelectedAddress_ByUserId(pageParam, "", ProjectSession.AdminId);

            if (response.TotalRecords > 0)
            {
                ViewBag.DCount = abstractDeliveryExecutiveServices.GetDeliveryExecutiveCountByAddressId(response.Values[0].AddressId.ToString());
            }
            else
            {
                ViewBag.DCount = null;
            }

            return View();
        }

        public ActionResult ManageRequest()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.RequestsDataOld = abstractRequestsServices.Requests_All(pageParam, "", ProjectSession.AdminId, 0, 0, 0, 0,0,0,2);
            ViewBag.RequestsDataToday = abstractRequestsServices.Requests_All(pageParam, "", ProjectSession.AdminId, 0, 0, 0, 0,0,0,1);

            return View();
        }

        public ActionResult RequestDetails(long ri = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.RequestsIdData = abstractRequestsServices.Requests_ById(ri);
            ViewBag.RequestsIdDocument = abstractRequestDocumentsServices.RequestDocuments_ByRequestId(pageParam, ri);
            return View();
        }

        [HttpPost]
        public JsonResult updateConfirmRequestes(long Id = 0, long deId = 0)
        {
            try
            {
                var result = abstractRequestsServices.Request_UpdateStatus(Id, 14, deId, 0, 0, 0);
                //PushNotification.Main();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult Update_TipAmount(long Id = 0, bool IsGiveTip = true, decimal TipAmount = 0)
        {
            try
            {
                var result = abstractRequestsServices.Update_TipAmount(Id, IsGiveTip, TipAmount);
                //PushNotification.Main();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult cancleNewrequestes(int reId = 0, int DId = 0)
        {
            try
            {
                var result = abstractRequestsServices.Request_UpdateStatus(reId, 5, DId, 0, 0, 0);
                //PushNotification.Main();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Feedback(long rId = 0)
        {
            ViewBag.RequestId = rId;
            return View();
        }

        [HttpPost]
        public JsonResult CreateFeedbackData(string description = "", int rating = 0, long requestId = 0)
        {
            UsersFeedback abstractUsersFeedbackModel = new UsersFeedback();
            abstractUsersFeedbackModel.UserId = ProjectSession.AdminId;
            abstractUsersFeedbackModel.Description = description;
            abstractUsersFeedbackModel.RequestId = requestId;
            abstractUsersFeedbackModel.Rettings = rating;

            var result = abstractUsersFeedbackServices.UsersFeedback_Insert(abstractUsersFeedbackModel);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataAddress()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            var result = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, "", ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChooseAddressAdd(long AddressId = 0)
        {
            var result = abstractAddressMasterServices.AddressDefaultById(Convert.ToInt32(ProjectSession.AdminId), ConvertTo.Integer(AddressId));

            //SelectedAddress selectedAddress = new SelectedAddress();
            //selectedAddress.AddressId = AddressId;
            //selectedAddress.UserId = ProjectSession.AdminId;

            //var result = abstractAddressMasterServices.SelectedAddress_Insert(selectedAddress);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChooseAddressData(long AddressId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            var result = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, "", ProjectSession.AdminId);
            var j = AddressId;
            if (result.Values.Count > 0)
            {
                var ss = result.Values.Where(x => x.IsDefault == 1).ToList();
                result.Values = ss;
            }

            //var result = abstractAddressMasterServices.SelectedAddress_ByUserId(pageParam, "", ProjectSession.AdminId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateRequestData(HttpPostedFileBase Files, string UserDescription = "", long ServiceMasterId = 0, long PayType = 0)
        {

            Requests requests = new Requests();
            requests.UserId = ProjectSession.AdminId;
            requests.AmountRequested = 0;
            requests.UserDescription = UserDescription;
            requests.ServiceMasterId = ServiceMasterId;
            requests.PayType = PayType;
            //----------------------------------------
            //PageParam pageParam = new PageParam();
            //pageParam.Offset = 0;
            //pageParam.Limit = 1000;
            //var response = abstractAddressMasterServices.SelectedAddress_ByUserId(pageParam, "", ProjectSession.AdminId);
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            var response = abstractAddressMasterServices.AddressMaster_ByUserId(pageParam, "", ProjectSession.AdminId);
            if (response.Values.Count > 0)
            {
                var ss = response.Values.Where(x => x.IsDefault == 1).ToList();
                response.Values = ss;
            }
            //----------------------------------------
            if (response != null)
            {
                requests.AddressId = response.Values[0].Id;
                var result = abstractRequestsServices.Requests_Insert(requests);


                //if(Files != null)
                //{
                //    string basePath = "Re/";
                //    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(Files.FileName);
                //    string path = Server.MapPath("~/" + basePath);
                //    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                //    {
                //        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                //    }
                //    Files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                //    requests.AudioUrl = basePath + fileName;
                //}



                if (result.Code == 200)
                {
                    //PushNotification.Main();

                    if (Files != null)
                    {
                        RequestDocuments requestDocuments = new RequestDocuments();
                        requestDocuments.CreatedBy = ProjectSession.AdminId;
                        requestDocuments.RequestId = result.Item.Id;
                        requestDocuments.IsUploadedByUser = true;
                        requestDocuments.IsUploadedByDeliveryExecutive = false;

                        string basePath = "RequestDocument/" + requestDocuments.RequestId + "/";
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(Files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        Files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                        requestDocuments.DocumentURL = basePath + fileName;

                        abstractRequestDocumentsServices.RequestDocuments_Insert(requestDocuments);
                    }
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

    }
}