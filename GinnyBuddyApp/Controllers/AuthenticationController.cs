﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Configuration;
using GinnyBuddyApp.Pages;

namespace GinnyBuddyApp.Controllers
{
    public class AuthenticationController : Controller
    {
        public readonly AbstractUsersServices abstractUsersServices;
        public AuthenticationController(AbstractUsersServices abstractUsersServices)
        {
            this.abstractUsersServices = abstractUsersServices;
        }

        public ActionResult SignIn(string DeviceToken  = "")
        {
            ViewBag.DeviceTokenStr = DeviceToken;
            return View();
        }

        public JsonResult SendOtp(string MobileNumber = "", string DeviceToken = "")
        {
            string otp = Convert.ToString(new Random().Next(111111, 999999).ToString("D6"));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).
                Replace("#mobile#", MobileNumber.ToString()).Replace("#otp#", otp).ToString());
            request.Method = "GET";
            request.ContentType = "application/json";

            if (MobileNumber == "1234512345")
            {
                otp = "123456";
            }

            var result = abstractUsersServices.Users_SendOTP(MobileNumber, DeviceToken, "NO","NO","NO", Convert.ToInt64(otp));
            if (result.Code == 200)
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                }

                Session.Clear();
                Session.Abandon();

                Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoStore();

                if (Request.Cookies["AdminLogin"] != null)
                {
                    var c = new HttpCookie("AdminLogin");
                    c.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(c);
                }
                if (Request.Cookies["SendOTP"] != null)
                {
                    var c = new HttpCookie("SendOTP");
                    c.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(c);
                }

                ProjectSession.AdminId = result.Item.Id;
                HttpCookie cookie = new HttpCookie("SendOTP");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Expires = DateTime.Now.AddYears(100);
                Response.Cookies.Add(cookie);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UsersVerifyOtp(long Otp = 0,string DeviceToken = "")
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["SendOTP"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            var result = abstractUsersServices.Users_VerifyOtp(Convert.ToInt64(Id), DeviceToken, Otp, "NO", "NO");

            if (result.Code == 200)
            {
                Session.Clear();
                ProjectSession.AdminId = result.Item.Id;
                HttpCookie cookie = new HttpCookie("AdminLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Expires = DateTime.Now.AddYears(100);
                Response.Cookies.Add(cookie);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete()
        {
            var Id = "0";
            HttpCookie reqCookies = Request.Cookies["AdminLogin"];
            if (reqCookies != null)
            {
                Id = reqCookies["Id"].ToString();
            }

            var result = abstractUsersServices.Users_Delete(Convert.ToInt64(Id), Convert.ToInt64(Id));

            if (result.Code == 200)
            {
                LogOut();
            }
            return RedirectToAction(Actions.SignIn, Pages.Controllers.Authentication);
        }

        public ActionResult LogOut()
        {
            PageParam pageParam = new PageParam();
            var dt = abstractUsersServices.UserDevices_ClearDeviceToken(pageParam, "", ProjectSession.AdminId);
   
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Request.Cookies["AdminLogin"] != null)
            {
                var c = new HttpCookie("AdminLogin");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }
            if (Request.Cookies["SendOTP"] != null)
            {
                var c = new HttpCookie("SendOTP");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }

            return RedirectToAction(Actions.SignIn, Pages.Controllers.Authentication);
        }

        public JsonResult ReferenceCode(long UserId = 0, string ReferenceCode = "")
        {
            var result = abstractUsersServices.Users_ReferenceCode(UserId, ReferenceCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}