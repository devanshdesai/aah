﻿using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System.Web.Mvc;

namespace GinnyBuddyApp.Controllers
{
    public class OurDeliveryLimitsController : BaseController
    {
        public readonly AbstractDeliveryLimitServices abstractDeliveryLimitServices;
        public OurDeliveryLimitsController(AbstractDeliveryLimitServices abstractDeliveryLimitServices)
        {
            this.abstractDeliveryLimitServices = abstractDeliveryLimitServices;
        }

        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.DeliveryLimitData = abstractDeliveryLimitServices.DeliveryLimit_All(pageParam,"");
            return View();
        }
    }
}