﻿using AAH.Common;
using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System.Web.Mvc;

namespace GinnyBuddyApp.Controllers
{
    public class NotificationsController : BaseController
    {
        public readonly AbstractUserNotificationsServices abstractUserNotificationsServices;
        public NotificationsController(AbstractUserNotificationsServices abstractUserNotificationsServices)
        {
            this.abstractUserNotificationsServices = abstractUserNotificationsServices;
        }


        public ActionResult Index(int ri = 0)
        {
            ViewBag.ri = ri;
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;
            ViewBag.NotificationsData = abstractUserNotificationsServices.UserNotifications_ByUserId(pageParam,ProjectSession.AdminId,0,"","",0);
            return View();
        }
    }
}