﻿using AAH.Common.Paging;
using AAH.Services.Contract;
using GinnyBuddyApp.Infrastructure;
using System.Web.Mvc;

namespace GinnyBuddyApp.Controllers
{
    public class HowItWorksController : BaseController
    {
        public readonly AbstractHowGinnyBuddyWorksServices abstractHowGinnyBuddyWorksServices;
        public HowItWorksController(AbstractHowGinnyBuddyWorksServices abstractHowGinnyBuddyWorksServices)
        {
            this.abstractHowGinnyBuddyWorksServices = abstractHowGinnyBuddyWorksServices;
        }

        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100;
            ViewBag.HowGinnyBuddyWorksData = abstractHowGinnyBuddyWorksServices.HowGinnyBuddyWorks_All(pageParam,"");
            return View();
        }
    }
}